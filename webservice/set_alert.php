<?php
#error_reporting(E_ALL);
#ini_set('display_errors','1');
require_once("appIncludes.php");
$response = array();
$userid = (int)$_REQUEST['userid'];
$secretekey = $_POST['secretekey'];
$type = $_REQUEST['type'];
if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if($userid > 0 && !empty($type))
{
	if($type=='enable')
	{
		$value = 1;
		$msg = "Enabled successfully";
		$success = AlertSetting::setAlert($userid,$value);
	}
	elseif($type=='disable')
	{
		$value = 0;
		$msg = "Disabled successfully";
		$success = AlertSetting::setAlert($userid,$value);
	}
	else
	{
		$msg = "Error";
	}
}
else
{
    $msg = PARAMETR_MISSING;
}
$response['msg'] = $msg;
echo json_encode($response);