<?php
#error_reporting(E_ALL);
#ini_set('display_errors','1');
require_once("appIncludes.php");
$response = array();
if(isset($_REQUEST['userid']))
{
	$userid = $_REQUEST['userid'];
}
if(isset($_REQUEST['name']))
{
	$name = $_REQUEST['name'];
}
if(isset($_REQUEST['email']))
{
	$email = $_REQUEST['email'];
}
if(isset($_REQUEST['city']))
{
	$city = $_REQUEST['city'];
}if($userid > 0)

if(isset($_REQUEST['desc']))
{
	$desc = $_REQUEST['desc'];
}
if(isset($_REQUEST['secretekey']))
{
	$secretekey = $_REQUEST['secretekey']; 
}
if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if($userid > 0)
{
	if(UserMasterTable::isEmailExistNotCurrent($email,$userid))
	{
		$msg = "Email already taken";
	}
	else
	{
		$success = UserMasterTable::updateUser($name,$email,$city,$desc,$userid);
		$msg = "Profile updated successfully";
		if($_FILES["profile_img"]["name"]!='')
		{
			$user_info = UserMasterTable::getUserData($userid);
			$upload_dir_path=SERVER_REAL_PATH."/images/profile_img/";
			if(!file_exists($upload_dir_path))
			{
				mkdir($upload_dir_path);
				chmod($upload_dir_path,0777);
			}
			$pngBase64 = base64_encode(file_get_contents($_FILES['profile_img']['tmp_name']));
			$ext = strtolower(end(explode('.',$_FILES["profile_img"]["name"])));
			$filename = sha1(substr($pngBase64, 0, 10).rand(11111, 99999)).'_'.$userid.'.'.$ext;
			if($_FILES['profile_img']['size'] > 0)
			{
				if($_FILES['profile_img']['error'] == 0)
				{
					
					if(!file_exists($upload_dir_path.$filename))
					{
						$success=move_uploaded_file($_FILES['profile_img']['tmp_name'],$upload_dir_path.$filename);
						if($success)
						{
							$imageUrl="http://".SERVER_PATH."/images/profile_img/".$filename;
							UserMasterTable::updateProfilePic($userid,$filename);

						}
					}	
				}
			}
			else
			{
				echo json_encode(array("msg"=>$result));die;
			}
		}	
	}
}
else{
	$msg = PARAMETR_MISSING;
}
$response['msg'] = $msg;
echo json_encode($response);