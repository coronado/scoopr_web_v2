<?php
#error_reporting(E_ALL);
#ini_set('display_errors','1');
require_once("appIncludes.php");
$userid = (int)$_REQUEST['userid'];
$secretekey = $_POST['secretekey'];
if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if(($_REQUEST['userid'] < 1) || empty($_REQUEST['newpass']) || empty($_REQUEST['confirmpass']) || empty($_REQUEST['currpass']))
{
	$msg = PARAMETR_MISSING;	
}
elseif($_REQUEST['newpass']!=$_REQUEST['confirmpass'])
{
	$msg="New password and confirm password not match";
}
else
{
	$userid=UserMasterTable::checkPassword($_REQUEST['userid'],$_REQUEST['currpass']);
	if($userid > 0 )
	{
		$success = UserMasterTable::updatePassword($_REQUEST['userid'],$_REQUEST['newpass']);
		if($success)
		{
			$msg="Password updated successfully!";
		}
		else
		{
			$msg = "Error occurred";
		}
	}
	else
	{
		$msg = "Wrong password";
	}	
}
echo json_encode(array("msg"=>$msg));
