<?php
/*
 Author :       A1 Technology
 Description :  Include common php script at one place
*/
define("SCOOPR_MAIL_FOOTER","<br/><br/>Regards,<br/>Scoopr Team");
define("ACCESS_TOKEN","dsfkn3_9k2n02n0f_un20n42_0nf02");
define("SOME_ERROR_OCCURED","Some error occurred");
define("PARAMETR_MISSING","Parameter missing");
define("ACCESS_DENIED","Access Denied");
define("INVALID_USER","Invalid user");
define("SUCCESS","Data inserted successfully");
define("PARA_CHANGE","<br/><br/>");
define("LINE_BREAK","<br/>");
define("DATE_FORMAT","d-M-Y");
require_once("config_pdo.php");
require_once("table_macros.php");
require_once("classes/ContactUs.php");
require_once("classes/CopyRight.php");
require_once("classes/Notification.php");
require_once("classes/CurrentLogin.php");
require_once("classes/AlertSetting.php");
require_once("classes/ForgotPassword.php");
require_once("classes/AssignmentTable.php");
require_once("classes/UserMasterTable.php");
require_once("classes/GeneralFunction.php");
require_once("classes/AssignModelTable.php");
require_once("classes/PushNotification.php");
require_once("classes/ModelReleaseTable.php");
require_once("classes/AssignmentOverview.php");
require_once("classes/FavouriteAssignTable.php");

require_once("classes/EmailClass.php");
require_once("classes/TwitterTrack.php");