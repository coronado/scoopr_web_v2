<?php
/*
 * Description : Macros of Scoopr database tables
*/
define('TABLE_ASSIGNMENT','Assignment');
define('TABLE_CURRENT_LOGIN','CurrentLogin');
define('TABLE_FORGOT_PWD','ForgotPwd');
define('TABLE_USER_MASTER','User');
define('TABLE_REFERENCE_IMAGE','ReferenceImage');
define('TABLE_IMAGERY_FOR','ImageryFor');
define('TABLE_ASSIGNMENT_IMG_SUBMISSION','AssignmentImgSubmission');
define('TABLE_ASSIGNMENT_OVERVIEW','AssignmentOverview');
define('TABLE_FAVOURITE_ASSIGNMENT','FavouriteAssignment');
define('TABLE_FAVOURITE_SUBMISSION','FavouriteSubmission');
define('TABLE_MODEL_RELEASE','Model');
define('TABLE_ASSIGNMENT_CATEGORY','AssignmentCategory');
define('TABLE_CONTACT_US','ContactUs');
define('TABLE_NOTIFICATION','Notification');
define('TABLE_DEMO_REQUEST','DemoRequest');
define('TABLE_CONTENT_MANAGEMENT','ContentManagement');
define('TABLE_BRAND_MESSAGE','BrandMessage');

define('TABLE_TWITTER_TRACK','TwitterTrack');