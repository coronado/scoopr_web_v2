<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
require_once("appIncludes.php");
$response = array();
$userid = (int)$_REQUEST['userid'];
$secretekey = $_POST['secretekey'];
if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if($userid > 0)
{
	$response["read_notification"] = (string)Notification::getNotificationCount($userid,1);
	$response["unread_notification"] = (string)Notification::getNotificationCount($userid,0);
}
else
{
    	$msg = PARAMETR_MISSING;
	$response['msg'] = $msg;
}
echo json_encode($response);
