<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
require_once("appIncludes.php");
$response = array();
$userid = (int)$_REQUEST['userid'];
$secretekey = $_POST['secretekey'];
if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if($userid > 0)
{
    	$notification_list = Notification::getNotification($userid);
    	#print_r($notification_list); die;
	$response["notification_list"] = $notification_list;	
	if(count($notification_list)>0)
	{
		$msg = "Notify list found";
	}
	else
	{
		$msg = "Notify not found";
	}
}
else
{
    $msg = PARAMETR_MISSING;
}
$response['msg'] = $msg;
if($_REQUEST['debug']==1){echo "<pre>";print_r($response);echo "</pre>";}
echo json_encode($response);
