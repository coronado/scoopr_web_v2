<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
require_once("appIncludes.php");
$response = array();
$userid = (int)$_REQUEST['userid'];
$secretekey = $_POST['secretekey'];
$assign_id = (int)$_REQUEST['assign_id'];
$buyer_id = (int)$_REQUEST['buyer_id'];
$submission_count = (int)$_REQUEST['submission_count'];
$type = (int)$_REQUEST['type']; # 1-view, 2-submission
$typeArray = array(1,2);
if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if($userid>0 && $assign_id>0 && $buyer_id>0 && in_array($type,$typeArray))
{
	$date = date("Y-m-d",time());
    	$message = AssignmentOverview::add_view_submission($assign_id,$buyer_id,$date,$submission_count,$type);
	$msg = $message;
}
else
{
    $msg = PARAMETR_MISSING;
}
$response['msg'] = $msg;
echo json_encode($response);