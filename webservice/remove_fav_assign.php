<?php
#error_reporting(E_ALL);
#ini_set('display_errors','1');
require_once("appIncludes.php");
$response = array();
$userid = (int)$_REQUEST['userid'];
$secretekey = $_POST['secretekey'];
$assign_id = (int)$_REQUEST['assign_id'];
if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if($userid > 0 && $assign_id > 0)
{
    	$success = FavouriteAssignTable::removeFavAssign($assign_id,$userid);
	if($success!='exist')
	{
		$msg = "Favourite assignment removed";
	}
	else
	{	
		$msg = "Already removed";
	}
}
else
{
    $msg = PARAMETR_MISSING;
}
$response['msg'] = $msg;
echo json_encode($response);