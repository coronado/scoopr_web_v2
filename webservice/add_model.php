<?php
#error_reporting(E_ALL);
ini_set('display_errors','1');
//ini_set('memory_limit','16M');
//ini_set('post_max_size','16M');
//ini_set('upload_max_filesize','16M');
require_once("appIncludes.php");
$response = array();
$userid = (int)$_REQUEST['userid'];
$secretekey = $_POST['secretekey'];
$shot_for = $_REQUEST['shot_for'];
$notes = $_REQUEST['notes'];  



if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if(!empty($shot_for) && !empty($notes) && $userid > 0)
{
    	$model_id = ModelReleaseTable::addModel($shot_for,$notes,$userid);
	if($model_id > 0)
	{
		$msg = "Model added successfully";
		if($_FILES["model_img"]["name"]!='')
		{
			$upload_dir_path=SERVER_REAL_PATH."/images/model_logo/";
			if(!file_exists($upload_dir_path))
			{
				mkdir($upload_dir_path);
				chmod($upload_dir_path,0777);
			}
			$pngBase64 = base64_encode(file_get_contents($_FILES['model_img']['tmp_name']));
			$ext = strtolower(end(explode('.',$_FILES["model_img"]["name"])));

			if(!in_array(strtolower($ext),array("jpg","jpeg","gif","png")))
			{			
				echo json_encode(array("msg"=>"Invalid logo file"));die;
			}

			$filename = sha1(substr($pngBase64, 0, 10).rand(11111, 99999).time()).'_'.$model_id.'.'.$ext;
			if($_FILES['model_img']['size'] > 0)
			{
				if($_FILES['model_img']['error'] == 0)
				{
					if(!file_exists($upload_dir_path.$filename))
					{
						$success=move_uploaded_file($_FILES['model_img']['tmp_name'],$upload_dir_path.$filename);
						if($success)
						{
							$imageUrl="http://".SERVER_PATH."/images/model_logo/".$filename;
							ModelReleaseTable::updateModelPic($model_id,$filename);
							$response['img'] = $imageUrl;
							
						}
					}
				}
			}
			else
			{
				echo json_encode(array("msg"=>"Invalid image data"));die;
			}
		}
	
		if($_FILES["model_pdf"]["name"]!='')
		{
			$upload_dir_path=SERVER_REAL_PATH."/images/model_logo/";
			if(!file_exists($upload_dir_path))
			{
				mkdir($upload_dir_path);
				chmod($upload_dir_path,0777);
			}
			$pngBase64 = base64_encode(file_get_contents($_FILES['model_pdf']['tmp_name']));
			$ext = strtolower(end(explode('.',$_FILES["model_pdf"]["name"])));
			if($ext!='pdf')
			{
				echo json_encode(array("msg"=>"Invalid PDF file"));die;
			}
			$filename_pdf = sha1(substr($pngBase64, 0, 10).rand(11111, 99999).time()).'_'.$model_id.'.'.$ext;
			#if($_FILES['model_pdf']['size'] > 0)
			if($_FILES['model_pdf']['name'] !='')
			{
				if($_FILES['model_pdf']['error'] == 0)
				{
					if(!file_exists($upload_dir_path.$filename_pdf))
					{
						$success=move_uploaded_file($_FILES['model_pdf']['tmp_name'],$upload_dir_path.$filename_pdf);
						if($success)
						{
							$imageUrl="http://".SERVER_PATH."/images/model_logo/".$filename_pdf;
							ModelReleaseTable::updateModelPdf($model_id,$filename_pdf);
							$response['pdf'] = $imageUrl;
						}
					}
				}
			}
			else
			{
				echo json_encode(array("msg"=>"Invalid pdf data"));die;
			}
		}
		$response['model_id'] = $model_id;
	}
}
else
{
    $msg = PARAMETR_MISSING;
}
$response['date'] = date('Y-m-d');
$response['msg'] = $msg;
echo json_encode($response);