<?php
#error_reporting(E_ALL);
#ini_set('display_errors','1');
require_once("appIncludes.php");
$response = array();
$secretekey = $_POST['secretekey'];
$userType = 2;

// if($secretekey!=ACCESS_TOKEN)
// {
//     	echo json_encode(array("msg"=>ACCESS_DENIED));die;
// }

if(isset($_REQUEST['fb_id']))
{
	$fb_id = (string)$_POST['fb_id'];
}
else
{
	$fb_id = "0";
	
}
if(!empty($_POST['email']) && (!empty($_POST['pwd']) || !empty($fb_id)))
{
	if(!empty($_POST['email']) && !empty($_POST['pwd']))
	{	
		$userid = (int)UserMasterTable::checkLogin($_POST['email'],$_POST['pwd'],$userType);
	}
	else if(!empty($_POST['email']) && !empty($fb_id))
	{
		
		//$updateFbId = UserMasterTable::updateFbId($_POST['email'],$fb_id);
		$userid = (int)UserMasterTable::checkFbLogin($_POST['email'],$fb_id,$userType);
		if($userid>0)
		{
			$updateFbId = UserMasterTable::updateFbId($_POST['email'],$_REQUEST['name'], $fb_id);
		}
		else
		{
			$userid = UserMasterTable::registerFbUser($_POST['email'],$_REQUEST['name'], $fb_id);
		}
	}
	
    	if($userid > 0)
	{
		$msg = "Login success";
		$scoopr_uid=md5("scoopr-".$userid.'-'.ACCESS_TOKEN.'-'.rand(1,99999).'-'.time());
		$response['secretekey'] = (string)$scoopr_uid;
		$login_id = CurrentLogin::addLoginUser($userid,$scoopr_uid);
    	}
    	else
	{
        	$msg = INVALID_USER;
    	}
}
else
{
    $msg = PARAMETR_MISSING;
}
if($userid > 0)
{
	$response['userid']=(string)$userid;
	$userData=UserMasterTable::getUserData($userid);
	$response['name']=(string)$userData['name'];
	$response['email']=(string)$userData['emailAddress'];
	$response['city'] = !empty($userData['city'])?$userData['city']:"";
	$response['desc']=(string)$userData['description'];
	$response['profile_img']=(string)GeneralFunction::getProfileImage($userData['profileImg']);
	$alert_setting = AlertSetting::getAlert($userid);
	if($alert_setting['value']==1)
	{
		$notify_msg = "on";
	}
	elseif($alert_setting['value']==0)
	{	
		$notify_msg = "off";
	}
	else
	{
		$notify_msg = "unknown";
	}
	$response['notify_msg'] = $notify_msg;
	$response["unread_notification"] = (string)Notification::getNotificationCount($userid,0);
	$response["model_count"] = (string)ModelReleaseTable::getModelCount($userid);
	$response["fav_assign_count"] = (string)FavouriteAssignTable::getFavAssignmentCount($userid);
}
$response['msg'] = $msg;
echo json_encode($response);
