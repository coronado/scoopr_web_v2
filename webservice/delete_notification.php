<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
require_once("appIncludes.php");
$response = array();
$userid = (int)$_REQUEST['userid'];
$notification_id = (int)$_REQUEST['notification_id'];
$secretekey = $_POST['secretekey'];
if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if($userid > 0)
{
    	$response = Notification::deleteNotification($userid,$notification_id);
    	
	if($response) {	
		$msg = "Notification removed sucessfully.";
	} else {
		$msg = "Could not able to remove.";
	}
}
else
{
    $msg = PARAMETR_MISSING;
}
$response['msg'] = $msg;
if($_REQUEST['debug']==1){echo "<pre>";print_r($response);echo "</pre>";}
echo json_encode($response);
