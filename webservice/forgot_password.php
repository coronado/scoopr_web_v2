<?php
#error_reporting(E_ALL);
#ini_set('display_errors','1');
require_once("appIncludes.php");
$response = array();
$email = $_POST['email'];
$secretekey = $_POST['secretekey'];
if($secretekey!=ACCESS_TOKEN)
{
    echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if(!empty($email))
{
    $userid = (int)UserMasterTable::isEmailExist($email);
    if($userid > 0)
    {	
    $path    = "http://scoopr.a1technology.asia/";
	$token=md5("scoopr-".$userid.'-'.ACCESS_TOKEN.'-'.rand(1,99999).'-'.time());
	$_id = ForgotPassword::addForgotUser($userid,$token);
	$email_subject = "Scoopr Forgot Password";
	//$email_body ="<img src='http://scoopr.a1technology.asia//images/lp_images/logo.png'></img><br><br>";
	$headers  = 'MIME-Version: 1.0' . "\r\n";
   $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
   
   $email_body .= '<table style="600px;border:1px solid #5ab7ce;" cellpadding="0" cellspacing="0"> 
   <tr>   <td><img src="'.$path.'images/email/header.png" width="600" height="115" alt="scooper"></td></tr> <tr>';
	$email_body .= "Dear User,";
	$email_body .= PARA_CHANGE;
	$email_body .="This email is being sent because you want to know your Scoopr account password.";
	$email_body .= PARA_CHANGE;
	$email_body .="Please click on below link to reset Your password (valid for 24 hours only)";
	$email_body .= PARA_CHANGE;
	
	$email_body .="<a href='".SERVER_HTTP_PATH."/user/change_pass/token/".$token."/user/".md5($userid)."' target='_blank'>Click here</a>";
		
	$email_body .= '<tr> <td style="background:#f56f6c;text-align:center;color:#fff;font-size:12px;padding:10px;font-family:Arial, Helvetica, sans-serif;">&copy; Copyright 2013 Scoopr Media Inc. </td></tr></table>';
	if(GeneralFunction::sendEmail($email,$email_subject,$email_body,$headers))
	{
		$msg = "Reset link sent on email";
	}
	else
	{
		$msg = SOME_ERROR_OCCURED;
	}
    }
    else
    {
        $msg = "Email address not exist";
    }
}
else
{
    $msg = PARAMETR_MISSING;
}
$response['msg'] = $msg;
echo json_encode($response);
