<?php
require_once("appIncludes.php");
$hostName = $_SERVER['HTTP_HOST'];

$aid        = $_REQUEST['assignment_id'];
$uid        = $_REQUEST['user_id'];
$tweet_id   = $_REQUEST['tweet_id'];
$posted_url = $_REQUEST['posted_url'];
$tweet_date = date('Y-m-d h:i:s');

$paras = array(
		'aid' => $aid,
		'uid' => $uid,
		'tweet_id' => $tweet_id,
		'posted_url' => $posted_url,
		'tweet_date' => $tweet_date
	      );

$m = array ();

if ($aid == '' || $uid == '' || $tweet_id =='' || $posted_url =='') {
	$m['message'] = "Parameters should not be blank";
	echo json_encode($m);
	exit;
}
else 
{
	$updatToken = TwitterTrack :: insertTwitterTrack($paras);
	$m['message'] = "New track record has been saved";
}
echo json_encode($m);

?>