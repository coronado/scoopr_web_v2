<?php
#error_reporting(E_ALL);
#ini_set('display_errors','0');
require_once("appIncludes.php");
$response = array();
$userid = (int)$_REQUEST['userid'];
$secretekey = $_POST['secretekey'];
if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if($userid > 0)
{
    	$model_result = ModelReleaseTable::listModel($userid);
	$response["model_list"] = $model_result;	
	if(count($model_result)>0)
	{
		$msg = "Model list found";
	}
	else
	{
		$msg = "Model not found";
	}
}
else
{
    $msg = PARAMETR_MISSING;
}
$response['msg'] = $msg;
if($_REQUEST['debug']==1){
	echo "<pre>";print_r($response);echo "</pre>";
}
echo json_encode($response);