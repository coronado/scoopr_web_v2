<?php
class TwitterTrack
{
	private static $_tableName = TABLE_TWITTER_TRACK;
	
	public static function insertTwitterTrack($paras)
	{
		global $conn;
		
		
      		$sql="INSERT INTO ".self::$_tableName." (AssignmentId, UserId, tweetId, postedUrl, tweetDate ) VALUES (:AssignmentId, :UserId, :tweetId, :postedUrl, :tweetDate)";
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':AssignmentId', $paras['aid']);
		$stmt->bindParam(':UserId', $paras['uid']);
		$stmt->bindParam(':tweetId', $paras['tweet_id']);
		$stmt->bindParam(':postedUrl', $paras['posted_url']);
		$stmt->bindParam(':tweetDate', $paras['tweet_date']);
		$stmt->execute();
		$insertedId = $conn->lastInsertId();
		return $insertedId;
	}

	
	
	

	
}
