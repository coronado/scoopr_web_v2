<?php
class AssignModelTable
{
	private static $_tableName = TABLE_ASSIGNMENT_IMG_SUBMISSION;
	private static $_tableUser = TABLE_USER_MASTER;
	
	public static function addModelToAssign($assign_id,$buyer_id,$model_id,$image_name,$userid)
	{
		global $conn;
		  $sql="INSERT INTO ".self::$_tableName."
		(`AssignmentImgSubmissionId`,`AssignmentId`,`buyerId`,`imageName`,`mediumSizeImage`,`smallSizeImage`,`ModelId`,`creatorId`,`isApproved`,`isRead`,`timestamp`,`updatedTS`)
		VALUES(NULL,'".$assign_id."','".$buyer_id."','".$image_name."','".$image_name."','".$image_name."','".$model_id."','".$userid."','0','0',UNIX_TIMESTAMP(NOW()),UNIX_TIMESTAMP(NOW()));";  
		$query = $conn->prepare($sql);
		$query->execute();
		$_id = $conn->lastInsertId();
		return (int)$_id;
	}
	
	# approve the submission by Buyer
	public static function approveSubmission($assign_id,$userid)
	{
		global $conn;
		if($userid > 0)
		{
			$sql = "UPDATE ".self::$_tableName." SET isApproved='1',updatedTS=UNIX_TIMESTAMP(NOW()) WHERE creatorId={$userid} AND AssignmentId={$assign_id}";
			$count = $conn->exec($sql);
		}
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public static function readNotification($_id)
	{
		global $conn;
		if($_id > 0)
		{
			$sql="UPDATE ".self::$_tableName." SET isRead='1' WHERE AssignmentImgSubmissionId={$_id}";
			$count = $conn->exec($sql);
		}
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public static function checkIfAlreadyAssign($assign_id,$userid)
	{
		global $conn;
		$sql = "SELECT count(AssignmentImgSubmissionId) AS is_assigned FROM ".self::$_tableName." WHERE UserId={$userid} AND AssignmentId={$assign_id}";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		return (int)$row['is_assigned'];
	}
	
	public static function getCreatorId($id){
		
		global $conn;
	    $sql = "SELECT UserId FROM " . self::$_tableName . " WHERE AssignmentImgSubmissionId = {$id}";
	    
	    $result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		return (int)$row['UserId'];	
	}

	public static function getCreatorEmail($creatorid)
	{
	    global $conn;
	    $sql = "SELECT emailAddress FROM " . self::$_tableUser . " WHERE UserId = {$creatorid}";
	    
	    $result = $conn->query($sql);
	    $row = $result->fetch(PDO::FETCH_ASSOC);
	    $email =  $row['emailAddress'];	
	    return $email; 		
	}

	public static function uploadMediumImg($resultWidth,$resultHeight,$upload_dir_path,$filename)
	{
		list($origWidth, $origHeight) = @getimagesize($upload_dir_path.$filename);
		$origRatio = $origWidth/$origHeight;
		if($origWidth>$resultWidth && $origHeight>$resultHeight)
		{					
					
			if ( $resultWidth/$resultHeight > $origRatio ) 
			{
				$resultWidth = $resultHeight * $origRatio;
			} 
			else 
			{
				$resultHeight = $resultWidth / $origRatio;
			}

			$imageUrl="http://".SERVER_PATH."/images/assign_img_submission/medium_size_image/".$filename;
			//Start Image resize
			if(class_exists(Imagick))
			{
					$thumb = new Imagick($upload_dir_path.$filename);

					// image rotation based on orientation
					$exif = exif_read_data($upload_dir_path.$filename);
					$orientation = $exif['Orientation'];
					
					switch($orientation) 
					{
						case 3:
                    				$thumb->rotateimage("#FFF",180);
                    				break;

						case 6: 
						$thumb->rotateimage("#FFF", 90);
						break;
					
						case 8: 
						$thumb->rotateimage("#FFF", -90);
						break;
					}

					$thumb->resizeImage($resultWidth,$resultHeight,Imagick::FILTER_LANCZOS,1);
					$thumb->writeImage($upload_dir_path.'medium_size_image/'.$filename);
					$thumb->destroy(); 
			}
			else
			{
					echo json_encode(array("msg"=>"server error."));die;
						
			} 
		} 
		else 
		{
			      $thumb = new Imagick($upload_dir_path.$filename);							
			      $thumb->writeImage($upload_dir_path.'medium_size_image/'.$filename);
			      $thumb->destroy(); 					  
		}
		
	}

	public static function uploadSmallImg($upload_dir_path,$filename)
	{
		        $resultWidth = "100";
			$resultHeight = "100";
		
			$imageUrl="http://".SERVER_PATH."/images/assign_img_submission/small_size_image/".$filename;
			//Start Image resize
			if(class_exists(Imagick))
			{
					$thumb = new Imagick($upload_dir_path.$filename);

					// image rotation based on orientation
					$exif = exif_read_data($upload_dir_path.$filename);
					$orientation = $exif['Orientation'];
					
					switch($orientation) 
					{
						case 3:
                    				$thumb->rotateimage("#FFF",180);
                    				break;

						case 6: 
						$thumb->rotateimage("#FFF", 90);
						break;
					
						case 8: 
						$thumb->rotateimage("#FFF", -90);
						break;
					}

					$thumb->resizeImage($resultWidth,$resultHeight,Imagick::FILTER_LANCZOS,1);
					$thumb->writeImage($upload_dir_path.'small_size_image/'.$filename);
					$thumb->destroy(); 
			}
			else
			{
					echo json_encode(array("msg"=>"server error."));die;
						
			} 
	}	
}
