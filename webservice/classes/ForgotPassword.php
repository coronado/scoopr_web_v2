<?php
class ForgotPassword
{
	private static $_tableName = TABLE_FORGOT_PWD;

	public static function checkForgotUser($userid)
	{
		global $conn;
		$sql = "SELECT ForgotPwdId AS is_forgot_requested FROM ".self::$_tableName." WHERE UserId={$userid}";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row['is_forgot_requested']>0)
		{
			return $row['is_forgot_requested'];
		}
		else
		{
			return 0;
		}
	}

	public static function addForgotUser($userid,$token)
	{
		global $conn;
		if(!self::checkForgotUser($userid))
		{
			$sql=
			"INSERT INTO ".self::$_tableName."
			(`ForgotPwdId`, `UserId`, `token`, `expirationDate`)
			VALUES (NULL, '".$userid."', '".$token."', DATE_ADD(NOW(), INTERVAL 1 DAY));";
			$query = $conn->prepare($sql);
			$query->execute();
			$_id = $conn->lastInsertId();
			return $_id;
		}
		else
		{
			return self::updateForgotUser($userid,$token);
		}
	}

	public static function updateForgotUser($userid,$token)
	{
		global $conn;
		$sql = "UPDATE ".self::$_tableName." SET token='".$token."',expirationDate=DATE_ADD(NOW(), INTERVAL 1 DAY) WHERE UserId={$userid}";
		$count = $conn->exec($sql);
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	public static function isValidForgotUser($u_Token,$Token)
	{
		global $conn;
		$sql = "SELECT ForgotPwdId FROM ".self::$_tableName." WHERE md5(UserId)='".$u_Token."' AND token='".$Token."' AND expirationDate>=NOW()";
		#echo $sql;die;
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row['ForgotPwdId']>0)
		{
			return $row['ForgotPwdId'];
		}
		else
		{
			return 0;
		}
	}
}