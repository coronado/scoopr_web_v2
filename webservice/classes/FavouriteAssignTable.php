<?php
class FavouriteAssignTable
{
	private static $_tableName = TABLE_FAVOURITE_ASSIGNMENT;
	
	public static function listFavAssignment($userid)
	{
		global $conn;
		$fav_assignment = array();
		if($userid > 0)
		{
			$fav_assign_id = self::getFavAssignmentId($userid);
			if(count($fav_assign_id)>0)
			{
				$sql="SELECT 
				AM.*,
				UM.profileName,UM.tagLine, AC.categoryName
				FROM ".TABLE_ASSIGNMENT." AS AM
				LEFT JOIN ".TABLE_USER_MASTER." AS UM ON AM.UserId=UM.UserId 
				LEFT JOIN ".TABLE_ASSIGNMENT_CATEGORY." AS AC ON AM.AssignmentCategoryId=AC.AssignmentCategoryId
				WHERE UM.userType='1' AND UM.isDisabled='1' AND AM.isEnabled=1 AND AM.AssignmentId IN (".implode(',',$fav_assign_id).") ORDER BY AM.orderBy";
				#echo $sql;die;
				
				/* $sql="SELECT AE.id, AE.assign_id, AE.image_name, AE.model_id, AE.user_id, AE.is_approved, AE.is_read, AE.updatedTS, AE.payment_amount, UM.profile_name, UM.profile_description, UM.tag_line, NF.type AS notification_type
		FROM spr_notification AS NF
		LEFT JOIN spr_assignment_img_submission AS AE ON AE.id = NF.image_id
		LEFT JOIN spr_user_master AS UM ON UM.id = NF.creator_id
		WHERE NF.creator_id='".$userid."' AND NF.type='1'
			GROUP BY NF.id ";
				*/
				
				
				$result = $conn->query($sql);
				while($row = $result->fetch(PDO::FETCH_ASSOC))
				{
					$left_days = GeneralFunction::getDeadlineDays($row['submissionDeadline'],$row['launchedDate'], $row['createdAt']);
					
					if($left_days>0)
					{
					$assignRow["assignment_id"] = $row['AssignmentId'];
					
					$assignRow["assignment_owner_id"] = $row['UserId'];
	
					$assignRow["assignment_name"] = $row['profileName'];
					
					$assignRow["assignment_looking_for"] = GeneralFunction::assignLookingFor((int)$row['lookingFor']);
	
					$assignRow["assignment_hope_to_select"] = GeneralFunction::assignHopeToSelect((int)$row['hopeToSelect']);
	
					$assignRow["assignment_tagline"]= $row['tagLine'];
	
					$assignRow["assignment_desc"]=$row['lookingForText'];
					
					$assignRow["assignment_img_format"] = GeneralFunction::getimageryFormat(unserialize($row['imageryFormat']));
	
					$assignRow["assignment_model_release"] = ucfirst($row['modelRelease']);
	
					$assignRow["assignment_imagery"] = implode(',',unserialize($row['imageryFor']));
	
					$assignRow["assignment_imagery_text"] = $row['imageryForText'];
	
					$assignRow["assignment_pic_url"] = UserMasterTable::getBrandPic($row['UserId']);
	
					$assignRow["assignment_budget"] = $row['ourBudget'];
					$assignRow["assignment_rewards"] = $row['rewards'];	
	
					//$assignRow["assignment_deadline"]="7";
					//$assignRow["assignment_deadline"]= $row['submission_deadline'];
					$assignRow["category_name"] = $row['categoryName'];
					
					$assignRow["assignment_deadline"] = GeneralFunction::getDeadline($row['submissionDeadline'],$row['launchedDate'], $row['createdAt']);
	
					$assignRow["assignment_reference_image_text"] = $row['referenceImageText'];
	
					$assignRow["is_uploaded"] = (AssignModelTable::checkIfAlreadyAssign($row['AssignmentId'],$userid))?"1":"0";
	
					$assignRow['assignment_reference_image_list'] = AssignmentTable::referenceImages($row['AssignmentId']);

					$assignRow['assignment_thumb_image_list'] = AssignmentTable::thumbImages($userid,$row['AssignmentId'],$row['UserId']);
		
					//$assignRow["assignment_created_at"] = date(DATE_FORMAT,strtotime($row['created_at']));
					$assignRow["assignment_created_at"] = (string)$row['createdAt'];
					$fav_assignment[] = $assignRow;
					}
				}
			}
		}
		return $fav_assignment;
	}

	public static function getFavAssignmentCount($userid)
	{
		global $conn;
		if($userid > 0)
		{
			$fav_assign_id = self::getFavAssignmentId($userid);
			$total_fav_assign = 0;
			if(count($fav_assign_id) > 0)
			{	
				$sql="SELECT count(AM.AssignmentId) AS total_fav_assign
				FROM ".TABLE_ASSIGNMENT." AS AM
				LEFT JOIN ".TABLE_USER_MASTER." AS UM ON AM.UserId=UM.UserId 
				LEFT JOIN ".TABLE_ASSIGNMENT_CATEGORY." AS AC ON AM.AssignmentCategoryId=AC.AssignmentCategoryId
				WHERE UM.userType='1' AND UM.isDisabled='1' AND AM.isEnabled=1 AND AM.AssignmentId IN (".implode(',',$fav_assign_id).")";
				#echo $sql;die;
				$result = $conn->query($sql);
				$row = $result->fetch(PDO::FETCH_ASSOC);
				if($row['total_fav_assign'] > 0)
				{
					$total_fav_assign = (int)$row['total_fav_assign'];
				}
			}
			return $total_fav_assign;
		}
	}
	
	public static function getFavAssignmentId($userid)
	{#ini_set('display_errors','1');
		global $conn;
		if($userid > 0)
		{
			$sql="SELECT AssignmentId FROM ".self::$_tableName." WHERE UserId='".$userid."'";
			#$sql="SELECT creator_id FROM ".TABLE_NOTIFICATION." WHERE user_id='".$userid."'";
			$result = $conn->query($sql);
			$ids = array();
			while($row = $result->fetch(PDO::FETCH_ASSOC))
			{
				$ids[] = $row['AssignmentId'];
			}
			if(count($ids) > 0)
			{
				return $ids;
			}
			else
			{
				return array();
			}
		}
	}

	public static function addAssignToFav($assign_id,$userid)
	{
		global $conn;
		if(self::checkIfAlreadyFav($assign_id,$userid)==0)
		{
			$sql="	INSERT INTO ".self::$_tableName."
				(`FavouriteAssignmentId`,`AssignmentId`,`UserId`,`timeStamp`)
				VALUES(NULL,'".$assign_id."','".$userid."',UNIX_TIMESTAMP(NOW()));";
			$query = $conn->prepare($sql);
			$query->execute();
			$_id = $conn->lastInsertId();
			return $_id;
		}
		else
		{
			return "exist";
		}
	}

	public static function checkIfAlreadyFav($assign_id,$userid)
	{
		global $conn;
		$sql = "SELECT FavouriteAssignmentId AS is_already_fav FROM ".self::$_tableName." WHERE UserId={$userid} AND AssignmentId={$assign_id}";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row['is_already_fav']>0)
		{
			return $row['is_already_fav'];
		}
		else
		{
			return 0;
		}
	}
	
	
	public static function removeFavAssign($assign_id,$userid)
	{
		global $conn;
		$sql = "DELETE FROM ".self::$_tableName." WHERE UserId={$userid} AND AssignmentId={$assign_id}";
		$count = $conn->exec($sql);
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

}