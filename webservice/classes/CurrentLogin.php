<?php
class CurrentLogin
{
	private static $_tableName = TABLE_CURRENT_LOGIN;
	
	public static function authenticateUser($userid,$token)
	{
		return 1;
		if(!UserMasterTable::isCreator($userid))
		{
			echo json_encode(array("msg"=>"Invalid request"));die;	
		}
		
		global $conn;
		$sql = "SELECT CurrentLoginId AS is_user_login FROM ".self::$_tableName." WHERE UserId={$userid} AND token='".$token."'";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row['is_user_login']>0)
		{
			
			return 1;
		}
		else
		{
			return 1;
		}
		
		
	}

	public static function checkUserLogin($userid)
	{
		global $conn;
		$sql = "SELECT CurrentLoginId AS is_user_login FROM ".self::$_tableName." WHERE UserId={$userid}";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row['is_user_login']>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public static function addLoginUser($userid,$token)
	{
		global $conn;
		if(!self::checkUserLogin($userid))
		{
			$sql=
			"INSERT INTO ".self::$_tableName."
			(`CurrentLoginId`, `UserId`, `isLogin`, `token`, `createdAt`, `updatedAt`)
			VALUES (NULL, '".$userid."', '1', '".$token."', NOW(), NOW());";
			$query = $conn->prepare($sql);
			$query->execute();
			$login_id = $conn->lastInsertId();
			return $login_id;
		}
		else
		{
			self::removeLoginUser($userid);
			self::addLoginUser($userid,$token);
		}
	}

	public static function removeLoginUser($userid)
	{
		global $conn;
		$sql = "DELETE FROM ".self::$_tableName." WHERE UserId={$userid}";
		$count = $conn->exec($sql);
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}
