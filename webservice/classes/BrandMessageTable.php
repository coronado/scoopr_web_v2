<?php
class BrandMessageTable
{
	private static $_tableName = TABLE_BRAND_MESSAGE;
	
	public static function listBrandMessage($userid)
	{
		global $conn;
		$list_brand_message = array();
		if($userid > 0)
		{
			/*$sql = "SELECT BM.BrandMessageId, BM.subject, BM.brandMessage, BM.modifiedDate, BM.UserId AS buyerId ".
			       "FROM ".self::$_tableName." as BM ".
			       "INNER JOIN ".TABLE_USER_MASTER." AS UM ON BM.UserId = UM.UserId ".
			       "WHERE BM.status = 1";	 */

			$sql = "SELECT BrandMessageId, subject, brandMessage, modifiedDate, UserId ".
			       "FROM ".self::$_tableName." WHERE status = 1";
			$sql .= " ORDER BY BrandMessageId DESC";
			
			
			$result = $conn->query($sql);
			while($row = $result->fetch(PDO::FETCH_ASSOC))
			{
			
				$assignRow["brand_message_id"] = $row['BrandMessageId'];

				$assignRow["subject"] = $row['subject'];

				$assignRow["brand_message"] = $row['brandMessage'];
				
				$assignRow["modified_date"] = $row['modifiedDate'];				

				$assignRow["assignment_pic_url"] = UserMasterTable::getBrandPic($row['UserId']);

				$list_brand_message[] = $assignRow;
			}
		}
		return $list_brand_message;
	}
	
}
