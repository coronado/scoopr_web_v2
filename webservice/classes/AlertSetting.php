<?php
class AlertSetting
{
	private static $_tableName = TABLE_ALERT_SETTING;
	
	public static function getAlert($userid)
	{
		global $conn;
		$sql = "SELECT * FROM ".self::$_tableName." WHERE UserId={$userid}";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row['AlertSettingId']>0)
		{
			return $row;
		}
		else
		{
			return self::addAlertUser($userid);
		}
	}
	
	public static function setAlert($userid,$value)
	{
		global $conn;
		$sql = "SELECT AlertSettingId FROM ".self::$_tableName." WHERE UserId={$userid}";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row['id'] > 0)
		{
			$_id = $row['AlertSettingId'];
		}
		else
		{
			$_id = self::addAlertUser($userid);
		}
		$sql = "UPDATE ".self::$_tableName." SET value='".$value."' WHERE AlertSettingId=".$_id;
		if($conn->exec($sql))
		{
			return 1;
		}
		else
		{
			return 0;
		}		
	}

	public static function addAlertUser($userid)
	{
		global $conn;
		$sql="INSERT INTO ".self::$_tableName." (`AlertSettingId`,`UserId`,`value`)
		VALUES (NULL,'".$userid."', '0');";
		$query = $conn->prepare($sql);
		$query->execute();
		$_id = $conn->lastInsertId();
		return (int)$_id;
	}
}