<?php
ini_set('display_errors', 0);
class PushNotification
{
	public $token = null;
        public $msg = null;

        public function __construct( $token )
	{
	    	$this->msg   = "Friend Request";
	    	$this->token = $token;
        }

      
	#  send common notification
        public function sendNotificationCommon($id,$user_full_name,$message,$type)
	{
            $badge = 0;

            // Put your private key's passphrase here:

            //$passphrase = '1234';
	    $passphrase = 'welcome';	

            ////////////////////////////////////////////////////////////////////////////////

            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

            // Open a connection to the APNS server
            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

            //$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

            if(!$fp)
            {
		return;
            }
            $freq = (string)$this->findCount($id);
	    
            // Create the payload body
            $body['aps'] = array(
	           'alert' => $message,
	           'sound' => 'default',
		   'type' => $type,
		   'fullname'=>$user_full_name,
	           'count' => $freq,
	           'badge' => $badge
	           );

            // Encode the payload as JSON
            $payload = json_encode($body);

            // Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $this->token) . pack('n', strlen($payload)) . $payload;

            #print $payload;
            fwrite($fp, $msg);
            fclose($fp); 
       	}
}