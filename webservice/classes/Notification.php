<?php
class Notification
{
	private static $_tableName = TABLE_ASSIGNMENT_IMG_SUBMISSION;
	
	public static function getNotification($userid)
	{
		global $conn;
		
		$sql="SELECT AE.AssignmentImgSubmissionId, AE.AssignmentId, AE.imageName, AE.ModelId, AE.creatorId, AE.isApproved, AE.isRead, AE.updatedTS, AE.paymentAmount, UM.profileName, UM.profileDescription, UM.tagLine, NF.type AS notification_type
		FROM ".TABLE_NOTIFICATION." NF
		LEFT JOIN ".TABLE_ASSIGNMENT_IMG_SUBMISSION." AS AE ON AE.AssignmentImgSubmissionId = NF.AssignmentImgSubmissionId
		LEFT JOIN ".TABLE_USER_MASTER." AS UM ON UM.UserId = NF.CreatorId
		WHERE NF.CreatorId='".$userid."'
		GROUP BY NF.NotificationId";
		
		
		$notificationList  =  array();
		$result = $conn->query($sql);
		while($row = $result->fetch(PDO::FETCH_ASSOC))
		{
			if($row['isApproved']==2){
				$row['submission'] = 'completed';
			}
			else {
				$row['submission'] = 'progress';
			}
			$row['notification_type'] = $row['notification_type'];
			$row['tag_line'] = $row['tagLine'];
			$row['brand_name'] = $row['profileName'];
			$row['notify_date'] = date("m/d/y",$row['updatedTS']);
			$row['image_url'] = GeneralFunction::getSubmissionImage($row['imageName']);
			$row['assignment_detail'] = array();
			$row['assignment_detail'] = AssignmentTable::detailAssignment($row['AssignmentId'],$userid);
			$notificationList[] = $row;
		}
		return $notificationList;
	}
	
	public static function getNotificationCount($userid,$type)
	{
		global $conn;
		//$sql="SELECT count(id) AS total_count FROM ".self::$_tableName." WHERE user_id='".$userid."' AND is_read='".$type."' AND is_approved!='0'";
		$sql="SELECT count(NotificationId) AS total_count FROM ".TABLE_NOTIFICATION." WHERE CreatorId='".$userid."'";
		$notificationList  =  array();
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row['total_count']>=0)
		{
			return $row['total_count'];
		}
		else
		{
			return 0;
		}
	}

	public static function deleteNotification($userid,$notification_id)
	{
		global $conn;
		$sql = "DELETE FROM ".TABLE_NOTIFICATION." WHERE NotificationId = '".$notification_id."'";
		$result = $conn->query($sql);
		if($result) {
			return true;
		} else {
			return false;
		}
	}
}