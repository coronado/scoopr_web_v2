<?php
class GeneralFunction
{
	public static function sendEmail($email,$subject,$message)
	{
		$from = 'Scoopr <noreply@scoopr.com>';
		$to = $email;
		$headers= "From: " . strip_tags($from) . "\r\n";
		$headers.= "Reply-To: ". strip_tags($from) . "\r\n";
		$headers.= "MIME-Version: 1.0\r\n";
		$headers.= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$send = @mail($to,$subject,$message,$headers);
		return $send;
	}

	public static function replaceNullFromJson($jsonData)
	{
		$temp = str_replace(":null",":''", $jsonData);
		return str_replace("''","\"\"", $temp);
	}
	
	public static function defaultImage()
	{
		return SERVER_HTTP_PATH.'/images/default.jpg';
	}

	public static function buyerDefaultImage()
	{
		return SERVER_HTTP_PATH.'/images/profile_img/default_profile_img.png';
	}

	public static function getProfileImage($filename,$h,$w)
	{
		$real_path = SERVER_REAL_PATH.'/images/profile_img';
		if(file_exists($real_path.'/'.$filename) && !empty($filename))
		{
			return 'http://'.SERVER_PATH.'/images/profile_img/'.$filename;
		}
		else
		{
			return self::defaultImage();
		}
	}
	
	public static function getModelLogo($filename,$h,$w)
	{
		$real_path = SERVER_REAL_PATH.'/images/model_logo';
		if(file_exists($real_path.'/'.$filename) && !empty($filename))
		{
			return 'http://'.SERVER_PATH.'/images/model_logo/'.$filename;
		}
		else
		{
			return self::defaultImage();
		}
	}
	
	public static function getModelPdf($filename,$h,$w)
	{
		$real_path = SERVER_REAL_PATH.'/images/model_logo';
		if(file_exists($real_path.'/'.$filename) && !empty($filename))
		{
			return 'http://'.SERVER_PATH.'/images/model_logo/'.$filename;
		}
		else
		{
			return self::defaultImage();
		}
	}
	
	public static function getReferenceImage($filename)
	{
		$real_path = SERVER_REAL_PATH.'/images/reference_img';
		if(file_exists($real_path.'/'.$filename) && !empty($filename))
		{
			return 'http://'.SERVER_PATH.'/images/reference_img/'.$filename;
		}
		else
		{
			$path = "";
			return $path;
		}
	}
	
	public static function getSubmissionImage($filename)
	{
		$real_path = SERVER_REAL_PATH.'/images/assign_img_submission/small_size_image';
		if(file_exists($real_path.'/'.$filename) && !empty($filename))
		{
			return 'http://'.SERVER_PATH.'/images/assign_img_submission/small_size_image/'.$filename;
		}
		else
		{
			return self::defaultImage();
		}
	}
	
	public static function getThumbImage($filename)
	{
		$real_path = SERVER_REAL_PATH.'/images/assign_img_submission/small_size_image';
		if(file_exists($real_path.'/'.$filename) && !empty($filename))
		{
			return 'http://'.SERVER_PATH.'/images/assign_img_submission/small_size_image/'.$filename;
		}
		else
		{
			return self::defaultImage();
		}
	}

	public static function getimageryFormat($format_val)
	{
		if(in_array('1',$format_val) && !in_array('2',$format_val))
		{
			return "Horizontal";
		}
		elseif(in_array('2',$format_val) && !in_array('1',$format_val))
		{
			return "Vertical";
		}
		else
		{
			return "Horizontal or Vertical";
		}
	}
	
	public static function assignLookingFor($val)
	{
		if($val==1)
		{
			return 'Photography';
		}	
		elseif($val==2)
		{
			return 'Video';
		}
		else
		{
			return 'Both';
		}
	}
	
	public static function assignHopeToSelect($val)
	{
		if($val==1)
		{
			return 'Single image';
		}	
		else
		{
			return 'Multiple images';
		}
	}
	
	public static function getDeadline($submission_deadline, $launched_date, $created_at)
	{
		$current_date = date('Y-m-d');
		$submission_date = date('Y-m-d',(strtotime($launched_date." +$submission_deadline day")));

		$diff = abs(strtotime($submission_date) - strtotime($current_date));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		if($days>1)
		{
			$deadline = $days. " days";
		}
		else
		{
			$deadline = $days. " day";
		}
		return $deadline;
	}

	public static function getDeadlineDays($submission_deadline, $launched_date, $created_at)
	{
		$current_date = date('Y-m-d');
		$submission_date = date('Y-m-d',(strtotime($launched_date." +$submission_deadline day")));

		$diff = abs(strtotime($submission_date) - strtotime($current_date));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		
		return $days;
	}
}
