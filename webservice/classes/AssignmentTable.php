<?php
class AssignmentTable
{
	private static $_tableName = TABLE_ASSIGNMENT;
	
	public static function listAssignment($userid,$filter_date)
	{
		global $conn;
		$list_assignment = array();
		if($userid > 0)
		{
			$sql="SELECT 
			AM.*,
			UM.profileName,UM.tagLine, AC.categoryName
			FROM ".self::$_tableName." AS AM 
			LEFT JOIN ".TABLE_USER_MASTER." AS UM ON AM.UserId=UM.UserId 
			LEFT JOIN ".TABLE_ASSIGNMENT_CATEGORY." AS AC ON AM.AssignmentCategoryId=AC.AssignmentCategoryId
			WHERE UM.userType='1' AND UM.isDisabled='1' AND AM.isEnabled=1 AND AM.isLaunched='1'"; 
			if($filter_date!='') {
				$sql .= " AND AM.createdAt > '".$filter_date."'";
			} else {
				$sql .= " ORDER BY AM.orderBy";
			}
			//echo $sql;die;
			$result = $conn->query($sql);
			while($row = $result->fetch(PDO::FETCH_ASSOC))
			{
				$left_days = GeneralFunction::getDeadlineDays($row['submissionDeadline'],$row['launchedDate'], $row['createdAt']);
			
				if($left_days>0) 
				{
				$assignRow["assignment_id"] = $row['AssignmentId'];

				$assignRow["assignment_owner_id"] = $row['UserId'];

				$assignRow["assignment_name"] = $row['profileName'];
				
				$assignRow["assignment_looking_for"] = GeneralFunction::assignLookingFor((int)$row['lookingFor']);

				$assignRow["assignment_hope_to_select"] = GeneralFunction::assignHopeToSelect((int)$row['hopeToSelect']);

				$assignRow["assignment_tagline"]=$row['tagLine'];

				$assignRow["assignment_desc"]=$row['lookingForText'];
				
				$assignRow["assignment_img_format"] = GeneralFunction::getimageryFormat(unserialize($row['imageryFormat']));

				$assignRow["assignment_model_release"] = ucfirst($row['modelRelease']);

				$assignRow["assignment_imagery"] =implode(" , ",self::getImageryNames(unserialize($row['imageryFor'])));

				$assignRow["assignment_imagery_text"] = $row['imageryForText'];

				$assignRow["assignment_pic_url"] = UserMasterTable::getBrandPic($row['UserId']);

				$assignRow["assignment_budget"]=(string)$row['ourBudget'];

				$assignRow["assignment_rewards"]=(string)$row['rewards'];
				
				//$assignRow["assignment_deadline"]= $row['submission_deadline'];

				$assignRow["assignment_deadline"] = GeneralFunction::getDeadline($row['submissionDeadline'],$row['launchedDate'], $row['createdAt']);

				$assignRow["assignment_reference_image_text"] = $row['referenceImageText'];

				$assignRow["is_uploaded"] = (AssignModelTable::checkIfAlreadyAssign($row['AssignmentId'],$userid))?"1":"0";

				$assignRow['assignment_reference_image_list'] = self::referenceImages($row['AssignmentId']);
				$assignRow['assignment_thumb_image_list'] = self::thumbImages($userid,$row['AssignmentId'],$row['UserId']);
	
				//$assignRow["assignment_created_at"] = date(DATE_FORMAT,strtotime($row['created_at']));

				$assignRow["assignment_created_at"] = (string)$row['createdAt'];

				$assignRow["assignment_is_launched"]=$row['isLaunched'];

				$assignRow["category_name"] = $row['categoryName'];

				$overview_data = array();

				$overview_data = AssignmentOverview::getTotalViewSubmission($row['AssignmentId']);
				
				$assignRow["assignment_total_submissions"] =$overview_data['total_submissions'];

				$assignRow["assignment_total_views"] =$overview_data['total_views'];
				
				$list_assignment[] = $assignRow;
				}
			}
		}
		return $list_assignment;
	}
	
	public static function getImageryNames($_ids)
	{
		global $conn;
		if($_ids)
		{
			$sql = "SELECT imageryForName FROM ".TABLE_IMAGERY_FOR." WHERE ImageryForid IN (".implode(",",$_ids).")";
			$result = $conn->query($sql);
			$_names = array();
			while($row = $result->fetch(PDO::FETCH_ASSOC))
			{
				$_names[] = $row['imageryForName'];
			}
		}
		else{
			return "-";
		}
		if(count($_names)>0){
			return $_names;
		}
		else{
			return "-";
		}
	}

	public static function detailAssignment($assign_id,$userid)
	{
		//echo "detailAssignment";die;
		global $conn;
		$assignRow = array();
		if($assign_id > 0)
		{
			$sql="SELECT 
			AM.*,
			UM.profileName,UM.tagLine
			FROM ".self::$_tableName." AS AM 
			LEFT JOIN ".TABLE_USER_MASTER." AS UM ON AM.UserId=UM.UserId
			WHERE AM.AssignmentId='".$assign_id."'";
			//echo $sql;die;
			$result = $conn->query($sql);
			$row = $result->fetch(PDO::FETCH_ASSOC);
			//print_r($row);die;
			if($row)
			{
				$assignRow["assignment_id"] = $row['AssignmentId'];

				$assignRow["assignment_owner_id"] = $row['UserId'];

				$assignRow["assignment_name"] = $row['profileName'];
				
				$assignRow["assignment_looking_for"] = GeneralFunction::assignLookingFor((int)$row['lookingFor']);

				$assignRow["assignment_hope_to_select"] = GeneralFunction::assignHopeToSelect((int)$row['hopeToSelect']);

				$assignRow["assignment_tagline"]=$row['tagLine'];

				$assignRow["assignment_desc"]=$row['lookingForText'];
				
				$assignRow["assignment_img_format"] = GeneralFunction::getimageryFormat(unserialize($row['imageryFormat']));

				$assignRow["assignment_model_release"] = ucfirst($row['modelRelease']);

				//$assignRow["assignment_imagery"] =implode(",",self::getImageryNames(implode(',',unserialize($row['imagery_for']))));
				
				$assignRow["assignment_imagery"] =implode(',',unserialize($row['imageryFor']));
				

				$assignRow["assignment_imagery_text"] = $row['imageryForText'];

				$assignRow["assignment_pic_url"] = UserMasterTable::getBrandPic($row['UserId']);

				$assignRow["assignment_budget"]=(string)$row['ourBudget'];
		
				$assignRow["assignment_rewards"]=(string)$row['rewards'];
				
				//$assignRow["assignment_deadline"]= $row['submission_deadline'];

				$assignRow["assignment_deadline"] = GeneralFunction::getDeadline($row['submissionDeadline'],$row['launchedDate'], $row['createdAt']);

				$assignRow["assignment_reference_image_text"] = $row['referenceImageText'];

				$assignRow["is_uploaded"] = (AssignModelTable::checkIfAlreadyAssign($row['AssignmentId'],$userid))?"1":"0";

				$assignRow['assignment_reference_image_list'] = self::referenceImages($row['AssignmentId']);
				
				
	         		//$assignRow['assignment_thumb_image_list'] = self::thumbImages($userid);
				//$assignRow["assignment_created_at"] = date(DATE_FORMAT,strtotime($row['created_at']));
				$assignRow["assignment_created_at"] = (string)$row['createdAt'];

				$assignRow["assignment_is_launched"]=$row['isLaunched'];

				$overview_data = array();

				$overview_data = AssignmentOverview::getTotalViewSubmission($row['AssignmentId']);
				$assignRow["assignment_total_submissions"] =$overview_data['total_submissions'];

				$assignRow["assignment_total_views"] =$overview_data['total_views'];
			}
		}
		//print_r($assignRow);die;
		return $assignRow;
	}

	public static function referenceImages($assign_id)
	{
		global $conn;
		$sql = "SELECT * FROM ".TABLE_REFERENCE_IMAGE." WHERE AssignmentId='".$assign_id."'";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		$reference_images = array();


		$image1 = GeneralFunction::getReferenceImage($row['image1']);
		$image2 = GeneralFunction::getReferenceImage($row['image2']);
		$image3 = GeneralFunction::getReferenceImage($row['image3']);
		if($image1!='') {
			$reference_images[] = $image1;	
		}
		if($image2!='') {
			$reference_images[] = $image2;	
		}
		if($image3!='') {
			$reference_images[] = $image3;	
		}
		
		return $reference_images;
	}
	public static function thumbImages($creator_id,$assignment_id,$buyer_id)
	{
		global $conn;
		$sql = "SELECT smallSizeImage FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." WHERE creatorId='".$creator_id."' AND AssignmentId = '".$assignment_id."' AND buyerId = '".$buyer_id."'";
		$result = $conn->query($sql);
		$thumb_images = array();
		while($row = $result->fetch(PDO::FETCH_ASSOC))
		{
		  
		 $thumb_images[] = GeneralFunction::getThumbImage($row['smallSizeImage']);
		
		}
		return $thumb_images;
		//return $sql;
	}
	/*public static function categoryList()
	{
		global $conn;
		$sql = "SELECT category_name FROM ".ASSIGNMENT_CATEGORY." WHERE status= 1";
		$result = $conn->query($sql);
		
		while($row = $result->fetch(PDO::FETCH_ASSOC))
		{
		    $category[] = $row['category_name'];
		}
		return $category;
	}*/
	
}
