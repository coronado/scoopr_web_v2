<?php

class ContactUs
{
	private static $_tableName = TABLE_CONTACT_US;

	public static function contact_us($userid, $contact_us)
	{
		global $conn;
		$sql="INSERT INTO ".self::$_tableName." (UserId, contactUsText) VALUES (:user_id, :contact_us)";
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':user_id', $userid);
		$stmt->bindParam(':contact_us', $contact_us);  
		$stmt->execute(); 
    	$stmt->errorInfo();
		
		echo $insertedId = $conn->lastInsertId();
		return $insertedId;
	}

}
