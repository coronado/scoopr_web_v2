<?php
class ModelReleaseTable
{
	private static $_tableName = TABLE_MODEL_RELEASE;
	
	public static function addModel($shot_for,$notes,$userid)
	{
		global $conn;
		$sql = "INSERT INTO ".self::$_tableName." VALUES (NULL,:model_shot_for,:model_notes,'','','".$userid."',NOW())";
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':model_shot_for',$shot_for);
		$stmt->bindParam(':model_notes',$notes);
		$stmt->execute();
		$insertedId = $conn->lastInsertId();
		return $insertedId;
	}
	
	public static function updateModelPic($model_id,$filename)
	{
		global $conn;
		if($model_id > 0)
		{
			$sql = "UPDATE ".self::$_tableName." SET modelLogo='".$filename."' WHERE ModelId={$model_id}";
			$count = $conn->exec($sql);
		}
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	public static function updateModelPdf($model_id,$filename)
	{
		global $conn;
		if($model_id > 0)
		{
			$sql = "UPDATE ".self::$_tableName." SET modelPdf='".$filename."' WHERE ModelId={$model_id}";
			$count = $conn->exec($sql);
		}
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public static function listModel($userid)
	{
		global $conn;
		$list_model = array();
		if($userid > 0)
		{
			$sql = "SELECT * FROM ".self::$_tableName." WHERE UserId={$userid}";
			$result = $conn->query($sql);
			while($row = $result->fetch(PDO::FETCH_ASSOC))
			{
				$row["model_logo_url"] = GeneralFunction::getModelLogo($row['modelLogo'],'','');
				$row["model_pdf_url"] = GeneralFunction::getModelLogo($row['modelPdf'],'','');
				$list_model[] = $row;	
			}
		}
		return $list_model;
	}
	
	public static function getModelCount($userid)
	{
		global $conn;
		$list_model = array();
		if($userid > 0)
		{
			//$sql = "SELECT count(*) AS total_model FROM ".TABLE_NOTIFICATION." WHERE creator_id={$userid}";
			$sql = "SELECT count(*) AS total_model FROM ".self::$_tableName." WHERE UserId={$userid}";
			$result = $conn->query($sql);
			$row = $result->fetch(PDO::FETCH_ASSOC);
			if($row['total_model'] > 0)
			{
				return (int)$row['total_model'];
			}
			else
			{
				return 0;
			}
		}
	}
	
	public static function removeModelRelease($userid, $modelid) 
	{
		global $conn;
		$sql = "DELETE FROM ".self::$_tableName." WHERE ModelId={$modelid}";
		$count = $conn->exec($sql);
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
	
	
	
	
}