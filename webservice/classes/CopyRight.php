<?php
class CopyRight
{
	private static $_tableName = TABLE_REPORT_COPYRIGHT;
	
	public static function reportCopyRight($img_desc,$who_is_using,$still_in_use,$discover_date)
	{
		global $conn;
		$sql = "INSERT INTO ".self::$_tableName."
		VALUES (NULL, :image_desc, :image_user, :being_used, :discover_date);";
		//echo $sql;die;
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':image_desc', $img_desc);
		$stmt->bindParam(':image_user', $who_is_using);
		$stmt->bindParam(':being_used', $still_in_use);
		$stmt->bindParam(':discover_date', $discover_date);
		$stmt->execute();
		$insertedId = $conn->lastInsertId();
		return $insertedId;
	}
}