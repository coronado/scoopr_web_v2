<?php
class AssignmentOverview
{
	private static $_tableName = TABLE_ASSIGNMENT_OVERVIEW;
	
	public static function add_view_submission($assign_id,$buyer_id,$date,$submission_count,$type)
	{
		global $conn;
		$overview_id = self::checkViewSubDate($assign_id,$buyer_id,$date);
		if($overview_id>0)
		{
			if($type==1)
			{
				$updateSql = "SET view=view+1";
			}
			elseif($type==2)
			{
				$updateSql = "SET submission=submission+$submission_count";
			}
			else
			{
				$updateSql = "SET view=view+1,submission=submission+1";
			}
			$sql = "UPDATE ".self::$_tableName." ".$updateSql." WHERE AssignmentOverviewId={$overview_id}";
			#echo $sql;die;	
			$count = $conn->exec($sql);
			if($count)
			{
				return "updated successfully";
			}
			else
			{
				return "error";
			}
				
		}
		else
		{
		   $sql="INSERT INTO ".self::$_tableName." (`AssignmentOverviewId`,`buyerId`,`AssignmentId`,`view`,`submission`,`date`)
			VALUES (NULL,'".$buyer_id."','".$assign_id."','1','0','".$date."');";
			#echo $sql;die;
			$query = $conn->prepare($sql);
			$query->execute();
			$_id = $conn->lastInsertId();
			if($_id)
			{
				return "add successfully";
			}
			else
			{
				return "error";
			}
		}
	}
	
	public static function checkViewSubDate($assign_id,$buyer_id,$date)
	{
		global $conn;
		$sql = "SELECT AssignmentOverviewId FROM ".self::$_tableName." WHERE buyerId='".$buyer_id."' AND date='".$date."'";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row['AssignmentOverviewId'] > 0)
		{
			return (int)$row['AssignmentOverviewId'];
		}
		else
		{
			return 0;
		}
	}
	
	public static function getTotalViewSubmission($assign_id)
	{
		global $conn;
		$sql = "SELECT 
		SUM(view) AS total_views,
		SUM(submission) AS total_submissions 
		FROM spr_assign_overview
		WHERE AssignmentId='".$assign_id."'
		GROUP BY AssignmentId";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row)
		{
			return $row;
		}
		else
		{
			return array();
		}
	}
}
