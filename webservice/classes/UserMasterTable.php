<?php
class UserMasterTable
{
	private static $_tableName = TABLE_USER_MASTER;
	
	public static function addUser($name,$email,$password,$city,$desc,$userType,$fb_id)
	{
		global $conn;
		/**$sql = "INSERT INTO ".self::$_tableName."
		VALUES (NULL, :name, :email_address, :password, '".$userType."', :description, :city, :profile_name, '', '', '', '1', '".(int)$fb_id."', NOW());";
		
		/*$testSql = "INSERT INTO ".self::$_tableName."
		VALUES (NULL, '".$name."', '".$email."', '".md5($password)."', '".$userType."', '".$desc."', '".$city."', '', '', '', '', '1', '".(int)$fb_id."', NOW());";*/
		#echo $sql;die;
		
      $sql="INSERT INTO ".self::$_tableName." (name, emailAddress, password, description, city, profileName, userType, fbId ) VALUES (:name, :email_address, :password, :description, :city, :profile_name, :user_type, :fb_id)";
		$new_password = md5($password);
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':email_address', $email);
		$stmt->bindParam(':password', $new_password);
		$stmt->bindParam(':description', $desc);
		$stmt->bindParam(':city', $city);
		$stmt->bindParam(':profile_name', $name);
		$stmt->bindParam(':user_type', $userType);
		$stmt->bindParam(':fb_id', $fb_id);   
		$stmt->execute();
		$insertedId = $conn->lastInsertId();
		return $insertedId;
	}

	public static function registerFbUser($email, $name, $fb_id)
	{
		global $conn;
		$userType = "2";
		$sql="INSERT INTO ".self::$_tableName." (name, emailAddress, profileName, userType, fbId ) VALUES (:name, :email_address, :profile_name, :user_type, :fb_id)";
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':name', $name);
		$stmt->bindParam(':email_address', $email);
		$stmt->bindParam(':profile_name', $name);
		$stmt->bindParam(':user_type', $userType);
		$stmt->bindParam(':fb_id', $fb_id);   
		$stmt->execute();
		$insertedId = $conn->lastInsertId();
		return $insertedId;
	}
	
	public static function isEmailExist($email)
	{
		global $conn;
		$sql = "SELECT UserId AS userid FROM ".self::$_tableName." WHERE emailAddress='".$email."' and userType='2'";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		return (int)$row['userid'];
	}
	
	public static function isCreator($userid)
	{
		global $conn;
		$sql = "SELECT userType FROM ".self::$_tableName." WHERE UserId='".$userid."'";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row['userType']==2){
			return 1;
		}
		else{ return 0;}
	}

	public static function isEmailExistNotCurrent($email,$userid)
	{
		global $conn;
		$sql = "SELECT UserId FROM ".self::$_tableName." WHERE emailAddress='".$email."' AND UserId!='".$userid."'";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		return (int)$row['UserId'];
	}

	public static function isFacebookUser($email,$fb_id){
		global $conn;
		$sql = "SELECT UserId AS userid,emailAddress,fbId FROM ".self::$_tableName." WHERE emailAddress='".$email."' AND fbId='".$fb_id."'";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row){
			return $row;
		}
		else{
			return 0;
		}
	}

	public static function updateProfilePic($userid,$filename)
	{
		global $conn;
		if($userid > 0)
		{
			$sql = "UPDATE ".self::$_tableName." SET profileImg='".$filename."' WHERE UserId={$userid}";
			$count = $conn->exec($sql);
		}
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	public static function checkLogin($email,$pwd,$userType)
	{
		global $conn;
		$sql = "SELECT UserId FROM ".self::$_tableName." WHERE emailAddress='".$email."' AND password='".md5($pwd)."' AND userType='".$userType."'";
		//echo $sql;die;
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		return (int)$row['UserId'];
	}
	
	public static function getUserProfilePic($userid)
	{
		global $conn;
		$sql = "SELECT profileImg FROM ".self::$_tableName." WHERE UserId='".$userid."'";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if(isset($row['profileImg'])===true)
		{
			return GeneralFunction::getProfileImage($row['profileImg'],'','');	
		}
		return '';
	}

	public static function getBrandPic($userid)
	{
		global $conn;
		$sql = "SELECT profileImg FROM ".self::$_tableName." WHERE UserId='".$userid."' AND userType='1'";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row['profileImg']!='default_profile_img.png')
		{
			return GeneralFunction::getProfileImage($row['profileImg'],'','');	
		}
		else
		{
			return GeneralFunction::buyerDefaultImage();
		}
	}

	public static function checkFbLogin($email,$fb_id,$userType)
	{
		global $conn;
		$sql = "SELECT UserId FROM ".self::$_tableName." WHERE emailAddress='".$email."' AND fbId='".$fb_id."' AND userType='".$userType."'"; 
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		return (int)$row['UserId'];
	}

	
	public static function getUserData($userid)
	{
		global $conn;
		$sql = "SELECT UserId,name,emailAddress,description,city,profileImg FROM ".self::$_tableName." WHERE UserId='".$userid."'";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		if($row)
		{
			return $row;
		}
		else
		{
			return 0;
		}
	}
	
	public static function checkPassword($userid,$user_pass)
	{
		global $conn;
		$sql = "SELECT UserId FROM ".self::$_tableName." WHERE UserId='".$userid."' AND password='".md5($user_pass)."'";
		$result = $conn->query($sql);
		$row = $result->fetch(PDO::FETCH_ASSOC);
		return (int)$row['UserId'];
	}

	public static function updatePassword($userid,$user_pass)
	{
		global $conn;
		$sql = "UPDATE ".self::$_tableName." SET password='".md5($user_pass)."' WHERE UserId='".$userid."'";
		$count = $conn->exec($sql);
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	public static function updateUser($name,$email,$city,$desc,$userid)
	{
		global $conn;
		if($userid > 0)
		{
			$sql = "UPDATE ".self::$_tableName." SET name='".$name."',city='".$city."',description='".$desc."' WHERE UserId={$userid}";
			$count = $conn->exec($sql);
		}
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	public static function updateFbId($email,$name, $fb_id)
	{
	  global $conn;
	  
	   $sql = "UPDATE ".self::$_tableName." SET fbId='".$fb_id."', name='".$name."', profileName='".$name."' WHERE emailAddress='".$email."'"; 
	   $count = $conn->exec($sql);
	   if($count) {
			return 1;
		} else {
			return 0;
		}
	}
	
	public static function updateDeviceToken($token, $userid)
	{
		global $conn;
		if($token!='')
		{
			$qry = "UPDATE ".self::$_tableName." SET deviceTokenId = '' WHERE deviceTokenId = '".$token."'";	
			$res =  $conn->exec($qry);

			$sql = "UPDATE ".self::$_tableName." SET deviceTokenId='".$token."' WHERE UserId={$userid}";
			$count = $conn->exec($sql);
		}
		if($count)
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
}
