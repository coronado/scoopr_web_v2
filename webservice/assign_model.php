<?php
set_time_limit(0);
error_reporting(E_ALL);
ini_set('display_errors','1');
require_once("appIncludes.php");
$response = array();
$userid = (int)$_REQUEST['userid'];
$secretekey = $_POST['secretekey'];

$assign_id = (int)$_REQUEST['assign_id'];
$buyer_id = (int)$_REQUEST['buyer_id'];
$model_id_0 = (int)$_REQUEST['model_id_0'];
$model_id_1 = (int)$_REQUEST['model_id_1'];
$model_id_2 = (int)$_REQUEST['model_id_2'];
$model_id_3 = (int)$_REQUEST['model_id_3'];

$uploaded_filename_0 = '';
$uploaded_filename_1 = '';
$uploaded_filename_2 = '';
$uploaded_filename_3 = '';

if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if($userid > 0 && $assign_id > 0)
{
	
	/*if(AssignModelTable::checkIfAlreadyAssign($assign_id,$userid))
	{
		echo json_encode(array("msg"=>"Already assigned"));die;
	}*/
	$upload_dir_path=SERVER_REAL_PATH."/images/assign_img_submission/";
	
	if(!file_exists($upload_dir_path))
	{
		mkdir($upload_dir_path);
		chmod($upload_dir_path,0777);
	}
		
	if($_FILES["upload_img_0"]["name"]!='')
	{		
		
		$ext = strtolower(end(explode('.',$_FILES["upload_img_0"]["name"])));
		$filename = rand(11111,99999).'_'.time().'.'.$ext;
		
		if($_FILES['upload_img_0']['size'] > 0)
		{
			if($_FILES['upload_img_0']['error'] == 0)
			{
				if(!file_exists($upload_dir_path.$filename))
				{
					$success=move_uploaded_file($_FILES['upload_img_0']['tmp_name'],$upload_dir_path.$filename);
					if($success) {
					$uploadMediumImg = AssignModelTable::uploadMediumImg("558","369",$upload_dir_path,$filename);
					$uploadSmallImg = AssignModelTable::uploadSmallImg($upload_dir_path,$filename);
					}
					
					
				}
				if($success)
				{
					$uploaded_filename_0 = $filename;
				}
				else
				{
					echo "Uploading failed";die;
				}
			}
			else
			{
				echo json_encode(array("msg"=>"Error in file"));die;
			}
		}
		else
		{	
			echo json_encode(array("msg"=>"Size error"));die;	
		}
	$success_0 = AssignModelTable::addModelToAssign($assign_id,$buyer_id,$model_id_0,$uploaded_filename_0,$userid);
	if($success_0) {
		$response['img'][] = "http://".SERVER_PATH."/images/assign_img_submission/small_size_image/".$uploaded_filename_0;
		$response['medium_img'][] = "http://".SERVER_PATH."/images/assign_img_submission/medium_size_image/".$uploaded_filename_0;
	}

	}


	if($_FILES["upload_img_1"]["name"]!='')
	{
		$ext = strtolower(end(explode('.',$_FILES["upload_img_1"]["name"])));
		$filename = rand(11111,99999).'_'.time().'.'.$ext;
		if($_FILES['upload_img_1']['size'] > 0)
		{
			if($_FILES['upload_img_1']['error'] == 0)
			{
				if(!file_exists($upload_dir_path.$filename)){
				 
					$success=move_uploaded_file($_FILES['upload_img_1']['tmp_name'],$upload_dir_path.$filename);
					
					if($success) {
					$uploadMediumImg = AssignModelTable::uploadMediumImg("558","369",$upload_dir_path,$filename);
					$uploadSmallImg = AssignModelTable::uploadSmallImg($upload_dir_path,$filename);
					}
					
				}
				if($success)
				{
					$uploaded_filename_1 = $filename;
				}
				else
				{
					echo "Uploading failed";die;
				}
			}
			else
			{
				echo json_encode(array("msg"=>"Error in file"));die;
			}
		}
		else
		{	
			echo json_encode(array("msg"=>"Size error"));die;	
		}
	$success_1 = AssignModelTable::addModelToAssign($assign_id,$buyer_id,$model_id_1,$uploaded_filename_1,$userid);
	if($success_1) {
		$response['img'][] = "http://".SERVER_PATH."/images/assign_img_submission/small_size_image/".$uploaded_filename_1;
		$response['medium_img'][] = "http://".SERVER_PATH."/images/assign_img_submission/medium_size_image/".$uploaded_filename_1;
	}
	}


	
	if($_FILES["upload_img_2"]["name"]!='')
	{
		$ext = strtolower(end(explode('.',$_FILES["upload_img_2"]["name"])));
		$filename = rand(11111,99999).'_'.time().'.'.$ext;
		if($_FILES['upload_img_2']['size'] > 0)
		{
			if($_FILES['upload_img_2']['error'] == 0)
			{
				if(!file_exists($upload_dir_path.$filename))
				{
					$success=move_uploaded_file($_FILES['upload_img_2']['tmp_name'],$upload_dir_path.$filename);
					if($success) {
					$uploadMediumImg = AssignModelTable::uploadMediumImg("558","369",$upload_dir_path,$filename);
					$uploadSmallImg = AssignModelTable::uploadSmallImg($upload_dir_path,$filename);
					}
				}
				if($success)
				{
					$uploaded_filename_2 = $filename;
				}
				else
				{
					echo "Uploading failed";die;
				}
			}
			else
			{
				echo json_encode(array("msg"=>"Error in file"));die;
			}
		}
		else
		{	
			echo json_encode(array("msg"=>"Size error"));die;	
		}
	$success_2 = AssignModelTable::addModelToAssign($assign_id,$buyer_id,$model_id_2,$uploaded_filename_2,$userid);
	if($success_2) {
		$response['img'][] = "http://".SERVER_PATH."/images/assign_img_submission/small_size_image/".$uploaded_filename_2;
		$response['medium_img'][] = "http://".SERVER_PATH."/images/assign_img_submission/medium_size_image/".$uploaded_filename_2;
	}
	}


	if($_FILES["upload_img_3"]["name"]!='')
	{
		$ext = strtolower(end(explode('.',$_FILES["upload_img_3"]["name"])));
		$filename = rand(11111,99999).'_'.time().'.'.$ext;
		if($_FILES['upload_img_3']['size'] > 0)
		{
			if($_FILES['upload_img_3']['error'] == 0)
			{
				if(!file_exists($upload_dir_path.$filename))
				{
					$success=move_uploaded_file($_FILES['upload_img_3']['tmp_name'],$upload_dir_path.$filename);
					if($success) {
					$uploadMediumImg = AssignModelTable::uploadMediumImg("558","369",$upload_dir_path,$filename);
					$uploadSmallImg = AssignModelTable::uploadSmallImg($upload_dir_path,$filename);
					}
				}
				if($success)
				{
					$uploaded_filename_3 = $filename;
				}
				else
				{
					echo "Uploading failed";die;
				}
			}
			else
			{
				echo json_encode(array("msg"=>"Error in file"));die;
			}
		}
		else
		{	
			echo json_encode(array("msg"=>"Size error"));die;	
		}
	$success_3 = AssignModelTable::addModelToAssign($assign_id,$buyer_id,$model_id_3,$uploaded_filename_3,$userid);
	if($success_3) {
		$response['img'][] = "http://".SERVER_PATH."/images/assign_img_submission/small_size_image/".$uploaded_filename_3;
		$response['medium_img'][] = "http://".SERVER_PATH."/images/assign_img_submission/medium_size_image/".$uploaded_filename_3;
	}
	}





	if($success_0 || $success_1 || $success_2 || $success_3)
	{

		$creatorEmail = AssignModelTable::getCreatorEmail($userid);

		$to      = $creatorEmail;
		$cc      = '';
		$bcc     = '';
		$from    = "info@scooprmedia.com"; 
		$subject = 'Thank you image submission';

		$message = "<tr>
					<td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;'>Hi There! Thank you for taking the time to shoot for this assignment.</td>
					</tr>
			<tr>
					<td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;'>We appreciate you submitting your image and wish you the best in being selected.</td>
					</tr>
					<tr>
					<td valign='top'>&nbsp;</td>
					</tr>";

		$email_obj = new email($to, $from, $subject, $cc, $bcc, $message);
		$send_email = $email_obj->send_email();


		$msg = "Assigned successfully";
	}
	else
	{	
		$msg = "Error occurred";
	}
}
else
{
    $msg = PARAMETR_MISSING;
}
$response['msg'] = $msg;
echo json_encode($response);
