<?php
require_once("appIncludes.php");
$response = array();
$secretekey = $_POST['secretekey'];
$userid = (int)$_REQUEST['userid'];
if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if($userid > 0 && !empty($secretekey))
{
	global $conn;
	CurrentLogin::removeLoginUser($userid);
	$msg="Successfully Logout";
}	
else
{
	$msg=PARAMETR_MISSING;
}
$response['msg'] = $msg;
echo json_encode($response);