<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
try
{
	$options = array(
		PDO::ATTR_PERSISTENT    => true,
		PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION
	);

	$conn = new PDO('mysql:host=localhost;dbname=Scoopr',"Scoopr",'Sc$oP@R',$options);
	if($conn){
		echo "PDO success";
	}
	else{
		echo "PDO failed";
	}
}
catch(PDOException $error){
    echo "<h1> PDO Connection error, because:</h1> : ".$error->getMessage();die;
}