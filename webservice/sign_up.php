<?php
//echo "aaaaa"; die;
error_reporting(E_ALL);
ini_set('display_errors','1');
require_once("appIncludes.php");
$is_success = false;
$response = array();
if(isset($_REQUEST['name']))
{
	$name = $_REQUEST['name'];
}
if(isset($_REQUEST['email']))
{
	$email = $_REQUEST['email'];
}
if(isset($_REQUEST['pwd']))
{
	$pwd = $_REQUEST['pwd'];
}
if(isset($_REQUEST['city']))
{
	$city = $_REQUEST['city'];
}
if(isset($_REQUEST['desc']))
{
	$desc = $_REQUEST['desc'];
}
if(isset($_REQUEST['fb_id']))
{
	$fb_id = (string)$_REQUEST['fb_id'];
}
else{
	$fb_id = "0";
}
$secretekey="";
if(isset($_REQUEST['secretekey'])){
	 $secretekey = $_REQUEST['secretekey']; 
}

if($secretekey!=ACCESS_TOKEN){ 
    echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if(!empty($email) && (!empty($pwd) || !empty($fb_id)))
{
	if(UserMasterTable::isEmailExist($email))
	{ 
	   if($fb_id!='') 
	   {
	       $updateFbId = UserMasterTable::updateFbId($email,$fb_id);
	   }
	   
	   $isFacebookUser = UserMasterTable::isFacebookUser($email,$fb_id);
	   if(empty($isFacebookUser['fb_id']))
	   {
		   $msg = "Email already registered";
		}
		else
		{
			 $msg = "File uploaded successfully";
		      $is_success = true;
			   $userid = $isFacebookUser['userid'];   
		}
	}
	else
	{
			$userType = 2;
			$userid = UserMasterTable::addUser($name,$email,$pwd,$city,$desc,$userType,$fb_id);
			if($userid > 0)
			{
				$is_success = true;
				$msg = "Signup successfully";
				if($_FILES["profile_img"]["name"]!='' && $userid > 0)
				{
					$upload_dir_path=SERVER_REAL_PATH."/images/profile_img/";
					if(!file_exists($upload_dir_path))
					{
						mkdir($upload_dir_path);
						chmod($upload_dir_path,0777);
					}
					$pngBase64 = base64_encode(file_get_contents($_FILES['profile_img']['tmp_name']));
					$ext = strtolower(end(explode('.',$_FILES["profile_img"]["name"])));
					$filename = sha1(substr($pngBase64, 0, 10).rand(11111, 99999)).'_'.$userid.'.'.$ext;
					if($_FILES['profile_img']['size'] > 0)
					{
						if($_FILES['profile_img']['error'] == 0)
						{
							if(!file_exists($upload_dir_path.$filename))
							{
							$success=move_uploaded_file($_FILES['profile_img']['tmp_name'],$upload_dir_path.$filename);
							if($success)
							{
							$imageUrl="http://".SERVER_PATH."/images/profile_img/".$filename;
							UserMasterTable::updateProfilePic($userid,$filename);
							}
							}
							else
							{
							$result = 'File with same name exists';
							}
							if($success)
							{	
							$result="File uploaded successfully";
							}
							else
							{
							$result="Error in uploading file";
							}
						}
						$msg = $result;
					}
					else
					{
						echo json_encode(array("msg"=>$result));die;
					}
				}
			}
			else
			{
				$msg = "Error occurred";
			}
		}
	
}
else
{
	$msg = PARAMETR_MISSING;
}
$response['msg'] = $msg;
if($is_success)
{
	$scoopr_uid=md5("scoopr-".$userid.'-'.ACCESS_TOKEN.'-'.rand(1,99999).'-'.time());
	$response['secretekey'] = (string)$scoopr_uid;
	CurrentLogin::addLoginUser($userid,$scoopr_uid);
	$response['userid'] = (string)$userid;
	$response['name'] = (string)$name;
	$response['email'] = (string)$email;
	$response['city'] = $city;
	$response['desc'] = (string)$desc;
	if(!empty($imageUrl))
	{
		$response['profile_img'] = $imageUrl;
	}

	$alert_setting = AlertSetting::getAlert($userid);
	if($alert_setting['value']==1)
	{
		$notify_msg = "off";
	}
	elseif($alert_setting['value']==0)
	{	
		$notify_msg = "on";
	}
	else
	{
		$notify_msg = "unknown";
	}
	$response['notify_msg'] = $notify_msg;
	$response["unread_notification"] = (string)Notification::getNotificationCount($userid,0);
	$response["model_count"] = (string)ModelReleaseTable::getModelCount($userid);
	$response["fav_assign_count"] = (string)FavouriteAssignTable::getFavAssignmentCount($userid);
	
}
echo json_encode($response);
