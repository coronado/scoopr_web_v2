<?php
#error_reporting(E_ALL);
#ini_set('display_errors','1');
require_once("appIncludes.php");
$response = array();
$userid = (int)$_REQUEST['userid'];
$secretekey = $_POST['secretekey'];
$img_desc = $_REQUEST['img_desc'];
$who_is_using = $_REQUEST['who_is_using'];
$still_in_use = $_REQUEST['still_in_use'];
$discover_date = $_REQUEST['discover_date'];
if(!CurrentLogin::authenticateUser($userid,$secretekey))
{
	echo json_encode(array("msg"=>ACCESS_DENIED));die;
}
if($userid>0 && !empty($img_desc) && !empty($who_is_using) && !empty($still_in_use) && !empty($discover_date))
{
	$id = CopyRight::reportCopyRight($img_desc,$who_is_using,$still_in_use,$discover_date);
	if($id > 0)
	{
		$msg = "Reported successfully";
	}
	else
	{	
		$msg = "Error occurred";
	}
}
else
{
    $msg = PARAMETR_MISSING;
}
$response['msg'] = $msg;
echo json_encode($response);