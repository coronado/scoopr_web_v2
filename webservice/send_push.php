<?php

$badge = 0;


if(isset($_REQUEST['token']))
{
	$token = $_REQUEST['token'];
}
else
{
	die("Token required");
}
// Put your private key's passphrase here:
$passphrase = '1234';

////////////////////////////////////////////////////////////////////////////////

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
//$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if(!$fp)
{
	return;
}
$freq = "10";
$message = "This is test Push";
// Create the payload body
$body['aps'] = array(
	'alert' => $message,
	'sound' => 'default',
	'name'=>"Vishal Srivastav",
	'count' => $freq,
	'badge' => $badge
	);

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen($payload)) . $payload;

#print $payload;
fwrite($fp, $msg);
fclose($fp); 
