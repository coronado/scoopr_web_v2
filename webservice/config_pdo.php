<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
if($_SERVER['REMOTE_ADDR'] != '127.0.0.1')
{
   //For other than local
   define("SERVER_PATH","scoopr.a1technology.asia");
   define("SERVER_HTTP_PATH","http://scoopr.a1technology.asia");
   define("SERVER_REAL_PATH","/home/sites/Scoopr");
   define("DB_USER","Scoopr");
   define("DB_PASS",'Sc$oP@R');

}
else{
   //For Local
   define("SERVER_PATH","scoopr.a1.com");
   define("SERVER_HTTP_PATH","http://scoopr.a1.com");
   define("SERVER_REAL_PATH","/var/www/html/scoopr");
   define("DB_USER","root");
   define("DB_PASS","welcome");
}
try
{
	$options = array(
		PDO::ATTR_PERSISTENT    => true,
		PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION
	);
	global $conn;
	$conn = new PDO('mysql:host=localhost;dbname=Scoopr',DB_USER,DB_PASS);
}
catch(PDOException $error){
    echo "<h1> PDO Connection error, because:</h1> : ".$error->getMessage();die;
}