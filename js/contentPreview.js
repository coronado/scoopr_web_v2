function addToFav(content_id)
{
	$.ajax({
	url: '/content/addToFav/content_id/'+content_id,
	type: 'GET',
	success: function(msg) {
		if(msg>0)
		{
			$("#addToFav").hide();
			$("#removeFromFav").show();
		}
	}

	});
}


function removeFromFav(content_id)
{
	$.ajax({
	url: '/content/removeFromFav/content_id/'+content_id,
	type: 'GET',
	success: function(msg) {
		if(msg==0)
		{
			$("#removeFromFav").hide();
			$("#addToFav").show();
		}
	}

	});
}


function payMethodRedirection(payment_method)
{
	if(payment_method == 2) 
	{
		var confirmMsg = "Are you sure you want to pay reward for this content?";
	}
		
	var confirmIt = confirm(confirmMsg); 
	if(confirmIt==true)
	{
		window.location.href = '/payment/rewards';
	}
}

function confirmPayment(payment_method)
{
	if(payment_method==1)
	{
		var confirmMsg = "Are you sure you want to proceed with paypal payment?";
	}
	else
	{
		var confirmMsg = "Are you sure you want to proceed with dollar payment & reward?";
	}

	return confirm(confirmMsg); 
}