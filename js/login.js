/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */


(function($,W,D)
{


$.validator.addMethod("nowhitespace", function(value, element) {
	return this.optional(element) || /^\S+$/i.test(value);
}, "No white space please");



    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
	setupFormValidation: function()
        {
            	//form validation rules
            	$("#frm").validate(
		{
			errorElement: 'div',
                	rules: 
			{
                    		email_address:
				{
                        		required: true,
					email: true
				},
				password: 
				{
                        		required: true
                        	}	
                	},
                	messages: 
			{
                		email_address:
				{
                        		required: "Please enter email address",
					email: "Invalid email address"
                        	},
				password: 
				{
                        		required: "Please enter your password"
                    		}
                	},
                	submitHandler: function(form) 
			{
                    		form.submit();
                	}
            	});

		
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($){

	$.validator.setDefaults({ ignore: '' });
        JQUERY4U.UTIL.setupFormValidation();

    });
})(jQuery, window, document);