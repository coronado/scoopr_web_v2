/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */


(function($,W,D)
{


$.validator.addMethod("nowhitespace", function(value, element) {
	return this.optional(element) || /^\S+$/i.test(value);
}, "No white space please");



    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
	setupFormValidation: function()
        {
            	//form validation rules
            	$("#contactFrm").validate(
		{
			errorElement: 'div',
                	rules: 
			{
                    		fullName: 
				{
                        		required: true
				},
				emailAddress:
				{
                        		required: true,
					email: true
				},
				subject:
				{
                        		required: true
				},
				message: 
				{
                        		required: true
                        	}	
                	},
                	messages: 
			{
                		fullName: 
				{
                        		required: "Please enter your name"
				},
				emailAddress:
				{
                        		required: "Please enter an email",
					email: "Invalid email address"
                        	},
				subject:
				{
                        		required: "Please enter subject"
				},
				message: 
				{
                        		required: "Please enter your message"
                    		}
                	},
                	submitHandler: function(form) 
			{
                    		 var url  = "info/contactus"; 
       				 var data = $("#contactFrm").serialize();
	
				 $.post(url, data, function(response){
				 	$("#contactMsg").html(response);
				 }); 
                	}
            	});

		
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($){

	$.validator.setDefaults({ ignore: '' });
        JQUERY4U.UTIL.setupFormValidation();

    });
})(jQuery, window, document);


/*$(document).ready(function(){
$("#send_message").click(function(){
	
       var url  = "info/contactus"; 
       var data = $("#contactFrm").serialize();
	
       $.post(url, data, function(response){
		//alert(response);
		$("#contactMsg").html(response);
       }); 
    });
 });*/