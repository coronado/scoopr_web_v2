/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */


(function($,W,D)
{


$.validator.addMethod("nowhitespace", function(value, element) {
	return this.optional(element) || /^\S+$/i.test(value);
}, "No white space please");



    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
	setupFormValidation: function()
        {
            	//form validation rules
            	$("#resetPwdFrm").validate(
		{
			errorElement: 'div',
                	rules: 
			{
                    		new_password: 
				{
                        		required: true,
					nowhitespace: true,
					minlength: 6
                    		},
				confirm_password:
				{
                        		equalTo: "#new_password"
				}	
                	},
                	messages: 
			{
                		new_password: 
				{
                        		required: "<br>Please enter a password",
					nowhitespace: "<br>White spaces are not allowed",
					minlength: "<br>Password should be at least 6 characters long."
                    		},
				confirm_password: 
				{
                        		equalTo: "<br>Password does not match."
                    		}
                	},
                	submitHandler: function(form) 
			{
                    		form.submit();
                	}
            	});

		
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($){

	$.validator.setDefaults({ ignore: '' });
        JQUERY4U.UTIL.setupFormValidation();

    });
})(jQuery, window, document);