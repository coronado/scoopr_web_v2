/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */


(function($,W,D)
{


$.validator.addMethod("nowhitespace", function(value, element) {
	return this.optional(element) || /^\S+$/i.test(value);
}, "No white space please");



    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
	setupFormValidation: function()
        {
            	//form validation rules
            	$("#changePwdFrm").validate(
		{
			errorElement: 'span',
                	rules: 
			{
                    		old_password: 
				{
                        		required: true,
					nowhitespace: true
                    		},
				new_password:
				{
                        		required: true,
					nowhitespace: true,
					minlength: 6
				},
				confirm_password: 
				{
                        		equalTo: "#new_password"
                        	}	
                	},
                	messages: 
			{
                		old_password: 
				{
                        		required: "Please enter your current password",
					nowhitespace: "White spaces are not allowed"
                    		},
				new_password:
				{
                        		required: "Please enter a new password",
					nowhitespace: "White spaces are not allowed",
					minlength: "Password should be at least 6 characters long."
                        	},
				confirm_password: 
				{
                        		equalTo: "Password does not match."
                    		}
                	},
                	submitHandler: function(form) 
			{
                    		form.submit();
                	}
            	});

		
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($){

	$.validator.setDefaults({ ignore: '' });
        JQUERY4U.UTIL.setupFormValidation();

    });
})(jQuery, window, document);