/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */



(function($,W,D)
{


$.validator.addMethod("minimumAmount", function(value, element) {
    return this.optional(element) || (parseFloat(value) >= 1);
}, "* Minimum Amount must be $250");

$.validator.addMethod("alphanumeric", function(value, element) {
	return this.optional(element) || /^\w+$/i.test(value);
}, "Letters, numbers, and underscores only please");

$.validator.addMethod("lettersonly", function(value, element) {
	return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please");

$.validator.addMethod("nowhitespace", function(value, element) {
	return this.optional(element) || /^\S+$/i.test(value);
}, "No white space please");

$.validator.addMethod("zipcodeUS", function(value, element) {
	return this.optional(element) || /\d{5}-\d{4}$|^\d{5}$/.test(value);
}, "The specified US ZIP Code is invalid");

$.validator.addMethod("integer", function(value, element) {
	return this.optional(element) || /^-?\d+$/.test(value);
}, "A positive or negative non-decimal number please");

$.validator.addMethod("maxWords", function(value, element, params) {
	return this.optional(element) || stripHtml(value).match(/\b\w+\b/g).length <= params;
}, jQuery.validator.format("Please enter {0} words or less."));

$.validator.addMethod("minWords", function(value, element, params) {
	return this.optional(element) || stripHtml(value).match(/\b\w+\b/g).length >= params;
}, jQuery.validator.format("Please enter at least {0} words."));

$.validator.addMethod("phoneUS", function(phone_number, element) {
	phone_number = phone_number.replace(/\s+/g, "");
	return this.optional(element) || phone_number.length > 9 &&
		phone_number.match(/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
}, "Please specify a valid phone number");

    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
	setupFormValidation: function()
        {
            	//form validation rules
            	$("#editUserFrm").validate(
		{
		  
		rules: 
		{ 
			
        		full_name: 
			{
            			required: true
			},
			telephone:
			{
                 		required: true
			},
			profile_name:
			{
                    		required: true  
			}					
            	},
        	messages: 
		{
			full_name: 
			{
                		required: "<br><span class='error_span'>Please enter your name</span>"
			},
            		telephone: 
			{
                		required: "<br><span class='error_span'>Please enter your telephone no.</span>"
            		},
            		profile_name: 
			{
                		required: "<br><span class='error_span'>Please enter your profile name</span>"
            		}
            	},
            	
                	submitHandler: function(form) 
			    {
            		form.submit();
            	}
    	});

		
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($){

	$.validator.setDefaults({ ignore: '' });
        JQUERY4U.UTIL.setupFormValidation();

    });
})(jQuery, window, document);
