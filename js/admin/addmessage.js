/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */



(function($,W,D)
{


$.validator.addMethod("maxWords", function(value, element, params) {
	return this.optional(element) || stripHtml(value).match(/\b\w+\b/g).length <= params;
}, jQuery.validator.format("Please enter {0} words or less."));

$.validator.addMethod("minWords", function(value, element, params) {
	return this.optional(element) || stripHtml(value).match(/\b\w+\b/g).length >= params;
}, jQuery.validator.format("Please enter at least {0} words."));


    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
	setupFormValidation: function()
        {
            	//form validation rules
            	$("#addMessageFrm").validate(
		{
		  
		rules: 
		{ 
			
        		UserId: 
			{
            			required: true
			},
        		subject:
        		{
        		    	required:true
			},
                	brandMessage:
			{
                    		required: true
			}					
            	},
        	messages: 
		{
			UserId: 
			{
                		required: "<br>Please select a brand",
			},
            		subject: 
			{
                		required: "<br>Please enter subject.",
			},
			brandMessage:
			{
                        	required: "<br>Please enter message.",
			}
            	},
            	
                	submitHandler: function(form) 
			    {
            		form.submit();
            	}
    	});

		
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($){

	$.validator.setDefaults({ ignore: '' });
        JQUERY4U.UTIL.setupFormValidation();

    });
})(jQuery, window, document);
