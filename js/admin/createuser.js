/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */



(function($,W,D)
{


$.validator.addMethod("minimumAmount", function(value, element) {
    return this.optional(element) || (parseFloat(value) >= 1);
}, "* Minimum Amount must be $250");

$.validator.addMethod("alphanumeric", function(value, element) {
	return this.optional(element) || /^\w+$/i.test(value);
}, "Letters, numbers, and underscores only please");

$.validator.addMethod("lettersonly", function(value, element) {
	return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please");

$.validator.addMethod("nowhitespace", function(value, element) {
	return this.optional(element) || /^\S+$/i.test(value);
}, "No white space please");

$.validator.addMethod("zipcodeUS", function(value, element) {
	return this.optional(element) || /\d{5}-\d{4}$|^\d{5}$/.test(value);
}, "The specified US ZIP Code is invalid");

$.validator.addMethod("integer", function(value, element) {
	return this.optional(element) || /^-?\d+$/.test(value);
}, "A positive or negative non-decimal number please");

$.validator.addMethod("maxWords", function(value, element, params) {
	return this.optional(element) || stripHtml(value).match(/\b\w+\b/g).length <= params;
}, jQuery.validator.format("Please enter {0} words or less."));

$.validator.addMethod("minWords", function(value, element, params) {
	return this.optional(element) || stripHtml(value).match(/\b\w+\b/g).length >= params;
}, jQuery.validator.format("Please enter at least {0} words."));

$.validator.addMethod("phoneUS", function(phone_number, element) {
	phone_number = phone_number.replace(/\s+/g, "");
	return this.optional(element) || phone_number.length > 9 &&
		phone_number.match(/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
}, "Please specify a valid phone number");

    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
	setupFormValidation: function()
        {
            	//form validation rules
            	$("#createUserFrm").validate(
		{
		  
		rules: 
		{ 
			
        		fname: 
			{
            			required: true,
				nowhitespace: true
        		},
        		lname:
        		{
        		    	required:true,
				nowhitespace: true
        		},
                	email_address:
			{
                    		required: true,
				email: true
			},
			password:
			{
                    		required: true,
				nowhitespace: true,
				minlength: 6
			},
			telephone:
			{
                 		required: true
			},
			profile_name:
			{
                    		required: true  
			}					
            	},
        	messages: 
		{
			fname: 
			{
                		required: "<br><span class='error_span'>Please enter first name</span>",
			        nowhitespace: "<br><span class='error_span'>White spaces are not allowed</span>"
            		},
            		lname: 
			{
                		required: "<br><span class='error_span'>Please enter last name</span>",
				nowhitespace: "<br><span class='error_span'>White spaces are not allowed</span>"
            		},
			email_address:
			{
                        	required: "<br><span class='error_span'>Please enter an email</span>",
				email: "<br><span class='error_span'>Invalid email address</span>"
                	},
                	password: 
			{
                		required: "<br><span class='error_span'>Please enter a password</span>",
				nowhitespace: "<br>White spaces are not allowed",
				minlength: "<br>Password should be at least 6 characters long."
            		},
            		telephone: 
			{
                		required: "<br><span class='error_span'>Please enter a telephone no.</span>"
            		},
            		profile_name: 
			{
                		required: "<br><span class='error_span'>Please enter company name</span>"
            		}
            	},
            	
                	submitHandler: function(form) 
			    {
            		form.submit();
            	}
    	});

		
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($){

	$.validator.setDefaults({ ignore: '' });
        JQUERY4U.UTIL.setupFormValidation();

    });
})(jQuery, window, document);
