/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */


(function($,W,D)
{

$.validator.addMethod("minimumAmount", function(value, element) {
    return this.optional(element) || (parseFloat(value) >= 1);
}, "* Minimum Amount must be $250");

$.validator.addMethod("alphanumeric", function(value, element) {
	return this.optional(element) || /^\w+$/i.test(value);
}, "Letters, numbers, and underscores only please");

$.validator.addMethod("lettersonly", function(value, element) {
	return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please");

$.validator.addMethod("nowhitespace", function(value, element) {
	return this.optional(element) || /^\S+$/i.test(value);
}, "No white space please");

$.validator.addMethod("zipcodeUS", function(value, element) {
	return this.optional(element) || /\d{5}-\d{4}$|^\d{5}$/.test(value);
}, "The specified US ZIP Code is invalid");

$.validator.addMethod("integer", function(value, element) {
	return this.optional(element) || /^-?\d+$/.test(value);
}, "A positive or negative non-decimal number please");

$.validator.addMethod("maxWords", function(value, element, params) {
	return this.optional(element) || stripHtml(value).match(/\b\w+\b/g).length <= params;
}, jQuery.validator.format("Please enter {0} words or less."));

$.validator.addMethod("minWords", function(value, element, params) {
	return this.optional(element) || stripHtml(value).match(/\b\w+\b/g).length >= params;
}, jQuery.validator.format("Please enter at least {0} words."));

$.validator.addMethod("phoneUS", function(phone_number, element) {
	phone_number = phone_number.replace(/\s+/g, "");
	return this.optional(element) || phone_number.length > 9 &&
		phone_number.match(/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
}, "Please specify a valid phone number");

    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
	setupFormValidation: function()
        {
            	//form validation rules
            	$("#requestDemoFrm").validate(
		{
			errorElement: 'span',
                	rules: 
			{
                    		first_name: 
				{
                        		required: true,
					nowhitespace : true
                    		},
				last_name:
				{
                        		required: true,
					nowhitespace : true
				},
				email_address: 
				{
                        		required: true,
					email   : true
                        	},
				telephone:
				{
                       			required: true
				},
				company_name:
				{
                       			required: true,
				},
				title_name:
				{
					required: true,
				}	
                	},
                	messages: 
			{
                		first_name: 
				{
                        		required: "<br><br>Please enter first name",
					nowhitespace: "<br><br>White spaces are not allowed"
                    		},
				last_name:
				{
                        		required: "<br><br>Please enter last name",
					nowhitespace: "<br><br>White spaces are not allowed"
				},
				email_address: 
				{
                        		required: "<br><br>Please enter an email.",
					email: "<br><br>Invalid email address"
                    		},
				telephone:
				{
                        		required: "<br><br>Please enter a telephone number"
				},
				company_name:
				{
                        		required: "<br><br>Please enter a company name"
				},
				title_name:
				{
                        		required: "<br><br>Please enter a title"	
				}
                	},
                	submitHandler: function(form) 
			{
                    		form.submit();
                	}
            	});

		
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($){

	$.validator.setDefaults({ ignore: '' });
        JQUERY4U.UTIL.setupFormValidation();

    });
})(jQuery, window, document);
