/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */


(function($,W,D)
{

$.validator.addMethod("minimumAmount", function(value, element) {
    return this.optional(element) || (parseFloat(value) >= 1);
}, "* Minimum Amount must be $250");

$.validator.addMethod("budgetValid", function(value, element) {
    var ourBudget = $("#our_budget").val();
    var rewards  = $("#rewards").val();	
    if(ourBudget != "" || rewards != "")		{
		return true;
	} else {
		return false;
	}
	
}, "* Please enter your budget or the reward.");


$.validator.addMethod("greaterOrEqualToMinAmt", function(value, element) {
    return this.optional(element) || (parseFloat(value) >= $("#budget_from").val());
}, "* Maximum Amount should be greater or equals to minimum amount");

$.validator.addMethod("needAnImage", function(value, element) {
	var totalNum = $('input:file[value!=""]').length;
	if(totalNum==0) { return false; } else { return true; }	
    //return this.optional(element) || (parseFloat(value) >= $("#budget_from").val());
}, "* Please upload at least an image");

$.validator.addMethod("validDate", function(value, element) {
	day = $('#day').val();
	month = $('#month').val();
	year  = $('#year').val();
	var submission_date =  new Date(year, month-1, day);

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if(dd<10){ dd='0'+dd; } if(mm<10) { mm='0'+mm; }
	var current_date = new Date(yyyy, mm-1, dd)	
	
	if(submission_date.getTime()<current_date.getTime()) {
		return false;
	} else { 
		return true;
	}
    //return this.optional(element) || (parseFloat(value) >= $("#budget_from").val());
}, "* Please upload at least an image");

    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
	setupFormValidation: function()
        {
            	//form validation rules
            	$("#assignmentFrm").validate(
		{
                	rules: 
			{
                    		looking_for_text: 
				{
                        		required: true
                    		},
				our_budget:
				{
                        		budgetValid: true,
					digits: true,
					minimumAmount: true
				},
				rewards: 
				{
                        		budgetValid: true
                        	},
				share_condition_1:
				{
					digits: true,
					minimumAmount: true
				},
				share_condition_2:
				{
					digits: true,
					minimumAmount: true
				},
				share_condition_3:
				{
					digits: true,
					minimumAmount: true
				},
				submission_deadline: 
				{
                        		required: true
                        	},
				category_id: 
				{
                        		required: true
                        	},
				reference_image_text:
				{
                       			required: true
				},
				'imagery_for[]':
				{
					required: true
				},
				'imagery_format[]':
				{
					required: true
				}	
                	},
                	messages: 
			{
                		looking_for_text: 
				{
                        		required: "<br/>Please Provide Description"
                    		},
				our_budget:
				{
                        		required: "<br/>Please Enter Budget",
                        		digits: "<br/> Please enter numeric value",
					minimumAmount: "<br/> Amount must be greater than 0"
				},
				rewards: 
				{
                        		budgetValid: "Please enter your budget or the reward."
                    		},
				share_condition_1:
				{
					digits: "<br/> Please enter numeric value",
					minimumAmount: "<br/> Amount must be greater than 0"
				},
				share_condition_2:
				{
					digits: "<br/> Please enter numeric value",
					minimumAmount: "<br/> Amount must be greater than 0"
				},
				share_condition_3:
				{
					digits: "<br/> Please enter numeric value",
					minimumAmount: "<br/> Amount must be greater than 0"
				},
				submission_deadline: 
				{
                        		required: "Please select a deadline"
                    		},
				category_id: 
				{
                        		required: "Please select a category"
                    		},
				reference_image_text:
				{
                        		required: "<br/>Please enter description"
				},
				'imagery_for[]':
				{
                        		required: "You must check at least an option"	
				},
				'imagery_format[]':
				{
                        		required: "Please check at least an option"	
				}
                	},
                	submitHandler: function(form) 
			{
                    		form.submit();
                	}
            	});

		if(editable=='edit'){
			$("[name='upload_image[]']").rules("remove");
		}
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($){




	$.validator.setDefaults({ ignore: '' });
        JQUERY4U.UTIL.setupFormValidation();

	$("#budget_from").blur(function() { 
		$("#budget_to").attr('value',$("#budget_from").val()); 
	 });

    });
})(jQuery, window, document);