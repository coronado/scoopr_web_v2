/**
  * Basic jQuery Validation Form Demo Code
  * Copyright Sam Deering 2012
  * Licence: http://www.jquery4u.com/license/
  */
(function($,W,D)
{
    var JQUERY4U = {};
    JQUERY4U.UTIL =
    {
	setupFormValidation: function()
        {
            	//form validation rules
            	$("#signUpFrm").validate(
		{
                	rules: 
			{
                    		email_address: 
				{
                        		required: true,
                        		email: true
                    		},
                    		password: 
				{
                        		required: true,
                        		minlength: 6
                    		},
				profile_name: 
				{
                        		required: true,
                        		minlength: 6
                    		},
				tag_line:
				{
                        		required: true,
                        		minlength: 15
				}
                	},
                	messages: 
			{
                		email_address: 
				{
                        		required: "<br/>Please enter email address",
                        		email: "<br/> Email address invalid"
                    		},
				password: 
				{
                        		required: "<br/>Please provide a password",
                        		minlength: "<br/>Your password must be at least 6 characters long"
                    		},
				profile_name: 
				{
                        		required: "<br/>Please provide a Company name",
                        		minlength: "<br/>Company name must be at least 6 characters long"
                    		},
				tag_line:
				{
					required: "<br/>Please provide a Tagline",
                        		minlength: "<br/>Tagline must be at least 15 characters long"
				}
                	},
                	submitHandler: function(form) 
			{
                    		form.submit();
                	}
            	});
            	
            	
// validation for request a demo page
            	
        $("#request_demo_frm").validate(
		{
                	rules: 
			{
                    		email_address: 
				{
                        		required: true,
                        		email: true
                    		}
        
                	},
                	messages: 
			{
                		email_address: 
				{
                        		required: "<br/>Please enter email address",
                        		email: "<br/> Email address invalid"
                    		}
				
                	},
                	submitHandler: function(form) 
			{
                    		form.submit();
                	}
            	});
        }
    }

    //when the dom has loaded setup form validation rules
    $(D).ready(function($){
        JQUERY4U.UTIL.setupFormValidation();
    });
})(jQuery, window, document);
