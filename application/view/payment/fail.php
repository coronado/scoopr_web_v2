<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Scoopr</title>
	<?php require_once("application/layout/headerContent.php"); ?>  
	</head>
	<body>

	<?php require_once("application/layout/header.php"); ?>  
        
<?php require_once("application/layout/dashboardSubHeader.php"); ?>  
	

	<section class="webaccount3">
       		<!--<div class="arrow_div">
                	<div class="inn_main_arrow">
                		<aside>&nbsp;</aside>
				<aside>&nbsp;</aside>
                          	<aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>
 	                </div>
		</div>-->

                 <div class="arrow_nav_div">
	 		<div class="innav_div">
		               <div class="btn_assignmen_div">&nbsp;</div>
 			</div>
		</div>
    	</section>

    

    <section class="webaccount5">

      <article class="inner_main_content">

<div class="view_submission">


<!-- ========================  brand info ========================   --> 
          <section class="section_brand">
                    	<article class="article_left">
                        	<aside class="red_ball_section"><img class="circleImg" src="images/profile_img/<?php echo $buyer_info['profileImg']; ?>" width="76" height="76" alt=""></aside>
                        	<aside class="content_section_vw_sub">
                            	<h1><?php echo $buyer_info['profileName']; ?></h1>
                                <h3><?php echo $buyer_info['tagLine']; ?></h3>
                            </aside>
                        </article>
	
			

                    	<p class="align_c"><img src="images/assignment_page/partition_line.png" alt="" width="1100" height="24" align="absmiddle"></p>
	  </section>      	
<!-- ======================== (END) ==============================   --> 


<!-- ========================  thank you message section ========================   --> 
                     <section class="section_list_brands">
			<aside class="box_list_view_sub" style="text-align:center; display:block; color:#FFFFFF; margin:0px auto;">
				<span><h3>Sorry,</h3></span>
				<h4>Your Payment has been failed.</h4>
				<h4>Please contact paypal support for more information.</h4>
			</aside>
                     </section>
<!-- ================================ (END) =============================   --> 



  </div>

      </article>

        </section>

    

<!--Body Ends Here-->



<?php require_once("application/layout/footer.php"); ?>  
<!--Footer Ends Here-->

<div style="display: none;" >

      

    </div>

<!--Popup SIgn In Ends Here-->

</body>

</html>