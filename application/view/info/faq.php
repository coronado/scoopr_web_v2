<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="">
<meta name="author" content="">
<base href="<?php echo PATH; ?>">
<title>Scoopr</title>
<!-- Fonts -->
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
<link rel="stylesheet" href="public/css/stylesheet.css" charset="utf-8" type="text/css">
<link rel="stylesheet" href="public/fonts/novecento/stylesheet.css">
<link rel="stylesheet" href="public/fonts/baronneue-bold/stylesheet.css">
<link rel="stylesheet" href="public/icons/glyphicons/style.css">
<link rel="stylesheet" href="public/icons/font-awesome/font-awesome.min.css">
<!-- Styles -->
<!--link rel="stylesheet" href="public/css/bootstrap-modal.css"-->
<link rel="stylesheet" href="public/css/bootstrap.css">
<link rel="stylesheet" href="public/css/style.css">
<link rel="stylesheet" href="public/css/scoopr.css">
<!-- Plugins -->
<link rel="stylesheet" href="public/plugins/royalslider/royalslider.min.css">
<link rel="stylesheet" href="public/plugins/owlcarousel/owl.carousel.min.css">
<link rel="stylesheet" href="public/plugins/mfp/jquery.mfp.css">
<!-- Additional styles -->
<style>
#culture {
	background-image: url(./public/img/culture-bg.jpg);
}
.style2 {color: #5ab7ce}
</style>

</head>
<body>
	<div class="popup_wrapper terms_popup">
        <div class="popup-header">
        	<!--<span class="button_close">X</span>-->
            <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10 info_block">
                <h1><span class="style2">Scoopr</span></h1>
                <p class="info_tagline">FAQ</p>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 text-right">
                <img src="public/img/scooper_icon.png" alt="" class="img-responsive">
            </div>
        </div>
        <div class="popup_content text-left">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p>
<?php echo $content; ?>
				   
				</p>
            </div>
        </div>
    </div>

</body>
</html>