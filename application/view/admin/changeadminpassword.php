<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Admin</title>
	<?php require_once("application/layout/headerContent.php"); ?>
	<link href="css/admin-style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo PATH;?>js/jquery.validate.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo PATH; ?>js/admin/changepassword.js"></script>
	<style>
		.error_span {
			margin: 0 0 0 223px;
		}
		.error{
			color:#FF0000;			
		}
.headingH1 {
text-transform: uppercase;
font-family: "helveticaneuelight", Helvetica, Arial, sans-serif;
color: #9e9d9d;
font-size: 20px;
font-weight: bold;
width: 100%;
margin: 2% 0px 2% 0px;
}
	</style>
	
	</head>
	<body>
<?php require_once("application/layout/adminHeader.php"); ?>
<?php require_once("application/layout/adminSubHeader.php"); ?>        
        		

    
   		<section class="webaccount9">
        		<article class="inner_main_content">
                	<article class="content_row_admin">
                    	<?php require_once("application/layout/adminLeft.php"); ?>        



                        <div class="adminright_main">
                        	<h1>SETTINGS</h1>
                            <div class="content_div_admin">
                            
				<table width="100%" border="0" cellspacing="0" cellpadding="0">

			

					<?php if($errMsg!='') { ?>
		<h1 style="font-size:15px;"><?php echo @$errMsg; ?></h1>
		<?php } ?>
                                  <tr>
                                    <td valign="top" class="table_head_borders">
                                    <form name="changePwdFrm" id="changePwdFrm" method="POST" action="" enctype="multipart/form-data">
                                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                         
                        <div>
                        	
                        
           

               
                            <div>
		<!--  ===========================  CHANGE PASSWORD  =========================  -->	
			<p>&nbsp;</p>	
			<h1 class="headingH1" style="color:#9e9d9d;">CHANGE PASSWORD</h1>
			<p>&nbsp;</p>
			    <p><label class="label">Old Password<font color="Red">*</font>:</label> <input class="input" name="old_password" id="old_password"  Placeholder="*******" type="password"></p>

			    <p><label class="label">New Password<font color="Red">*</font>:</label> <input class="input" name="new_password" id="new_password"  Placeholder="*******" type="password"></p>

			<p><label class="label">Confirm Password<font color="Red">*</font>:</label> <input class="input" name="confirm_password" id="confirm_password"  Placeholder="*******" type="password"></p>	
			<p>&nbsp;</p>		



		


			
                            </div>
	                           
	                         
	                        
	                         
	                            
	                          
	                            
	                           
	                            
	                         
	                         
	                        
                       	</div>
                         <div><input style="width:160px;" type="submit" name="change_password" id="change_password" value = "Change Password" class="next_button_diff"></div>
                        
                                        </table>
                                        </form> 
                                    </td>
                                  </tr>
                                </table>




                            </div>
                        </div>
                    </article>
                </article>
               
         </section>
         
<!--Body Ends Here-->

<?php require_once("application/layout/adminFooter.php"); ?>
<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>