	<?php
if($result['lookingFor']==1) {
	$looking_for = "Photography";
} elseif($result['looking_for']==2) {
	$looking_for = "Video";
} else {
	$looking_for = "Both Video & Photography";
}
//echo "<pre>"; print_r($imagery_for_array); die;
?>
	<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="">
	<meta name="author" content="">
			<base href="<?php echo PATH; ?>">

	<title>Scoopr</title>

	<!-- Fonts -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="public/admin/css/stylesheet.css" charset="utf-8" type="text/css">
	<link rel="stylesheet" href="public/admin/fonts/novecento/stylesheet.css">
	<link rel="stylesheet" href="public/admin/fonts/baronneue-bold/stylesheet.css">
	<link rel="stylesheet" href="public/admin/icons/glyphicons/style.css">
	<link rel="stylesheet" href="public/admin/icons/font-awesome/font-awesome.min.css">

	<!-- Styles -->
	<!--link rel="stylesheet" href="public/admin/css/bootstrap-modal.css"-->
	<link rel="stylesheet" href="public/admin/css/bootstrap.css">
	<link rel="stylesheet" href="public/admin/css/style.css">
	<link rel="stylesheet" href="public/admin/css/scoopr.css">
    <link rel="stylesheet" href="public/admin/css/style_admin.css">

	<!-- Plugins -->
	<link rel="stylesheet" href="public/admin/plugins/royalslider/royalslider.min.css">
	<link rel="stylesheet" href="public/admin/plugins/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="public/admin/plugins/mfp/jquery.mfp.css">
       <script type="text/javascript" src="public/admin/js/jquery-1.11.0.min.js"></script>
</head>
<body>

	
                          
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 popup_contanier">
                            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 popup_wraps view_assignemnt_popup">
                                    <div class="popup_header">
                                        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                                            Brand
                                        </div>
                                        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                                            Active
                                        </div>
                                        <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                                            Launched
                                        </div>
                                        <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                                            Budget
                                        </div>
                                        <div class="col-xs-8 col-sm-3 col-md-3 col-lg-3">
                                            # of Submisison
                                        </div>
                                    </div><!-- END .popup_header -->
                                    <div class="popup_content_main">
							
					<?php
				
					if($result['lookingFor']==1) {
					$looking_for = "Photography";
					} elseif($result['looking_for']==2) {
					$looking_for = "Video";
					} else {
					$looking_for = "Both Video & Photography";
					}
					//echo "<pre>"; print_r($imagery_for_array); die;
					?>

                                        <div class="single_line actualdata">
                                            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                                                <?php echo $result['profileName']; ?>
                                            </div>
                                            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                                                <ul class="yes_no_button">
                                                    <li class="yes"><a href="" class="active">Y</a></li>
                                                    <li class="no"><a href="">N</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                                                <?php if($result['isLaunched']==1) { echo "Yes"; } else { echo "No"; } ?><?php if($result['isLaunched']==1) { echo "(".$detail['launchedDate'].")";} ?>
                                            </div>
                                            <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                                                 <?php if($result['ourBudget']!='') { echo "$".$result['ourBudget']; } else { echo "NA"; } ?>
                                            </div>
                                            <div class="col-xs-8 col-sm-3 col-md-3 col-lg-3">
                                               <?php echo $result['submission_deadline']; ?> <span>(<?php echo $result['submissionDeadline']; ?>)
                                            </div>
                                        </div><!-- END .single_line -->
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p class="small_heading_popup">
                                                <strong>Full Brief</strong>
                                            </p>
                                        </div>
                                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                            <p class="fullcontent"><?php echo $result['lookingForText']; ?></p>
                                            <ul class="single_line_view main_image_thread">
                                                <li><i class=""><img src="public/admin/img/icon_3.png"></i>either orientation</li>
                                                <li><i class="fa fa-calendar-o"></i>deadline:<?php echo $deadline['submission_date']; ?> <span>(<?php echo $deadline['submission_deadline']; ?>)</li>
                                                <li><i class=""><img src="public/admin/img/check.png"></i>Yes, Modle Rlease</li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                            <ul class="image_mode">
                                                <li>
                                                    <img src="public/admin/img/selector_image.png">
                                                    <span class="text-left">7.3 KB</span>
                                                </li>
                                                <li>
                                                    <img src="public/admin/img/selector_image.png">
                                                    <span class="text-left">7.3 KB</span>
                                                </li>
                                                <li>
                                                    <img src="public/admin/img/selector_image.png">
                                                    <span class="text-left">7.3 KB</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p class="small_heading_popup">
                                                <strong>Usage</strong>
                                            </p>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <div class="full_width single_thread">
                                                <ul class="single_line_view">
                                                    <li><i class=""><img src="public/admin/img/icon_4.png"></i>multiple images for:</li>
                                                </ul>
                                                <div class="full_width">
                                                    <p class="percent50 details_product padding_left_first_prod">
						   <?php foreach($imagery_for_array as $key => $name) { ?>
                                                   <?php echo $name; ?></p>
					           <?php } ?>
						    
						    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 reward_story">
				<?php if($result['rewards']!='') { ?>
                                <p>Image Reward : $ <?php echo $result['rewards']; ?> </p> 
				<?php } ?>

                                            <p>Categorey: <?php echo $result['categoryName']; ?></p>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right edit_button">
                                           <a target="_parent" href="/adminassignment/designassignment/mode/edit/aid/<?php echo $assignment_id; ?>"><strong>Edit</strong></a>
                                        </div>
                                    </div><!-- END .popup_content_main -->
                                </div><!-- END .popup_wraps -->
                            </div><!-- END .popup_contanier -->
</body>
</html>