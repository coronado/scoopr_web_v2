<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Admin</title>
	<?php require_once("application/layout/headerContent.php"); ?>
	<link href="css/admin-style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo PATH;?>js/jquery.validate.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo PATH; ?>js/admin/addmessage.js"></script>
	<style>
		.error_span {
			margin: 0 0 0 223px;
		}
		.error{
			color:#FF0000;			
		}
.headingH1 {
text-transform: uppercase;
font-family: "helveticaneuelight", Helvetica, Arial, sans-serif;
color: #9e9d9d;
font-size: 20px;
font-weight: bold;
width: 100%;
margin: 2% 0px 2% 0px;
}
	</style>
	
	</head>
	<body>
<?php require_once("application/layout/adminHeader.php"); ?>
<?php require_once("application/layout/adminSubHeader.php"); ?>        
        		

    
   		<section class="webaccount9">
        		<article class="inner_main_content">
                	<article class="content_row_admin">
                    	<?php require_once("application/layout/adminLeft.php"); ?>        



                        <div class="adminright_main">
                        	<h1>Edit message</h1>
                            <div class="content_div_admin">
                            
				<table width="100%" border="0" cellspacing="0" cellpadding="0">

			

					<?php if($errMsg!='') { ?>
		<h1 style="font-size:15px;"><?php echo @$errMsg; ?></h1>
		<?php } ?>
                                  <tr>
                                    <td valign="top" class="table_head_borders">
                                    <form name="addMessageFrm" id="addMessageFrm" method="POST" action="" enctype="multipart/form-data">
                                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                         
                        <div>
                        	
                        
           

               			
                            <div class="step_content_block">
		<!--  ===========================  CHANGE PASSWORD  =========================  -->	
			
			<p>&nbsp;</p>

			<div class="checkbox_sec">
			<div class="assignment_checkbox_sec4">
                	<div class="styled-select" style="width:450px;">
                  		<select name="UserId" id="UserId" style="width:400px;">
					<option value="">Select a brand</option>
					<?php foreach($brands as $value) { ?>	
				<option value="<?php echo $value['UserId']; ?>" <?php if($details['UserId']==$value['UserId']) { echo "selected"; } ?> >
				<?php echo $value['profileName']." (".$value['emailAddress'].")"; ?>
				</option>
			<?php } ?>
				</select>
			</div>
			</div>
			</div>

		
			<div class="content_box_small_div">
			<h2>Subject:</h2>
			    <p class="budget_input clearfix dp"><input name="subject" id="subject"  Placeholder="Enter the subject." type="text" value="<?php echo $details['subject']; ?>"></p>
			</div>

			<div class="content_box_small_div">
			<h2>Message:</h2>
			    <textarea class="txtAreaContent" rows="4" style="width:98%;" name="brandMessage" id="brandMessage"><?php echo $details['brandMessage']; ?></textarea>	
			</div>


			<div class="checkbox_sec">
			<div class="assignment_checkbox_sec4">
                	<div class="styled-select" style="width:450px;">
                  		<select name="status" id="status" style="width:400px;">
					<option value="1">Select Status</option>
					<option value="1" <?php if($details['status']==1) { echo "selected"; } ?>>Enable</option>
					<option value="0" <?php if($details['status']==0) { echo "selected"; } ?>>Disable</option>
				</select>
			</div>
			</div>
			</div>
				
			<p>&nbsp;</p>		



		
 <div><input style="width:160px;" type="submit" name="edit_message" id="edit_message" value = "Edit Message" class="next_button_diff"></div>

			
                            </div>
	                           
	                         
	                        
	                         
	                            
	                          
	                            
	                           
	                            
	                         
	                         
	                        
                       	</div>
                        
                        
                                        </table>
                                        </form> 
                                    </td>
                                  </tr>
                                </table>




                            </div>
                        </div>
                    </article>
                </article>
               
         </section>
         
<!--Body Ends Here-->

<?php require_once("application/layout/adminFooter.php"); ?>
<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>