<?php 

$lastpage   = $paginator_arr['lastpage'];
$page       = $paginator_arr['page'];
$targetpage = "/admin/home";
$prev       = $paginator_arr['prev'];
$stages     = 1;
$LastPagem1 = $paginator_arr['LastPagem1'];
$next       = $paginator_arr['next'];

//$paginator_arr['paginated_result'] = array();
?>

<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Admin</title>
	 <base href="<?php echo PATH; ?>">
	<body>
        
 
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Fonts -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
	<link rel="stylesheet" href="public/admin/fonts/novecento/stylesheet.css">
	<link rel="stylesheet" href="public/admin/fonts/baronneue-bold/stylesheet.css">
	<link rel="stylesheet" href="public/admin/icons/glyphicons/style.css">
	<link rel="stylesheet" href="public/admin/icons/font-awesome/font-awesome.min.css">

	<!-- Styles -->
	<!--link rel="stylesheet" href="assets/css/bootstrap-modal.css"-->
	<link rel="stylesheet" href="public/admin/css/bootstrap.css">
	<link rel="stylesheet" href="public/admin/css/style.css">
	<link rel="stylesheet" href="public/admin/css/scoopr.css">
		
	<script type="text/javascript" src="public/admin//js/jquery-1.11.0.min.js"></script>	
        <script type="text/javascript" src="<?php echo PATH;?>js/jquery.validate.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo PATH; ?>js/admin/createuser.js"></script>
	<style>
		.error, .error_span{
			color:#FF0000;
			float: left;
			margin-top:-5px;
		}
		.
	</style>

</head>

<body class="dashboard_body">

<?php require_once("application/layout/adminHeader_new.php"); ?>
  
<?php require_once("application/layout/adminLeft_new.php"); ?>  
		<div class="main-wraps">
            
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-gutter">
				<header class="dashboard-header">
					<div class="col-xs-7 col-sm-3 col-md-3 col-lg-3 alpha search-box">
						<form>
							<div class="form-group">
								<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
							</div>
						</form>
					</div><!-- END .search-box -->
					<div class="col-xs-offset-7 col-sm-offset-9 col-md-offset-9 col-lg-offset-9">
						<div class="brand-name">
							<div class="seprat">
								<img src="assets/img/profile-pic.png">
								<label></label>
							</div>
						</div>
					</div>
				</header>
				<header class="account-name">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 acc-name">
						<p>Dahboard <span>/ Scoopr / Dashboard / Account / <a href="">Create</a></span></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 location-head text-right">
						
					</div>
				</header><!-- END .account-name -->
			</div>
			<div class="ratings_boxes">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="image_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>IMAGE VIEWS TODAY</p>
							<h1>100k+</h1>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>OPEN/CLICKS<span>7.80%</span></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>UNIQUE VIEWS<span>76.43%</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="submission_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>TOTAL SUBMISSIONS</p>
							<h1>6,954</h1>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details_rate">
							<p># OF ASSIGNMENTS<span>14</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="assignment_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>ASSIGNMENT VIEWS</p>
							<h1>950k+</h1>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details_rate">
							<p>OPEN/CLICKS<span>34.23%</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="reward_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>REWARDS TO DATE</p>
							<h1>$8,655</h1>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>LAST WEEK<span>$1,322</span></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>LAST MONTH<span>$4,121</span></p>
						</div>
					</div>
				</div>
			</div><!-- END .ratings_boxes -->
			<div class="datapanel_container">
				
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 data_panel">
					<div class="panel_header">Create Account</div><!-- END .panel_header -->
					<div class="panel_content">
                    	         <div class="bridge_form_container">
			 <form name="createUserFrm" id="createUserFrm" method="POST" action="" enctype="multipart/form-data">
                        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form_format">Account Info</div>
                        	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            	<input type="text"   name="email_address" id="email_address" class="form-control info_txtfeild" placeholder="Email Address">
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group has-success">
                                	<input type="password" class="form-control" name="password" id="inputSuccess1" placeholder="Password">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form_format">General Info</div>
                        	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            	<input type="text"  name="profile_name" class="form-control info_txtfeild" placeholder="Company Name">
                                <input type="text" name="tag_line" class="form-control info_txtfeild" placeholder="Brand Slogan">
                                <input type="text" name="founded" class="form-control info_txtfeild" placeholder="Founded">
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group has-warning">
                                    <input  name="website" type="text" class="form-control" id="inputWarning1" placeholder="Website">
                                </div>
                                <div class="input_replacer text-right"><input  name="profile_img"  Placeholder="Browse" type="file" style="height:auto !important;padding:0px;"> </div>
				
				
				
				
                                <select class="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>                
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            	<input type="text" name="profile_description" class="form-control info_txtfeild" placeholder="Brand Description">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form_format">General Info</div>
                        	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            	<input type="text" name="fname" class="form-control info_txtfeild" placeholder="First name">
                                <input type="text" name="address" class="form-control info_txtfeild" placeholder="Address">
                                <input type="text" name="telephone" class="form-control info_txtfeild" placeholder="Telephone">
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            	<input type="text"    name="lname" class="form-control info_txtfeild" placeholder="Last name">
                                <input type="text" name="city"  class="form-control info_txtfeild" placeholder="City/State">
                                <input type="text" class="form-control info_txtfeild" placeholder="Notes">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            	<input type="submit" name="save_profile" id="save_profile" class="btn btn-default" value="Submit"></input>
				
		
                            </div>
                        </div> <!-- END .bridge_form_container -->
					</div><!-- END .panel_content -->
				</div>
			</div><!-- END .datapanel_container -->

		</div><!-- END .main-wraps -->
</form> <!-- END .full-site-container -->

<!--Body Ends Here-->

<?php require_once("application/layout/adminFooter_new.php"); ?>
<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->




<script type="text/javascript">
	$(document).ready(function() {	
	    $('.accordionButton').click(function() {
	        $('.accordionButton').removeClass('on');
	        $('.accordionContent').slideUp('normal');
	        $('.plusMinus').text('+');
	        if($(this).next().is(':hidden') == true) {
	            $(this).addClass('on');
	            $(this).next().slideDown('normal');
	            $(this).children('.plusMinus').text('-');
	         } 
	     });
	    $('.accordionButton').mouseover(function() {
	        $(this).addClass('over');
	    }).mouseout(function() {
	        $(this).removeClass('over');
	    });
	    $('.accordionContent').hide();
	});
</script>
<script type="text/javascript">
    var leftHeight = $('.main-wraps').height();
    $('.side-section').css({'height':leftHeight});
</script>

<script type="text/javascript" src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.setup.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.scripts.min.js"></script>
<script type="text/javascript" src="public/admin//js/scoopr.plugins.js"></script>



</body>
</html>