<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Admin</title>
	<?php require_once("application/layout/headerContent.php"); ?>
	<link href="css/admin-style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo PATH;?>js/jquery.validate.min.js"></script>
	
	<style>
		.error_span {
			margin: 0 0 0 223px;
		}
		.error{
			color:#FF0000;			
		}
		.headingH1 {
		text-transform: uppercase;
		font-family: "helveticaneuelight", Helvetica, Arial, sans-serif;
		color: #9e9d9d;
		font-size: 20px;
		font-weight: bold;
		width: 100%;
		margin: 2% 0px 2% 0px;
		}
	</style>
<script type="text/javascript" src="<?php echo PATH;?>js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
    mode : "textareas",
    theme : "advanced",
    theme_advanced_buttons1 : "cut,copy,paste,pastetext,|,print,|,bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright, justifyfull,bullist,numlist,undo,redo,link,unlink,anchor,image,|,sub,sup,|,cleanup,help,code",
    theme_advanced_buttons2 :      "tablecontrols,insertdate,inserttime,preview,|,forecolor,backcolor,|,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons3 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "center",
    theme_advanced_statusbar_location : "bottom",
    plugins : 'print,inlinepopups,insertdatetime,table,contextmenu'
});
</script>
	
	</head>
	<body>
<?php require_once("application/layout/adminHeader.php"); ?>
<?php require_once("application/layout/adminSubHeader.php"); ?>        
        		

    
   		<section class="webaccount9">
        		<article class="inner_main_content">
                	<article class="content_row_admin">
                    	<?php require_once("application/layout/adminLeft.php"); ?>        



                        <div class="adminright_main">
                        	<h1>Update Content </h1>
                            <div class="content_div_admin">
                            
				<table width="100%" border="0" cellspacing="0" cellpadding="0">

				
                                  <tr>
                                    <td valign="top" class="table_head_borders">
                                    <form name="createUserFrm" id="createUserFrm" method="POST" action="" enctype="multipart/form-data">
                                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                         
                        <div>
                        	
                        
           

               
                            <div>
		<!--  ===========================  ACCOUNT INFORMATION   =========================  -->	
			<p>&nbsp;</p>	
			<h1 class="headingH1" style="color:#9e9d9d;"><?php echo urldecode($type); ?></h1>
			<p>&nbsp;</p>

			<?php if($errMsg!='') { ?>
					<p style="color:#FF0000;"><?php echo @$errMsg; ?></p>
				<?php } ?>

			    <p><!--<label class="label">Email Address<font color="Red">*</font>:</label> -->

			<textarea name="content" id="content" rows="15" cols="60"><?php echo $content; ?></textarea>
			</p>	

			  


                            </div>
	                           
	                         
	                        
	                         
	                            
	                          
	                            
	                           
	                            
	                         
	                         
	                        
                       	</div>
                         <div><input style="width:150px;" type="submit" name="update_content" id="update_content" value = "Update Content" class="next_button_diff"></div>
                        
                                        </table>
                                        </form> 
                                    </td>
                                  </tr>
                                </table>




                            </div>
                        </div>
                    </article>
                </article>
               
         </section>
         
<!--Body Ends Here-->

<?php require_once("application/layout/adminFooter.php"); ?>
<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>