<?php 
//echo "<pre>"; print_r($imagery_name); die;

//$details['hope_to_select']==2;
//echo "<pre>"; print_r($details); die;
$imagery_for_arr = unserialize($details['imageryFor']);
//echo "<pre>"; print_r($imagery_for_arr); die;
$year_month_day = explode('-',$details['submissionDate']);
list($year,$month,$day) = $year_month_day;
$mode_type = "create";
if($mode=='edit' && $this->getsession('assignment_id')!='')  {
	$mode_type = "edit";
	$days = $details['submissionDeadline'];
	$submissionDeadline = date('m/d/Y',(strtotime($details['launchedDate']." +$days day")));
}
$saved_imagery_format = unserialize($details['imageryFormat']);


 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="">
	<meta name="author" content="">
	<base href="<?php echo PATH; ?>">

	<title>Scoopr</title>

	<!-- Fonts -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
        <link rel="stylesheet" href="public/admin/css/stylesheet.css" charset="utf-8" type="text/css">
	<link rel="stylesheet" href="public/admin/fonts/novecento/stylesheet.css">
	<link rel="stylesheet" href="public/admin/fonts/baronneue-bold/stylesheet.css">
	<link rel="stylesheet" href="public/admin/icons/glyphicons/style.css">
	<link rel="stylesheet" href="public/admin/icons/font-awesome/font-awesome.min.css">
        
	<!-- Styles -->
	<!--link rel="stylesheet" href="assets/css/bootstrap-modal.css"-->
	<link rel="stylesheet" href="public/admin/css/bootstrap.css">
        <link rel="stylesheet" href="public/admin/css/style_admin.css">
	<link rel="stylesheet" href="public/admin/css/style.css">
	<link rel="stylesheet" href="public/admin/css/scoopr.css">
	<link href="public/admin/css/jquery.datepick.css" rel="stylesheet">

	<!-- Plugins -->
	<link rel="stylesheet" href="public/admin/plugins/royalslider/royalslider.min.css">
	<link rel="stylesheet" href="public/admin/plugins/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="public/admin/plugins/mfp/jquery.mfp.css">
        <script type="text/javascript" src="public/admin/js/jquery-1.11.0.min.js"></script>

<script>
   function chooseFile() {
      $("#fileInput").click();
   }
</script>

<script type="text/javascript">
editable = "<?php echo $mode_type; ?>";
	// Run the script on DOM ready:
	$(function(){
		//$('input').customInput();

		//initCanvas();
$('.upload_box').click(function(){ 
	var fileDiv = $(this);
	if(fileDiv != undefined){
      var fileInput = $("#upload_" + fileDiv.attr('id'));
      fileInput.change(function(e) {
	
         var files = this.files;
	 if(editable=='create')	{ $("#assignmentFrm").valid();	}
         showThumbnail(files, fileDiv);
      });

	$(fileInput).show().focus().click().hide();
   
   }
  });
		
	});
	</script>

<script type="text/javascript">
function showThumbnail(files, fileDiv) {
   for ( var i = 0; i < files.length; i++) {
      var file = files[i];
      var imageType = /image.*/;
      if (!file.type.match(imageType)) {
         alert("Not an Image");
         continue;
      }

      var image = document.createElement("img");
        
	image.width = '190';
	image.height = '150';
      // image.classList.add("")
      image.file = file;
      $(fileDiv).html(image);
      var reader = new FileReader();
      reader.onload = (function(aImg) {
         return function(e) {
            aImg.src = e.target.result;
         };
      }(image));
      var ret = reader.readAsDataURL(file);
      
   }
}
</script>

<script type="text/javascript" src="public/plugins/form/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/assignment.js"></script> 
<style>
.error{color:#a94442;}
</style>
</head>


<body class="dashboard_body">
<?php require_once("application/layout/adminHeader_new.php"); ?>		
<?php require_once("application/layout/adminLeft_new.php"); ?>  <!-- END .side-section -->
		<div class="main-wraps">
            <!--<div class="alert alert-danger">
            	<strong>Oh snap!</strong> Change a few things up and try submitting again.
            </div>-->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-gutter">
					<?php require_once("application/layout/v2/top-search.php"); ?>
				<header class="account-name">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 acc-name">
						<p>Dahboard <span>/ Scoopr / Dashboard / Account / <a href="">Create</a></span></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 location-head text-right">
						
					</div>
				</header><!-- END .account-name -->
			</div>
			<div class="ratings_boxes">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="image_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>IMAGE VIEWS TODAY</p>
							<h1>100k+</h1>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>OPEN/CLICKS<span>7.80%</span></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>UNIQUE VIEWS<span>76.43%</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="submission_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>TOTAL SUBMISSIONS</p>
							<h1>6,954</h1>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details_rate">
							<p># OF ASSIGNMENTS<span>14</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="assignment_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>ASSIGNMENT VIEWS</p>
							<h1>950k+</h1>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details_rate">
							<p>OPEN/CLICKS<span>34.23%</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="reward_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>REWARDS TO DATE</p>
							<h1>$8,655</h1>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>LAST WEEK<span>$1,322</span></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>LAST MONTH<span>$4,121</span></p>
						</div>
					</div>
				</div>
			</div><!-- END .ratings_boxes -->
		        <form action="" name="assignmentFrm" id="assignmentFrm" method="POST" enctype="multipart/form-data">
			<div class="datapanel_container">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 data_panel">
					<div class="panel_header">Create Assignment</div><!-- END .panel_header -->
					<div class="panel_content bridge_cr_assignment">
                    	        <div class="row_full2">
                        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-gutter">
                            	<p class="main_heading"><strong>Assignment Info</strong></p>
				
				<p>Describe what you need to your community of image creators.</p>
				
				<!--  ================  BRAND SELECT BOX =============== -->
		                <?php if($mode!='edit') { ?>
                                <select  name="buyer_id" id="buyer_id" class="form-control">
                                <option value="">Select a brand</option>
		    	        <?php foreach($brands as $value) { ?>	
				<option value="<?php echo $value['UserId']; ?>" <?php if($uid==$value['UserId']) { echo "selected"; } ?> >
				<?php echo $value['profileName']." (".$value['emailAddress'].")"; ?>
				</option>
			        <?php } ?>
                                </select>
				<label for="buyer_id" generated="true" class="error"></label>
				 <?php } ?>	
	                        <!--  ================  (END) =============== -->	
                                <p>Image Brief</p>
                                <textarea name="looking_for_text" id="looking_for_text" placeholder="Describe exactly what your looking for. Feel free to speak naturally about all of the aspects you want in the imagery. Try to specify the subject matter, mood, color, candid or staged, etc. If you need room for text/copy, be sure to mention that as well. Remember, the more detailed you are with your vision, the better chances of receiving submissions cloest to the perfect shot."></textarea>
                            </div>
                            <div class="row_full">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 main_bridgel">
                                    <p>Image Format</p>
                                  	<div class="buttons_bridge text-center">
                                    	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 no-gutter">
                                    		
						<span class="icon1 float_left testStyle selectedHover" style="padding-top:10px; " >Horizontal</span>
						<input type="hidden" class="test_check" value="1" id="male9"  name="imagery_format[]" <?php if(in_array('1',$saved_imagery_format)) { echo "checked"; } ?>/>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 no-gutter">
                                    		
						<span class="icon2  testStyle selectedHover" style="padding-top:10px; " >Vertical</span>
						<input type="hidden" class="test_check" value="2"  id="female9"  name="imagery_format[]"  <?php if(in_array('2',$saved_imagery_format)) { echo "checked"; } ?>/>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 no-gutter">
                                   			
							<span class="icon3 float_right testStyle selectedHover" style="padding-top:10px; " >Either</span>
						        <input type="hidden" class="test_check" value="2">
                                        </div>
                                    </div><!-- END .buttons_bridge -->
                                    <p>Reward per selected image</p>
				    <input type="text" name="rewards" class="form-control info_txtfeild red_border" id="rewards" placeholder="Can be anything from cash to trips or product" value="<?php echo $details['rewards']; ?>">
				  
                                </div><!-- END .main_bridgel -->
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 main_bridger">
                                    <p>Deadline &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Category</p>
                                    <div class="buttons_bridge">
                                    	<div  class="col-xs-6 col-sm-5 col-md-5 col-lg-5 no-gutter">
							
						<span  class="float_left testStyle" ><input  name="submission_deadline" class="cal"  style="width:130px; "   id="inlineDatepicker" type="text"  placeholder="MM/DD/YYYY" value="<?php echo $submissionDeadline; ?>"/></span>
						
						<div id="inlineDatepicker" class="datepick-popup"></div>
						
						
   
   
						
						
						
					</div>
                                        <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 no-gutter">
	                                    	<span>(5days)</span>
                                        </div>
					
                                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 no-gutter">
                                            
                                                <select name="category_id" id="category_id" class="form-control">
						<option value="">Category</option>
						<?php for($i=0; $i<count($categories); $i++) { ?>	
						<option value="<?php echo $categories[$i]['AssignmentCategoryId']; ?>" <?php if($details['AssignmentCategoryId']==$categories[$i]['AssignmentCategoryId']) { echo "selected"; }?>><?php echo $categories[$i]['categoryName']; ?></option>
						<?php } ?>	
						
						</select>
                                        </div>
                                    </div><!-- END .buttons_bridge -->
                                </div><!-- END .main_bridger -->
                            </div><!-- END .row_full -->
                            <div class="row_full">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 main_bridgel">
                                    <p>Refrence Images</p>
                                    <div class="box_upload_imge">
                                    	<div class="upload_box_main ">
                                            <div class="image_upload">
                                                <div class="upload_box_bridge upload_box_bridge" >
						<div class="upload_box" id="image1">
						<img height="130" width="200" src="<?php if($details['image1']=='')
						{ echo "public/admin/img/upload_image_process.png"; } else { echo "images/reference_img/".$details['image1']; } ?>" /></div>
						
						
                                                    <div class="image_container red_line">
                                                      
						       <input class="fileupload " type="file" name="upload_image[]" id="upload_image1" style="display: none;">                                            </div><!-- END .image_container -->
                                                    <span class="file_size">7.3 KB</span>                                               
                                            	 </div><!-- END .upload_box_bridge -->
                                            </div><!-- END .image_upload -->
                                          
					    <div class="image_upload">
                                                <div class="upload_box_bridge upload_box_bridge" >
						<div class="upload_box" id="image2">
						<img height="130" width="200" src="<?php if($details['image2']=='')
						{ echo "public/admin/img/upload_image_process.png"; } else { echo "images/reference_img/".$details['image2']; } ?>" /></div>
						
						
                                                    <div class="image_container green_line">
                                                      
						       <input class="fileupload " type="file" name="upload_image[]" id="upload_image2" style="display: none;">                                            </div><!-- END .image_container -->
                                                    <span class="file_size">7.3 KB</span>                                               
                                            	 </div><!-- END .upload_box_bridge -->
                                            </div><!-- END .image_upload -->
                                          
					   <div class="image_upload">
                                                <div class="upload_box_bridge upload_box_bridge" >
						<div class="upload_box" id="image3">
						<img height="130" width="200" src="<?php if($details['image3']=='')
						{ echo "public/admin/img/upload_image_process.png"; } else { echo "images/reference_img/".$details['image3']; } ?>" /></div>
						
						
                                                    <div class="image_container green_line">
                                                      
						       <input class="fileupload " type="file" name="upload_image[]" id="upload_image3" style="display: none;">                                            </div><!-- END .image_container -->
                                                    <span class="file_size">7.3 KB</span>                                               
                                            	 </div><!-- END .upload_box_bridge -->
                                            </div><!-- END .image_upload -->
                                         </div><!-- END .upload_box_main -->
                                    </div><!-- END .box_upload_imge -->
                                </div><!-- END .main_bridgel -->
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 main_bridger">
                                	<p>&nbsp;</p>
                                	<textarea class="explanation" name="reference_image_text"  id="reference_image_text" placeholder="Explain what interest you about the reference images. Be as specific as possible (mood, color, pattern, etc.)"><?php echo $details['referenceImageText']; ?></textarea>
                                </div><!-- END .main_bridger -->
                            </div><!-- END .row_full -->
                            <div class="row_full">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 main_bridgel">
                                    <p class="main_heading"><strong>Usage Info</strong></p>
                                    <p>How many images do you hope to use?</p>
                                    <div class="buttons_bridge">
	                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 no-gutter">
						<span class=" testStyle selectedHover" >1</span>
						 <input type="hidden" id="single" name="hope_to_select" value="1" <?php if(@$details['hopeToSelect']!=2) { echo "checked"; } ?> class="test_check"/>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 no-gutter">
        	                           
						<span class=" icon4 testStyle selectedHover" style="padding-top:10px; " >multiple</span>
						 <input type="hidden" id="multiple" name="hope_to_select" value="2" <?php if($details['hopeToSelect']==2) { echo "checked"; } ?>  class="test_check"/>
                                        </div>
                                    </div><!-- END .buttons_bridge -->
                                    <p>Do you need a model release?</p>
                                    <div class="buttons_bridge">
                                    	<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 no-gutter">
                                        	
						<span class=" float_left testStyle selectedHover" >Yes, we do</span>
						<input type="hidden" class="test_check"  id="modelyes" name="model_release" value="male" <?php if(@$details['modelRelease']!='no') { echo "checked"; } ?>/>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 no-gutter">
                                    		
						<span class="testStyle selectedHover" >No, we don't</span>
						 <input type="hidden" class="test_check" id="modelno" name="model_release" value="modelno"  <?php if($details['modelRelease']=='no') { echo "checked"; } ?>/>
                                        </div>
                                    </div><!-- END .buttons_bridge -->
                                    <p class="main_padding_bridge">Budget for each image</p>
                                    <div class="trigger_input">
                                    	<span>$USD</span>
					
                                        <input type="text" name="share_condition_1" class="form-control info_txtfeild" value="<?php echo $details['shareCondition1']; ?>" placeholder="ex. 2,000">
                                    
				    <input type="hidden" name="share_condition_2" id="share_condition_2" value="<?php echo $details['shareCondition2']; ?>">
				    <input type="hidden" name="share_condition_3" id="share_condition_3" value="<?php echo $details['shareCondition3']; ?>">
				    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 main_bridger text-center">
                                	<p class="main_heading"><strong>&nbsp;</strong></p>
                                    <p>We’ll be using this for:  <span>(select all that apply) </span></p>
                              <div class="buttons_bridge list-one">
				<?php    $k = 4;						   
				for($i=0; $i<count($imagery_name); $i++) { 
				$j = $i+1;
				$k++;
				$checkIds = ($i+1); ?>  
				<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 no-gutter">
				<span class="float_left  testStyle selectedHover" ><?php echo $imagery_name[$i]['imageryForName']; ?></span>
				
				<input type="hidden" name="imagery_for[]" id="<?php echo $checkIds; ?>" value="<?php echo $imagery_name[$i]['ImageryForid']; ?>" <?php if(in_array($imagery_name[$i]['ImageryForid'],$imagery_for_arr)) { echo "checked"; } ?>/>	
				<label for="imagery_for[]" generated="true" class="error"><?php echo @$errors['imagery_for']; ?></label>		
				
				</div>
				<?php if($j%4==0) { 
				
				} ?>
				<?php } ?>
				<textarea name="imagery_for_text" id="imagery_for_text" placeholder="Please describe your final use for the licensed photo. If you are unsure, please describe a general use."><?php echo $details['imageryForText']; ?></textarea>
                                </div><!-- END .main_bridger -->
                               
                            </div><!-- END .row_full -->
			     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 submit_bridge">
				
				<input type="submit" name="save_now" id="save_now" value="Submit" >
                                	
                                </div>
                        </div><!-- END .row_full -->
					</div><!-- END .panel_content -->
				</div><!-- END .col-xs-12 .col-sm-12 .col-md-12 .col-lg-12 .data_panel -->
			</div></form><!-- END .datapanel_container -->
		</div><!-- END .main-wraps -->
</section><!-- END .full-site-container -->
<?php require_once("application/layout/adminFooter_new.php"); ?> 


<script type="text/javascript">
$(function(){
	$.fn.fancyRadio = function(){
		return $(this).each(function(){
			var p = $(this),
				container = $('<span class="radio-container"/>'),
				radio = $('<span class="radio"/>');
			p.find('input[type="radio"]').wrap(container);
			p.find('span.radio-container').append(radio);
			p.find('input:checked').parent()
									.find('span.radio').addClass('selected');
			p.find('input[type="radio"]').on('click',function(){
				p.find('span.selected').removeClass('selected');
				$(this).parent().find('span.radio').addClass('selected');
			});
		});
	};
	$('p.radio').fancyRadio();
	 var leftHeight = $('.main-wraps').height();
    $('.side-section').css({'height':leftHeight});
});
</script>

<script src="public/js/jquery.plugin.js"></script>
<script src="public/js/jquery.datepick.js"></script>

<script>
$(function() {
	$('#popupDatepicker').datepick();
	$('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	//alert('The date chosen is ' + date);
}
</script>


<script>
$(".checkbox").click(function(){
    $(this).toggleClass('checked')
});
</script>
<script type="text/javascript">
	$(document).ready(function() {	
	    $('.accordionButton').click(function() {
	        $('.accordionButton').removeClass('on');
	        $('.accordionContent').slideUp('normal');
	        $('.plusMinus').text('+');
	        if($(this).next().is(':hidden') == true) {
	            $(this).addClass('on');
	            $(this).next().slideDown('normal');
	            $(this).children('.plusMinus').text('-');
	         } 
	     });
	    $('.accordionButton').mouseover(function() {
	        $(this).addClass('over');
	    }).mouseout(function() {
	        $(this).removeClass('over');
	    });
	    $('.accordionContent').hide();
	});
</script>
<script type="text/javascript">
    var leftHeight = $('.main-wraps').height();
    $('.side-section').css({'height':leftHeight});

</script>
<script type="text/javascript">
$( ".selectedHover" ).click(function() {
  $( this ).toggleClass( "test11" );
});	
</script>

<script type="text/javascript" src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.setup.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.scripts.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.plugins.js"></script>




</body>
</html>