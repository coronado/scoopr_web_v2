<?php
$this->include_file('admin_class.php','application/lib');
$obj = new adminlib;

$creator_name = $obj->getCreatorName($details['userid']);
$images = $obj->getImagesByModelId($details['id']);


//print_r($images); 
//echo "<pre>"; print_r($details); die;
?>
<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Scoopr</title>
	<?php require_once("application/layout/headerContent.php"); ?>
	<link rel="stylesheet" href="css/colorbox.css" />
	<script src="js/jquery.colorbox.js"></script>
	<script>
	$(document).ready(function(){
	$(".details").colorbox({
				
				innerWidth:'500px',
				innerHeight:'200px'
				});

	});
	</script>
	</head>
	<body>
<?php require_once("application/layout/adminHeader.php"); ?>
<?php require_once("application/layout/adminSubHeader.php"); ?>   




    
   		<section class="webaccount9">
			<article class="inner_main_content">
        		<article class="content_row_admin">
                    	<?php require_once("application/layout/adminLeft.php"); ?>       

		 <div class="adminright_main">
                <section>
                	<article class="content_box_mid">
                    		<div class="main_content_left">
                            	<h1>Model Detail</h1>
                            </div>
                          
                            <div class="content_flow">
                            	<h2>Model shot for :</h2>
                             	<p><?php echo $details['modelShotFor']; ?></p>
                                
                                <h2>Model notes:</h2>
                                <p><?php echo $details['modelNotes']; ?></p>  
                                



				<h2><strong>Model Logo:</strong></h2>
          			<p><img src="images/model_logo/<?php echo $details['modelLogo']; ?>" height="60" width="60"></p>
	  			

                                
                                <h2>Model PDF:</h2>
				<p><a class="admin_links" target="_blank" href="images/model_logo/<?php echo $details['modelPdf']; ?>">PDF</a></p> 

				<h2>Creator:</h2>
                                <p><a class="details" href ="/adminsubmission/creatorDetails/creator_id/<?php echo $details['UserId']; ?>"><?php echo $creator_name; ?></a></p> 
                                
                                
                                

				
                

                <h3>Submission images</h3>

		<?php foreach($images as $image) { ?>
                <div class="div100percent">
          	<div class="div30percent"><img src="images/assign_img_submission/medium_size_image/<?php echo $image['imageName']; ?>" width="226" height="166" alt="" border="0"></div>
          	</div>
		<p>&nbsp;</p>
		<?php } ?>



                                         
                                           
         
	<!--<div class="content_box_small_div">
         <div class="btn_pink"><a href="/adminassignment/designassignment/mode/edit/aid/<?php echo $assignment_id; ?>"><strong>Edit</strong></a></div>
        </div>-->

       
                    </article>
                </section></div></article></article>



        </section>
         
    
<!--Body Ends Here-->
<script type="text/javascript" src="js/jquerypp.custom.js"></script> 
<script type="text/javascript" src="js/jquery.elastislide.js"></script> 
<script type="text/javascript">
	//$( '#carousel' ).elastislide();
</script>

<?php require_once("application/layout/footer.php"); ?>
<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>