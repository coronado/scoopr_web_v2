<?php 
$lastpage   = $paginator_arr['lastpage'];
$page       = $paginator_arr['page'];
$targetpage = "/adminassignment/disabledlist";
$prev       = $paginator_arr['prev'];
$stages     = 1;
$LastPagem1 = $paginator_arr['LastPagem1'];
$next       = $paginator_arr['next'];
$num_rows   = count($paginator_arr['paginated_result']);

// search and pagination link variables
$extraLink = "";
$uid = $search_arr['uid'];
if($uid!='') {
	$extraLink = "uid/".$uid;
}
?>
<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Admin</title>
	<?php require_once("application/layout/headerContent.php"); ?>
	</head>
	<body>
<?php require_once("application/layout/adminHeader.php"); ?>
<?php require_once("application/layout/adminSubHeader.php"); ?>        
        		

    
   		<section class="webaccount9">
        		<article class="inner_main_content">
                	<article class="content_row_admin">
                    	<?php require_once("application/layout/adminLeft.php"); ?>        



                        <div class="adminright_main">
                        	<h1>Disabled Assignments (<?php echo $paginator_arr['total_records']; ?>)</h1>

<form name="searchFrm" id="searchFrm" method="POST" action="">
	
	<input style="width:140px;" class="input" name="assignment_id" id="assignment_id"  type="text" placeholder="assignment id">
	<input style="width:120px;" class="input" name="email_address" id="email_address"  type="text" placeholder="email">&nbsp;
	<input type="submit" name="search" id="search" value="Search" class="next_button_diff">
	</form>					
                        	
                            <div class="content_div_admin">
 		<?php if($errMsg!='') { ?>
		<h1 style="font-size:15px;"><?php echo @urldecode($errMsg); ?></h1>
		<?php } ?>
                     
                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">

					<?php if($num_rows==0) { ?>
					<tr>
					   <td align="left" colspan="6"><h1>Sorry! No records available.</h1></td>	
					</tr>
					<?php }  else { ?>

                                  <tr>
                                    <td valign="top" class="table_head_borders">
                                    
                                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    	                        	
        					
                                          <tr>
                                            <td align="left" class="blue_headings" width="13%">Brand</td>
                                            <td align="left" class="blue_headings" width="21%">Looking For</td>
                                            <td align="left" class="blue_headings" width="14%">Budget</td>
					    <td align="left" class="blue_headings" width="16%">Model release</td>
					    <td align="left" class="blue_headings" width="17%">Launched</td>		
                                            <td align="center" class="blue_headings"  width="20%">Actions</td>
                                          </tr>



                                        </table>
                                    </td>
                                  </tr>
				<?php } ?>

                                  <?php 
					$i= 1;
                                    	foreach ($paginator_arr['paginated_result'] as $detail) 
                                    	{
						$class = "table_head_2bg";	
                                    		if($i%2 == 0) {
                                    			$class = "table_head_2bg bg_r2";
                                    		}

						if($detail['ourBudget']!='' && $detail['rewards']!='') {
							$budgetAndReward = "$ ".$detail['ourBudget']." (".$detail['rewards'].")";
						} elseif($detail['ourBudget']!='' && $detail['rewards']=='') {
							$budgetAndReward = "$ ".$detail['ourBudget'];
						} else {
							$budgetAndReward = $detail['rewards'];
						}
                                    		
                                    		?>
                                  <tr>
                                    <td valign="top" class="<?php echo $class ?>">
                                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    	
                                          <tr>
                                            <td  width="13%" class="head_bold"><a class="admin_links" href="/adminsubmission/index/aid/<?php echo $detail['AssignmentId']; ?>/uid/<?php echo $detail['UserId']; ?>"><?php echo $detail['profileName']; ?></a></td>


                                            <td  width="21%" class="head_normal"><?php echo substr($detail['lookingForText'],0,20); ?>..</td>
                                            <td width="14%" class="head_normal"><?php if($detail['ourBudget']!='') { echo "$".$detail['ourBudget']; } else { echo "NA"; } ?></td>
					    <td width="16%" class="head_normal"><?php echo $detail['modelRelease']; ?></td>	
					     <td width="17%" class="head_normal"><?php if($detail['isLaunched']==1) { echo "Yes"; } else { echo "No"; } ?>  
						<?php if($detail['isLaunched']==1) { echo "(".$detail['launchedDate'].")";} ?>
						</td>		

                                            <td  width="20%">
                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                  <tr>
                                                    <td ><a title="View details" href="/adminassignment/assignmentDetail/aid/<?php echo $detail['AssignmentId']; ?>/uid/<?php echo $detail['UserId']; ?>"><img src="images/admin_panel_web/icon_panel_1.png"  alt=""></a></td>
                                                    <td width="1%"></td>

                                                    <td><a title="Enable" href="/adminassignment/enableit/aid/<?php echo $detail['AssignmentId']; ?>" onclick="return confirm('Are you sure you want to enable this assignment?');"><img src="images/admin_panel_web/enable_icon.png"   alt=""></a></td>
                                                    <td  width="1%"></td>

                                                    <td ><a title="Delete" href="/adminassignment/deleteit/aid/<?php echo $detail['AssignmentId']; ?>" onclick="return confirm('Are you sure you want to delete this assignment?');"><img src="images/admin_panel_web/icon_panel_3.png"   alt=""></a></td>
                                                  </tr>
                                                </table>
                                            </td>
                                          </tr>
                                       
                                        </table>
                                    </td>
                                  </tr>
                                     <?php
                                     $i++;
				      } 
                                    ?>



                             	<table width="100%" border="0" cellspacing="0" cellpadding="0" >
                                	<tr>
                                    	<td valign="top" align="left">&nbsp;</td>
                                    </tr>
<?php if($lastpage > 1) { ?>
                                    <tr>
                                    	<td valign="top" align="left" class="heading_paging">Page <?php echo $page; ?> of <?php echo $lastpage; ?>, Showing <?php echo $paginator_arr['limit']; ?> records out of <?php echo $paginator_arr['total_records']; ?> total, starting on record 1, ending on <?php echo $paginator_arr['limit']; ?></td>
                                    </tr>
<?php } ?>
                                    <tr>
                                    	<td valign="top" align="left">&nbsp;</td>
                                    </tr>
                                </table>




				







<!--  ==========================pagination starts here=========================  -->
<?php
		$paginate = '';
		if($lastpage > 1)
		{	
			$paginate .= " <div class='paging_div_admin'>";
			// Previous
			if ($page > 1)
			{
				$paginate.= "<div class='left_previous'><a href='$targetpage/page/$prev/$extraLink'>&lt;  &lt;previous</a></div>";
			}
			else
			{
				$paginate.= "<div class='left_previous'><a href='javascript:void(0);'>&lt;  &lt;previous</a></div>";	
			}
			

			$paginate.= "<div class='paging_center_main'>";
			// Pages	
			if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
					{
						$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
					}
					else
					{
						$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
					}					
				}
			}
			elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
			{
			// Beginning only hide later pages
				if($page < 1 + ($stages * 2))		
				{
					for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
					{
						if ($counter == $page)
						{
							$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
						}
						else
						{
							$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
						}					
					}
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					$paginate.= "<a href='$targetpage/page/$LastPagem1/$extraLink'>$LastPagem1</a>";
					$paginate.= "<a href='$targetpage/page/$lastpage/$extraLink'>$lastpage</a>";		
				}
				// Middle hide some front and some back
				elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
				{
					$paginate.= "<a href='$targetpage/page/1/$extraLink'>1</a>";
					$paginate.= "<a href='$targetpage/page/2/$extraLink'>2</a>";
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
					{
						if ($counter == $page)
						{
							$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
						}
						else
						{
						$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
						}					
					}
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					$paginate.= "<a href='$targetpage/page/$LastPagem1/$extraLink'>$LastPagem1</a>";
					$paginate.= "<a href='$targetpage/page/$lastpage/$extraLink'>$lastpage</a>";		
			}
			// End only hide early pages
			else
			{
				$paginate.= "<a href='$targetpage/page/1/$extraLink'>1</a>";
				$paginate.= "<a href='$targetpage/page/2/$extraLink'>2</a>";
				$paginate.= "<a href='javascript:void(0);'>...</a>";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
					{
						$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
					}
					else
					{
						$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
					}					
				}
			}
		}
		$paginate.= "</div>";				
			// Next
			if ($page < $counter - 1)
			{ 
				$paginate.= "<div class='right_next'><a href='$targetpage/page/$next/$extraLink'>next&gt; &gt;</a></div>";
			}
			else
			{
				$paginate.= "<div class='right_next'><a href='javascript:void(0);'>next&gt; &gt;</a></div>";
			}
			$paginate.= "</div>";		
	
	
		}
		echo $paginate;
?>
<!--  ==========================pagination ends here=========================  -->




                            </div>
                        </div>
                    </article>
                </article>
               
         </section>
         
    
<!--Body Ends Here-->

<?php require_once("application/layout/footer.php"); ?>
<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>