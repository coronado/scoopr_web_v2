<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="">
	<meta name="author" content="">
        <base href="<?php echo PATH; ?>">
	<title>Scoopr</title>

	<!-- Fonts -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
        <link rel="stylesheet" href="public/admin/css/stylesheet.css" charset="utf-8" type="text/css">
	<link rel="stylesheet" href="public/admin/css/fonts/novecento/stylesheet.css">
	<link rel="stylesheet" href="public/admin/css/fonts/baronneue-bold/stylesheet.css">
	<link rel="stylesheet" href="public/admin/css/icons/glyphicons/style.css">
	<link rel="stylesheet" href="public/admin/css/icons/font-awesome/font-awesome.min.css">

	<!-- Styles -->
	<!--link rel="stylesheet" href="assets/css/bootstrap-modal.css"-->
	<link rel="stylesheet" href="public/admin/css/bootstrap.css">
        <link rel="stylesheet" href="public/admin/css/style_admin.css">
	<link rel="stylesheet" href="public/admin/css/style.css">
	<link rel="stylesheet" href="public/admin/css/scoopr.css">

	<!-- Plugins -->
	<link rel="stylesheet" href="public/admin/css/plugins/royalslider/royalslider.min.css">
	<link rel="stylesheet" href="public/admin/css/plugins/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="public/admin/css/plugins/mfp/jquery.mfp.css">
        <script type="text/javascript" src="public/admin/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="<?php echo PATH;?>js/jquery.validate.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo PATH; ?>js/admin/edituser.js"></script>
<style>
		.error_span {
			margin: 0 0 0 223px;
		}
		.error{
			color:#FF0000;			
		}
	</style>	
</head>

<body class="dashboard_body">
<?php require_once("application/layout/adminHeader_new.php"); ?>		
<?php require_once("application/layout/adminLeft_new.php"); ?> <!-- END .side-section -->
		<div class="main-wraps">
            
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-gutter">
				<?php require_once("application/layout/v2/top-search.php"); ?>
				<header class="account-name">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 acc-name">
						<p>Dahboard <span>/ Scoopr / Dashboard / Account / <a href="">Create</a></span></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 location-head text-right">
						
					</div>
				</header><!-- END .account-name -->
			</div>
			<div class="ratings_boxes">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="image_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>IMAGE VIEWS TODAY</p>
							<h1>100k+</h1>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>OPEN/CLICKS<span>7.80%</span></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>UNIQUE VIEWS<span>76.43%</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="submission_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>TOTAL SUBMISSIONS</p>
							<h1>6,954</h1>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details_rate">
							<p># OF ASSIGNMENTS<span>14</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="assignment_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>ASSIGNMENT VIEWS</p>
							<h1>950k+</h1>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details_rate">
							<p>OPEN/CLICKS<span>34.23%</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="reward_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>REWARDS TO DATE</p>
							<h1>$8,655</h1>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>LAST WEEK<span>$1,322</span></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>LAST MONTH<span>$4,121</span></p>
						</div>
					</div>
				</div>
			</div><!-- END .ratings_boxes -->
			<div class="datapanel_container">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 data_panel">
					<div class="panel_header">Update Account</div><!-- END .panel_header -->
					<?php if($errMsg!='') { ?>
		<div style='margin: 0% auto 0px auto; position:absolute; top:20%; left:25%;  width:50%;' ><font color="red"><?php echo @$errMsg; ?></font></div>
		<?php } ?>
					<div class="panel_content">
						
				 <form name="editUserFrm" id="editUserFrm" method="POST" action="" enctype="multipart/form-data">		
                    	        <div class="bridge_form_container edit_page">
                        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form_format">Account Info</div>
                        	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            	<input type="text" class="form-control info_txtfeild" placeholder="cocacola@scooprmedia.com">
                                <select class="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select> 
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            	<input type="text" class="form-control info_txtfeild" id="inputSuccess1" placeholder="*********">
                                <input type="text" class="form-control info_txtfeild" id="inputSuccess1" placeholder="*********">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form_format">General Info</div>
                        	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            	<input type="text" class="form-control info_txtfeild" name="profile_name"  value="<?php echo $detail[0]['profileName']; ?>" placeholder="Coca Cola">
				
					
					
                                <input type="text" name="tag_line"  value="<?php echo $detail[0]['tagLine']; ?>" class="form-control info_txtfeild" placeholder="Open Happiness">
                                <input type="text"  name="founded"  value="<?php echo $detail[0]['founded']; ?>" class="form-control info_txtfeild" placeholder="1886">
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <input type="text" name="website" value="<?php echo $detail[0]['website']; ?>"  class="form-control info_txtfeild" id="inputWarning1" placeholder="www.cocacola.com">
                               
			      <img src="images/profile_img/<?php echo $detail[0]['profileImg']; ?>" width="80" height="80" alt=""><input type="file" class="form-control info_txtfeild" id="profile_img" name="profile_img" > 
			       
			        
                                <select class="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>                
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            	<textarea   name="profile_description"  value="" placeholder="Markets, manufactures and sells beverage concentrates, and syrups, including fountain syrups, and finished sparkling and still beverages." class="desc-brand"><?php echo $detail[0]['profileDescription']; ?></textarea>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form_format">Contact Info</div>
                        	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<?php
					$name1 = explode(" ", $detail[0]['name']);
					$fname = $name1[0];
					$lname = $name1[1];
					
					?>
					
                            	<input name="full_name" id="full_name"  value="<?php echo $fname ;?>" type="text" class="form-control info_txtfeild" placeholder="Joseph">
                                <input type="text"  name="address"  value="<?php echo $detail[0]['address']; ?>" class="form-control info_txtfeild" placeholder="1 Coca Cola Plaza">
                                <input type="text" class="form-control info_txtfeild" name="telephone"  value="<?php echo $detail[0]['telephone']; ?>"  placeholder="(404) 676 - 2121">
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            	<input type="text" id="full_name"  value="<?php echo $lname; ?>" class="form-control info_txtfeild" placeholder="Tripodi">
                                <input type="text" name="city"  value="<?php echo $detail[0]['city']; ?>" class="form-control info_txtfeild" placeholder="Atlanta, GA">
                                <input type="text" class="form-control info_txtfeild" placeholder="Notes">
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 editacc_button">
                            	<!--<input type="button" name="save_profile" id="save_profile" value = "Submit" class="btn btn-default"></input>-->
				<input style="width:155px;" type="submit" name="save_profile" id="save_profile" value = "Update Account Info" class="btn btn-default">
				
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 editacc_button">
                            	<button type="button" class="btn btn-default">Disable</button>
                            	<button type="button" class="btn btn-default">Delete</button>
                            </div>
                        </div></form> <!-- END .bridge_form_container -->
					</div><!-- END .panel_content -->
				</div>
			</div>	<!-- END .datapanel_container -->

		</div><!-- END .main-wraps -->
</section><!-- END .full-site-container -->
<?php require_once("application/layout/adminFooter_new.php"); ?>
<script type="text/javascript">
	$(document).ready(function() {	
	    $('.accordionButton').click(function() {
	        $('.accordionButton').removeClass('on');
	        $('.accordionContent').slideUp('normal');
	        $('.plusMinus').text('+');
	        if($(this).next().is(':hidden') == true) {
	            $(this).addClass('on');
	            $(this).next().slideDown('normal');
	            $(this).children('.plusMinus').text('-');
	         } 
	     });
	    $('.accordionButton').mouseover(function() {
	        $(this).addClass('over');
	    }).mouseout(function() {
	        $(this).removeClass('over');
	    });
	    $('.accordionContent').hide();
	});
</script>
<script type="text/javascript">
    var leftHeight = $('.main-wraps').height();
    $('.side-section').css({'height':leftHeight});
</script>

<script type="text/javascript" src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.setup.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.scripts.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.plugins.js"></script>



</body>
</html>