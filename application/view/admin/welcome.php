<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="">
	<meta name="author" content="">
	<base href="<?php echo PATH; ?>">
	<title>Scoopr</title>

	<!-- Fonts -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
    <link rel="stylesheet" href="public/admin/css/stylesheet.css" charset="utf-8" type="text/css">
	<link rel="stylesheet" href="public/admin/fonts/novecento/stylesheet.css">
	<link rel="stylesheet" href="public/admin/fonts/baronneue-bold/stylesheet.css">
	<link rel="stylesheet" href="public/admin/icons/glyphicons/style.css">
	<link rel="stylesheet" href="public/admin/icons/font-awesome/font-awesome.min.css">

	<!-- Styles -->
	<!--link rel="stylesheet" href="public/admin/css/bootstrap-modal.css"-->
	<link rel="stylesheet" href="public/admin/css/bootstrap.css">
	<link href="public/admin/css/style_admin.css" rel="stylesheet">
   	 <link rel="stylesheet" type="text/css" href="public/admin/css/style.css">
	<link rel="stylesheet" type="text/css" href="public/admin/css/scoopr.css">

	<!-- Plugins -->
	<link rel="stylesheet" href="public/admin/plugins/royalslider/royalslider.min.css">
	<link rel="stylesheet" href="public/admin/plugins/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="public/admin/plugins/mfp/jquery.mfp.css">

</head>

<body class="dashboard_body">

<?php require_once("application/layout/adminHeader_new.php"); ?>


<?php require_once("application/layout/adminLeft_new.php"); ?> 

		<div class="main-wraps">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-gutter">
				<?php require_once("application/layout/top-search.php"); ?>
				<header class="account-name">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 acc-name">
						<p>Dahboard <span>/ Quick View</span></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 location-head text-right">
						<p>YOU ARE HERE: <span>Scoopr</span>/ Dashboard</p>
					</div>
				</header><!-- END .account-name -->
			</div>
			<div class="ratings_boxes">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="image_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>IMAGE VIEWS TODAY</p>
							<h1>100k+</h1>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>OPEN/CLICKS<span>7.80%</span></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>UNIQUE VIEWS<span>76.43%</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="submission_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>TOTAL SUBMISSIONS</p>
							<h1>6,954</h1>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details_rate">
							<p># OF ASSIGNMENTS<span>14</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="assignment_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>ASSIGNMENT VIEWS</p>
							<h1>950k+</h1>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details_rate">
							<p>OPEN/CLICKS<span>34.23%</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="reward_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>REWARDS TO DATE</p>
							<h1>$8,655</h1>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>LAST WEEK<span>$1,322</span></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>LAST MONTH<span>$4,121</span></p>
						</div>
					</div>
				</div>
			</div><!-- END .ratings_boxes -->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="fullhead_dash">
					Basic panel example
				</div><!-- END .fullhead_dash -->
			</div><!-- END .col-xs-12 .col-sm-12 .col-md-12 .col-lg-12 -->
			<div class="datapanel_container">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 data_panel">
					<div class="panel_header">Data Panel Place Holder</div><!-- END .panel_header -->
					<div class="panel_content">Data Panel content</div><!-- END .panel_content -->
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 data_panel">
					<div class="panel_header">Data Panel Place Holder</div><!-- END .panel_header -->
					<div class="panel_content">Data Panel content</div><!-- END .panel_content -->
				</div>
			</div><!-- END .datapanel_container -->

			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<div class="like_panel">
					<div class="image_box">
						<img src="public/admin/img/profile_like.png">
					</div>
					<div class="profile_details">
						<div class="heading_like"><h1>Lead Image being shared</h1></div>
						<div class="views"><i class="fa fa-eye"></i>732,102</div>
						<p class="geolocation"><i class="fa fa-map-marker"></i>San Francisco, California, USA</p>
						<span class="author">By: <a href="">Genevieve Rosenthal</a></span>
					</div><!-- END .profile_details -->
					<div class="like_diffrence">
						<ul>
							<li><a href="">FB <span class="fa fa-thumbs-o-up">201,057</span></a></li>
							<li><a href="">FBs <span>21,003</span></a></li>
							<li><a href="">FB <span><i class="fa fa-comments"></i>452</span></a></li>
							<li><a href="">TW <span>301,057</span></a></li>
							<li><a href="">G+ <span>11,629</span></a></li>
							<li><a href="">Tmb <span>30,321</span></a></li>
							<li><a href="">Insta <span>11,023</span></a></li>
							<li><a href="">Insta RG <span>1,023</span></a></li>
							<li><a href="">Insta <span><i class="fa fa-comments"></i>623</span></a></li>
						</ul>
					</div><!-- END .like_diffrence -->
				</div><!-- END .like_panel -->
			</div><!-- END .col-xs-12 .col-sm-4 .col-md-4 .col-lg-4 -->
				<div class="datapanel_container">
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 data_panel">
						<div class="panel_header">Data Panel Place Holder</div><!-- END .panel_header -->
						<div class="panel_content">
							<img src="public/admin/img/graph_big.png" class="img-responsive" style="width:100%; height:342px;">
						</div><!-- END .panel_content -->
					</div>
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 data_panel">
						<div class="panel_header">Data Panel Place Holder</div><!-- END .panel_header -->
						<div class="panel_content">Data Panel content</div><!-- END .panel_content -->
					</div>
				</div><!-- END .datapanel_container -->

			<div class="datapanel_container">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 data_panel">
					<div class="panel_header">Data Panel Place Holder</div><!-- END .panel_header -->
					<div class="panel_content">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="like_panel">
								<div class="image_box">
									<img src="public/admin/img/profile_like2.png">
								</div>
								<div class="profile_details">
									<div class="heading_like"><h1>Currently Being Shared</h1></div>
									<div class="views"><i class="fa fa-eye"></i>23,645</div>
									<p class="geolocation"><i class="fa fa-map-marker"></i>Austin Texas</p>
									<span class="author">By: <a href="">Jane Dorsey</a></span>
								</div><!-- END .profile_details -->
								<div class="like_diffrence">
									<ul>
										<li><a href="">FB <span class="fa fa-thumbs-o-up">201,057</span></a></li>
										<li><a href="">FBs <span>21,003</span></a></li>
										<li><a href="">FB <span><i class="fa fa-comments"></i>452</span></a></li>
										<li><a href="">TW <span>301,057</span></a></li>
										<li><a href="">G+ <span>11,629</span></a></li>
										<li><a href="">Tmb <span>30,321</span></a></li>
										<li><a href="">Insta <span>11,023</span></a></li>
										<li><a href="">Insta RG <span>1,023</span></a></li>
										<li><a href="">Insta <span><i class="fa fa-comments"></i>623</span></a></li>
									</ul>
								</div><!-- END .like_diffrence -->
							</div><!-- END .like_panel -->
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 view_add_image">
							<ul>
								<li><img src="public/admin/img/small_image_2.png"><p>By: Jane Dorsey <span><i class="fa fa-eye"></i>23,645</span></p></li>
								<li><img src="public/admin/img/small_image_3.png"><p>By: Jane Dorsey <span><i class="fa fa-eye"></i>23,645</span></p></li>
								<li><img src="public/admin/img/small_image_4.png"><p>By: Jane Dorsey <span><i class="fa fa-eye"></i>23,645</span></p></li>
								<li><img src="public/admin/img/small_image_5.png"><p>By: Jane Dorsey <span><i class="fa fa-eye"></i>23,645</span></p></li>
								<li><img src="public/admin/img/small_image_6.png"><p>By: Jane Dorsey <span><i class="fa fa-eye"></i>23,645</span></p></li>
								<li><img src="public/admin/img/small_image_2.png"><p>By: Jane Dorsey <span><i class="fa fa-eye"></i>23,645</span></p></li>
							</ul>
						</div>
					</div><!-- END .panel_content -->
				</div>
			</div><!-- END .datapanel_container -->

		</div><!-- END .main-wraps -->
</section><!-- END .full-site-container -->

<?php require_once("application/layout/adminFooter_new.php"); ?> 

<script type="text/javascript" src="public/admin/js/jquery-1.11.0.min.js"></script>


<script type="text/javascript">
	$(document).ready(function() {	
	    $('.accordionButton').click(function() {
	        $('.accordionButton').removeClass('on');
	        $('.accordionContent').slideUp('normal');
	        $('.plusMinus').text('+');
	        if($(this).next().is(':hidden') == true) {
	            $(this).addClass('on');
	            $(this).next().slideDown('normal');
	            $(this).children('.plusMinus').text('-');
	         } 
	     });
	    $('.accordionButton').mouseover(function() {
	        $(this).addClass('over');
	    }).mouseout(function() {
	        $(this).removeClass('over');
	    });
	    $('.accordionContent').hide();
	});
</script>
<script type="text/javascript">
    var leftHeight = $('.main-wraps').height();
    $('.side-section').css({'height':leftHeight});
</script>

<script type="text/javascript" src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.setup.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.scripts.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.plugins.js"></script>



</body>
</html>