<?php
$lastpage   = $paginator_arr['lastpage'];
$page       = $paginator_arr['page'];
$targetpage = "/adminsubmission/index";
$prev       = $paginator_arr['prev'];
$stages     = 1;
$LastPagem1 = $paginator_arr['LastPagem1'];
$next       = $paginator_arr['next'];
$num_rows   = count($paginator_arr['paginated_result']);

// search and pagination link variables
$extraLink = "";
$aid = $search_arr['aid'];
if($aid!='') {
	$extraLink = "aid/".$aid."/uid/".$search_arr['uid'];
}
?>
<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Admin</title>
	<?php require_once("application/layout/headerContent.php"); ?>
	<link rel="stylesheet" href="css/colorbox.css" />
	<script src="js/jquery.colorbox.js"></script>
	<script>
	$(document).ready(function(){
	$(".group").colorbox({rel: 'group', innerWidth:'600px', title: function(){
		var aImageName = $(this).attr('id');	
		return '<a style="color:#ffffff;" href=/download/images/img/'+aImageName+'>Download Image</a>';
	}});

	$(".details").colorbox({
				
				innerWidth:'500px',
				innerHeight:'200px'
				});

	});
	</script>
	</head>
	<body>
<?php require_once("application/layout/adminHeader.php"); ?>
<?php require_once("application/layout/adminSubHeader.php"); ?>        

        

    
    <section class="webaccount9">
      <article class="inner_main_content">
	<article class="content_row_admin">
	<?php require_once("application/layout/adminLeft.php"); ?>        

<div class="adminright_main">
<div class="view_submission" style="padding-top:0;">


                	<section class="section_brand">
			<?php if($aid!='') { ?>
                    	<article class="article_left" style="width:80%; float:none; padding: 0% 0px 2% 2%;">
                        	<aside class="red_ball_section"><img class="circleImg" src="images/profile_img/<?php echo $buyer_info['profileImg']; ?>" width="76" height="76" alt=""></aside>
                        	<aside class="content_section_vw_sub">
                            	<h3>&nbsp;</h3>
				<h1>&nbsp;&nbsp;<?php echo $buyer_info['profileName']; ?> - Assignment <?php echo $aid; ?></h1>
                                <h4>&nbsp;</h4>
                            </aside>
                        </article>
                        <?php } ?>
                    	<p class="align_c"><img src="images/assignment_page/partition_line.png" alt="" width="600" height="24" align="absmiddle"></p>
      			</section>



<!-- ========================  MIDDLE CONTENT LISTING ========================   -->                     
                     <section class="section_list_brands">

		     <?php 
		if($num_rows>0) {
		foreach ($paginator_arr['paginated_result'] as $detail) { ?>	
                     	<aside class="box_list_view_sub">
                        	<aside class="inner_box_view_sub">
                                	<div class="top_bar">


                                    	<div class="headi_vw_sub"><a class="details headi_vw_sub" href="adminsubmission/creatorDetails/creator_id/<?php echo $detail['creatorId']; ?>"><?php if($detail['full_name']!='') { echo $detail['full_name']; } else { echo "NA"; }?></a></div>

                                        <div class="star_vw_sub"><img align="absmiddle" src="images/assignment_page/<?php echo (isset($detail['fav_submission'])===true?'blue':'grey')?>_stared.png" width="22" height="24" alt=""></div>
                                 	 </div>
                                    <div class="mid_bar"><a id="<?php echo $detail['imageName']; ?>" class="group"  href="images/assign_img_submission/<?php echo $detail['imageName']; ?>"><img src="images/assign_img_submission/medium_size_image/<?php echo $detail['mediumSizeImage']; ?>" height="100" width="185" border="0" ></a></div>

				<div class="top_bar">
                                    	<div class="headi_vw_sub"><a class="headi_vw_sub" href="/adminsubmission/submissiondetail/sid/<?php echo $detail['AssignmentImgSubmissionId']; ?>">View detail</a></div>
                          	 </div>

                                </aside>
                        </aside>
		      <?php } } else { ?>	
<aside class="box_list_view_sub" style="text-align:center; display:block; color:#FFFFFF; margin-top:5%; margin-bottom:6%;">
				<h4>Sorry! No Records available.</h4>
			</aside>
		<?php } ?>
                     </section>
<!-- ========================  MIDDLE CONTENT LISTING (END) ========================   --> 



<section>
<!--  ==========================pagination starts here=========================  -->
<?php
		$paginate = '';
		if($lastpage > 1)
		{	
			$paginate .= " <div class='paging_div_admin'>";
			// Previous
			if ($page > 1)
			{
				$paginate.= "<div class='left_previous'><a href='$targetpage/page/$prev/$extraLink'>&lt;  &lt;previous</a></div>";
			}
			else
			{
				$paginate.= "<div class='left_previous'><a href='javascript:void(0);'>&lt;  &lt;previous</a></div>";	
			}
			

			$paginate.= "<div class='paging_center_main'>";
			// Pages	
			if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
					{
						$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
					}
					else
					{
						$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
					}					
				}
			}
			elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
			{
			// Beginning only hide later pages
				if($page < 1 + ($stages * 2))		
				{
					for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
					{
						if ($counter == $page)
						{
							$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
						}
						else
						{
							$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
						}					
					}
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					$paginate.= "<a href='$targetpage/page/$LastPagem1/$extraLink'>$LastPagem1</a>";
					$paginate.= "<a href='$targetpage/page/$lastpage/$extraLink'>$lastpage</a>";		
				}
				// Middle hide some front and some back
				elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
				{
					$paginate.= "<a href='$targetpage/page/1/$extraLink'>1</a>";
					$paginate.= "<a href='$targetpage/page/2/$extraLink'>2</a>";
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
					{
						if ($counter == $page)
						{
							$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
						}
						else
						{
						$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
						}					
					}
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					$paginate.= "<a href='$targetpage/page/$LastPagem1/$extraLink'>$LastPagem1</a>";
					$paginate.= "<a href='$targetpage/page/$lastpage/$extraLink'>$lastpage</a>";		
			}
			// End only hide early pages
			else
			{
				$paginate.= "<a href='$targetpage/page/1/$extraLink'>1</a>";
				$paginate.= "<a href='$targetpage/page/2/$extraLink'>2</a>";
				$paginate.= "<a href='javascript:void(0);'>...</a>";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
					{
						$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
					}
					else
					{
						$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
					}					
				}
			}
		}
		$paginate.= "</div>";				
			// Next
			if ($page < $counter - 1)
			{ 
				$paginate.= "<div class='right_next'><a href='$targetpage/page/$next/$extraLink'>next&gt; &gt;</a></div>";
			}
			else
			{
				$paginate.= "<div class='right_next'><a href='javascript:void(0);'>next&gt; &gt;</a></div>";
			}
			$paginate.= "</div>";		
	
	
		}
		echo $paginate;
?>
<!--  ==========================pagination ends here=========================  -->

</section>


                     


                </div></div>
    </article>
      </article>
        </section>
        
    
<!--Body Ends Here-->

<?php require_once("application/layout/footer.php"); ?>
<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>