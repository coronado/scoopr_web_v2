<?php 
$this->include_file('admin_class.php','application/lib');
$obj = new adminlib;

$lastpage   = $paginator_arr['lastpage'];
$page       = $paginator_arr['page'];
$targetpage = "/admin/disabledcreatorlist";
$prev       = $paginator_arr['prev'];
$stages     = 1;
$LastPagem1 = $paginator_arr['LastPagem1'];
$next       = $paginator_arr['next'];

//$paginator_arr['paginated_result'] = array();
?>
<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Admin</title>
	<?php require_once("application/layout/headerContent.php"); ?>
	<link rel="stylesheet" href="css/colorbox.css" />
	<script src="js/jquery.colorbox.js"></script>
	<script>
	$(document).ready(function(){
	$(".details").colorbox({
				
				innerWidth:'500px',
				innerHeight:'200px'
				});

	});
	</script>
	</head>
	<body>
<?php require_once("application/layout/adminHeader.php"); ?>
<?php require_once("application/layout/adminSubHeader.php"); ?>        
        		

    
   		<section class="webaccount9">
        		<article class="inner_main_content">
                	<article class="content_row_admin">
                    	<?php require_once("application/layout/adminLeft.php"); ?>        



                        <div class="adminright_main">
                        	<h1>Manage Creators (<?php echo $paginator_arr['total_records']; ?>)</h1>
					
<form name="searchFrm" id="searchFrm" method="POST" action="">
	
	<input style="width:180px;" class="input" name="email_address" id="email_address"  type="text" placeholder="email address">&nbsp;
	<input type="submit" name="search" id="search" value="Search" class="next_button_diff">
	</form>                        	

                            <div class="content_div_admin">

                     <?php if($errMsg!='') { ?>
		<h1 style="font-size:15px;"><?php echo @urldecode($errMsg); ?></h1>
		<?php } ?>
                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td valign="top" class="table_head_borders">
                                    
                                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    	                        	
        					
                                          <tr>
                                            <td align="left" class="blue_headings" width="32%">Name</td>
                                            <td align="left" class="blue_headings" width="32%">Username/Email</td>
                                            <td align="left" class="blue_headings" width="15%">Submissions</td>
					    <!--<td align="left" class="blue_headings" width="20%">Name</td>-->
					     	
                                            <td align="center" class="blue_headings"  width="20%">Actions</td>
                                          </tr>
                                        </table>
                                    </td>
                                  </tr>
                                  <?php 
					$i= 1;
                                    	foreach ($paginator_arr['paginated_result'] as $detail) 
                                    	{
						
                                    		$class = "table_head_2bg";	
                                    		if($i%2 == 0) {
                                    			$class = "table_head_2bg bg_r2";
                                    		}
                                    		
						$submissions = adminlib::getSubmissionCountForCreators($detail['id']);
                                    		?>
                                  <tr>
                                    <td valign="top" class="<?php echo $class ?>">
                                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    	
                                          <tr>
                                            <td  width="32%" class="head_normal"><?php echo $detail['name']; ?></td>

                                            <td  width="32%" class="head_normal"><?php echo $detail['emailAddress']; ?></td>

					   <td class="head_normal" width="15%">
						<?php if($submissions>0) { ?>
						<a class="admin_links" href="/adminsubmission/index/cid/<?php echo $detail['UserId']; ?>"><?php echo $submissions; ?></a>
						<?php } else { ?>
						<?php echo $submissions; ?>
						<?php } ?>
						</td>	

                                          <!--  <td width="20%" class="head_normal"><?php if($detail['name']!='') { echo $detail['name']; } else { echo "NA"; } ?></td>-->
					    	

                                            <td  width="20%">
                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                  <tr>

                                                    <!--<td ><a href="adminaccount/userdetail/id/<?php echo $detail['UserId']; ?>/page/<?php echo $page; ?>"><img src="images/admin_panel_web/icon_panel_1.png"  alt=""></a></td>-->

						 <td ><a class="details" title="View creator details" href="/adminsubmission/creatorDetails/creator_id/<?php echo $detail['UserId']; ?>"><img src="images/admin_panel_web/icon_panel_1.png"  alt=""></a></td>

                                                    <td width="1%"></td>

                                                  <!--  <td  ><a title="Edit" href="adminaccount/edituser/id/<?php echo $detail['id']; ?>/page/<?php echo $page; ?>"><img src="images/admin_panel_web/icon_panel_2.png"   alt=""></a></td>
                                                    <td  width="1%"></td>-->


                                                    <td ><a title="Disable creator" href="adminaccount/disablecreator/id/<?php echo $detail['UserId']?>/page/<?php echo $page; ?>" onclick="return confirm('Are you sure you want to disable this app user?');"><img src="images/admin_panel_web/icon_panel_3.png"   alt=""></a></td>

						<!--<td ><a href="#" ><img src="images/admin_panel_web/icon_panel_3.png"   alt=""></a></td>-->


                                                  </tr>
                                                </table>
                                            </td>
                                          </tr>
                                       
                                        </table>
                                    </td>
                                  </tr>
                                     <?php
                                     $i++;
				      } 
                                    ?>



                             	<table width="100%" border="0" cellspacing="0" cellpadding="0" >
                                	<tr>
                                    	<td valign="top" align="left">&nbsp;</td>
                                    </tr>

<?php if($lastpage > 1) { ?>
                                    <tr>
                                    	<td valign="top" align="left" class="heading_paging">Page <?php echo $page; ?> of <?php echo $lastpage; ?>, Showing <?php echo $paginator_arr['limit']; ?> records out of <?php echo $paginator_arr['total_records']; ?> total, starting on record 1, ending on <?php echo $paginator_arr['limit']; ?></td>
                                    </tr>
<?php } ?>

                                    <tr>
                                    	<td valign="top" align="left">&nbsp;</td>
                                    </tr>
                                </table>



				







<!--  ==========================pagination starts here=========================  -->
<?php
		$paginate = '';
		if($lastpage > 1)
		{	
			$paginate .= " <div class='paging_div_admin'>";
			// Previous
			if ($page > 1)
			{
				$paginate.= "<div class='left_previous'><a href='$targetpage/page/$prev'>&lt;  &lt;previous</a></div>";
			}
			else
			{
				$paginate.= "<div class='left_previous'><a href='javascript:void(0);'>&lt;  &lt;previous</a></div>";	
			}
			

			$paginate.= "<div class='paging_center_main'>";
			// Pages	
			if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
					{
						$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
					}
					else
					{
						$paginate.= "<a href='$targetpage/page/$counter'>$counter</a>";
					}					
				}
			}
			elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
			{
			// Beginning only hide later pages
				if($page < 1 + ($stages * 2))		
				{
					for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
					{
						if ($counter == $page)
						{
							$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
						}
						else
						{
							$paginate.= "<a href='$targetpage/page/$counter'>$counter</a>";
						}					
					}
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					$paginate.= "<a href='$targetpage/page/$LastPagem1'>$LastPagem1</a>";
					$paginate.= "<a href='$targetpage/page/$lastpage'>$lastpage</a>";		
				}
				// Middle hide some front and some back
				elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
				{
					$paginate.= "<a href='$targetpage/page/1'>1</a>";
					$paginate.= "<a href='$targetpage/page/2'>2</a>";
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
					{
						if ($counter == $page)
						{
							$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
						}
						else
						{
						$paginate.= "<a href='$targetpage/page/$counter'>$counter</a>";
						}					
					}
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					$paginate.= "<a href='$targetpage/page/$LastPagem1'>$LastPagem1</a>";
					$paginate.= "<a href='$targetpage/page/$lastpage'>$lastpage</a>";		
			}
			// End only hide early pages
			else
			{
				$paginate.= "<a href='$targetpage/page/1'>1</a>";
				$paginate.= "<a href='$targetpage/page/2'>2</a>";
				$paginate.= "<a href='javascript:void(0);'>...</a>";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
					{
						$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
					}
					else
					{
						$paginate.= "<a href='$targetpage/page/$counter'>$counter</a>";
					}					
				}
			}
		}
		$paginate.= "</div>";				
			// Next
			if ($page < $counter - 1)
			{ 
				$paginate.= "<div class='right_next'><a href='$targetpage/page/$next'>next&gt; &gt;</a></div>";
			}
			else
			{
				$paginate.= "<div class='right_next'><a href='javascript:void(0);'>next&gt; &gt;</a></div>";
			}
			$paginate.= "</div>";		
	
	
		}
		echo $paginate;
?>
<!--  ==========================pagination ends here=========================  -->





                                <!--<div class="paging_div_admin">
                                	<div class="left_previous"> <a href="#">&lt;  &lt;previous </a></div>
                                    <div class="paging_center_main"><a href="#">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a><a href="#">6</a><a href="#">7</a><a href="#">8</a><a href="#">9</a><a href="#">10</a>	</div>
                                    <div class="right_next"><a href="#">next &gt; &gt;</a></div>
                                </div>-->


                            </div>
                        </div>
                    </article>
                </article>
               
         </section>
         
    
<!--Body Ends Here-->

<?php require_once("application/layout/adminFooter.php"); ?>
<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>