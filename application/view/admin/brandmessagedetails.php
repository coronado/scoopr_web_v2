<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Scoopr</title>
	<?php require_once("application/layout/headerContent.php"); ?>
	<link rel="stylesheet" href="css/colorbox.css" />
	<script src="js/jquery.colorbox.js"></script>
	<script>
	$(document).ready(function(){
	$(".details").colorbox({
				
				innerWidth:'500px',
				innerHeight:'200px'
				});

	});
	</script>
	</head>
	<body>
<?php require_once("application/layout/adminHeader.php"); ?>
<?php require_once("application/layout/adminSubHeader.php"); ?>   




    
   		<section class="webaccount9">
			<article class="inner_main_content">
        		<article class="content_row_admin">
                    	<?php require_once("application/layout/adminLeft.php"); ?>       

		 <div class="adminright_main">
                <section>
                	<article class="content_box_mid">
                    		<div class="main_content_left">
                            	<h1>Message Detail</h1>
                            </div>
                          
                            <div class="content_flow">
                            	<h2>Brand :</h2>
                             	<p><a class="admin_links" href="/adminaccount/brandDetails/uid/<?php echo $brandMsgDetails['buyerId']; ?>" style="font-size:16px;"><?php echo $brandMsgDetails['profileName']; ?></a></p>
                                
				<h2>Subject :</h2>
                             	<p><?php echo $brandMsgDetails['subject']; ?></p>

				<h2>Message :</h2>
                             	<p><?php echo $brandMsgDetails['brandMessage']; ?></p>

				<h2>Modified Date :</h2>
                             	<p><?php echo $brandMsgDetails['modifiedDate']; ?></p>                                

				<h2>Status :</h2>
                             	<p><?php if($brandMsgDetails['status']==1) { echo "Active"; } else { echo "Inactive"; } ?></p>                                
				<h2>Total Replies :</h2>
                             	<p><?php echo count($repliedMsgDetails); ?></p>                                
                                

	<div class="content_box_small_div">
         <div class="btn_pink"><a style="width:130px;" href="/adminmessage/editbrandmessage/mid/<?php echo $brandMsgDetails['BrandMessageId']; ?>"><strong>Edit &nbsp;Message</strong></a></div>
        </div>


<?php if(count($repliedMsgDetails)>0) { ?>
<p>&nbsp;</p>
  <h1 style="color:#9e9d9d; font-size:20px;">Replied Messages</h1>
<?php foreach($repliedMsgDetails as $repliedMsg) { ?>
<p><?php echo $repliedMsg['repliedMessage']; ?></p> 
<p> - <a class="details admin_links" href="/adminsubmission/creatorDetails/creator_id/<?php echo $repliedMsg['creatorId']; ?>" style="font-size:14px;"><?php echo $repliedMsg['name']; ?></a></p>
<p>&nbsp;</p>
<?php } } ?>

       
                    </article>
                </section></div></article></article>



        </section>
         
    
<!--Body Ends Here-->
<script type="text/javascript" src="js/jquerypp.custom.js"></script> 
<script type="text/javascript" src="js/jquery.elastislide.js"></script> 
<script type="text/javascript">
	//$( '#carousel' ).elastislide();
</script>

<?php require_once("application/layout/footer.php"); ?>
<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>