<?php //echo "<pre>"; print_r($details); die; 
$dimensions = getimagesize("images/assign_img_submission/".$details['imageName']);
$resolution = $dimensions[0]."x".$dimensions[1];
if($details['paymentMethod']==1) {
	$payment_method = "Paypal";
} elseif($details['paymentMethod']==2)  {
	$payment_method = "Reward";
} elseif($details['paymentMethod']==3) {
	$payment_method = "Paypal payment & Reward";
}
//echo "<pre>"; print_r($details); die;
?>
<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Admin</title>
	<?php require_once("application/layout/headerContent.php"); ?>
<style>
.main_demonstration .left_demonstration{ font-weight:normal; font-family:Verdana, Geneva, sans-serif; color:#959595; font-size:14px; padding:0% 0px 0% 0%; margin:0% 0% 0% 0px; display:inline-block; width:35%; text-align:left;}

	.main_demonstration .right_demonstration{ font-weight:normal; font-family:Verdana, Geneva, sans-serif; color:#959595; font-size:14px;padding:0% 0px 0% 0%; margin:0% 0px 0% 0px; display:inline-block;  width:45%; text-align:left;}

	.imgSize {
  		max-width:558px;
		height:auto; 
		max-height:369px;
	    }
</style>
	<link rel="stylesheet" href="css/colorbox.css" />
	<script src="js/jquery.colorbox.js"></script>
	<script>
	$(document).ready(function(){
	$(".details").colorbox({
				
				innerWidth:'500px',
				innerHeight:'200px'
				});

	});
	</script>
	</head>
	<body>

<?php require_once("application/layout/adminHeader.php"); ?>
<?php require_once("application/layout/adminSubHeader.php"); ?>    
        

    
   
         <section class="webaccount5">
         		<article class="inner_main_content">
<article class="content_row_admin">

			<?php require_once("application/layout/adminLeft.php"); ?>        


			<div class="adminright_main">
                	<div class="view_submission" style="padding-top:0;">
                	<section class="section_brand">
                    	
                        
                    	<p><img src="images/assignment_page/partition_line.png" width="888" height="24" alt=""></p>
                    </section>                     
                </div>
                	   
                 <section class="single_submission">
                 	<article class="sng_sub_left_section" style="width:55%;">
                    	<aside class="main_demonstration">
			    
			    <article class="left_demonstration">Submitted by:</article>
                            <article class="right_demonstration"><a class="admin_links details" href="/adminsubmission/creatorDetails/creator_id/<?php echo $details['creatorId']; ?>">@<?php echo $details['full_name']; ?></a></article>
                            <article class="left_demonstration">Submitted on:</article>
                            <article class="right_demonstration"><?php echo date('d/m/y',$details['timestamp']); ?></article>
                            <article class="left_demonstration">Resolution:</article>
                            <article class="right_demonstration"><?php echo $resolution; ?></article>

			<article class="left_demonstration">Assignment Id:</article>
                            <article class="right_demonstration"><a class="admin_links" href="/adminassignment/assignmentDetail/aid/<?php echo $details['AssignmentId']; ?>"><?php echo $details['AssignmentId']; ?></a></article>

			<article class="left_demonstration">Payment Status:</article>
                            <article class="right_demonstration"><?php echo $details['paymentStatus']; ?></article>


			<?php if($details['payment_status']=='success') { ?>
			<article class="left_demonstration">Payment Method:</article>
                            <article class="right_demonstration"><?php echo $payment_method; ?></article>

			<?php if($details['payment_method']==1) { ?>
			<article class="left_demonstration">Payment Amount:</article>
                            <article class="right_demonstration"><?php echo "$".$details['paymentAmount']; ?></article>
			<?php } elseif($details['paymentMethod']==2) { ?>			

			<article class="left_demonstration">Reward:</article>
                            <article class="right_demonstration"><?php echo $details['rewards']; ?></article>

			<?php } elseif($details['paymentMethod']==3) { ?>

			<article class="left_demonstration">Payment Amount:</article>
                            <article class="right_demonstration"><?php echo "$".$details['paymentAmount']; ?></article>

			<article class="left_demonstration">Reward:</article>
                            <article class="right_demonstration"><?php echo $details['rewards']; ?></article>
			<?php } ?>

			<article class="left_demonstration">Payment Date:</article>
                            <article class="right_demonstration"><?php echo $details['paymentDate']; ?></article>

			<?php } ?>

			

                            <article class="left_demonstration">Model Release:</article>
                            <article class="right_demonstration "><span class="sk_blue"><?php if($details['ModelId']==0) { echo "No"; } else { echo "Yes"; } ?></span></article>

				<?php if($details['ModelId']!=0) { ?>
				 <article class="right_demonstration"><a class="admin_links" href="/adminsubmission/modelreleaseDetail/model_id/<?php echo $details['ModelId']; ?>">Model detail</a></article>
				<?php } ?>
                        </aside>


<?php if($details['ModelId']!=0 && $details['modelPdf']!='') { ?>	
                        <aside class="main_demonstration">
                                 <div class="content_box_small_div">
     					<div class="btn_pink" style="width:160px;"><a target="_blank" href="/images/model_logo/<?php echo $details['modelPdf']; ?>"><strong>View Model Release</strong></a></div>
                           	 </div>
                        </aside>
<?php } ?>
                        

                    </article>

                    	<article class="sng_sub_right_section" style="width:44%;"><img class="imgSize" align="absmiddle" src="images/assign_img_submission/medium_size_image/<?php echo $details['mediumSizeImage']; ?>" alt=""></article>

                 </section>
</div>


                </article></article>
         </section>
    
<!--Body Ends Here-->

<?php require_once("application/layout/footer.php"); ?>

<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>