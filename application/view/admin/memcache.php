<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Admin</title>
	<?php require_once("application/layout/headerContent.php"); ?>
<link rel="stylesheet" href="css/jquery-ui.css" />
<!--<script src="js/jquery-1.9.1.js"></script>-->
<script src="js/jquery-ui.js"></script>
<style>
.ui-progressbar {
position: relative;
}
.progress-label {
font-size:12px;
position: absolute;
left: 40%;
top: 16px;
font-weight: bold;
text-shadow: 1px 1px 0 #fff;
}
</style>
<script type="text/javascript" language="JavaScript">
$(function(){
	showProgress();
});
	function showProgress() 
	{	
		var progressbar = $( "#progressbar" ),
		progressLabel = $( ".progress-label" );
		progressbar.progressbar({
		value: false,
		change: function() {
		progressLabel.text( progressbar.progressbar( "value" ) + "%" );
		},
		complete: function() {
		progressLabel.text( "Cache has been cleared." );
		}
		});

		function progress() 
		{
			var val = progressbar.progressbar( "value" ) || 0;
			progressbar.progressbar( "value", val + 1 );
			if ( val < 99 ) 
			{
				setTimeout( progress, 100 );
			}
		}
		progress();
	}
</script>
	</head>
	<body>

<?php require_once("application/layout/adminHeader.php"); ?>
<?php require_once("application/layout/adminSubHeader.php"); ?>    
        

    
   
         <section class="webaccount5">
         		<article class="inner_main_content">
<article class="content_row_admin">

			<?php require_once("application/layout/adminLeft.php"); ?>        


			<div class="adminright_main">
                	<div class="view_submission" style="padding-top:0;">
                	<section class="section_brand">
                    	
                        
                    	<p><img src="images/assignment_page/partition_line.png" width="888" height="24" alt=""></p>
                    </section>                     
                </div>
                	   
                 <section class="single_submission">
                 	<article class="sng_sub_left_section" style="width:55%;">
                    	<aside class="main_demonstration">
			    
			

			<article class="left_demonstration"><div><div style="width:700px; top:60px;" id="progressbar" class="progressbar"><div class="progress-label"></div></div></div></article>
                            
			</aside>

			

                 </section>
</div>


                </article></article>
         </section>
    
<!--Body Ends Here-->

<?php require_once("application/layout/footer.php"); ?>

<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>