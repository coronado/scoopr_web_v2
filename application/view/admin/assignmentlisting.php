
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="">
	<meta name="author" content="">
	<base href="<?php echo PATH; ?>">
	<title>Scoopr</title>

	<?php require_once("application/layout/v2/headerContent.php"); ?>
		
		
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
	<link rel="stylesheet" href="public/admin/fonts/novecento/stylesheet.css">
	<link rel="stylesheet" href="public/admin/fonts/baronneue-bold/stylesheet.css">
	<link rel="stylesheet" href="public/admin/icons/glyphicons/style.css">
	<link rel="stylesheet" href="public/admin/icons/font-awesome/font-awesome.min.css">

	<!-- Styles -->
	<!--link rel="stylesheet" href="assets/css/bootstrap-modal.css"-->
	<link rel="stylesheet" href="public/admin/css/bootstrap.css">
        <link rel="stylesheet" href="public/admin/css/style_admin.css">
	<link rel="stylesheet" href="public/admin/css/style.css">
	<link rel="stylesheet" href="public/admin/css/scoopr.css">
		

	<!-- Plugins -->
	<link rel="stylesheet" href="public/admin/css/plugins/royalslider/royalslider.min.css">
	<link rel="stylesheet" href="public/admin/css/plugins/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="public/admin/css/plugins/mfp/jquery.mfp.css">

	<style>
		.modal button.close {
			top: 11px;
			position: absolute;
			right: 40px;
			z-index: 99999;
		}
	</style>
	
	
	
	

</head>

<body class="dashboard_body">
<?php require_once("application/layout/adminHeader_new.php"); ?>		
<?php require_once("application/layout/adminLeft_new.php"); ?> <!-- END .side-section --><!-- END .side-section -->
		<div class="main-wraps">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-gutter">
				<header class="dashboard-header">
					<div class="col-xs-7 col-sm-3 col-md-3 col-lg-3 alpha search-box">
						<form>
							<div class="form-group">
								<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
							</div>
						</form>
					</div><!-- END .search-box -->
					<div class="col-xs-offset-7 col-sm-offset-9 col-md-offset-9 col-lg-offset-9">
						<div class="brand-name">
							<div class="seprat">
								<img src="public/admin/img/profile-pic.png">
								<label></label>
							</div>
						</div>
					</div>
				</header>
				<header class="account-name">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 acc-name">
						<p>Dashboard <span>/ Scoopr / Dashboard / <a href="#">Account</a></span></p>
					</div>
				</header><!-- END .account-name -->
			</div>
			<div class="ratings_boxes">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="image_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>IMAGE VIEWS TODAY</p>
							<h1>100k+</h1>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>OPEN/CLICKS<span>7.80%</span></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>UNIQUE VIEWS<span>76.43%</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="submission_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>TOTAL SUBMISSIONS</p>
							<h1>6,954</h1>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details_rate">
							<p># OF ASSIGNMENTS<span>14</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="assignment_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>ASSIGNMENT VIEWS</p>
							<h1>950k+</h1>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details_rate">
							<p>OPEN/CLICKS<span>34.23%</span></p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="reward_view">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main_rate">
							<p>REWARDS TO DATE</p>
							<h1>$8,655</h1>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>LAST WEEK<span>$1,322</span></p>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 details_rate">
							<p>LAST MONTH<span>$4,121</span></p>
						</div>
					</div>
				</div>
			</div><!-- END .ratings_boxes -->
			<div class="datapanel_container view_account_setup">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 data_panel">
					<div class="panel_header">View Accounts</div><!-- END .panel_header -->
					<div class="panel_content">
                    
                    	<div class="view_account">
<?php 
$lastpage   = $paginator_arr['lastpage'];
$page       = $paginator_arr['page'];
$targetpage = "/adminassignment/index";
$prev       = $paginator_arr['prev'];
$stages     = 1;
$LastPagem1 = $paginator_arr['LastPagem1'];
$next       = $paginator_arr['next'];
$num_rows   = count($paginator_arr['paginated_result']);

// search and pagination link variables
$extraLink = "";
$uid = $search_arr['uid'];
if($uid!='') {
	$extraLink = "uid/".$uid;
}
?>
				
				
                        	<div class="view_header">
			<?php if($errMsg!='') { ?>
			<div style='margin: 0% auto 0px auto; position:absolute; top:20%; left:25%;  width:50%;' ><font color="red"><?php echo @$errMsg; ?></font></div>
			<?php } ?>
                            	<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                	<span>Brand<span class="caret"></span></span>
                                </div>
                                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                                	<span>Breif<span class="caret"></span></span>
                                </div>
                                <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                	<span>Active<span class="caret"></span></span>
                                </div>
                                <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                	<span>Launched<span class="caret"></span></span>
                                </div>
                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
                                	<span>Budget<span class="caret"></span></span>
                                </div>
				<div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                	<span>#of Submission<span class="caret"></span></span>
                                </div>
			
                            </div><!-- END .view_header -->
                            <div class="view_content">
				 <?php 
					$i= 1;
                                    	foreach ($paginator_arr['paginated_result'] as $detail) 
                                    	{
						$class = "odd";	
                                    		if($i%2 == 0) {
                                    			$class = "odd bg_r2";
                                    		}

						if($detail['ourBudget']!='' && $detail['rewards']!='') {
							$budgetAndReward = "$ ".$detail['ourBudget']." (".$detail['rewards'].")";
						} elseif($detail['ourBudget']!='' && $detail['rewards']=='') {
							$budgetAndReward = "$ ".$detail['ourBudget'];
						} else {
							$budgetAndReward = $detail['rewards'];
						}
                                    		
						
                                    		?>
				
				
				
                            	<div class="<?php echo $class ?>">
                                    <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                    	<span><a data-width="920" data-height="440" class="fancybox fancybox.iframe admin_links"  href="/adminassignment/assignmentDetail/aid/<?php echo $detail['AssignmentId']; ?>/uid/<?php echo $detail['UserId']; ?>" data-toggle="modal" data-target="#myModal2"><?php echo $detail['profileName']; ?></a></span>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">  
                                    	<span><?php if(substr($detail['lookingForText'], 0, 8)!='') { echo substr($detail['lookingForText'], 0, 19); } else { echo "NA"; } ?></span>
                                    </div>
                                    <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                    	<ul class="yes_no_button" style="margin-left: 18px;">
                                            <li class="yes"><a  class="active">Y</a></li>
                                            <li class="no"><a >N</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                    	<span><?php if($detail['isLaunched']==1) { echo "Yes"; } else { echo "No"; } ?><?php if($detail['isLaunched']==1) { echo "(".$detail['launchedDate'].")";} ?></span>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                                    	<span><?php if($detail['ourBudget']!='') { echo "$".$detail['ourBudget']; } else { echo "NA"; } ?></span>
                                    </div>
				    
				    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                                    	<span><?php echo $detail['submission_deadline']; ?> <span>(<?php echo $detail['submissionDeadline']; ?>)</span>
                                    </div>
				    
                                </div>
                               <!-- <div class="even">
                                    <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                    	<span>Sample Co.</span>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                    	<span>sample@scooprmedia.com</span>
                                    </div>
                                    <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                    	<ul class="yes_no_button">
                                            <li class="yes"><a href="" class="active">Y</a></li>
                                            <li class="no"><a href="">N</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                    	<span>Premium</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    	<span>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                                                </div>
                                            </div>
                                        </span>
                                    </div>
				</div>-->
                          
                         <?php } ?>
                            </div><!-- END .view_content -->
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pagination_wrapper">
			

<!--  ==========================pagination starts here=========================  -->
<?php
		$paginate = '';
		if($lastpage > 1)
		{	
			$paginate .= " <ul class='pagination'>";
			// Previous
			if ($page > 1)
			{
				$paginate.= "<li><a href='$targetpage/page/$prev/$extraLink'>&laquo;</a></li>";
				
			}
			else
			{
				
			}
			

			$paginate.= "<li class='paging_center_main'>";
			// Pages	
			if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
					{
						$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
					}
					else
					{
						$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
					}					
				}
			}
			elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
			{
			// Beginning only hide later pages
				if($page < 1 + ($stages * 2))		
				{
					for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
					{
						if ($counter == $page)
						{
							$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
						}
						else
						{
							$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
						}					
					}
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					$paginate.= "<a href='$targetpage/page/$LastPagem1/$extraLink'>$LastPagem1</a>";
					$paginate.= "<a href='$targetpage/page/$lastpage/$extraLink'>$lastpage</a>";		
				}
				// Middle hide some front and some back
				elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
				{
					$paginate.= "<a href='$targetpage/page/1/$extraLink'>1</a>";
					$paginate.= "<a href='$targetpage/page/2/$extraLink'>2</a>";
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
					{
						if ($counter == $page)
						{
							$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
						}
						else
						{
						$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
						}					
					}
					$paginate.= "<a href='javascript:void(0);'>...</a>";
					$paginate.= "<a href='$targetpage/page/$LastPagem1/$extraLink'>$LastPagem1</a>";
					$paginate.= "<a href='$targetpage/page/$lastpage/$extraLink'>$lastpage</a>";		
			}
			// End only hide early pages
			else
			{
				$paginate.= "<a href='$targetpage/page/1/$extraLink'>1</a>";
				$paginate.= "<a href='$targetpage/page/2/$extraLink'>2</a>";
				$paginate.= "<a href='javascript:void(0);'>...</a>";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
					{
						$paginate.= "<a class='current' href='javascript:void(0);'>$counter</a>";
					}
					else
					{
						$paginate.= "<a href='$targetpage/page/$counter/$extraLink'>$counter</a>";
					}					
				}
			}
		}
		$paginate.= "</li>";				
			// Next
			if ($page < $counter - 1)
			{ 
				$paginate.= "<li><a href='$targetpage/page/$next/$extraLink'>&raquo;</a></li>";
			}
			else
			{
				$paginate.= "<li><a href='javascript:void(0);'>&raquo;</a></li>";
			}
			$paginate.= "</ul>";		
	
	
		}
		echo $paginate;
?>
<!--  ==========================pagination ends here=========================  -->
                            
                        </div>
			 <!-- END .pagination_wrapper -->
                        </div>
                        
                        
					</div><!-- END .panel_content -->
				</div>
			</div><!-- END .datapanel_container -->

		</div><!-- END .main-wraps -->
</section><!-- END .full-site-container -->
<?php require_once("application/layout/adminFooter_new.php"); ?>

<script type="text/javascript">
	$(document).ready(function() {	
	    $('.accordionButton').click(function() {
	        $('.accordionButton').removeClass('on');
	        $('.accordionContent').slideUp('normal');
	        $('.plusMinus').text('+');
	        if($(this).next().is(':hidden') == true) {
	            $(this).addClass('on');
	            $(this).next().slideDown('normal');
	            $(this).children('.plusMinus').text('-');
	         } 
	     });
	    $('.accordionButton').mouseover(function() {
	        $(this).addClass('over');
	    }).mouseout(function() {
	        $(this).removeClass('over');
	    });
	    $('.accordionContent').hide();
	});
</script>
<script type="text/javascript">
    var leftHeight = $('.main-wraps').height();
    $('.side-section').css({'height':leftHeight});
</script>

<script type="text/javascript" src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>

<script type="text/javascript" src="public/admin/js/scoopr.scripts.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.setup.min.js"></script>
<script type="text/javascript" src="public/admin/js/scoopr.plugins.js"></script>


</body>
</html>