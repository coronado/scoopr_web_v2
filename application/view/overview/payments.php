<?php
#--- required initializations (init)---
$controller_data = $vars;
$no_of_record    = $controller_data['listings']['total_count'];
$paginated_array = $controller_data['listings']['paginated_array'];
$links           = $controller_data['listings']['pagination_links'];
$num_paginated_rec = $controller_data['listings']['num_paginated_rec'];
$params          = $this->getrequest();
$pageNo          = $params['page_no'];

//echo "<pre>"; print_r($paginated_array); die;
#---------(/init)----------------------
?>
<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<title>Scoopr</title>
	<base href="<?php echo PATH;?>">
	<?php require_once("application/layout/headerContent.php"); ?> 
	<link rel="stylesheet" href="css/colorbox.css" />
	<script src="js/jquery.colorbox.js"></script>
<style>
.details {
padding: 0px;
margin: 0px;
color: #5ab7ce;
font-size: 14px;

}
</style>
<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				//$(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
				$(".group").colorbox({rel:'group'});

          
           			
				$(".details").colorbox({
				opacity:0,
				innerWidth:'500px',
				innerHeight:'200px'
				});

    });
</script>

	</head>
	<body id="payment-listing">
<?php require_once("application/layout/header.php"); ?>  
        
<?php require_once("application/layout/dashboardSubHeader.php"); ?>  
		
<section class="webaccount3">
       			 <div class="arrow_div">
                 		<div class="inn_main_arrow">
                        	<aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>
                          	<!--<aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>
                            <aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>
                            <aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>
                            <aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>-->
                        </div>
         </div>
                 <div class="arrow_nav_div">
                 		<div class="innav_div">
                        	<div class="btn_overview_div"><div class="btn-grey2"><a href="/overview" title="Overview">Overview</a></div></div>
                            <div  class="btn_payment_div"><div class="btn-red2"><a href="/overview/payments" title="Payments">Payments</a></div></div>
                            <!--<div  class="btn_payment_div"><div class="btn-grey2"><a href="#">&nbsp;</a></div></div>-->
                        </div>
                  </div>
    </section>




    
 
<section style="  width:100%;display:block; ">
	<section class="webaccount6">


<!-- ========================  upper pagination section ========================   --> 
<?php if($no_of_record>$num_paginated_rec) { ?> 
	<section class="section_paging_and_brands page-padds ">
                   	  <div class="paging_bar">
                       		<aside class="section_numbering_1" >
                        	<?php foreach ($links['rest_pages'] as $ky=>$value)  
				{	
				$class = "fix_grey"; 
				if($current_page_no==$value) { $class = "fix_red"; } 
				//echo '<span><a href="#"  class="fix_grey">2</a></span>';	
				echo "<span><a href='/overview/payments/page_no/".$value."' class='".$class."'>".$value."</a></span>     ";
			 	} ?>
                        	</aside>

                       <?php if($links['previous']!="") { 
			echo "<aside class='section_arrow_left' >
				<a href=/overview/payments/page_no/".$links['previous_link']."><img  align='absmiddle' src='images/assignment_page/arrow_left.png' width='17' height='18' alt=''></a>
			      </aside>";

			} ?>

			<?php if($links['next']!="") {
                        echo "<aside class='section_arrow_right'>
				<a href=/overview/payments/page_no/".$links['next_link']."><img align='absmiddle' src='images/assignment_page/arrrow_right.png' width='17' height='18' alt=''></a>
			</aside>";
			} ?>

                      </div>
             </section>
<?php } ?>
<!-- ================================ (END) =============================   --> 



<!-- ========================  middle content listing ========================   --> 
      	<article class="inner_main_content">
      		<div class="payment_tbl">
			<table width="94%" border="0" cellpadding="0" cellspacing="0" id="payment_tbl">

				<?php if($no_of_record>0) { ?>
  				<tr>
    					<th width="14%" style="padding-left:3px;">Assignment</th>
    					<th width="13%">Date</td>
    					<th width="23%">Payment made to </th>
    					<th width="21%">Photographer </th>
    					<th width="17%">Amount/Reward</th>
    					<th width="12%">Image</th>
  				</tr>

				<?php foreach ($paginated_array as $content) { ?>
  				<tr>
    					<td><?php echo str_pad($content['AssignmentId'],3,'0',STR_PAD_LEFT); ?></td>
    					<td><?php echo date('Y-m-d',strtotime($content['paymentDate'])); ?></td>
    					<td><?php echo $content['paymentMadeTo']; ?></td>

    					<!--<td><a class="details" href="overview/creatorDetails/creator_id/<?php echo $content['creatorId']; ?>"><?php echo $content['profileName']; ?></a></td>-->

					<td><?php echo $content['name']; ?></td>

					<?php if($content['paymentMethod']==1) { 
    						echo "<td>$".$content['paymentAmount']."</td>";
					} elseif($content['paymentMethod']==2) { 
						echo "<td>".'Reward'."</td>";
					} elseif($content['paymentMethod']==3) { 
						echo "<td>$".$content['paymentAmount']." &nbsp; with reward</td>";	
					} else {
						echo "<td>$".$content['paymentAmount']."</td>";
					}
					?>

    					<td><a class="group" href="images/assign_img_submission/<?php echo $content['imageName']; ?>"><img src="images/assign_img_submission/small_size_image/<?php echo $content['imageName']; ?>" width="107" height="71" border="0"></a></td>
  				</tr>
				
				<?php } } else { ?>
					<tr>
						<th colspan="6" style="padding-top:14%; padding-bottom:14%;">No records available!</th>
					</tr>
				<?php } ?>


			</table>
		</div>
	</article>
<!-- ================================ (END) =============================   --> 





<!-- ========================  lower pagination section ========================   --> 
<?php if($no_of_record>$num_paginated_rec) {  ?> 
	<section class="section_paging_and_brands page-padds">
                   	  <div class="paging_bar">
                       		<aside class="section_numbering_1" >
                        	<?php foreach ($links['rest_pages'] as $ky=>$value)  
				{	
				$class = "fix_grey"; 
				if($current_page_no==$value) { $class = "fix_red"; } 
				//echo '<span><a href="#"  class="fix_grey">2</a></span>';	
				echo "<span><a href='/overview/payments/page_no/".$value."' class='".$class."'>".$value."</a></span>     ";
			 	} ?>
                        	</aside>

                       <?php if($links['previous']!="") { 
			echo "<aside class='section_arrow_left' >
				<a href=/overview/payments/page_no/".$links['previous_link']."><img  align='absmiddle' src='images/assignment_page/arrow_left.png' width='17' height='18' alt=''></a>
			      </aside>";

			} ?>

			<?php if($links['next']!="") {
                        echo "<aside class='section_arrow_right'>
				<a href=/overview/payments/page_no/".$links['next_link']."><img align='absmiddle' src='images/assignment_page/arrrow_right.png' width='17' height='18' alt=''></a>
			</aside>";
			} ?>

                      </div>
             </section>
<?php } ?>

<!-- ================================ (END) =============================   --> 

</section>
</section>

        
    
<!--Body Ends Here-->

<?php require_once("application/layout/footer.php"); ?>  

<!--Footer Ends Here-->
<div style="display: none;" > </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>