<?php 
//echo "<pre>"; print_r($imagery_name); die;

//$details['hope_to_select']==2;
//echo "<pre>"; print_r($details); die;
$imagery_for_arr = unserialize($details['imageryFor']);
//echo "<pre>"; print_r($imagery_for_arr); die;
$year_month_day = explode('-',$details['submissionDate']);
list($year,$month,$day) = $year_month_day;
$mode_type = "create";
if($mode=='edit' && $this->getsession('assignment_id')!='')  {
	$mode_type = "edit";
	$days = $details['submissionDeadline'];
	$submissionDeadline = date('m/d/Y',(strtotime($details['launchedDate']." +$days day")));
}
$saved_imagery_format = unserialize($details['imageryFormat']);


 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="">
	<meta name="author" content="">
	<base href="<?php echo PATH; ?>">
	<title>Scoopr</title>
	<?php require_once("application/layout/v2/headerContent.php"); ?>
	<!-- Fonts -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
	<link rel="stylesheet" href="public/fonts/novecento/stylesheet.css">
	<link rel="stylesheet" href="public/fonts/baronneue-bold/stylesheet.css">
	<link rel="stylesheet" href="public/icons/glyphicons/style.css">
	<link rel="stylesheet" href="public/icons/font-awesome/font-awesome.min.css">


	<!-- Styles -->
	<!--link rel="stylesheet" href="public/css/bootstrap-modal.css"-->
	<link rel="stylesheet" href="public/css/bootstrap.css">
	<link rel="stylesheet" href="public/css/style.css">
	<link rel="stylesheet" href="public/css/scoopr.css">
	<link rel="stylesheet" href="public/css/stylesheet.css">

	<!-- Plugins -->
	<link rel="stylesheet" href="public/plugins/royalslider/royalslider.min.css">
	<link rel="stylesheet" href="public/plugins/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="public/plugins/mfp/jquery.mfp.css">
	<link href="public/css/jquery.datepick.css" rel="stylesheet">


<script type="text/javascript">
editable = "<?php echo $mode_type; ?>";
	// Run the script on DOM ready:
	$(function(){
		//$('input').customInput();

		//initCanvas();
$('.upload_box').click(function(){ 
	var fileDiv = $(this);
	if(fileDiv != undefined){
      var fileInput = $("#upload_" + fileDiv.attr('id'));
      fileInput.change(function(e) {
	
         var files = this.files;
	 if(editable=='create')	{ $("#assignmentFrm").valid();	}
         showThumbnail(files, fileDiv);
      });

	$(fileInput).show().focus().click().hide();
   
   }
  });
		
	});
	</script>

<script type="text/javascript">
function showThumbnail(files, fileDiv) {
   for ( var i = 0; i < files.length; i++) {
      var file = files[i];
      var imageType = /image.*/;
      if (!file.type.match(imageType)) {
         alert("Not an Image");
         continue;
      }

      var image = document.createElement("img");
        
	image.width = '190';
	image.height = '150';
      // image.classList.add("")
      image.file = file;
      $(fileDiv).html(image);
      var reader = new FileReader();
      reader.onload = (function(aImg) {
         return function(e) {
            aImg.src = e.target.result;
         };
      }(image));
      var ret = reader.readAsDataURL(file);
      
   }
}
</script>

<script type="text/javascript" src="public/plugins/form/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/assignment.js"></script> 
<style>
.error{color:#a94442;}
</style>
</head>

<body class="dashboard_body">

<?php require_once("application/layout/v2/inner-page-header.php"); ?>

<section class="full-site-container">
		
<?php require_once("application/layout/v2/left.php"); ?>

		<div class="main-wraps">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-gutter">
				
				<?php require_once("application/layout/v2/top-search.php"); ?>

				<header class="account-name">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 acc-name">
						<p>Create Assignment <span>/ Design Assignment</span></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 location-head text-right">
						<p>YOU ARE HERE: <span>Scoopr</span>/ Create Assignment</p>
					</div>
				</header><!-- END .account-name -->
				<div class="steps_form">
					<h1>Create a new assignment</h1>
					<ul>
						<li class="one_step active"><span class="active"><i>1</i></span>Design Assignment</li>
						<li class="two_step"><span><i>2</i></span>Preview Assignment</li>
						<li class="three_step"><span><i>3</i></span>Launch Assignment</li>
					</ul>
				</div><!-- END .steps_form -->
			</div>

			<!-- <html> -->

			 <form action="" name="assignmentFrm" id="assignmentFrm" method="POST" enctype="multipart/form-data">
			<div class="content_dash_3">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cont_dash">
					<h1>Step 1. Design brief</h1>
					<h2>Describe what you need to our community of image creators.</h2>
					<p>To achieve the perfect image or video your looking for, our creators need to know exactly what you have in mind. Be as specific as possible. By doing so, your submission gallery will be exactly or at least close to what your searching for.</p>
					<h3>We’re looking for:</h3>
					<span class="notice_box">
						<textarea name="looking_for_text" id="looking_for_text" placeholder="This is where you describe exactly what your looking for. Feel free to speak naturally and informative for all aspects of the imagery your looking for. Try to specify the subject matter, mood, color, vertical, horizontal, candid or staged. If you need room for text/copy, be sure to mention that as well. Remember, the more detailed you are with your vision of the perfect shot, the better chances of receiving submissions that are closest to what your searching for." ><?php echo $details['lookingForText']; ?></textarea>
					</span>
					<h3>Out of all the submissions we hope to select:</h3>
					
					   <p class="radio"> 
					    <input type="radio" id="single" name="hope_to_select" value="1" <?php if(@$details['hopeToSelect']!=2) { echo "checked"; } ?>/>
					    <label for="male">A single image</label>
					    <input type="radio" id="multiple" name="hope_to_select" value="2" <?php if($details['hopeToSelect']==2) { echo "checked"; } ?>/>
					    <label for="female">Multiple images</label>
					   </p>
					
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-gutter">
						<h3>Our budget for each image is:</h3>
						<input type="text" name="our_budget" id="our_budget" value="<?php echo $details['ourBudget']; ?>">
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-gutter">
						<h3>Our brand reward for each image is:</h3>
						<input type="text" name="rewards" id="rewards" value="<?php echo $details['rewards']; ?>">
					</div>
					<p>We have the ability to track posts, shares and retweets etc. Maximize your user engagement by incentivising them to share.</p>
					<h3>By sharing the completion of this assignment on Social Media, we will reward you with an additional :</h3>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-gutter">
						<h4>10-249 shares/retweets:</h4>
						<input type="text" name="share_condition_1" id="share_condition_1" value="<?php echo $details['shareCondition1']; ?>">
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-gutter">
						<h4>250-499 shares/retweets:</h4>
						<input type="text" name="share_condition_2" id="share_condition_2" value="<?php echo $details['shareCondition2']; ?>">
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-gutter">
						<h4>500+ shares/retweets:</h4>
						<input type="text" name="share_condition_3" id="share_condition_3" value="<?php echo $details['shareCondition3']; ?>">
					</div>
					<h5>Keep in mind that you are not obligated to award payment to any creators, unless you decide to license the imagery. We also suggest to price fairly, for the effort it takes our creators to go out and shoot. The more intisive the budget, the more our shooter will want to go out and compete to win your assignment.</h5>


					<h3>Submission Deadline:</h3>
	                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-gutter">
						
					<input name="submission_deadline" class="cal" type="text" id="inlineDatepicker"  placeholder="MM/DD/YYYY" value="<?php echo $submissionDeadline; ?>"/>
<!--<select name="submission_deadline" id="submission_deadline">
                    <option value="">Deadline</option>
		    <option value="7" <?php if($details['submissionDeadline']==7) { echo "selected"; } ?>>7 days</option>
		    <option value="14" <?php if($details['submissionDeadline']==14) { echo "selected"; } ?>>14 days</option>
		    <option value="21" <?php if($details['submissionDeadline']==21) { echo "selected"; } ?>>21 days</option>	
		    <option value="28" <?php if($details['submissionDeadline']==28) { echo "selected"; } ?>>28 days</option>		
                  </select>-->
					</div>
                
                
                
                    
                    
					<div id="inlineDatepicker"></div>
                    
                    <h3>Category</h3>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-gutter" >
					
			<select name="category_id" id="category_id">
                       <option value="">Category</option>
		    <?php for($i=0; $i<count($categories); $i++) { ?>	
				 <option value="<?php echo $categories[$i]['AssignmentCategoryId']; ?>" <?php if($details['AssignmentCategoryId']==$categories[$i]['AssignmentCategoryId']) { echo "selected"; }?>><?php echo $categories[$i]['categoryName']; ?></option>
		    <?php } ?>	
                        
                        </select>
					</div>
                    
                    
					<h3>We will be using the imagery for:</h3>
						
							<p class="radio">

	<?php 
	$k = 5;
	for($i=0; $i<count($imagery_name); $i++) { 
	$j = $i+1;
	$k++;
	$checkIds = "test".($i+1);	
	?>
		<i>
				<input type="checkbox" name="imagery_for[]" id="<?php echo $checkIds; ?>" value="<?php echo $imagery_name[$i]['ImageryForid']; ?>" <?php if(in_array($imagery_name[$i]['ImageryForid'],$imagery_for_arr)) { echo "checked"; } ?>/>
			<label for="<?php echo $checkIds; ?>"><?php echo $imagery_name[$i]['imageryForName']; ?></label>
		</i>

		<?php if($j%4==0) { 
			echo '</p> <p class="radio">';	
		} ?>
	<?php } ?>
							</p>
<p><label for="imagery_for[]" generated="true" class="error"><?php echo @$errors['imagery_for']; ?></label></p>
							
						
					<span class="notice_box"><textarea name="imagery_for_text" id="imagery_for_text"  placeholder="* Optional: Please describe your final usage for the imagery (how you will be using the licensed photo) If you are not sure, please descibe a general usage then."><?php echo $details['imageryForText']; ?></textarea></span>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-gutter">
						<h3>We require a model release:</h3>
						
							<p class="radio"> 
								<input type="radio" id="modelyes" name="model_release" value="yes" <?php if(@$details['modelRelease']!='no') { echo "checked"; } ?>/>
								<label for="modelyes">Yes</label>
								<input type="radio" id="modelno" name="model_release" value="no"  <?php if($details['modelRelease']=='no') { echo "checked"; } ?>/>
								<label for="female">No</label>
							</p>
						
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-gutter">
						<h3>Imagery Format:</h3>
						
							<p class="radio"> 
								<input type="checkbox" id="male9"  name="imagery_format[]" value="1"  <?php if(in_array('1',$saved_imagery_format)) { echo "checked"; } ?>/>
								<label for="male9" style="min-width:113px;">Horizontal</label>
								<input type="checkbox" id="female9"  name="imagery_format[]" value="2"  <?php if(in_array('2',$saved_imagery_format)) { echo "checked"; } ?>/>
								<label for="female9" style="min-width:113px;">Vertical</label>

								<!--<input type="checkbox" id="female2"  name="imagery_format[]" value="3"  <?php if(in_array('3',$saved_imagery_format)) { echo "checked"; } ?>/>
								<label for="female2" style="min-width:100px;">Square</label>

								<input type="checkbox" id="female3"  name="imagery_format[]" value="4"  <?php if(in_array('4',$saved_imagery_format)) { echo "checked"; } ?>/>
								<label for="female3" style="min-width:100px;">All</label>-->
							</p>
							<p><label for="imagery_format[]" generated="true" class="error"></label></p>
						
					</div>
					<h3>Upload reference images:</h3>
					<p>These images are to give our image creators a similar sense of what your looking for. </p>


					<div class="col-xs-12 col-sm-4 col-md-2 col-lg-2 upload-main_wrap">
						<div class="upload_box" id="image1">
							<img height="130" width="200" src="<?php if($details['image1']=='') { echo "images/upload.gif"; } else { echo "images/reference_img/".$details['image1']; } ?>" /></div>
						<input class="fileupload" type="file" name="upload_image[]" id="upload_image1" style="display: none;">
						
					</div>


					<div class="col-xs-12 col-sm-4 col-md-2 col-lg-2 upload-main_wrap">
						<div class="upload_box" id="image2">
							<img height="130" width="200" src="<?php if($details['image2']=='') { echo "images/upload.gif"; } else { echo "images/reference_img/".$details['image2']; } ?>" /></div>
						<input class="fileupload" type="file" name="upload_image[]" id="upload_image2" style="display: none;">
						
					</div>


					<div class="col-xs-12 col-sm-4 col-md-2 col-lg-2 upload-main_wrap">
						<div class="upload_box" id="image3">
							<img height="130" width="200" src="<?php if($details['image3']=='') { echo "images/upload.gif"; } else { echo "images/reference_img/".$details['image3']; } ?>" /></div>
						<input class="fileupload" type="file" name="upload_image[]" id="upload_image3" style="display: none;">
						
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 upload-main_wrap">
						<span class="notice_box"><textarea name="reference_image_text"  id="reference_image_text" placeholder="Explain what interests you about the reference images you uploaded. It could be the mood, feeling, colors, location, expressions or you might want to recreate the reference image entirely. Be as specific as possible."><?php echo $details['referenceImageText']; ?></textarea></span>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 submit no-gutter btn-design">
						<input type="submit" name="next" id="next" value="Next" ></input>
						<input type="submit" name="save_now" id="save_now" value="Save Now" ></input>
						<p>Note: You can always edit, upload files and update your assignment after you have launched. </p>
					</div>
				</div>
			</div>
			</form>
		</div><!-- END .main-wraps -->
</section><!-- END .full-site-container -->

<?php require_once("application/layout/v2/footer.php"); ?>








<script type="text/javascript">
$(function(){
	$.fn.fancyRadio = function(){
		return $(this).each(function(){
			var p = $(this),
				container = $('<span class="radio-container"/>'),
				radio = $('<span class="radio"/>');
			p.find('input[type="radio"]').wrap(container);
			p.find('span.radio-container').append(radio);
			p.find('input:checked').parent()
									.find('span.radio').addClass('selected');
			p.find('input[type="radio"]').on('click',function(){
				p.find('span.selected').removeClass('selected');
				$(this).parent().find('span.radio').addClass('selected');
			});
		});
	};
	$('p.radio').fancyRadio();
	 var leftHeight = $('.main-wraps').height();
    $('.side-section').css({'height':leftHeight});
});
</script>

<script src="public/js/jquery.plugin.js"></script>
<script src="public/js/jquery.datepick.js"></script>

<script>
$(function() {
	$('#popupDatepicker').datepick();
	$('#inlineDatepicker').datepick({onSelect: showDate});
});

function showDate(date) {
	//alert('The date chosen is ' + date);
}
</script>
<script type="text/javascript" src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="public/js/scoopr.setup.min.js"></script>
<script type="text/javascript" src="public/js/scoopr.scripts.min.js"></script>
<script type="text/javascript" src="public/js/scoopr.plugins.js"></script>

<script>
$(".checkbox").click(function(){
    $(this).toggleClass('checked')
});
</script>




</body>
</html>
