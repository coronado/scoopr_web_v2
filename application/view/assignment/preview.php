<?php
if($result['ourBudget']!='' && $result['rewards']=='') {
	$earn = "$".$result['ourBudget'];
} elseif($result['rewards']!='' && $result['ourBudget']=='') {
	$earn = "Brand reward";
} elseif($result['rewards']!='' && $result['ourBudget']!='') {
	$earn = "$".$result['ourBudget']." + Brand reward";
}

$nameStr = "";
foreach($imagery_for_array as $key => $name) 
{ 
	$nameStr .= $name.", ";
}

//echo "<pre>"; print_r($result); die;
?>
<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<meta name="description" content="">

	<meta name="author" content="">


	<base href="<?php echo PATH; ?>">
	<title>Scoopr</title>
	<?php require_once("application/layout/v2/headerContent.php"); ?>


	<!-- Fonts -->

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">

	<link rel="stylesheet" href="public/fonts/novecento/stylesheet.css">

	<link rel="stylesheet" href="public/fonts/baronneue-bold/stylesheet.css">

	<link rel="stylesheet" href="public/icons/glyphicons/style.css">

	<link rel="stylesheet" href="public/icons/font-awesome/font-awesome.min.css">

	



	<!-- Styles -->

	<!--link rel="stylesheet" href="public/css/bootstrap-modal.css"-->

	<link rel="stylesheet" href="public/css/bootstrap.css">

	<link rel="stylesheet" href="public/css/style.css">

	<link rel="stylesheet" href="public/css/scoopr.css">
	<link rel="stylesheet" href="public/css/stylesheet.css">


	<!-- Plugins -->

	<link rel="stylesheet" href="public/plugins/royalslider/royalslider.min.css">

	<link rel="stylesheet" href="public/plugins/owlcarousel/owl.carousel.min.css">

	<link rel="stylesheet" href="public/plugins/mfp/jquery.mfp.css">



</head>



<body class="dashboard_body">



<?php require_once("application/layout/v2/inner-page-header.php"); ?>


<section class="full-site-container">


		<?php require_once("application/layout/v2/left.php"); ?>

		<div class="main-wraps">


			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-gutter">


				<?php require_once("application/layout/v2/top-search.php"); ?>

				<header class="account-name">

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 acc-name">

						<p>Create Assignment <span>/ Preview and Launch</span></p>

					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 location-head text-right">

						<p>YOU ARE HERE: <span>Scoopr</span>/ Create Assignment</p>

					</div>

				</header><!-- END .account-name -->

				<div class="steps_form">

					<h1>Preview and Launch</h1>

					<ul>

						<li class="one_step"><span><i>1</i></span>Design Assignment</li>

						<li class="two_step active"><span class="active"><i>2</i></span>Preview & Launch Assignment</li>

						<li class="three_step"><span><i>3</i></span>Confirmation</li>

					</ul>

				</div><!-- END .steps_form -->

			</div>

			<div class="content_dash_3">

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cont_dash">

					<h1>Step 2. Preview and launch your assignment</h1>

					<h2>Assignment listing preview</h2>

					<p>This is how our shooters will see your assignment on the Scoopr App’s assignment feed.</p>

                    

                    <div class="active_box">

                    	<div class="header_actve">

                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 profile_customize">

                                <img src="images/profile_img/<?php echo $result['profileImg']; ?>" class="img-circle">

                            </div>

                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 explain_customize">

                                <h3><?php echo $result['profileName']; ?>.</h3>

                                <p><?php echo $result['tagLine']; ?><br>

                                <span>Earn: <?php echo $earn; ?></span>  Submission Deadline: <?php echo $result['submissionDeadline']." days"; ?></p>

                            </div>

                        </div>

                    </div>

                    

					<h3>Full Assignment View:</h3>

					<p>This is how our shooters will see your assignment in it’s full glory.</p>



					<div class="active_box">

                    	<div class="header_actve">

                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 profile_customize">

                                <img src="images/profile_img/<?php echo $result['profileImg']; ?>" class="img-circle">

                            </div>

                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 explain_customize">

                                <h3><?php echo $result['profileName']; ?>.</h3>

                                <p><?php echo $result['tagLine']; ?><br>

                                <span>Earn: <?php echo $earn; ?></span>  Submission Deadline: <?php echo $result['submissionDeadline']." days"; ?></p>

                            </div>

                        </div>

                        <div class="active_body">

                            <h2>The Assignment:</h2>

                            <p><?php echo $result['lookingForText']; ?></p>


                            <h2>Earn:</h2>

                            <p><?php echo $earn; ?></p>


			<?php if($result['rewards']!='') { ?>		
                            <h2>Brand Reward:</h2>

			    <p><?php echo $result['rewards']; ?></p>
	
			<?php } ?>


                            <h2>Earn by sharing completed assignment:</h2>

                            <p>10-249      shares/retweets:   <?php echo $result['shareCondition1']; ?><br>

                            250-499   shares/retweets:    <?php echo $result['shareCondition2']; ?><br>

                            500+          shares/retweets:   <?php echo $result['shareCondition3']; ?></p>


                            <h2>Format/Frame:</h2>

                            <?php foreach($imagery_format_array as $key1 => $name1) { ?>
				<p><?php echo $name1; ?></p>
			    <?php } ?>

                            <h2>We are using it for:</h2>

			    <p><?php echo trim($nameStr,', '); ?></p>


                            <h2>Model Release Required:</h2>

                            <p><?php echo ucfirst($result['modelRelease']); ?></p>


                            <h2>We will be selecting:</h2>

                            <p><?php if($result['hopeToSelect']==1) { echo "Single Image"; } else { echo "Multiple Images"; } ?></p>



			<?php if($result['image1']=='' && $result['image2']=='' && $result['image3']=='') { ?>		
                            <h2>Reference Images/Top Submissions: Not Available</h2>

			<?php }  else { ?>
				<h2>Reference Images/Top Submissions:</h2>

			<?php } ?>
                            
				<p><?php echo $result['referenceImageText']; ?> </p>


                            <ul>

				<?php if($result['image1']!='') { ?>
                                <li><img src="images/reference_img/<?php echo $result['image1']; ?>" width="184" height="137"></li>

				<?php } ?>

				<?php if($result['image2']!='') { ?>
                                <li><img src="images/reference_img/<?php echo $result['image2']; ?>" width="184" height="137"></li>

				<?php } ?>

				

                            </ul>


                        </div>

                    </div>



					<h3>*Share your assignment on your social media accounts to let your community know:</h3>

					<p>To be able to simoltaneously post to all platforms, please add social Media platform Logins within your account settings. Its easy and just takes a second.</p>

                    <form class="inline_radio">

                        <p class="radio">

                        <span class="radio-container"><input type="radio" id="male" name="facebook" value="male"><span class="radio"></span></span>

                        <label for="male">FACEBOOK</label>

                        </p>

                        <p class="radio">

                        <span class="radio-container"><input type="radio" id="female" name="twitter" value="female"><span class="radio selected"></span></span>

                        <label for="female">TWITTER</label>

                        </p>

                        

                        <p class="radio"> 

                        <span class="radio-container"><input type="radio" id="female" name="instagram" value="female"><span class="radio selected"></span></span>

                        <label for="female">INSTAGRAM</label>

                        </p>

                        

                        <p class="radio">

                        <span class="radio-container"><input type="radio" id="female" name="googleplus" value="female"><span class="radio selected"></span></span>

                        <label for="female">GOOGLE+</label>

                        </p>

                        

                        <p class="radio"> 

                        <span class="radio-container"><input type="radio" id="female" name="tumblr" value="female"><span class="radio selected"></span></span>

                        <label for="female">TUMBLR</label>

                        </p>

                    </form>

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 submit">

						<a href="/assignment/launchit">Launch</a>

						<a href="/content/assignments">Save now</a>

						<a href="/assignment/edit/id/<?php echo $_SESSION['assignment_id']; ?>">Edit</a>

						<h4>Note: You can always edit, upload files and update your assignment after you have launched. </h4>

					</div>

				</div>

			</div>

		</div><!-- END .main-wraps -->

</section><!-- END .full-site-container -->



<?php require_once("application/layout/v2/footer.php"); ?>
















</body>

</html>