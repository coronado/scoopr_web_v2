<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Scoopr</title>
	<?php require_once("application/layout/headerContent.php"); ?> 
	<link rel="stylesheet" type="text/css" href="css/elastislide.css" />
	<script src="js/modernizr.custom.17475.js"></script>
	</head>
	<body>

<?php require_once("application/layout/header.php"); ?> 
<?php require_once("application/layout/dashboardSubHeader.php"); ?>  
	
<section class="webaccount3">

  <div class="arrow_div">
    <div class="inn_main_arrow">
      <aside>&nbsp;</aside>
      <aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>
  </div>
  </div>

  <div class="arrow_nav_div">
<?php if(!empty($assignment_ids)) { ?>
  <div class="innav_div">

    <ul id="carousel" class="elastislide-list">

	<?php foreach($assignment_ids as $id) { ?>
      		<li class="<?php if($assignment_id==$id) { echo "btn-red_small"; } else { echo "btn-grey_small"; }?>"><a href="/content/submission/assignment_id/<?php echo $id; ?>"><?php echo str_pad($id,3,"0",STR_PAD_LEFT); ?></a></li>

	<?php } ?>
    </ul>

  </div>

<?php } else { ?>
  <div class="innav_div" style="line-height:30px;">

    	&nbsp;
  </div>

<?php } ?>
</section>

    
   
         <section class="webaccount5">
         		<article class="inner_main_content">
                	<div class="view_submission" >
                	<section class="section_brand">
                    	<article class="article_left">
                        	<aside class="red_ball_section"><img src="images/assignment_page/red_ball.png" width="78" height="78" alt=""></aside>
                        	<aside class="content_section_vw_sub">
                            	<h1>Brand Inc</h1>
                                <h2>Posted by: John Smith</h2>
                                <h3>Submission Deadline: 9/12/13.  9am EST</h3>
                                <h4>View Assignment Details </h4>
                            </aside>
                        </article>
                        
                    	<p><img src="images/assignment_page/partition_line.png" width="888" height="24" alt=""></p>
                    </section>                     
                </div>


                	   <section class="single_submission">
                 	<article class="order_summary_left_section">
                   <div class="order_summary_hd"> Your order Summary </div>
                    <table width="90%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <th width="23%">Description</th>
    <th>&nbsp;</th>
    <th>Amount</th>
  </tr>
  <tr>
    <td><img src="images/order_thumb_img.png" width="129" height="85"></td>
    <td width="54%">High Resulation Royalty Free License<br>
      Delivery; Download Link<br>
      Single Image<br>
      Model Release<br>
      image Size: 4500x3000<br>
      Photographer: @johnsmith</td>
    <td width="23%">$250.00 USD</td>
  </tr>
  <tr>
    <td >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="os_subtotal">
    <td >Subtotal:</td>
    <td>&nbsp;</td>
    <td>$250.00</td>
  </tr>
  <tr class="os_gap">
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    </tr>
  <tr class="os_total">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Total: $250.00</td>
  </tr>
</table>
</article>


                    	<article class="order_summary_right_section">
	                        <div style="margin-top:40%;">
        	        	        <div class="pay_pal"><img src="images/pay_pal.png"></div>
                	        </div>
				<div style="height:120px;">&nbsp;</div>
                        </article>

                 </section>
                </article>
         </section>
    
<!--Body Ends Here-->

<script type="text/javascript" src="js/jquerypp.custom.js"></script> 
<script type="text/javascript" src="js/jquery.elastislide.js"></script> 
<script type="text/javascript">
	$( '#carousel' ).elastislide();
</script>


<?php require_once("application/layout/footer.php"); ?> 
 
<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>