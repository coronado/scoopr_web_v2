<!doctype html>
<html>
<head>
<meta charset="utf-8">
<base href="<?php echo PATH; ?>">
<title>Assignment submission</title>
<link href="css/lp_style.css" rel="stylesheet" type="text/css">
<?php require_once("application/layout/headerContent.php");?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="js/customInput.jquery.js"></script>
<script type="text/javascript" src="js/assignment.js"></script> 
<style>.error{color:#FF0000;}</style>
</head>
<body>
<?php require_once("application/layout/header.php");?>
<div id="container">
	<?php
	if(empty($error) && $id > 0)
	{
	?>
	<section>
		<div id="step_block">
			<div class="step_top_block">
				<img src="<?php echo PATH?>images/profile_img/default_profile_img.png"/> <h2><?php echo $profile_name?></h2><br>
				<label>Posted by : <?php echo $full_name?></label><br>
				<b>Submission Deadline : <?php echo date("d/m/y",strtotime($submission_date)).", ".$submission_time?></b><br>
				<div style="float:left;"><a href="#">View assignment Details</a></div>
				<div style="float:right;"><a href="#">Back to Submission</a></div>

			</div>
			
			<!--</div>-->
			<section>
				<div class="step_content_block">
					<div class="content_box_small_div">
					<h3><strong>We are looking</strong></h3>
					<p><?php echo GeneralFunction::assignLookingFor($looking_for);?></p>
				
					<p><?php echo $looking_for_text;?></p>
					<p>&nbsp;</p>

					<h3><strong>Out of all submission we hope to select:</strong></h3>

					<p><?php echo GeneralFunction::assignHopeToSelect($hope_to_select);?></p>
					<p>&nbsp;</p>

					<h3><strong>Our budget for each image is:</strong></h3>
					<p><?php echo CURRENCY.$budget_from;?>&nbsp;&nbsp;&nbsp;<i>per image</i></p>
					<p>&nbsp;</p>

					<p><h3><strong>Submission Deadline:</strong></h3>
					<p> <?php echo date("d/m/y",strtotime($submission_date)).", ".$submission_time?></p>
					<p>&nbsp;</p>

					<p><h3><strong>We will be using imagery for:</strong></h3>
					<p> <?php echo implode("<br>",unserialize($imagery_for));?></p><br>

					<p> <?php echo $imagery_for_text;?></p>
					<p>&nbsp;</p>

					<p><h3><strong>We require a model release:</strong></h3>
					<p><?php echo ucwords($model_release)?></p><br>
					
					<p><h3><strong>Reference images:</strong></h3><br>
					
					<img src="<?php echo GeneralFunction::getReferenceImage($image1)?>" width="226" height="166" alt=""/>
					<img src="<?php echo GeneralFunction::getReferenceImage($image2)?>" width="226" height="166" alt=""/>
					<img src="<?php echo GeneralFunction::getReferenceImage($image3)?>" width="226" height="166" alt=""/>

					<p>Do not try to copy these images. These are for reference only, regarding the feel and vibe. These images have great sense of fun, playfull and sillyness to them.</p><br>

					<p><a href="#">Back to Submission</a></p>

					</div>
				</div>
			</section>
   		</div>
  	</section>
	</form>
	<?php
	}
	else
	{
	?>
		<div class="error"><?php echo $error?></div>
	<?php
	}
	?>
</div>
<!--Body Ends Here-->
<?php require_once("application/layout/footer.php");?>
<!--Footer Ends Here-->
<div style="display: none;" > </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>