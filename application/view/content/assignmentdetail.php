<?php
if($result['lookingFor']==1) {
	$looking_for = "Photography";
} elseif($result['looking_for']==2) {
	$looking_for = "Video";
} else {
	$looking_for = "Both Video & Photography";
}

$key = array_search($assignment_id, array_keys($assignment_ids));
?>
<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Scoopr</title>
	<?php require_once("application/layout/headerContent.php"); ?>

	<link rel="stylesheet" type="text/css" href="css/elastislide.css" />
	<script src="js/modernizr.custom.17475.js"></script>
	<script type="text/javascript" language="JavaScript">
	$( document ).ready(function() {
		var key = '<?php echo $key; ?>';
		if(key<9) {
			$( '#carousel' ).elastislide();
		} else {
			$( '#carousel' ).elastislide()._slide('next');	
		}
	});
	</script>

	</head>
	<body>
	<?php require_once("application/layout/header.php"); ?>        
	<?php require_once("application/layout/dashboardSubHeader.php"); ?>  	




<section class="webaccount3">
  <div class="arrow_div">
    <div class="inn_main_arrow">
      <aside>&nbsp;</aside>
      <aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>
   </div>
  </div>
  <div class="arrow_nav_div">
<?php if(!empty($assignment_ids)) { ?>
  <div class="innav_div">
    <ul id="carousel" class="elastislide-list">
	<?php foreach($assignment_ids as $id => $ref_id) { ?>
      		<li class="<?php if($assignment_id==$id) { echo "btn-red_small"; } else { echo "btn-grey_small"; }?>"><a href="/content/assignmentDetail/assignment_id/<?php echo $id; ?>"><?php echo str_pad($ref_id,3,"0",STR_PAD_LEFT); ?></a></li>
	<?php } ?>
    </ul>
  </div>
<?php } else { ?>
  <div class="innav_div" style="line-height:30px;">&nbsp;</div>
<?php } ?>
</section>

    
   		<section class="webaccount5">
        		<article class="inner_main_content">
                	<div class="view_submission">
                	<section class="section_brand">
                    	<article class="article_left">
                        	<aside class="red_ball_section"><img class="circleImg" src="images/profile_img/<?php echo $buyer_info['profileImg']; ?>" width="76" height="76" alt=""></aside>
                        	<aside class="content_section_vw_sub">
                            	<h1><?php echo $buyer_info['profileName']; ?></h1>
                                <h2>Posted by: <?php if($buyer_info['name']!='') { echo $buyer_info['name']; } else { echo $buyer_info['profileName']; } ?></h2>
                                <h3>Submission Deadline: <?php echo $deadline['submission_date']; ?>  </h3>
                                <a href="/content/assignmentDetail/assignment_id/<?php echo $assignment_id; ?>"><h4>View Assignment Details </h4></a>
                            </aside>
                        </article>
                        <article class="article_right">
                        	<aside class="grey_btn_big"><a href="/content/submission/assignment_id/<?php echo $assignment_id; ?>/fav/1" class="grey_btn_big" title="View Favorites">VIEW FAVORITES</a></aside>
                            <h1><?php echo date('m',strtotime($deadline['created_at']))."/".str_pad($assignment_id,3,"0",STR_PAD_LEFT); ?></h1>
                        </article>
                    	<p><img src="images/assignment_page/partition_line.png" width="1000" height="24" alt=""></p>
                    </section>                     
                </div>
                </article>


                <section>
                	<article class="content_box_mid">
                    		<div class="main_content_left">
                            	<h1>We're Looking For:</h1>
                            </div>
                            <div class="main_content_right"><a href="/content/submission/assignment_id/<?php echo $assignment_id; ?>" title="Back To Submission">Back to submission</a></div>
                            <div class="content_flow">
                            	<h1><?php echo $looking_for; ?></h1>
                             	<p><?php echo $result['lookingForText']; ?></p>
                                
                                <h2>Out of all the submissions we hope to select:</h2>
                                <p><?php if($result['hopeToSelect']==1) { echo "Single Image"; } else { echo "Multiple Images"; } ?></p>  
                                
                                <?php if($result['ourBudget']!='') { ?>
                                 <h2>Our budget for each image is</h2>
                                <p><?php echo "$".$result['ourBudget']; ?> Per image</p> 
				<?php } ?>

				<?php if($result['rewards']!='') { ?>
				  <h2>Rewards</h2>
                                <p><?php echo $result['rewards']; ?> </p> 
				<?php } ?>

				
				<h2><strong>Earn money by sharing completed assignment:</strong></h2>
          			<p>10-249&nbsp;&nbsp;&nbsp;shares/retweets: <?php echo $result['shareCondition1']; ?></p>
	  			<p>250-499&nbsp;shares/retweets: <?php echo $result['shareCondition2']; ?></p>
          			<p>500+&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;shares/retweets: <?php echo $result['shareCondition3']; ?></p>	

                                
                                <h2>Submission Deadline:</h2>
                                <p><?php echo $deadline['submission_date']; ?> <span>(<?php echo $deadline['submission_deadline']; ?>)</span></p> 

				
                                <h2>We will be using the imagery for:</h2>
				<?php foreach($imagery_for_array as $key => $name) { ?>
						<p><?php echo $name; ?></p>
					<?php } ?>

                                 <p><?php echo $result['imageryForText']; ?></p> 
                                 
                                  	<h2>We required a model release</h2>
                                	<p><?php echo ucfirst($result['modelRelease']); ?></p>

				<h2>Format/Frame</h2>
                                <?php foreach($imagery_format_array as $key1 => $name1) { ?>
				<p><?php echo $name1; ?></p>
				<?php } ?>
                
		<?php if($result['image1']=='' && $result['image2']=='' && $result['image3']=='') { ?>
                <h3>Reference images : Not Available</h3>
		<?php } else { ?>
                <h3>Reference images</h3>
		<?php } ?>

                <div class="div100percent">

		<?php if($result['image1']!='') { ?>
          	<div class="div25percent"><img src="images/reference_img/<?php echo $result['image1']; ?>" width="226" height="166" alt="" border="0"></div>
		<?php } ?>

		<?php if($result['image2']!='') { ?>
          	<div class="div25percent"><img src="images/reference_img/<?php echo $result['image2']; ?>" width="226" height="166" alt=""></div>
		<?php } ?>	

		<?php if($result['image3']!='') { ?>
          	<div class="div25percent"><img src="images/reference_img/<?php echo $result['image3']; ?>" width="226" height="166" alt=""></div>
		<?php } ?>
          	</div>

                                      <p><?php echo $result['referenceImageText']; ?></p>
                                        <p>&nbsp;</p>
                                         
                                           
         
	<div class="content_box_small_div">
         <div class="btn_pink"><a href="/assignment/edit/id/<?php echo $assignment_id; ?>"><strong>Edit</strong></a></div>
        </div>

        <p>Note: You can always edit, upload files and update your assignment after you have launched. </p>
        <p><a href="/content/submission/assignment_id/<?php echo $assignment_id; ?>" title="">Back to submissions</a></p>
                    </article>
                </section>
        </section>
         
    
<!--Body Ends Here-->
<script type="text/javascript" src="js/jquerypp.custom.js"></script> 
<script type="text/javascript" src="js/jquery.elastislide.js"></script> 
<script type="text/javascript">
	//$( '#carousel' ).elastislide();
</script>

<?php require_once("application/layout/footer.php"); ?>
<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>