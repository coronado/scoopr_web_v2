<?php
#--- required initializations (init)---
$controller_data = $vars;
$no_of_record    = $controller_data['listings']['total_count'];
$paginated_array = $controller_data['listings']['paginated_array'];
$links           = $controller_data['listings']['pagination_links'];
$num_paginated_rec = $controller_data['listings']['num_paginated_rec'];
$params          = $this->getrequest();
$pageNo          = $params['page_no'];

//echo "<pre>"; print_r($assignment_ids); echo $assignment_id; die;
$key = array_search($assignment_id, array_keys($assignment_ids));
//$key = array_search ($assignment_id, $assignment_ids);
#---------(/init)----------------------
?>
<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<meta name="description" content="">

	<meta name="author" content="">

	<base href="<?php echo PATH; ?>">
	<title>Scoopr</title>
	<?php require_once("application/layout/v2/headerContent.php"); ?>


	<!-- Fonts -->

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">

	<link rel="stylesheet" href="public/fonts/novecento/stylesheet.css">

	<link rel="stylesheet" href="public/fonts/baronneue-bold/stylesheet.css">

	<link rel="stylesheet" href="public/icons/glyphicons/style.css">

	<link rel="stylesheet" href="public/icons/font-awesome/font-awesome.min.css">
	


	<!-- Styles -->

	<!--link rel="stylesheet" href="public/css/bootstrap-modal.css"-->

	<link rel="stylesheet" href="public/css/bootstrap.css">

	<link rel="stylesheet" href="public/css/style.css">

	<link rel="stylesheet" href="public/css/scoopr.css">
	<link rel="stylesheet" href="public/css/stylesheet.css">


	<!-- Plugins -->

	<link rel="stylesheet" href="public/plugins/royalslider/royalslider.min.css">

	<link rel="stylesheet" href="public/plugins/owlcarousel/owl.carousel.min.css">

	<link rel="stylesheet" href="public/plugins/mfp/jquery.mfp.css">
	

<style>
		.folder-group ul li a:hover, .folder-group ul li a.activeassignment { 	color: #5ab7ce; 	font-size: 94%; 	background: url("../img/folder_icon.png") no-repeat left -12px !important; }
	</style>

</head>



<body class="dashboard_body">



<?php require_once("application/layout/v2/inner-page-header.php"); ?>


<section class="full-site-container">


<?php require_once("application/layout/v2/left.php"); ?>


		<div class="main-wraps">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-gutter">

				

			<?php require_once("application/layout/v2/top-search.php"); ?>

				<header class="account-name">

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 acc-name">

						<p>My Account <span>/ Assignment Galleries</span></p>

					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 location-head text-right">

						<p>YOU ARE HERE: <span>Scoopr</span>/ Assignment Galleries</p>

					</div>

				</header><!-- END .account-name -->

			</div>

			<!--<div class="action_container">

				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 nav_dashboard">

					<nav class="dash_nav">

						<ul>

							<li><a href=""><input type="checkbox"><label>Select All</label></a></li>

							<li><a href=""><i class=""></i>Message</a></li>

							<li><a href=""><span class="glyphicon glyphicon-download-alt"></span>Filter images</a></li>

							<li>

								<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email">

							</li>

						</ul>

					</nav>

				</div>

				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 sorting-links">

					<p class="navbar-text navbar-right">

						Show: 

						<a href="#" class="navbar-link">All</a>

						<a href="#" class="navbar-link">Documents</a>

						<a href="#" class="navbar-link">Images</a>

					</p>

				</div>

			</div>-->

			<div class="content-wraps-dash">

				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

					<ul>

						<?php 
						if($no_of_record>0) { 
						$iCount = 0;
						foreach ($paginated_array as $content) {  
						$iCount++;
						?>
							<li>

								<a href="/content/preview/assignment_id/<?php echo $content['AssignmentId']; ?>/fav/<?php echo $fav; ?>/page_no/<?php echo $iCount; ?>"><img style="width:143px; height:96px;"  src="images/assign_img_submission/medium_size_image/<?php echo $content['mediumSizeImage']; ?>" border="0"></a>

								<span>By: <?php echo ucwords($content['full_name']); ?></span>

								<p>Added: <?php echo date('M d. Y',$content['timestamp']); ?></p>

							</li>

						<?php } } else { ?>
							<li style="margin-left:42%; margin-top:10%;">Sorry! No records available.</li>
						<?php } ?>
					</ul>





					<div class="btn-group text-right pagination-dashboard-con">


					<?php foreach($links['rest_pages'] as $ky=>$value) { 
						if($value>3) {
							break;
						}
						?>
						<button type="button" class="btn btn-default" onclick="window.location.href='/content/submission/assignment_id/<?php echo $assignment_id; ?>/page_no/<?php echo $value; ?>'"><?php echo $value; ?></button>

					<?php } ?>	

					<?php if(count($links['rest_pages'])>3) {?>
						<div class="btn-group">

							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">

								Dropdown

								<span class="caret"></span>

							</button>

							<ul class="dropdown-menu">

							<?php foreach ($links['rest_pages'] as $ky=>$value) { ?>
								<li><a href="/content/submission/assignment_id/<?php echo $assignment_id; ?>/page_no/<?php echo $value; ?>"><?php echo $value; ?></a></li>

							<?php } ?>	
							</ul>

						</div>

					<?php } ?>
					</div>

				</div>





				<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 folder-group" >

					<p>License Image</p>

					<div class="heading-folder"><span class="stronghead">Folders</span><span>+Add New Folder</span></div>

					<ul>

						<?php foreach($assignment_ids as $id => $ref_id) { ?>

						<li><a class="<?php if($assignment_id==$id) { echo "activeassignment"; } ?>" href="/content/submission/assignment_id/<?php echo $id; ?>">Assignment <?php echo str_pad($ref_id,4,"0",STR_PAD_LEFT); ?></a></li>

						
						<?php } ?>
						<li><a href="">Favorite Selects</a></li>

					</ul>

					<div class="heading-folder"><span class="stronghead">Tags</span></div>

					<div class="tag_container">

						<a href="javascript:void(0);">Animation</a>

						<a href="javascript:void(0);"">Design</a>

						<a href="javascript:void(0);"">Trailer</a>

						<a href="javascript:void(0);"">Short Film</a>

						<a href="javascript:void(0);"">Dubstep</a>

						<a href="javascript:void(0);"">Photography</a>

						<a href="javascript:void(0);"">Macro</a>

						<a href="javascript:void(0);"">Tutorials</a>

						<a href="javascript:void(0);"">Documentation</a>

					</div><!-- END .tag_container -->

				</div>





			</div><!-- END .content-wraps-dash -->

		</div><!-- END .main-wraps -->

</section><!-- END .full-site-container -->


<?php require_once("application/layout/v2/footer.php"); ?>







</body>

</html>
