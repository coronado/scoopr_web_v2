<?php
#--- required initializations (init)---
/*$controller_data = $vars;
$no_of_record    = $controller_data['listings']['total_count'];
$paginated_array = $controller_data['listings']['paginated_array'];
$links           = $controller_data['listings']['pagination_links'];
$num_paginated_rec = $controller_data['listings']['num_paginated_rec'];
$params          = $this->getrequest();
$pageNo          = $params['page_no'];*/

$this->include_file('common_class.php','application/lib');
$obj = new CommonClass;



$lastpage   = $paginator_arr['lastpage'];
$page       = $paginator_arr['page'];
$targetpage = "/content/assignments";
$prev       = $paginator_arr['prev'];
$stages     = 1;
$LastPagem1 = $paginator_arr['LastPagem1'];
$next       = $paginator_arr['next'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="description" content="">
	<meta name="author" content="">
	<base href="<?php echo PATH; ?>">
	<title>Scoopr</title>
	<?php require_once("application/layout/v2/headerContent.php"); ?>
	<!-- Fonts -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
	<link rel="stylesheet" href="public/fonts/novecento/stylesheet.css">
	<link rel="stylesheet" href="public/fonts/baronneue-bold/stylesheet.css">
	<link rel="stylesheet" href="public/icons/glyphicons/style.css">
	<link rel="stylesheet" href="public/icons/font-awesome/font-awesome.min.css">
	

	<!-- Styles -->
	<!--link rel="stylesheet" href="public/css/bootstrap-modal.css"-->
	<link rel="stylesheet" href="public/css/bootstrap.css">
	<link rel="stylesheet" href="public/css/style.css">
	<link rel="stylesheet" href="public/css/scoopr.css">
	<link rel="stylesheet" href="public/css/stylesheet.css">
	<!-- Plugins -->
	<link rel="stylesheet" href="public/plugins/royalslider/royalslider.min.css">
	<link rel="stylesheet" href="public/plugins/owlcarousel/owl.carousel.min.css">
	<link rel="stylesheet" href="public/plugins/mfp/jquery.mfp.css">

</head>

<body class="dashboard_body">

<?php require_once("application/layout/v2/inner-page-header.php"); ?>

<section class="full-site-container">
		<?php require_once("application/layout/v2/left.php"); ?>

		<div class="main-wraps">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-gutter">
				<?php require_once("application/layout/v2/top-search.php"); ?>
				<header class="account-name">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 acc-name">
						<p>Dashboard <span>/ Assignments</span></p>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 location-head text-right">
						<p>YOU ARE HERE: <span>Scoopr</span>/ Assignments</p>
					</div>
				</header><!-- END .account-name -->

			</div>
			<div class="content_dash_4">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


		<?php 
		foreach ($paginator_arr['paginated_result'] as $content) 
                                    	{
			$iCount++;
		$overview =  $obj->getAssignmentViews($content['AssignmentId']);
		$submissions = $obj->getAssignmentSubmissions($content['AssignmentId']);
		//print_r($content); die;
		  ?>
		    <div style="cursor:pointer;" class="fullhead_dash" onclick="window.location.href='/content/assignmentDetail/aid/<?php echo $content['AssignmentId']; ?>';">
                   	  <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"><a href="/content/assignmentDetail/aid/<?php echo $content['AssignmentId']; ?>" style="color:#000000;"><?php echo str_pad($content['assignmentRefId'],4,"0",STR_PAD_LEFT); ?> <strong>Assignment</strong></a></div>
                      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"><?php echo substr($content['lookingForText'],0,25).".."; ?></div>
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2"><strong>Date:</strong> <?php echo date('m/d/y',strtotime($content['createdAt'])); ?></div>

                      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2"><strong>Views:</strong> <?php echo number_format($overview['views']); ?></div>

                      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2"><strong>Submissions:</strong> <?php echo number_format($submissions); ?></div>
                    </div><!-- END .fullhead_dash -->
		<?php } ?>
                   


<!--  ==========================pagination starts here=========================  -->
<?php
		$paginate = '';
		if($lastpage > 1)
		{	
			$paginate .= " <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 pagination_wrapper'><ul class='pagination'>";
			// Previous
			if ($page > 1)
			{
				$paginate.= "<li><a href='$targetpage/page/$prev'>&laquo;</a></li>";
			}
			
			

			
			// Pages	
			if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
					{
						$paginate.= "<li><a class='active' href='javascript:void(0);'>$counter</a></li>";
					}
					else
					{
						$paginate.= "<li><a href='$targetpage/page/$counter'>$counter</a></li>";
					}					
				}
			}
			elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
			{
			// Beginning only hide later pages
				if($page < 1 + ($stages * 2))		
				{
					for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
					{
						if ($counter == $page)
						{
							$paginate.= "<li><a class='active' href='javascript:void(0);'>$counter</a></li>";
						}
						else
						{
							$paginate.= "<li><a href='$targetpage/page/$counter'>$counter</a></li>";
						}					
					}
					$paginate.= "<a href='javascript:void(0);'>...</a>";

					$paginate.= "<li><a href='$targetpage/page/$LastPagem1'>$LastPagem1</a></li>";

					$paginate.= "<li><a href='$targetpage/page/$lastpage'>$lastpage</a></li>";

				}
				// Middle hide some front and some back
				elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
				{
					$paginate.= "<li><a href='$targetpage/page/1'>1</a></li>";
					$paginate.= "<li><a href='$targetpage/page/2'>2</a></li>";
					$paginate.= "<li><a href='javascript:void(0);'>...</a></li>";
					for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
					{
						if ($counter == $page)
						{
							$paginate.= "<li><a class='active' href='javascript:void(0);'>$counter</a></li>";
						}
						else
						{
						$paginate.= "<li><a href='$targetpage/page/$counter'>$counter</a></li>";
						}					
					}
					$paginate.= "<li><a href='javascript:void(0);'>...</a></li>";
					$paginate.= "<li><a href='$targetpage/page/$LastPagem1'>$LastPagem1</a></li>";
					$paginate.= "<li><a href='$targetpage/page/$lastpage'>$lastpage</a></li>";		
			}
			// End only hide early pages
			else
			{
				$paginate.= "<li><a href='$targetpage/page/1'>1</a></li>";

				$paginate.= "<li><a href='$targetpage/page/2'>2</a></li>";


				$paginate.= "<a href='javascript:void(0);'>...</a>";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
					{
						$paginate.= "<li><a class='active' href='javascript:void(0);'>$counter</a></li>";

					}
					else
					{
						$paginate.= "<li><a href='$targetpage/page/$counter'>$counter</a></li>";

					}					
				}
			}
		}
		

			
			// Next
			if ($page < $counter - 1)
			{ 
				$paginate.= "<li><a href='$targetpage/page/$next'>&raquo;</a></li>";
			}
			
			$paginate.= "</ul></div>";		
	
	
		}
		echo $paginate;
?>
<!--  ==========================pagination ends here=========================  -->

 			


                </div>
            </div>
		</div><!-- END .main-wraps -->
</section><!-- END .full-site-container -->

<?php require_once("application/layout/v2/footer.php"); ?>








</body>
</html>