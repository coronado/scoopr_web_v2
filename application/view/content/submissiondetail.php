<!doctype html>
<html>
<head>
<meta charset="utf-8">
<base href="<?php echo PATH;?>">
<title>Scoopr</title>
<?php require_once("application/layout/headerContent.php");?>
<link rel="stylesheet" type="text/css" href="css/elastislide.css"/>
<script src="js/modernizr.custom.17475.js"></script>
</head>
<body>
<?php require_once("application/layout/header.php");?>
<?php require_once("application/layout/contentHeader.php");?>
<section class="webaccount3">
  	<div class="arrow_div">
    		<div class="inn_main_arrow">
			<aside>&nbsp;</aside>
			<aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>
			<!--<aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>
			<aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>
			<aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside> -->
    		</div>
  	</div>
  	<div class="arrow_nav_div">
		<div class="innav_div">
			<ul id="carousel" class="elastislide-list">
				<?php foreach($assign_ids as $assign_id){ ?>
					<li class="btn-<?php echo ($assign_id==$param_id?'red':'grey')?>_small"><a href="/content/submission/assignid/<?php echo $assign_id?>"><?php echo $assign_id?></a></li>
				<?php 
				}
				?>
			</ul>
		</div>
	</div>
</section>
<?php
echo "<pre>";
//print_r($assign_detail);
//print_r($submission_detail);
echo "</pre>";
?>
<section class="webaccount5">
	<article class="inner_main_content">
		<div class="view_submission">
			<section class="section_brand">
				<article class="article_left">
					<aside class="red_ball_section"><img src="images/assignment_page/red_ball.png" width="76" height="76" alt=""></aside>
					<aside class="content_section_vw_sub">
					<h1><?php echo $assign_detail['profile_name']?></h1>
					<h2>Posted by: <?php echo $assign_detail['full_name']?></h2>
					<h3>Submission Deadline: <?php echo date("d/m/y",strtotime($assign_detail['submission_date'])).", ".$assign_detail['submission_time']?></h3>
					<h4><a href="content/assignment_detail/assignid/<?php echo $assign_detail['id']?>">View Assignment Details</a></h4>
					</aside>
				</article>
	
				<article class="article_right">
                        		<aside class="grey_btn_big"><a class="grey_btn_big" title="View Favorites">VIEW FAVORITES</a></aside>
                            		<h1>09/0124 </h1>
                        	</article>
                    		<p><img src="images/assignment_page/partition_line.png" width="888" height="24" alt=""></p>
                    	</section>                     
                </div>
                	   
		<section class="single_submission ">
                	<a href="/content/submission/assignid/<?php echo $submission_detail['assign_id']?>"><article class="sng_sub_top_section"><img src="images/assignment_page/single_submission/square_box.png" width="39" height="38" alt=""></article>
			</a>
                </section>
                <section class="single_submission">
                 	<article class="sng_sub_left_section">
				<aside class="main_demonstration">
					<article class="left_demonstration">Submitted by:</article>
				<article class="right_demonstration">@<?php echo $submission_detail['full_name']?></article>
				<article class="left_demonstration">Submitted on:</article>
				<article class="right_demonstration"><?php echo date("d/m/y",$submission_detail['timestamp'])?></article>
				<article class="left_demonstration">Resolution:</article>
				<article class="right_demonstration">4500x3000</article>
				<article class="left_demonstration">Model Release:</article>
				<article class="right_demonstration "><span class="sk_blue"><?php echo ucwords($submission_detail['model_release'])?></span></article>
				</aside>
				<aside class="main_demonstration">
					<a href=""><h1>Add to Favorites:</h1></a>                        
					<div class="content_box_small_div">
						<div class="btn_pink"><a href="#"><strong>Add</strong></a></div>
					</div>
				</aside>
				<aside class="main_demonstration">
					<h1>License this:</h1>
					<div class="content_box_small_div">
						<div class="btn_pink"><a href="#"><strong>Buy</strong></a></div>
					</div>
				</aside>
                   	</article>
                    	<article class="sng_sub_right_section">
			<img align="absmiddle" src="<?php echo GeneralFunction::getSubmissionImage($submission_detail['image_name'])?>" width="558" height="369" alt=""></article>
                 </section>
	</article>
</section>
<?php require_once("application/layout/footer.php");?>
<!--Footer Ends Here-->
<div style="display: none;"></div>
<!--Popup SIgn In Ends Here--> 
<script type="text/javascript" src="js/jquerypp.custom.js"></script> 
<script type="text/javascript" src="js/jquery.elastislide.js"></script> 
<script type="text/javascript">$( '#carousel' ).elastislide();</script>
</body>
</html>