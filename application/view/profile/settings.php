<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<title>Scoopr</title>
	<base href="<?php echo PATH;?>">
	<?php require_once("application/layout/headerContent.php"); ?>  
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/changepassword.js"></script> 
	<style>
	span.error{
			color:#FF0000;
			vertical-align:top; display:block;width:100%; margin-bottom:2%; margin-top:-4%;
			
		}
	</style>
	</head>
	<body>
	<?php require_once("application/layout/header.php"); ?>          
	<?php require_once("application/layout/dashboardSubHeader.php"); ?>          
<section class="webaccount3">
       			 <div class="arrow_div">
                 		<div class="inn_main_arrow">
                        	<aside>&nbsp;</aside>
				<aside>&nbsp;</aside>
				<aside>&nbsp;</aside>
				<aside>&nbsp;</aside>
                          	<aside><img src="images/app_accnt_images/arrow_icon.png" width="14" height="10" alt=""></aside>
                        </div>
         </div>

                <div class="arrow_nav_div">
	 		<div class="innav_div">
		               <div class="btn_assignmen_div">&nbsp;</div>
 			</div>
		</div>
    </section>
    
   		<section class="webaccount9">
        		<article class="inner_main_content">
                	<article class="content_row_password">
			
			<?php if($errMsg!='') { ?>
			<aside class="box_list_view_sub" style="text-align:center; color:red; display:block; margin:0px auto;">
				<h4><?php echo $errMsg; ?></h4>
			</aside>
			<?php } ?>

                    	<aside class="change_password_box">
                        	<div class="in_change_pass_cnt_box">
                            	<h1>Change Password</h1>
                                <form name="changePwdFrm" id="changePwdFrm" method="POST" action="">
                                		<p><label>Current Password</label>
                                        <input name="old_password" id="old_password" type="password" Placeholder="**********"  class="input_1"></p>
                                       <p><label>New Password</label>
                                        <input name="new_password" id="new_password" type="password"  Placeholder="**********" class="input_1"></p>
                                        <p><label>New Password Confirmation</label>
                                        <input  type="password" name="confirm_password" id="confirm_password" Placeholder="**********"  class="input_1"></p>


                                     <!--  <div class="btn-red3"><a href="#" title="Change Password">Change Password</a></div>-->
<div><input type="submit" name="change_password" id="change_password" value = "Change Password" class="pwd_button_diff"></div>

</div>
                                </form>
                            </div>
                        </aside>
                    </article>
                </article>
               
         </section>
         
    
<!--Body Ends Here-->

<?php require_once("application/layout/footer.php"); ?>  

<!--Footer Ends Here-->
<div style="display: none;" >
      
    </div>
<!--Popup SIgn In Ends Here-->
</body>
</html>