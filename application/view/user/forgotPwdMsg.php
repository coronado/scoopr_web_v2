<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="">
<meta name="author" content="">
<base href="<?php echo PATH; ?>">
<title>Scoopr</title>
<!-- Fonts -->
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:300,400,600,700,300italic,400italic,600italic,700italic|Droid+Serif:400,400italic">
<!--link rel="stylesheet" href="public/css/bootstrap-modal.css"-->
<link rel="stylesheet" href="public/css/bootstrap.css">
<link rel="stylesheet" href="public/css/style.css">
<link rel="stylesheet" href="public/css/scoopr.css">
<style>
#culture {
	background-image: url(./public/img/culture-bg.jpg);
}
.style2 {color: #5ab7ce}
</style>
</head>
<body>
<div id="block_div">
  <div class="block_container">
    <div class="infosection">
      <div class="info_block">
        <h1><span class="style2">Scoopr</span> Message</h1>
        <div class="info_tagline"></div>
      </div>
      <div class="info_section_icon"><img src="public/img/scooper_icon.png" alt="" width="94" height="94"></div>
    </div>
    <div class="infocontent">
      <p align="center" style="font-size:30px;">We have sent you an email to reset a password.</p>
      <p align="center"><br>
      <p align="center">-The Scoopr Team </p>
    </div>
  </div>
  <div class="clear"></div>
</div>

</body>
</html>