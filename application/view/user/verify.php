<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Scoopr</title>
	<link href="css/lp_style.css" rel="stylesheet" type="text/css">
	<!--Landing Page CSS-->
	<?php require_once("application/layout/headerContent.php");?>
	<script type="text/javascript">
	$(window).load(function(){
   	 // Simulate click on trigger element
	setTimeout(function(){
    	$('.fancybox').trigger('click');
	},4000);
	});
	</script>
	</head>
	<body>

<?php require_once("application/layout/header.php");?>

<section class="body_content">
      <div class="inner_container">
    <div class="slogan">

<?php if($vars['msg']=='Re-Verification') { ?>
          <h1>Your email is already verified.<br></h1>
	  <h3> Please login your account to proceed further.</h3>	
<?php } ?>

<?php if($vars['msg']=='success') { ?>
 	<h1>Thanks <?php echo $vars['email_address']; ?>, &nbsp;You have successfully verified your email address.<br></h1>
	<h3> Please login your account to proceed further.</h3>		
<?php } ?>

<?php if($vars['msg']=='Invalid-Token') { ?>
	<h1>Sorry, invalid request or request has been expired.<br></h1>
<?php } ?>
          
          <!--Button Ends Here--> 
        </div>
  </div>
    </section>

<?php require_once("application/layout/footer.php");?>
</body>
</html>

