<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Scoopr</title>
	<link href="css/lp_style.css" rel="stylesheet" type="text/css">
	<!--Landing Page CSS-->
	<?php require_once("application/layout/headerContent.php");?>
	<script type="text/javascript">
	$(window).load(function(){
   	 // Simulate click on trigger element
	setTimeout(function(){
    	$('.fancybox').trigger('click');
	},3000);
	});
	</script>
	</head>
	<body>

<?php require_once("application/layout/header.php");?>

<section class="body_content">
      <div class="inner_container">
    <div class="slogan">
          <h1>Thanks For Registration!<br>
		<h3>We have sent you an email in your account. Please verify your email befor login.</h3>
        </h1>
          
          <!--Button Ends Here--> 
        </div>
  </div>
    </section>

<?php require_once("application/layout/footer.php");?>
</body>
</html>

