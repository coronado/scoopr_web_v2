<!DOCTYPE html>
<html>
<head>
<base href="<?php echo PATH;?>">
<title>Forgot Password</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="css/stylesheet.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="<?php echo PATH; ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" language="JavaScript" src="js/forgotpassword.js"></script>
	<style>
	.error{color:#FF0000;}

.fancybox-outer, .fancybox-inner {
    height: 200px;    position: relative;
}
	</style>
</head>
<body>
<div class="popup_block">
    <div class="popup_inner_container">
          <div class="clearfix db header">
        <div class="left">
              <h1>Forgot Password</h1>
              <p class="signup">Please enter your email address to reset the password.</p>
            </div>
        <!--<div class="right signin-fb"><span>Sign In with</span><img src="images/lp_images/icon_fb.png" width="51" height="52" onclick="login();" style="cursor:pointer;"></div>-->
      </div>
          <!--Popup Header Ends Here-->
        <?php if(@$errorMsg!='') { ?>  
	<div><p><font color="red"><?php echo @urldecode($errorMsg); ?></font></p>
	<p>&nbsp;</p>	</div>
	<?php } ?>

          <div>
        <form action="" name="forgotPwdFrm" id="forgotPwdFrm" method="POST">
              <p>
            <label>Email Address</label>
            <input type="email" name="email_address" id="email_address" placeholder="example@email.com" value="">
          </p>

              <div class="right">

            <div class="btn-red left">
                  <input type="submit" name="submit" value="Submit">
                </div>
          </div>
            </form>
        <!--Form Ends Here--> 
      </div>
        </div>
  </div>
</body>
</html>