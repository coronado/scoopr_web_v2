<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Scoopr</title>
	<!--Landing Page CSS-->
	<?php require_once("application/layout/headerContent.php"); ?>

<style>
#display_database_error
{
   display:none;
}


.transparent
{
    background-color: #666666;
    border: 1px solid black;
    height: 600px;
    opacity: 0.6;
    position: absolute;
    width: 100%;
    z-index: 1;
}
</style>
	</head>
	<body>

<?php require_once("application/layout/header.php");?>


<section class="container clearfix dp">
<div style="color:red;font-size:16px;padding:10px;" >
 JavaScript is disabled on your browser.
Please enable JavaScript or upgrade to a JavaScript-capable browser to use InsureMyCompany.net.
Then refresh this page. <br> <a href="/">Try Again </a> <br><br>
In order to view the InsureMyCompany.net, you need to have JavaScript enabled in your browser. To do so, please follow the instructions below: Mozilla Firefox (12.0)
</div>





<div>
	<div class="colm first" style="width:345px; height:280px; background:#EAEAEA; border:1px solid #C4C4C4;padding:15px;float:left">
	<h1 style="color: #FB5248;text-align:center;padding:5px 10px; font: 20px "Helvetica Neue",Helvetica,Arial,sans-serif;margin-bottom:20px">Mozilla Firefox (12.0)</h1>
	
	<ul class="instruction_points" style="line-height:24px;">
	<li >Click on the Edit menu</li>
	<li >Select Preferences</li>
	<li >Select the Content panel.</li>
	<li >Check enable javascript check box</li>
	<li >Click Close to close the Preferences window</li>
	</ul>
	</div>

	<div class="colm first" style="width:345px; height:280px; background:#EAEAEA; border:1px solid #C4C4C4;padding:15px;float:left">
	<h1 style="color: #FB5248;text-align:center;padding:5px 10px; font: 20px "Helvetica Neue",Helvetica,Arial,sans-serif;margin-bottom:20px">Google Chrome (15.0)</h1>
	
	<ul class="instruction_points" style="line-height:24px;">
	<li >Click the spanner icon on the browser toolbar.</li>
	<li >Select <strong>Options</strong></li>
	<li >Click the Under the Hood tab.</li>
	<li >Click Content Settings in the 'Privacy section.</li>
	<li >Select Allow all sites to run JavaScript in the 'JavaScript' section.</li>
	</ul>
	</div>


	<div class="colm first" style="width:345px; height:280px; background:#EAEAEA; border:1px solid #C4C4C4;padding:15px;float:left">
	<h1 style="color: #FB5248;text-align:center;padding:5px 10px; font: 20px "Helvetica Neue",Helvetica,Arial,sans-serif;margin-bottom:20px">Internet Explorer (9.0)</h1>
	
	<ul class="instruction_points" style="line-height:24px;">
	<li >Select Tools from the top menu.</li>
	<li >Choose Internet Options.</li>
	<li >Click on the Security tab.</li>
	<li >Click on Custom Level.</li>
	<li >Scroll down until you see the section labeled 'Scripting.</li>
	<li >Under 'Active Scripting,' select Enable and click OK..</li>
	</ul>
	</div>

	<div class="colm first" style="width:345px; height:280px; background:#EAEAEA; border:1px solid #C4C4C4;padding:15px;float:left">
	<h1 style="color: #FB5248;text-align:center;padding:5px 10px; font: 20px "Helvetica Neue",Helvetica,Arial,sans-serif;margin-bottom:20px">Apple Safari (5.0)</h1>
	
	<ul class="instruction_points" style="line-height:24px;">
	<li >Open the Safari menu on your browser's toolbar.</li>
	<li >Choose Preferences.</li>
	<li >Choose Security.</li>
	<li >Select the checkbox next to Enable JavaScript.</li>
	
	</ul>
	</div>

</div>

<div> &nbsp;</div>

</section>

<?php require_once("application/layout/footer.php");?>