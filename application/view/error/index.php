<html>
	<head>
	<meta charset="utf-8">
	<base href="<?php echo PATH;?>">
	<title>Scoopr</title>
	
	<!--Landing Page CSS-->
	<?php require_once("application/layout/headerContent.php");?>
	</head>
	<body>

<?php require_once("application/layout/header.php");?>

<section class="body_content">
      <div class="inner_container" style="background-image:none;">
    <div class="slogan" style="padding-top:120px;">
          <h1>404 NOT FOUND<br>
		<h1>The page you are looking for is not available</h1>
        </h1>
          
          <!--Button Ends Here--> 
        </div>
  </div>
    </section>

<?php require_once("application/layout/footer.php");?>
</body>
</html>

