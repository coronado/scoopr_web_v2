<?php
	class model_user extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		#------ This function is to valid forgot user
		/*
		* Params : posted data token and userID
		* Return : true or false
		*/
		function check_forgot_user($token,$user)
		{
			$select = "SELECT ForgotPwdId,UserId FROM ".TABLE_FORGOT_PWD." WHERE md5(UserId)='".$user."' AND token='".$token."' AND expirationDate>=NOW()";
			$user_row = $this->db->fetch_array($select);
			if($user_row[0]['UserId']>0){ 
				return $user_row[0]['UserId'];
			} 
			else{
				return 0;
			}
		}

		function update_password($user,$new_password)
		{
			$update="UPDATE ".TABLE_USER_MASTER." SET password='".md5($new_password)."' WHERE md5(UserId)='$user'";
			$process = $this->db->query($update);
			if($process) {
				return true;	
			} else {
				return false;
			}	  
		}

		function reset_password($request)
		{
			$reset_key = $request['reset_key'];
			$new_password = $request['new_password'];
			$confirm_password = $request['confirm_password'];

			$qry = "UPDATE ".TABLE_USER_MASTER." SET password = '".md5($new_password)."' WHERE resetKey = '".$reset_key."'";
			$result = $this->db->query($qry);
		}

		function checkResetKey($reset_key)
		{
			$qry = "SELECT UserId FROM ".TABLE_USER_MASTER." WHERE resetKey = '".$reset_key."'";
			$result = $this->db->query($qry);
			$numrows = $this->db->num_rows($result);
			if($numrows>0)
			{
				return true;
			}
			else
			{
				return false;
			} 
		}
	}
?>