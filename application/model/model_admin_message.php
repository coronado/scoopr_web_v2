<?php
//===================================================================================
/* This class is for the purpose to manage message section */
//===================================================================================

	class model_admin_message extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}


		function addbrandmessage($request)
		{
			$current_date = date('Y-m-d H:i:s');
			$query = "INSERT into ".TABLE_BRAND_MESSAGE." SET
				        UserId  =  '".$request['UserId']."',
					subject =  '".mysql_escape_string($request['subject'])."',
					brandMessage =  '".mysql_escape_string($request['brandMessage'])."',
					status  =  '".$request['status']."',
					ModifiedDate = '".$current_date."'";
			$result = $this->db->query($query);
			if($result) {
				return true;
			} else {
				return false;
			}
		}

		function getAllBrands()
		{
			$select = "SELECT UserId, profileName, emailAddress FROM ".TABLE_USER_MASTER." WHERE userType=1 AND isDisabled =1";
			$result = $this->db->fetch_array($select);
			return $result;
		}

		function viewbrandmessage()
		{
			$query = "SELECT bm.UserId, bm.BrandMessageId, bm.subject, bm.status, bm.ModifiedDate, um.profileName ".
				 "FROM ".TABLE_BRAND_MESSAGE." AS bm ".
				 "INNER JOIN ".TABLE_USER_MASTER." AS um ".
				 "ON bm.UserId = um.UserId";


			$query .= " order by bm.BrandMessageId desc";

			// ======= pagination starts here =======
			$current_page = 1;
			$param =  $this->getrequest();
			if($param['page']!='') 
			{
				$current_page = $param['page'];			
			} 
			$limit = 10;
			$this->include_file('new_paginator.php','application/lib');
			$page = new pagination;
			$result = $page->paginate($query,$limit,$current_page);

			return $result;
		}

		function getMessageDetails($mid)
		{
			$query = "SELECT * FROM ".TABLE_BRAND_MESSAGE." WHERE BrandMessageId = '".$mid."'";
			$result = $this->db->fetch_array($query);
			return $result[0];
		}

		function editbrandmessage($mid,$request)
		{
			$modified_date = date('Y-m-d H:i:s');

			$query = "UPDATE ".TABLE_BRAND_MESSAGE." SET 
					UserId        = '".$request['UserId']."',
					subject       = '".mysql_escape_string($request['subject'])."',
					brandMessage  = '".mysql_escape_string($request['brandMessage'])."',
					modifiedDate  = '".$modified_date."',
					status        = '".$request['status']."'
					WHERE BrandMessageId = '".$mid."'"; 
			$result = $this->db->query($query) or die("aaaaa");
			
			if($result) {
				return true;			
			} else {
				return false;
			}
		}

		function brandmessagedetails($mid)
		{
			$query = "SELECT bm.BrandMessageId, bm.UserId AS buyerId, bm.subject, bm.brandMessage, bm.modifiedDate, bm.status, um.profileName ".
				"FROM ".TABLE_BRAND_MESSAGE." AS bm ".
				"INNER JOIN ".TABLE_USER_MASTER." AS um ".
				"ON bm.UserId = um.UserId ".
				"WHERE bm.BrandMessageId = '".$mid."'";
			$result = $this->db->fetch_array($query);
			return $result[0];
			//echo "<pre>"; print_r($result); die;
		}

		function repliedmessagedetails($mid)
		{
			$query = "SELECT bmr.repliedMessage, bmr.UserId AS creatorId, um.name ".
				 "FROM ".TABLE_BRAND_MESSAGE_REPLY." AS bmr ".
				 "INNER JOIN ".TABLE_USER_MASTER." AS um ".
				 "ON bmr.UserId = um.UserId ".
				 "WHERE bmr.BrandMessageId = '".$mid."'";

			$result = $this->db->fetch_array($query) or die("aaaa");
			return $result;
		}

		function deletemessage($mid)
		{
			$query = "DELETE FROM ".TABLE_BRAND_MESSAGE_REPLY." WHERE BrandMessageId = '".$mid."'";
			$result = $this->db->query($query);

			$query1 = "DELETE FROM ".TABLE_BRAND_MESSAGE." WHERE BrandMessageId = '".$mid."'";
			$result1 = $this->db->query($query1);
		}

	}
?>
