<?php
//===================================================================================
/* This class is for info controller functionality */
//===================================================================================

	class model_info extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		#------ This function is to check wheather user exists or not ---------
		/*
		* Params : posted data array with information to be checked 
		* Return : error message
		*/ 
		function contactus($request)
		{
			$name       = $request['fullName'];
			$email      = $request['emailAddress'];
			$websiteUrl = $request['websiteUrl'];
			$subject    = $request['subject'];
			$message    = $request['message'];
			
			$to      = $email;
			$cc      = '';
			$bcc     = '';
			$from    = FROM_EMAIL; 
			$logosource = PATH."images/logo.png";

			$subject_to_user = 'Thank you for contacting with us';
			$message_to_user = '<tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">Hi there! Thank you for contacting with us.</td>
                                                      </tr>
                                                      <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">We always love to hear from you. We will get back to you as soon as possible.</td>
                                                      </tr>';


			$subject_to_admin = $subject;	
			$message_to_admin = '<tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">Hi Admin! '.$name.' wants to contact us.</td>
                                                      </tr>
                                                      <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">Message : '.$message.'</td>
                                                      </tr>';

			$this->contact_email_touser($to, $from, $subject_to_user, $cc, $bcc, $message_to_user);
			$this->contact_email_toadmin($to, $from, $subject_to_admin, $cc, $bcc, $message_to_admin);
		}

		function contact_email_toadmin($from, $to, $subject, $cc, $bcc, $message)
		{
			$this->include_file('email_class.php','application/lib');
			
			$email_obj = new email($to, $from, $subject, $cc, $bcc, $message);
		  	$send_email = $email_obj->send_email();
		}

		function contact_email_touser($to, $from, $subject, $cc, $bcc, $message)
		{
			$this->include_file('email_class.php','application/lib');
			
			$email_obj = new email($to, $from, $subject, $cc, $bcc, $message);
		  	$send_email = $email_obj->send_email();
		}

		function getContent($id)
		{
			$select = "SELECT content FROM ".TABLE_CONTENT_MANAGEMENT." WHERE contentManagementId = '".$id."'";

			//  ======== memcache implementation ===============
			$this->key_mi_getContent =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mi_getContent);
		    
		    	if($this->details)
		    	{	
		      		$records = $this->details;  
		    	}
		    	else
		    	{	
		      		$records      = $this->db->fetch_array($select);
		      		$this->memcache->set($this->key_mi_getContent, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}

			//$result = $this->db->fetch_array($query); 
			//print_r($result); die;
			return $records[0]['content'];
		}

	}
?>
