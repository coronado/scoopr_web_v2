<?php
	class model_signup extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}


		#------ This function is to insert user
		/*
		* Params : posted data array with information to be insert
		* Return : inserted ID
		*/
		function add_user($request)
		{
			$email     = mysql_escape_string($request['email_address']);
	                $password  = md5(mysql_escape_string($request['password']));
			$profile_name = mysql_escape_string($request['profile_name']);
			$user_type = 1;
			$tag_line  = mysql_escape_string($request['tag_line']);
			$cdate = date('Y-m-d h:i:s');

			$pwd = $request['password'];
 
			$select = "INSERT INTO ".TABLE_USER_MASTER." SET
					emailAddress = '".$email."',
					password      = '".$password."',
					profileName  = '".$profile_name."',
					tagLine      = '".$tag_line."',
					userType     = '".$user_type."',
					isVerified   = 0,
					emailVerifyTokenid = '".sha1($cdate)."',
					createdAt    = NOW()"; 
			$process = $this->db->query($select) or die("aaa");
			if($process)
			{
				$to      = $email;
				$cc      = '';
				$bcc     = '';
				$from    = FROM_EMAIL; 
				$subject = 'Registration Details for Scoopr Media';
				$logosource = PATH."images/logo.png";
				

				$message= '<tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">Hi there! Thank you for creating an account.<br><br>We are exiceted to have you on board. Please let us know if there is any way we can make your experience better.</td>
                                                      </tr>
                                                      <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">Here are your registration details for Scoopr Media :<br><br>
							Email    : '.$email.'<br>
							Password : '.$pwd.'<br><br>
							<a href='.PATH.'signup/email_verify/tokenid/'.sha1($cdate).'>Click here</a> to verify your email address.
							</td>
                                                       </tr>';

 				
				$this->registration_email($to, $from, $subject, $cc, $bcc, $message);
			}

			$user_id = (int)$this->db->insert_id();
			return $user_id;
		}

		#------ This function is to check email address
		/*
		* Params : posted data email address to check the existing email address
		* Return : true or false
		*/
		function check_email($email)
		{
			$email = mysql_escape_string($email);
			$select = "SELECT count(*) as is_user FROM ".TABLE_USER_MASTER." WHERE emailAddress = '".$email."'";
			//echo $select;
			$user_row = $this->db->fetch_array($select);
			//print_r($user_row);die;
			if($user_row[0]['is_user']==0){ 
				return 0;
			} else {
				return 1;
			}
		}

		#------ This function is to send the registration email to verify ---------
		/*
		* Params : to , from, subject, cc, bcc, message
		* Return : nothing
		*/ 
		function registration_email($to, $from, $subject, $cc, $bcc, $message)
		{
			$this->include_file('email_class.php','application/lib');
			
			$email_obj = new email($to, $from, $subject, $cc, $bcc, $message);
		  	$send_email = $email_obj->send_email();
		}

		#------ to check wheateher email verified or not ---------
		/*
		* Params : token id
		* Return : 
		*/ 
		function emailverification($tokenid)
		{
			$select = "SELECT UserId ".
				  "FROM ".TABLE_USER_MASTER." ".
				  "WHERE emailVerifyTokenid = '".$tokenid."'";
			$process = $this->db->query($select);
			$num_rows = $this->db->num_rows($process);

			$result = $this->db->fetch($process);
			$user_id = $result['id'];

			if($num_rows==1)
			{
				$query = "SELECT isVerified ".
					 "FROM ".TABLE_USER_MASTER." ".
					 "WHERE UserId = '".$user_id."'";
				$execute = $this->db->query($query);	
				$result1 = $this->db->fetch($execute);
				$is_verified = $result1['isVerified'];
				$is_mail_verified  = ($is_verified == 1)?true:false;

				if( ! $is_mail_verified )
				{
					$update = "UPDATE ".TABLE_USER_MASTER." SET isVerified=1 WHERE UserId = '".$user_id."'";
					$this->db->query($update);

					$selQry = "SELECT UserId, name, emailAddress ".
						  "FROM ".TABLE_USER_MASTER." ".
						  "WHERE UserId = '".$user_id."'";
		            
			                $res_arr  = $this->db->fetch_array($selQry);
					
					$data['user_id']       = $res_arr[0]['UserId'];
					$data['name']          = $res_arr[0]['name'];
					$data['email_address'] = $res_arr[0]['emailAddress'];
					$data['password']      = $res_arr[0]['password'];
					
					$data['msg']    = "success";
				}
				else
				{
					$data['msg'] = "Re-Verification";
				}
			}
			else
			{
				$data['msg'] = "Invalid-Token";
			}	
			return $data;	
		}
	
		#------ to update profile pic or company logo ---------
		/*
		* Params : userid and filename
		* Return : bool val
		*/
		function update_profile_pic($userid,$filename)
		{
			$update="UPDATE ".TABLE_USER_MASTER." SET profileImg='".$filename."' WHERE UserId='".$userid."'";
			$process = $this->db->query($update);
			if($process) {
				return true;	
			} else {
				return false;
			}	  
		}
	}
?>