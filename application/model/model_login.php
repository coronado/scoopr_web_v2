<?php
//===================================================================================
/* This class is for the purpose to check the login and return the user basic info */
//===================================================================================

	class model_login extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		#------ This function is to check wheather user exists or not ---------
		/*
		* Params : posted data array with information to be checked 
		* Return : error message
		*/ 
		function check_login($request)
		{
			$email     = mysql_escape_string($request['email_address']);
	                $password  = md5(mysql_escape_string($request['password']));
			//$user_type = $request['user_type'];

			$select = "SELECT UserId,isVerified ".
				  "FROM ".TABLE_USER_MASTER." ".
				  "WHERE emailAddress = '$email' ".
				  "AND password = '$password' ".
				  "AND (userType = 1 OR userType = 3) AND isDisabled=1";
				  

			$result = $this->db->query($select);
			$numrows = $this->db->num_rows($result);			
			
			$errMsg = "";
			if($numrows>0)
			{
				$record = $this->db->fetch_array($select);
				$is_verified = $record[0]['isVerified'];
				if($is_verified==0)
				{
					$errMsg = "You haven't verified your email yet.";
				}
			}
			else
			{
				$errMsg = "Incorrect Username or Password";
			}
			
			return $errMsg;
		}

		#------ This function is to get user record need to stored in session ---------
		/*
		* Params : posted data array with information to be checked 
		* Return : array of userid, firstname, lastname, email
		*/ 
		function get_user_details($email)
		{
			$select = "SELECT UserId, name, password, profileName, profileImg, userType ".
				  "FROM ".TABLE_USER_MASTER." ".
				  "WHERE emailAddress = '$email'";

			//  ======== memcache implementation ===============
			$this->key_ml_get_user_details =  md5($select);
		    	$this->udetails = $this->memcache->get($this->key_ml_get_user_details);
		    
		    	if($this->udetails)
		    	{
		      		$records = $this->udetails;  
		    	}
		    	else
		    	{
		      		$records      = $this->db->fetch_array($select);
		      		$this->memcache->set($this->key_ml_get_user_details, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}

			//$records  = $this->db->fetch_array($select);
			
			$sessionRecords['user_id']= $records[0]['UserId'];
			$sessionRecords['name']= $records[0]['name'];
			$sessionRecords['email_address']= $email;
			$sessionRecords['profile_name']= $records[0]['profileName'];
			$sessionRecords['profile_img']= $records[0]['profileImg'];
			$sessionRecords['user_type']= $records[0]['userType'];
			return $sessionRecords;
		}

		#------ This function is facebook login functionality ---------
		/*
		* Params : array of user details fetched from facebook
		* Return : 
		*/ 
		function check_fb_login($details)
		{
			//echo "<pre>"; print_r($details); die;
			$first_name    = $details['first_name'];
			$last_name     = $details['last_name'];
			$email_address = $details['email'];
			$fb_id         = $details['id'];
			
			$fb_details = array(
						'first_name'=>$first_name, 
						'last_name'=>$last_name, 
						'email_address'=>$email_address,
						'fb_id'=>$fb_id
					    );
			
			$is_email_exists = $this->is_email_exists($email_address);
			if($is_email_exists)
			{  
				$this->update_fbid($fb_id, $email_address);
				$fb_flag = "1";
			}
			else
			{  
				$this->register_fb_user($fb_details);
				$fb_flag = "0";
			}
			return $fb_flag;
		}

		#------ This function is to check weather email already exists or not ---------
		/*
		* Params : email address
		* Return : true or false based on email exists or not
		*/ 
		function is_email_exists($email_address)
		{
			$select  = "SELECT UserId ".
				   "FROM ".TABLE_USER_MASTER." ".
				   "WHERE emailAddress = '$email_address'";
			$result  = $this->db->query($select);
			$numrows = $this->db->num_rows($result);
			if($numrows>0) {
				return true;
			} else {
				return false;
			}		
		}

		#------ This function is to update fbid  ---------
		/*
		* Params : fbid and email address
		* Return : true or false 
		*/ 
		function update_fbid($fb_id, $email_address)
		{ 
			$update  = "UPDATE ".TABLE_USER_MASTER." ".
				   "SET fbId = '$fb_id' ".
				   "WHERE emailAddress = '$email_address'";		
			$process = $this->db->query($update);
			if($process) {
				return true;	
			} else {
				return false;
			}	  
		}

		#------ This function is to register the facebook user  ---------
		/*
		* Params : fb user basic details like email, fbid, firstname , lastname
		* Return : 
		*/ 
		function register_fb_user($fb_details)
		{ 
			$password = $this->randompassword(8);
			$name     = $fb_details['first_name'].' '.$fb_details['last_name']; 
			$insert   = "INSERT into ".TABLE_USER_MASTER." SET
						name          = '".$name."',
						emailAddress = '".$fb_details['email_address']."',
						fbId         = '".$fb_details['fb_id']."',
						userType     = '1',
						isVerified   = '1',
						password      =  '".md5($password)."',
						createdAt    =  NOW()"; 

			$process  = $this->db->query($insert);
			if($process)
			{ 
				$to      = $fb_details['email_address'];
				$cc      = '';
				$bcc     = '';
				$from    = FROM_EMAIL; 
				$subject = 'Registration Details for Scoopr Media';

				$message = "<p>Welcome ".$fb_details['first_name']." ".$fb_details['last_name'].",</p>";

		  		$message.= "Here are your registration details for Scoopr Media :";

		  		$message.= "<table width=600px><tr><td width=200px>Email :</td><td>".$fb_details['email_address']."</td></tr>";

		  		$message.= "<tr><td width=200px>Password :</td><td>".$password."</td></tr>";
		  		$message.= "<tr><td colspan=2>&nbsp;</td></tr>";

		 		//$message.= "<tr><td colspan=2>Click on the below logo to verify your email address.</td></tr>";

		  		//$message.= "<tr><td colspan=2><a href=''><img src='' style='border:0; height:70px'></a></td></tr>";

		  		 $message.= "</table>"; 

				$this->fb_registration_email($to, $from, $subject, $cc, $bcc, $message);
			}	
				  	
		}

		#------ This function is to generate random password  ---------
		/*
		* Params : length
		* Return : random password
		*/ 
		function randompassword($length = 8)
		{
    			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    			$password = substr( str_shuffle( $chars ), 0, $length );
			return $password;
		}


		#------ This function is to send the random password in email (for first time fb users) ---------
		/*
		* Params : to , from, subject, cc, bcc, message
		* Return : 
		*/ 
		function fb_registration_email($to, $from, $subject, $cc, $bcc, $message)
		{
			$this->include_file('email_class.php','application/lib');
			//echo $message; die;	
			$email_obj = new email($to, $from, $subject, $cc, $bcc, $message);
		  	$send_email = $email_obj->send_email(); 
		}		

	}
?>
