<?php
	class model_payment extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		#------ This function is to insert paypal transaction details
		/*
		* Params : posted data from paypal
		* Return : true or false
		*/
		function savePaymentDetails($request,$content_id)
		{
			$qry = "SELECT AssignmentId FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." ".
			       "WHERE AssignmentImgSubmissionId = '".$content_id."'";
			$res = $this->db->fetch_array($qry);
			$a_id = $res[0]['AssignmentId']; 

			$qry1 = "SELECT paymentMethod,rewards,ourBudget FROM ".TABLE_ASSIGNMENT." ".
			       "WHERE AssignmentId = '".$a_id."'";
			$res1 = $this->db->fetch_array($qry1);


			$query = "UPDATE ".TABLE_ASSIGNMENT_IMG_SUBMISSION." SET 
				  paymentStatus   =  'success',
				  paymentAmount   =  '".$request['payment_gross']."',
				  paymentMadeTo   =  'Scoopr Media Inc',
				  transactionId   =  '".$request['txn_id']."',
				  customId        =  '".$request['custom']."',
				  isApproved      =  '2',
				  paymentMethod   =  '".$res1[0]['payment_method']."',
				  rewards	  =  '".$res1[0]['rewards']."',
				  originalAmount  =  '".$res1[0]['our_budget']."',
				  paymentDate     =  now() 
				  WHERE AssignmentImgSubmissionId = '".$content_id."'";	
			$process = $this->db->query($query);
			
			if($process) {
				return true;
			} else {
				return false;
			}
		}

		function saveNotification($content_id)
		{
			$query = "SELECT creatorId FROM " . TABLE_ASSIGNMENT_IMG_SUBMISSION . " WHERE AssignmentImgSubmissionId = {$content_id}";
			$row = $this->db->fetch_array($query);
			$creatorId = $row[0]['creatorId'];
			
			$query = "INSERT INTO ".TABLE_NOTIFICATION." SET 
				  CreatorId                   = '".$creatorId."',
				  AssignmentImgSubmissionId   = '".$content_id."',
				  type 			      = '2',
				  date     		      = '".date('Y-m-d H:i:s')."'";
			$process = $this->db->query($query);

			if($process) {
				return true;
			} else {
				return false;
			}
		}

		#------ This function is show buyer company info
		/*
		* Params : user id
		* Return : details
		*/
		function getBuyerInfo($user_id)
		{
			$query = "SELECT name, profileName, tagLine, city, profileImg ".
				 "FROM ".TABLE_USER_MASTER." ".
				 "WHERE UserId = '".$user_id."'";
			$row = $this->db->fetch_array($query);
			return 	$row[0];
		}

		function getContentInfo($content_id)
		{
			$qrystr = "SELECT 
				   AIS.*,
				   AM.*,	
				   UM.name AS full_name
				   FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." AS AIS
				   LEFT JOIN ".TABLE_ASSIGNMENT." AS AM ON AIS.AssignmentId=AM.AssignmentId
				   LEFT JOIN ".TABLE_USER_MASTER." AS UM ON AIS.creatorId=UM.UserId
				   WHERE AIS.AssignmentImgSubmissionId='".$content_id."'";
			$row = $this->db->fetch_array($qrystr); 
			return $row[0];
		}

		function getUserDeviceToken($content_id)
		{
			$qry = "SELECT creatorId FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." ".
			       "WHERE AssignmentImgSubmissionId = '".$content_id."'";
			$res = $this->db->fetch_array($qry);
			$creator_id = $res[0]['creatorId']; 

			$query = "SELECT deviceTokenId FROM ".TABLE_USER_MASTER." ".
				 "WHERE userType = 2 ".
				 "AND UserId = '".$creator_id."'"; 

			$result = $this->db->fetch_array($query);
			$device_token = array($result[0]['deviceTokenId']); 
			
			return $device_token;
		}
		
		function getAllDeviceToken_pay($userid)
		{
			$query = "SELECT deviceTokenId FROM ".TABLE_USER_MASTER." ".
				 "WHERE UserId = $userid";
			$result = $this->db->fetch_array($query);
			
			$device_token = array();
			foreach($result as $row)
			{	
				if($row['deviceTokenId']!='') 
				{
					array_push($device_token,$row['deviceTokenId']); 
				}
			}
			return $device_token;
		}

		function mailToBuyer()
		{
			$email   = $this->getsession('email_address');
			$to      = $email;
			$cc      = '';
			$bcc     = '';
			$from    = FROM_EMAIL; 
			$subject = 'Thank you for payment';

			$message = "<tr>
						<td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;'>Hi There! Thank you for your payment!</td>
						</tr>
				<tr>
						<td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;'>We are looking into this situation. We will respond to you shortly.</td>
						</tr>
						<tr>
						<td valign='top'>&nbsp;</td>
						</tr>";

			$this->email_touser($to, $from, $subject, $cc, $bcc, $message);
			return true;
		}

		function mailToAdmin($content_info,$buyer_info)
		{
			$to      = FROM_EMAIL;
			$cc      = '';
			$bcc     = '';
			$from    = FROM_EMAIL; 
			$subject = "A new payment has been made";
			
			if($content_info['paymentMethod']==1) {
				$payment_method = "Paypal payment";
			} elseif($content_info['paymentMethod']==2) {
				$payment_method = "Reward payment";
			} else {
				$payment_method = "Paypal payment + Reward";
			}
	

			 $message = "<tr>
						<td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;'>Hi Admin! ".$buyer_info['profileName']." made a payment!</td>
						</tr>
				<tr>
						<td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;'>Here is the payment details:<br><br>
						Brand name      : ".$buyer_info['profileName']."<br>
						Payment status  : ".$content_info['paymentStatus']."<br>
						Payment method  : ".$payment_method."<br>";
						
						if($content_info['paymentMethod']==1) {
						$message .= "Payment amount  : ".$content_info['paymentAmount']."<br>";
						} elseif($content_info['paymentMethod']==2) {
						$message .= "Reward  : ".$content_info['rewards']."<br>";
						} else {
						$message .= "Payment amount  : ".$content_info['paymentAmount']."<br>";
						$message .= "Reward  : ".$content_info['rewards']."<br>";
						}

						$message .= "</td>
						</tr>
						<tr>
						<td valign='top'>&nbsp;</td>
						</tr>"; 

			$this->email_touser($to, $from, $subject, $cc, $bcc, $message);
			return true;
		}

		function email_touser($to, $from, $subject, $cc, $bcc, $message)
		{
			$this->include_file('email_class.php','application/lib');
			
			$email_obj = new email($to, $from, $subject, $cc, $bcc, $message);
		  	$send_email = $email_obj->send_email();
		}

		function mailToCreator($content_id)
		{
			$qry = "SELECT creatorId FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." ".
			       "WHERE AssignmentImgSubmissionId = '".$content_id."'";
			$res = $this->db->fetch_array($qry);
			$creator_id = $res[0]['creatorId']; 

			$query = "SELECT emailAddress FROM ".TABLE_USER_MASTER." ".
				 "WHERE userType = 2 ".
				 "AND UserId = '".$creator_id."'"; 
			$result = $this->db->fetch_array($query);

			$email   = $result[0]['emailAddress'];
			$to      = $email;
			$cc      = '';
			$bcc     = '';
			$from    = FROM_EMAIL; 
			$subject = 'Thank you for payment';
			
			$message = "<tr>
						<td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;'>Hi There! CONGRATULATIONS!!!</td>
						</tr>
				<tr>
						<td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;'>We are thrilled to let you know that your image has been selected. Please review your notifications tab within the Scoopr App for further details. </td>
						</tr>
						<tr>
						<td valign='top'>&nbsp;</td>
						</tr>";

			$this->email_touser($to, $from, $subject, $cc, $bcc, $message);
			return true;
		}

		function saveRewardStatus($content_id)
		{
			$qry = "SELECT AssignmentId FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." ".
			       "WHERE AssignmentImgSubmissionId = '".$content_id."'";
			$res = $this->db->fetch_array($qry);
			$a_id = $res[0]['AssignmentId']; 


			$qry1 = "SELECT paymentMethod,rewards,ourBudget FROM ".TABLE_ASSIGNMENT." ".
			       "WHERE AssignmentId = '".$a_id."'";
			$res1 = $this->db->fetch_array($qry1);
			

			$query = "UPDATE ".TABLE_ASSIGNMENT_IMG_SUBMISSION." SET 
				  paymentStatus = 'success',
				  isApproved = '2',
				  paymentMethod = '".$res1[0]['paymentMethod']."',
				  rewards = '".$res1[0]['rewards']."',
				  originalAmount = '".$res1[0]['ourBudget']."',
				  paymentDate = now() WHERE AssignmentImgSubmissionId = '".$content_id."'";	
			$process = $this->db->query($query);
			if($process) 
			{
				return true;
			} 
			else 
			{
				return false;
			}
		}

	}
?>