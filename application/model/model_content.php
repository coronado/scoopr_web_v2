<?php

	class model_content extends Application
	{
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		function getAssignmentIds($user_id)
		{
			$select="SELECT AssignmentId, assignmentRefId FROM ".TABLE_ASSIGNMENT." WHERE UserId='".$user_id."' AND isEnabled=1";

			//  ======== memcache implementation ===============
			$this->key_mc_getAssignmentIds =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mc_getAssignmentIds);
		    
		    	if($this->details)
		    	{	
		      		$result = $this->details;  
		    	}
		    	else
		    	{	
		      		$result      = $this->db->fetch_array($select);
		      		$this->memcache->set($this->key_mc_getAssignmentIds, $result, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_TEN_MINUTES);
		    	}

			//$result = $this->db->fetch_array($select);
		
			$ids = array();
			if($result)
			{	
				foreach($result as $val)
				{
					$ids[$val['AssignmentId']] = $val['assignmentRefId'];
				}		
			}
			return $ids; 
		}

		function getUserDeviceToken($content_id)
		{
			$qry = "SELECT user_id FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." ".
			       "WHERE id = '".$content_id."'";
			$res = $this->db->fetch_array($qry);
			$creator_id = $res[0]['user_id']; 

			$query = "SELECT device_token_id FROM ".TABLE_USER_MASTER." ".
				 "WHERE user_type = 2 ".
				 "AND id = '".$creator_id."'"; 

			$result = $this->db->fetch_array($query);
			$device_token = array($result[0]['device_token_id']); 
			
			return $device_token;
		}


		function getAssignmentDetails($assignment_id)
		{
			$select_qry  = "SELECT sa.AssignmentId, sa.UserId, sa.lookingFor, sa.lookingForText, sa.hopeToSelect, sa.ourBudget, sa.submissionDeadline, sa.imageryFor, sa.imageryForText, sa.rewards, sa.shareCondition1, sa.shareCondition2, sa.shareCondition3, sa.modelRelease, sa.imageryFormat, sa.referenceImageText, sa.createdAt, sa.isLaunched, sa.launchedDate, su.profileName, su.name, su.profileImg, su.tagLine, sr.image1, sr.image2, sr.image3 ".
			"FROM ".TABLE_ASSIGNMENT." AS sa ".
			"INNER JOIN ".TABLE_USER_MASTER." AS su ".
			"ON sa.UserId=su.UserId ".
			"LEFT JOIN ".TABLE_REFERENCE_IMAGE." AS sr ".
			"ON sa.AssignmentId=sr.AssignmentId ".
			"WHERE sa.AssignmentId = '".$assignment_id."'";

			//  ======== memcache implementation ===============
			$this->key_mc_getAssignmentDetails =  md5($select_qry);
		    	$this->details = $this->memcache->get($this->key_mc_getAssignmentDetails);
		    
		    	if($this->details)
		    	{	
		      		$result = $this->details;  
		    	}
		    	else
		    	{
				$process_qry = $this->db->query($select_qry);
				$result      = $this->db->fetch($process_qry);	
		      		
		      		$this->memcache->set($this->key_mc_getAssignmentDetails, $result, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_TEN_MINUTES);
		    	}
			
			//$process_qry = $this->db->query($select_qry);
			//$result      = $this->db->fetch($process_qry);
			return $result;			
		}
	
		function getContent($assignment_id, $user_id, $fav, $page_no)
		{
			// load the pagination class
			$this->include_file('pagination_class.php','application/lib');
			// create the object of pagination
			$page= new pagination;	
			// call the setMax method of pagination class with "no of records" value and requested page no as a parameter
			$page->setMax(16,$page_no);
         		// call the setData method of pagination class with "table name" as a parameter	

			$joinAnd = "";
			$whereAnd = "";
			$whereClause = " AND FS.UserId='".$user_id."'";
			if($fav==1) {
				$whereAnd = $whereClause;
			} else {
				$joinAnd = $whereClause;	
			}

			$qrystr = "SELECT 
				   AIS.*,
				   FS.AssignmentImgSubmissionId AS fav_submission,
				   UM.name AS full_name
				   FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." AS AIS
				   LEFT JOIN ".TABLE_ASSIGNMENT." AS AM ON AIS.AssignmentId=AM.AssignmentId
				   LEFT JOIN ".TABLE_FAVOURITE_SUBMISSION." AS FS ON FS.AssignmentImgSubmissionId=AIS.AssignmentImgSubmissionId $joinAnd
				   LEFT JOIN ".TABLE_USER_MASTER." AS UM ON AIS.creatorId=UM.UserId
				   WHERE AIS.AssignmentId='".$assignment_id."' $whereAnd ORDER BY AIS.AssignmentImgSubmissionId desc"; 
		
			$page->setData1($qrystr);
         		// call the display method of pagination class with for getting the content of the database table
         		$display = $page->display();         
         		// call the displayLinks method of pagination class for getting the links by passing "number of links value and current page number from the url as a parameter
	         	$links = $page->displayLinks(4,$page_no);

			$contents = array();
			array_push($contents,$display);  
			array_push($contents,$links);

		      	$total_record_count = $contents[0]['total_records'];
		      	$total_content = $contents[0]['total_content']; //total records in paginated array
	      		$count = 1;

		      	#-- create the array that contains the paginated record (paginated record)------
		      	for($counter = 1; $counter <= $total_content; $counter++ )
	      		{
	        		$contentKey = "content$count";
	        		foreach($contents[0]['header'] as $key => $value)
	        		{
	        			$temp_array[$value] = $contents[0][$contentKey][$key]; 
	        		}
	        		$paginated_array[]= $temp_array;
	        		$count++;
	      		}
	     		#-----------------------(/paginated array)----------------------
	
			$return['paginated_array']    = $paginated_array;
        	  	$return['pagination_links']   = $contents[1];   
          		$return['total_count']        = $total_record_count;
          		$return['num_paginated_rec']  = $total_content;

			return $return;
		}


		function getContentPreview($assignment_id, $user_id, $fav, $page_no)
		{
			// load the pagination class
			$this->include_file('pagination_class.php','application/lib');
			// create the object of pagination
			$page= new pagination;	
			// call the setMax method of pagination class with "no of records" value and requested page no as a parameter
			$page->setMax(1,$page_no);
         		// call the setData method of pagination class with "table name" as a parameter	

			$joinAnd = "";
			$whereAnd = "";
			$whereClause = " AND FS.UserId='".$user_id."'";
			if($fav==1) {
				$whereAnd = $whereClause;
			} else {
				$joinAnd = $whereClause;	
			}


			
			$qrystr = "SELECT 
				   AIS.*,
				   FS.AssignmentImgSubmissionId AS fav_submission,
				   AM.paymentMethod,	
				   UM.name AS full_name,
				   MR.modelPdf 
				   FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." AS AIS
				   LEFT JOIN ".TABLE_ASSIGNMENT." AS AM ON AIS.AssignmentId=AM.AssignmentId
				   LEFT JOIN ".TABLE_FAVOURITE_SUBMISSION." AS FS ON FS.AssignmentImgSubmissionId=AIS.AssignmentImgSubmissionId $joinAnd
				   LEFT JOIN ".TABLE_USER_MASTER." AS UM ON AIS.creatorId=UM.UserId 
				   LEFT JOIN ".TABLE_MODEL_RELEASE." AS MR ON AIS.ModelId = MR.ModelId	
				   WHERE AIS.AssignmentId='".$assignment_id."' $whereAnd ORDER BY AIS.AssignmentImgSubmissionId desc";
		
			$page->setData1($qrystr);
         		// call the display method of pagination class with for getting the content of the database table
         		$display = $page->display();         
         		// call the displayLinks method of pagination class for getting the links by passing "number of links value and current page number from the url as a parameter
	         	$links = $page->displayLinks(4,$page_no);

			$contents = array();
			array_push($contents,$display);  
			array_push($contents,$links);

		      	$total_record_count = $contents[0]['total_records'];
		      	$total_content = $contents[0]['total_content']; //total records in paginated array
	      		$count = 1;

		      	#-- create the array that contains the paginated record (paginated record)------
		      	for($counter = 1; $counter <= $total_content; $counter++ )
	      		{
	        		$contentKey = "content$count";
	        		foreach($contents[0]['header'] as $key => $value)
	        		{
	        			$temp_array[$value] = $contents[0][$contentKey][$key]; 
	        		}
	        		$paginated_array[]= $temp_array;
	        		$count++;
	      		}
	     		#-----------------------(/paginated array)----------------------
	
			$return['paginated_array']    = $paginated_array;
        	  	$return['pagination_links']   = $contents[1];   
          		$return['total_count']        = $total_record_count;
          		$return['num_paginated_rec']  = $total_content;

			return $return;
		}

		function getBuyerInfo($user_id)
		{
			$query = "SELECT name, profileName, tagLine, city, profileImg ".
				 "FROM ".TABLE_USER_MASTER." ".
				 "WHERE UserId = '".$user_id."'";

			//  ======== memcache implementation ===============
			$this->key_mc_getBuyerInfo =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mc_getBuyerInfo);
		    
		    	if($this->details)
		    	{	
		      		$records = $this->details;  
		    	}
		    	else
		    	{	
		      		$records      = $this->db->fetch_array($query);
		      		$this->memcache->set($this->key_mc_getBuyerInfo, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_TEN_MINUTES);
		    	}

			//$row = $this->db->fetch_array($query);
			return 	$records[0];
		}

		function getDeadlineDate($assignment_id)
		{
			$query = "SELECT submissionDeadline, isLaunched, launchedDate, createdAt ".
				 "FROM ".TABLE_ASSIGNMENT." ".
				 "WHERE AssignmentId = '".$assignment_id."'";

			//  ======== memcache implementation ===============
			$this->key_mc_getDeadlineDate =  md5($query);
		    	$this->details = $this->memcache->get($this->key_mc_getDeadlineDate);
		    
		    	if($this->details)
		    	{	
		      		$records = $this->details;  
		    	}
		    	else
		    	{	
		      		$records      = $this->db->fetch_array($query);
		      		$this->memcache->set($this->key_mc_getDeadlineDate, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_TEN_MINUTES);
		    	}
	
			//$row = $this->db->fetch_array($query);
			return 	$records[0];	
		}

		function addToFavourite($content_id, $user_id)
		{
			$query = "SELECT creatorId FROM " . TABLE_ASSIGNMENT_IMG_SUBMISSION . " WHERE AssignmentImgSubmissionId = {$content_id}";
			$row = $this->db->fetch_array($query);
			$creatorId = $row[0]['creatorId'];
			
			$timestamp = strtotime(date('Y-m-d'));
			$query = "INSERT INTO ".TABLE_FAVOURITE_SUBMISSION." SET 
				  AssignmentImgSubmissionId = '".$content_id."',
				  UserId       = '".$user_id."',
				  timeStamp     = '".$timestamp."'";
			$process = $this->db->query($query);
			
			$query = "INSERT INTO ".TABLE_NOTIFICATION." SET 
				  creatorId = '".$creatorId."',
				  AssignmentImgSubmissionId       = '".$content_id."',type = '1',
				  date     = '".date('Y-m-d H:i:s')."'";
			$process = $this->db->query($query);
			
			
			$select = "SELECT FavouriteSubmissionId FROM ".TABLE_FAVOURITE_SUBMISSION." ".
				  "WHERE AssignmentImgSubmissionId = '".$content_id."' ".
				  "AND UserId = '".$user_id."'";
			$selProcess = $this->db->query($select);
			$num_rows   = $this->db->num_rows($selProcess);	
			return 	$num_rows;
		}

		function removeFromFavourite($content_id, $user_id)
		{
			$query = "DELETE FROM ".TABLE_FAVOURITE_SUBMISSION." ".
			         "WHERE AssignmentImgSubmissionId = '".$content_id."' ".
			         "AND UserId = '".$user_id."'";
			$process = $this->db->query($query);	

			$select = "SELECT FavouriteSubmissionId FROM ".TABLE_FAVOURITE_SUBMISSION." ".
				  "WHERE AssignmentImgSubmissionId = '".$content_id."' ".
				  "AND UserId = '".$user_id."'";
			$selProcess = $this->db->query($select);
			$num_rows   = $this->db->num_rows($selProcess);	
			return 	$num_rows;
		}

		#------ This function is to get imagery format name array ------------
		/*
		* Params : array of imagery ids
		* Return : array of fetched imagery names
		*/
		function getImageryFormat($imagery_format)
		{	
			$imagery_format_array = array();
			foreach($imagery_format as $key => $id)
			{
				if($id==1) 
				{ 
					$imagery_format_name = "Horizontal"; 
				} 
				elseif($id==2) 
				{	
					$imagery_format_name = "Vertical"; 
				}			
				array_push($imagery_format_array,$imagery_format_name);	
			}
			return $imagery_format_array;
		}


		function getAssignmentListing($userId)
		{
			/*// load the pagination class
			$this->include_file('pagination_class.php','application/lib');
			// create the object of pagination
			$page= new pagination;	
			// call the setMax method of pagination class with "no of records" value and requested page no as a parameter
			$page->setMax(10,$page_no);
         		// call the setData method of pagination class with "table name" as a parameter	*/

			$qrystr = "SELECT AssignmentId, assignmentRefId, lookingForText, AssignmentCategoryId, createdAt ".
				 " FROM ".TABLE_ASSIGNMENT." ".
				 " WHERE UserId = '".$userId."' AND isEnabled = 1 ORDER BY AssignmentId DESC";
			

			/*$page->setData1($qrystr);
         		// call the display method of pagination class with for getting the content of the database table
         		$display = $page->display();         
         		// call the displayLinks method of pagination class for getting the links by passing "number of links value and current page number from the url as a parameter
	         	$links = $page->displayLinks(4,$page_no);

			$contents = array();
			array_push($contents,$display);  
			array_push($contents,$links);

		      	$total_record_count = $contents[0]['total_records'];
		      	$total_content = $contents[0]['total_content']; //total records in paginated array
	      		$count = 1;

		      	#-- create the array that contains the paginated record (paginated record)------
		      	for($counter = 1; $counter <= $total_content; $counter++ )
	      		{
	        		$contentKey = "content$count";
	        		foreach($contents[0]['header'] as $key => $value)
	        		{
	        			$temp_array[$value] = $contents[0][$contentKey][$key]; 
	        		}
	        		$paginated_array[]= $temp_array;
	        		$count++;
	      		}
	     		#-----------------------(/paginated array)----------------------
	
			$return['paginated_array']    = $paginated_array;
        	  	$return['pagination_links']   = $contents[1];   
          		$return['total_count']        = $total_record_count;
          		$return['num_paginated_rec']  = $total_content;

			return $return;*/

			// ======= pagination starts here =======
			$current_page = 1;
			$param =  $this->getrequest();
			if($param['page']!='') 
			{
				$current_page = $param['page'];			
			} 
			$limit = 8;
			$this->include_file('new_paginator.php','application/lib');
			$page = new pagination;
			$result = $page->paginate($qrystr,$limit,$current_page);

			return $result;

			
		}
	

}