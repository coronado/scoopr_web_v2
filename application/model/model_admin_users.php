<?php
//===================================================================================
/* This class is for the purpose to check the admin login and return the admin basic info */
//===================================================================================

	class model_admin_users extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		
		function viewUserListing($search_arr)
		{ 
			$query = "SELECT UserId, name, profileName, emailAddress, userType ".
				 "FROM ".TABLE_USER_MASTER." where userType='".$search_arr['user_type']."' AND isDisabled = '".$search_arr['is_disabled']."'";

			if($search_arr['email_address']!='')	
			{
				$query .= " AND emailAddress = '".$search_arr['email_address']."'";				
			}

			$query .= " order by UserId desc";

			// ======= pagination starts here =======
			$current_page = 1;
			$param =  $this->getrequest();
			if($param['page']!='') 
			{
				$current_page = $param['page'];			
			} 
			$limit = 10;
			$this->include_file('new_paginator.php','application/lib');
			$page = new pagination;
			$result = $page->paginate($query,$limit,$current_page);

			return $result;
			//$row = $this->db->fetch_array($query);
			//return 	$row;			
		}

		function checkOldPassword($user_id, $request)
		{
			$old_password = md5($request['old_password']);
			$query = "SELECT UserId FROM ".TABLE_USER_MASTER." ".
				 "WHERE UserId = '".$user_id."' ".
				 "AND password = '".$old_password."'";
			$proccess = $this->db->query($query);
			$num_rows = $this->db->num_rows($proccess);
			if($num_rows==1) {
				return true;
			} else {
				return false;
			}
		}

		function updatePassword($user_id, $request)
		{
			$new_password = md5($request['new_password']);
			$query = "UPDATE ".TABLE_USER_MASTER." SET 
				  password = '".$new_password."'
				  WHERE UserId = '".$user_id."'";
			$proccess = $this->db->query($query);
			if($proccess) {
				return true;	
			} else {
				return false;
			}	
		}




		
	}
?>
