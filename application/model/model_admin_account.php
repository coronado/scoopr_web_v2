<?php
//=========================================================
/* This class is for the purpose to check the create admin */
//=========================================================

	class model_admin_account extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		#------ This function is to insert user
		/*
		* Params : posted data array with information to be insert
		* Return : inserted ID
		*/
		function create($request)
		{ 
			$user_type = '1';	
			$pwd = $request['password'];
			$email               =   mysql_escape_string($request['email_address']);
	        	$password            =   md5(mysql_escape_string($request['password']));
			$city                =   mysql_escape_string($request['city']);
			$password            =   md5($request['password']);
			$address             =   mysql_escape_string($request['address']);
			$telephone           =   mysql_escape_string($request['telephone']);
			
			$fname               =   mysql_escape_string($request['fname']);
			$lname               =   mysql_escape_string($request['lname']);
			$name		     =   $fname." ".$lname;	

			$profile_name        =   mysql_escape_string($request['profile_name']);
			$profile_description =   mysql_escape_string($request['profile_description']);
			$website             =   mysql_escape_string($request['website']);
			$founded             =   mysql_escape_string($request['founded']);
			$tag_line            =   mysql_escape_string($request['tag_line']);

			$title               =   mysql_escape_string($request['title_name']);
			$cdate               =   date('Y-m-d h:i:s');
 
			$select = "INSERT INTO ".TABLE_USER_MASTER." SET
					emailAddress 	    =  '".$email."',
					password            =  '".$password."',
					city 	            =  '".$city."',
					address             =  '".$address."',
					telephone           =  '".$telephone."',
					name                =  '".$name."',
					profileName         =  '".$profile_name."',
					profileDescription  =  '".$profile_description."',
					website             =  '".$website."',
					founded             =  '".$founded."',
					tagLine             =  '".$tag_line."',
					title               =  '".$title."',
					userType            =  '".$user_type."',
					isVerified          =  '1',
					createdAt           =  NOW()"; 
			$process = $this->db->query($select) or die("mysql_error");
			
			/*end*/
			
			if($process)
			{
				$to      = $email;
				$cc      = '';
				$bcc     = '';
				$from    = FROM_EMAIL; 
				$subject = 'Registration Details for Scoopr Media';
				
				$message= '<tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">Hi there! Thank you for creating an account.<br><br>We are excited to have you on board. Please let us know if there is any way we can make your experience better.</td>
                                                      </tr>
                                                      <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">Here are your registration details for Scoopr Media :<br><br>
							Email    : '.$email.'<br>
							Password : '.$pwd.'<br><br>
							</td>
                                                       </tr>';

 				
				$this->admin_registration_email($to, $from, $subject, $cc, $bcc, $message);
			}
			
			$user_id = (int)$this->db->insert_id();
			return $user_id;
		}
		
		function admin_registration_email($to, $from, $subject, $cc, $bcc, $message)
		{
			$this->include_file('email_class.php','application/lib');
			
			$email_obj = new email($to, $from, $subject, $cc, $bcc, $message);
		  	$send_email = $email_obj->send_email();
		}
		


		function getUserData($id)
		{
			 $query = "SELECT * FROM ".TABLE_USER_MASTER." WHERE UserId = $id"; 
			 	
			$row = $this->db->fetch_array($query) or die("error");
			#print_r($row);die;
			return 	$row;			
		}


		function updateUsers($user_id,$request)
		{
			$profile_name          = mysql_escape_string($request['profile_name']);
			$website               = mysql_escape_string($request['website']);
			$tag_line              = mysql_escape_string($request['tag_line']);
			$profile_description   = mysql_escape_string($request['profile_description']);
			$founded               = $request['founded'];
			$name                  = mysql_escape_string($request['full_name']);
			$address               = mysql_escape_string($request['address']);	
			$city                  = mysql_escape_string($request['city']);
			$telephone             = $request['telephone'];	

			$title                 = mysql_escape_string($request['title_name']);

			$update = "UPDATE ".TABLE_USER_MASTER." SET 
				   profileName           = '".$profile_name."',
				   website               = '".$website."',
				   tagLine               = '".$tag_line."',
				   profileDescription    = '".$profile_description."',
				   founded 	         = '".$founded."',
				   name                  = '".$name."',	
				   address               = '".$address."',
				   city                  = '".$city."',
				   telephone             = '".$telephone."',
				   title                 = '".$title."' 
				   WHERE UserId = '".$user_id."'";	

			$proccess = $this->db->query($update) or die("mysql database error");
			if($proccess) {
				return true;
			} else {
				return false;
			}
		}
		
		
		
		function update_admin_profile_pic($userid,$filename)
		{
			$update="UPDATE ".TABLE_USER_MASTER." SET profileImg='".$filename."' WHERE UserId='".$userid."'";
			$process = $this->db->query($update);
			if($process) {
				return true;	
			} else {
				return false;
			}	  
		}
		


		function updateProfileLogo($user_id, $filename)
		{
			$select = "SELECT profileImg FROM ".TABLE_USER_MASTER." ".
				  "WHERE UserId = '".$user_id."'";
			$process = $this->db->query($select);
			$result = $this->db->fetch($process);
			$image =  $result['profileImg'];	

			$full_image_path = "images/profile_img/".$image;
			if (file_exists($full_image_path)) 
			{
				if($image!='default_profile_logo.png') {	
					$del = unlink($full_image_path);
				}
			}

			$update = "UPDATE ".TABLE_USER_MASTER." SET 
				   profileImg	= '".$filename."' 
				   WHERE UserId = '".$user_id."'";
			$proccess = $this->db->query($update) or die("Mysql Server Database Error: function - updateProfileLogo");	
		}

		#------ This function is to check email address
		/*
		* Params : posted data email address to check the existing email address
		* Return : true or false
		*/
		function checkEmailExistence($email)
		{
			$email = mysql_escape_string($email);
			$select = "SELECT count(*) as is_user FROM ".TABLE_USER_MASTER." WHERE emailAddress = '".$email."' AND userType=1";
			//echo $select;
			$user_row = $this->db->fetch_array($select);
			//print_r($user_row);die;
			if($user_row[0]['is_user']==0){ 
				return 0;
			} else {
				return 1;
			}
		}

		function enableUser($user_id)
		{
			$query = "UPDATE ".TABLE_USER_MASTER." SET isDisabled = 1 WHERE UserId = '".$user_id."'";
			$proccess = $this->db->query($query);
			
			$query1 = "UPDATE ".TABLE_ASSIGNMENT." SET isEnabled = 1 WHERE UserId = $user_id";
			$proccess1   = $this->db->query($query1);

			if($proccess) {
				return true;
			} else {
				return false;
			}
		}

		function disableUser($user_id)
		{ 
			$query = "UPDATE ".TABLE_USER_MASTER." SET isDisabled = 0 WHERE UserId = '".$user_id."'";
			$proccess = $this->db->query($query);

			$query1 = "UPDATE ".TABLE_ASSIGNMENT." SET isEnabled = 0 WHERE UserId = $user_id";
			$proccess1   = $this->db->query($query1);

			return true;
		}

		function disableCreator($creator_id)
		{ 
			$query = "UPDATE ".TABLE_USER_MASTER." SET isDisabled = 0 WHERE UserId = '".$creator_id."'";
			$proccess = $this->db->query($query);

			return true;
		}


		function deleteUser($user_id)
		{		

			$selQuery = "SELECT AssignmentId FROM ".TABLE_ASSIGNMENT." WHERE UserId = $user_id";
			$result   = $this->db->fetch_array($selQuery);
			foreach($result as $value)
			{
				$aid = $value['AssignmentId'];
								
				/* ==== SELECT IMAGES FROM TABLE_ASSIGNMENT_IMG_SUBMISSION FOR DELETION ===== */
				$query1 = "SELECT AssignmentImgSubmissionId, imageName, mediumSizeImage, smallSizeImage FROM ".
					" ".TABLE_ASSIGNMENT_IMG_SUBMISSION." WHERE AssignmentId = '".$aid."'";	
				$proccess1 = $this->db->query($query1) or die("database query1 error");
				$num_rows1 = $this->db->num_rows($proccess1);
				$result1 = $this->db->fetch_array($query1);
				
				if($num_rows1>0)
				{
					foreach($result1 as $records)
					{
						/* ==== DELETE FAVOURITE SUBMISSIONS FROM TABLE_FAVOURITE_SUBMISSION ==== */
						$query2 = "DELETE FROM ".TABLE_FAVOURITE_SUBMISSION." ".
							"WHERE AssignmentImgSubmissionId = '".$records['AssignmentImgSubmissionId']."'";
						$proccess2 = $this->db->query($query2) or die("database query2 error");
		
						/* ======= DELETING IMAGES FROM THE FOLDER  ========= */
						$original_image_path = "images/assign_img_submission/".$records['imageName'];
						$medium_image_path = "images/assign_img_submission/medium_size_image/".$records['imageName'];
						$small_image_path = "images/assign_img_submission/small_size_image/".$records['imageName'];
						if(file_exists($original_image_path)) 
						{
							unlink($original_image_path);
						}
						if(file_exists($medium_image_path)) 
						{
							unlink($medium_image_path);
						}
						if(file_exists($small_image_path)) 
						{
							unlink($small_image_path);
						}
		
					}
				}
			
				/* ======= DELETING TABLE_ASSIGNMENT_IMG_SUBMISSION RECORDS ======= */
				$query3 = "DELETE FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." WHERE AssignmentId = '".$aid."'";
				$proccess3 = $this->db->query($query3) or die("database query3 error");
		
				/* ======= DELETING TABLE_ASSIGN_OVERVIEW RECORDS ======= */
				$query4 = "DELETE FROM ".TABLE_ASSIGN_OVERVIEW." WHERE AssignmentId = '".$aid."'";
				$proccess4 = $this->db->query($query4) or die("mysql deleting assignment overview error");
	
				/* ======= DELETING TABLE_FAVOURITE_ASSIGNMENT RECORDS ======= */
				$query5 = "DELETE FROM ".TABLE_FAVOURITE_ASSIGNMENT." WHERE AssignmentId = '".$aid."'";
				$proccess5 = $this->db->query($query5) or die("mysql deleting favourite assignment error");
	
	
				/* ========= DELETING IMAGES FROM TABLE_REFERENCE_IMAGE ============ */
				$query6 = "SELECT image1, image2, image3 ".
					"FROM ".TABLE_REFERENCE_IMAGE." ".
					"WHERE AssignmentId = '".$aid."'";	
				$proccess6 = $this->db->query($query6) or die("database query6 error");
				$num_rows6 = $this->db->num_rows($proccess6);
				$result6 = $this->db->fetch($proccess6);
				if($num_rows6 > 0)
				{
					$image_path1 = "images/reference_img/".$result6['image1'];
					$image_path2 = "images/reference_img/".$result6['image2'];
					$image_path3 = "images/reference_img/".$result6['image3'];
					if(file_exists($image_path1)) 
					{
						unlink($image_path1);
					}
					if(file_exists($image_path2)) 
					{
						unlink($image_path2);
					}
					if(file_exists($image_path3)) 
					{
						unlink($image_path3);
					}
				}
	
				/* ========= DELETING RECORDS FROM TABLE_ASSIGNMENT ============ */
				$query7 = "DELETE FROM ".TABLE_ASSIGNMENT." WHERE AssignmentId = '".$aid."'";
				$proccess7 = $this->db->query($query7) or die("mysql deleting assignment error");

			}
			
			/* ========= SELECT AND DELETING BRAND IMAGE ============ */		
			$query8 = "SELECT profileImg ".
				  "FROM ".TABLE_USER_MASTER." ".
				  "WHERE UserId = '".$user_id."'";
			$result8 = $this->db->fetch_array($query8);
			$brand_image_path = "images/profile_img/".$result8[0]['profileImg'];
			if(file_exists($brand_image_path)) 
			{
				if($result8[0]['profileImg']!='default_profile_logo.png') 
				{	
					unlink($brand_image_path);
				}
			}		

			/* ========= DELETING RECORDS FROM TABLE_USER_MASTER ============ */
			$query9 = "DELETE FROM ".TABLE_USER_MASTER." WHERE UserId = '".$user_id."'";
			$result9 = $this->db->query($query9) or die("database query9 error");
		}
		
		function profileDetail($user_id)
		{
			$query = "SELECT UserId, name, emailAddress, password, userType, city, address, telephone, profileName, profileDescription, website, founded, tagLine, profileImg, createdAt ".
				 "FROM ".TABLE_USER_MASTER." ".
				 "WHERE UserId = '".$user_id."'";
			$result  = $this->db->fetch_array($query);
			return $result[0];
		}

		

	}
?>
