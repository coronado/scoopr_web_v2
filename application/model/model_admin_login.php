<?php
//===================================================================================
/* This class is for the purpose to check the admin login and return the admin basic info */
//===================================================================================

	class model_admin_login extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		#------ This function is to check wheather user exists or not ---------
		/*
		* Params : posted data array with information to be checked 
		* Return : error message
		*/ 
		function check_login($request)
		{	
			$email     = mysql_escape_string($request['email_address']);
	                $password  = md5(mysql_escape_string($request['password']));
			//$user_type = $request['user_type'];

			$select = "SELECT UserId,isVerified ".
				  "FROM ".TABLE_USER_MASTER." ".
				  "WHERE emailAddress = '$email' ".
				  "AND password = '$password' ".
				  "AND userType = 3"; 
				  

			$result = $this->db->query($select);
			$numrows = $this->db->num_rows($result);			
			
			$errMsg = "";
			if($numrows==0)
			{
				$errMsg = "Incorrect Username or Password";
			}

			return $errMsg;
		}

		#------ This function is to get user record need to stored in session ---------
		/*
		* Params : posted data array with information to be checked 
		* Return : array of userid, firstname, lastname, email
		*/ 
		function get_user_details($email)
		{
			$select = "SELECT UserId, name, password, profileName, profileImg ".
				  "FROM ".TABLE_USER_MASTER." ".
				  "WHERE emailAddress = '$email'";
			$records  = $this->db->fetch_array($select);
			
			$sessionRecords['admin_user_id']= $records[0]['UserId'];
			$sessionRecords['admin_name']= $records[0]['name'];
			$sessionRecords['admin_email_address']= $email;
			$sessionRecords['admin_profile_name']= $records[0]['profileName'];
			$sessionRecords['admin_profile_img']= $records[0]['profileImg'];
			return $sessionRecords;
		}

	}
?>
