<?php
//===================================================================================
/* This class is for the purpose to check the login and return the user basic info */
//===================================================================================

	class model_request_demo extends Application
	{
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		function requestdemo($request)
		{	
			$saveRequest = $this->saveDemoRequests($request);
			
			$first_name     =  mysql_escape_string($request['first_name']);	
			$last_name      =  mysql_escape_string($request['last_name']);	
			$email_address  =  mysql_escape_string($request['email_address']);	
			$telephone      =  $request['telephone'];	
			$company_name   =  mysql_escape_string($request['company_name']);	
			$title_name     =  mysql_escape_string($request['title_name']);	

			$to      = $email_address;
			$cc      = '';
			$bcc     = '';
			$from    = FROM_EMAIL; 
			$logosource = PATH."images/logo.png";

			$subject_to_user = 'Thank you for requesting a demo';
			$message_to_user = '<tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">Hi there! and <b>thank you</b> for requesting a demo.</td>
                                                      </tr>
                                                      <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">We will be in contact with you shortly, to help you learn how Scoopr can benifit your business.</td>
                                                      </tr>';


			$subject_to_admin = 'Request a demo message';	
			$message_to_admin = '<tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">Hi Admin! '.$first_name.' requested for the demo.</td>
                                                      </tr>
                                                      <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;"><br>
							First name    : '.$first_name.'<br>
							Last name     : '.$last_name.'<br>
							Email address : '.$email_address.'<br>
							Telephone     : '.$telephone.'<br>
							Company       : '.$company_name.'<br>
							Title         : '.$title_name.'<br>	
							</td>
                                                      </tr>';

			$this->request_demo_email_toadmin($from, $to, $subject_to_admin, $cc, $bcc, $message_to_admin);
			$this->request_demo_email_touser($to, $from, $subject_to_user, $cc, $bcc, $message_to_user);
		}

		function saveDemoRequests($request)
		{
			$first_name     =  mysql_escape_string($request['first_name']);	
			$last_name      =  mysql_escape_string($request['last_name']);	
			$email_address  =  mysql_escape_string($request['email_address']);	
			$telephone      =  $request['telephone'];	
			$company_name   =  mysql_escape_string($request['company_name']);	
			$title_name     =  mysql_escape_string($request['title_name']);	
			$requested_date =  date('Y-m-d h:i:s');
			
			$qry = "INSERT into ".TABLE_DEMO_REQUEST." SET 
				firstName     = '".$first_name."',
				lastName      = '".$last_name."',
				emailAddress  = '".$email_address."',
				telephone      = '".$telephone."',
				companyName   = '".$company_name."',
				titleName     = '".$title_name."',
				requestedDate = '".$requested_date."'"; 

			$proccess =  $this->db->query($qry);
			if($proccess) {
				return true;
			} else {
				return false;	
			}
		}

		function checkEmail($request)
		{
			$query = "SELECT DemoRequestId FROM ".TABLE_DEMO_REQUEST." WHERE emailAddress = '".$request['email_address']."'";
			$result =  $this->db->query($query);
			$num_rows = $this->db->num_rows($result);
			
			if($num_rows>0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function request_demo_email_toadmin($to, $from, $subject, $cc, $bcc, $message)
		{
			$this->include_file('email_class.php','application/lib');
			
			$email_obj = new email($to, $from, $subject, $cc, $bcc, $message);
		  	$send_email = $email_obj->send_email();
		}
		function request_demo_email_touser($to, $from, $subject, $cc, $bcc, $message)
		{
			$this->include_file('email_class.php','application/lib');
			
			$email_obj = new email($to, $from, $subject, $cc, $bcc, $message);
		  	$send_email = $email_obj->send_email();
		}

	

	}
?>
