<?php
	class model_review_assignment extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		#------ This function is to fetch assignment details
		/*
		* Params : assignment id
		* Return : array of fetched assignment details
		*/
		function preview_assignment($assignment_id)
		{
			// TABLE_REFERENCE_IMAGE
			 $select  = "SELECT sa.AssignmentId, sa.UserId, sa.lookingFor, sa.lookingForText, sa.hopeToSelect, sa.ourBudget, sa.submissionDeadline, sa.imageryFor, sa.imageryForText, sa.rewards, sa.shareCondition1, sa.shareCondition2, sa.shareCondition3, sa.modelRelease, sa.imageryFormat, sa.referenceImageText, sa.createdAt, sa.isLaunched, sa.launchedDate, su.profileName, su.profileImg, su.tagLine, sr.image1, sr.image2, sr.image3, sc.categoryName ".
			"FROM ".TABLE_ASSIGNMENT." AS sa ".
			"INNER JOIN ".TABLE_USER_MASTER." AS su ".
			"ON sa.UserId=su.UserId ".
			"LEFT JOIN ".TABLE_ASSIGNMENT_CATEGORY." AS sc ".
			"ON sa.AssignmentCategoryId=sc.AssignmentCategoryId ".
			"LEFT JOIN ".TABLE_REFERENCE_IMAGE." AS sr ".
			"ON sa.AssignmentId=sr.AssignmentId ".
			"WHERE sa.AssignmentId = '".$assignment_id."'"; 

			//  ======== memcache implementation ===============
			$this->key_mra_preview =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mra_preview);
		    
		    	if($this->details)
		    	{	
		      		$records = $this->details;  
		    	}
		    	else
		    	{	
				$process_qry = $this->db->query($select);
		      		$records      = $this->db->fetch($process_qry);
		      		$this->memcache->set($this->key_mra_preview, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_TEN_SECONDS);
		    	}
			
			//$result      = $this->db->fetch($process_qry);
			return $records;
		}

		#------ This function is to get imagery name array ------------
		/*
		* Params : array of imagery ids
		* Return : array of fetched imagery names
		*/
		function getImagery($imagery_for)
		{	
			$imagery_for_array = array();
			foreach($imagery_for as $key => $id)
			{
				$select = "SELECT imageryForName FROM ".TABLE_IMAGERY_FOR." ".
					  "WHERE ImageryForId = '".$id."'";
				$query = $this->db->query($select);
				$result = $this->db->fetch($query);	
				array_push($imagery_for_array,$result['imageryForName']);	
			}
			return $imagery_for_array;
		}


		#------ This function is to get imagery format name array ------------
		/*
		* Params : array of imagery ids
		* Return : array of fetched imagery names
		*/
		function getImageryFormat($imagery_format)
		{	
			$imagery_format_array = array();
			foreach($imagery_format as $key => $id)
			{
				if($id==1) 
				{ 
					$imagery_format_name = "Horizontal"; 
				} 
				elseif($id==2) 
				{	
					$imagery_format_name = "Vertical"; 
				}			
				array_push($imagery_format_array,$imagery_format_name);	
			}
			return $imagery_format_array;
		}


		#------ This function is to finally launch assignment ------------
		/*
		* Params : assignment id
		* Return : 
		*/
		function launch_assignment($assignment_id)
		{
			$update = "UPDATE ".TABLE_ASSIGNMENT." SET 
				   isLaunched = 1,
				   launchedDate = now() 	
				   WHERE AssignmentId = '".$assignment_id."'";
			$process = $this->db->query($update);

			$email   = $this->getsession('email_address');
			$to      = $email;
			$cc      = '';
			$bcc     = '';
			$from    = FROM_EMAIL; 
			$subject = 'Thank you for creating an assignment';

			$message = "<tr>
						<td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;'>Hi There! Thank you for creating an assignment with Scoopr!</td>
						</tr>
				<tr>
						<td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;'>We're excited to serve you! Your assignment will be posted shortly.</td>
						</tr>
						<tr>
						<td valign='top'>&nbsp;</td>
						</tr>";
			
			$this->email_touser($to, $from, $subject, $cc, $bcc, $message);

			return true;
		}

		function email_touser($to, $from, $subject, $cc, $bcc, $message)
		{
			$this->include_file('email_class.php','application/lib');
			
			$email_obj = new email($to, $from, $subject, $cc, $bcc, $message);
		  	$send_email = $email_obj->send_email();
		}
			

		function getAllDeviceToken()
		{
			$select = "SELECT deviceTokenId FROM ".TABLE_USER_MASTER." ".
				 "WHERE userType = 2";

			//  ======== memcache implementation ===============
			$this->key_mra_gettoken =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mra_gettoken);
		    
		    	if($this->details)
		    	{	
		      		$records = $this->details;  
		    	}
		    	else
		    	{	
		      		$records      = $this->db->fetch_array($select);
		      		$this->memcache->set($this->key_mra_gettoken, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_ONE_MINUTE);
		    	}

			$device_token = array();
			foreach($records as $row)
			{	
				if($row['deviceTokenId']!='') 
				{
					array_push($device_token,$row['deviceTokenId']); 
				}
			}
			
			return $device_token;
		}

	}
?>
