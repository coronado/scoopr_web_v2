<?php
//===================================================================================
/* This class is the model of adminassignment controller */
//===================================================================================

	class model_admin_assignment extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
			
		}

		
		function viewAssignmentListing($search_arr)
		{
			$query = "SELECT sa.AssignmentId, sa.UserId, sa.lookingForText, sa.ourBudget, sa.submissionDeadline, sa.rewards, sa.modelRelease, sa.createdAt, sa.isLaunched, sa.launchedDate, sa.orderBy, su.profileName ".
			"FROM ".TABLE_ASSIGNMENT." AS sa ".
			"INNER JOIN ".TABLE_USER_MASTER." AS su ".
			"ON sa.UserId=su.UserId"; 
			
			if($search_arr['is_enabled']==0)
			{
				$query .= " AND sa.isEnabled = 0";
			}
			else
			{
				$query .= " AND su.isDisabled = 1 AND sa.isEnabled = 1";
			}			

			if($search_arr['uid']!='')	
			{
				$query .= " AND sa.UserId = '".$search_arr['uid']."'";				
			}

			if($search_arr['aid']!='' && $search_arr['email_address']!='')	
			{
				$query .= " AND sa.AssignmentId = '".$search_arr['aid']."' AND su.emailAddress = '".$search_arr['email_address']."'";				
			}
			elseif($search_arr['aid']!='' && $search_arr['email_address']=='')
			{
				$query .= " AND sa.AssignmentId = '".$search_arr['aid']."'";				
			}
			elseif($search_arr['email_address']!='' && $search_arr['aid']=='')
			{
				$query .= " AND su.emailAddress = '".$search_arr['email_address']."'";				
			}		

			$query .= " ORDER BY sa.AssignmentId desc";
			/*$query  = "SELECT sa.id, sa.user_id, sa.looking_for, sa.looking_for_text, sa.hope_to_select, sa.our_budget, sa.submission_deadline, sa.imagery_for, sa.imagery_for_text, sa.rewards, sa.model_release, sa.imagery_format, sa.reference_image_text, sa.created_at, sa.is_launched, sa.launched_date, su.profile_name, su.name, su.profile_img, su.tag_line, sr.image1, sr.image2, sr.image3 ".
			"FROM ".TABLE_ASSIGNMENT." AS sa ".
			"INNER JOIN ".TABLE_USER_MASTER." AS su ".
			"ON sa.user_id=su.id ".
			"LEFT JOIN ".TABLE_REFERENCE_IMAGE." AS sr ".
			"ON sa.id=sr.assignment_id ".
			"WHERE sa.id = '".$assignment_id."'";*/

			//$query = "SELECT "

			// ======= pagination starts here =======
			
			$limit = 10;
			$this->include_file('new_paginator.php','application/lib');
			$page = new pagination;
			$result = $page->paginate($query,$limit,$search_arr['current_page']);

			return $result;
		}
		
		function createAssignment($request, $assignment_id)
		{
			$ref = $this->getRefNumber($request['buyer_id']);
			$ref_number = $ref+1;

			$imagery_for = serialize($request['imagery_for']);
			$imagery_format = serialize($request['imagery_format']);
			$request =  $this->escapeString($request);

			/* ===========  checking a payment method ============ */
			if($request['our_budget']!='' && $request['rewards']!='') {
				$payment_method  = 3;
			} elseif($request['our_budget']!='' && $request['rewards']=='') {
				$payment_method  = 1;
			} elseif($request['rewards']!='' && $request['our_budget']=='') {
				$payment_method  = 2;
			}

			
			$insert_qry = "INSERT INTO ".TABLE_ASSIGNMENT." SET 
				      		UserId               =       '".$request['buyer_id']."',
						assignmentRefId      =       '".$ref_number."',
						AssignmentCategoryId =       '".$request['category_id']."',
						hopeToSelect         =       '".$request['hope_to_select']."',
						lookingForText       =       '".$request['looking_for_text']."',
						ourBudget            =       '".$request['our_budget']."',
						rewards              =       '".$request['rewards']."',
						shareCondition1      =       '".$request['share_condition_1']."',
						shareCondition2      =       '".$request['share_condition_2']."',
						shareCondition3      =       '".$request['share_condition_3']."',
						submissionDeadline   =       '".$request['submission_deadline']."',
						imageryFor           =       '".$imagery_for."',
						imageryForText       =       '".$request['imagery_for_text']."',
						modelRelease         =       '".$request['model_release']."',
						imageryFormat        =       '".$imagery_format."',
						referenceImageText   =       '".$request['reference_image_text']."',
						paymentMethod	     =       '".$payment_method."',
						orderBy              =       '".$request['order_by']."',
						createdAt            =       NOW()"; 

			$process_insert  = $this->db->query($insert_qry);
			$assignment_id = (int)$this->db->insert_id();
			return $assignment_id;		
		}

		function insertReferenceImages($assignment_id,$success_file_array)
		{
			$query = "INSERT INTO ".TABLE_REFERENCE_IMAGE." SET 
				  AssignmentId    =    '".$assignment_id."',	
				  image1    =    '".@$success_file_array[0]."',	
				  image2    =    '".@$success_file_array[1]."',	
				  image3    =    '".@$success_file_array[2]."'";

			$process = $this->db->query($query);
		}

		function disableassignment($aid)
		{
			$query = "UPDATE ".TABLE_ASSIGNMENT." SET ".
				 "isEnabled = 0 ".
				 "WHERE AssignmentId = '".$aid."'";
			$proccess = $this->db->query($query);
			if($proccess) { 
				return true;
			} else {
				return false;
			}
		}

		function enableassignment($aid)
		{
			$query = "UPDATE ".TABLE_ASSIGNMENT." SET ".
				 "isEnabled = 1 ".
				 "WHERE AssignmentId = '".$aid."'";
			$proccess = $this->db->query($query);
			if($proccess) { 
				return true;
			} else {
				return false;
			}
		}

		function deleteassignment($aid)
		{	
			/* ==== SELECT IMAGES FROM TABLE_ASSIGNMENT_IMG_SUBMISSION FOR DELETION ===== */
			$query1 = "SELECT AssignmentImgSubmissionId, imageName, mediumSizeImage, smallSizeImage FROM ".
				  " ".TABLE_ASSIGNMENT_IMG_SUBMISSION." WHERE AssignmentId = '".$aid."'";	
			$proccess1 = $this->db->query($query1) or die("database query1 error");
			$num_rows1 = $this->db->num_rows($proccess1);
			$result1 = $this->db->fetch_array($query1);
			
			if($num_rows1>0)
			{
				foreach($result1 as $records)
				{
					/* ==== DELETE FAVOURITE SUBMISSIONS FROM TABLE_FAVOURITE_SUBMISSION ==== */
					$query2 = "DELETE FROM ".TABLE_FAVOURITE_SUBMISSION." ".
						"WHERE AssignmentImgSubmissionId = '".$records['AssignmentImgSubmissionId']."'";
					$proccess2 = $this->db->query($query2) or die("database query2 error");
	
					/* ======= DELETING IMAGES FROM THE FOLDER  ========= */
					$original_image_path = "images/assign_img_submission/".$records['imageName'];
					$medium_image_path = "images/assign_img_submission/medium_size_image/".$records['imageName'];
					$small_image_path = "images/assign_img_submission/small_size_image/".$records['imageName'];
					if(file_exists($original_image_path)) 
					{
						unlink($original_image_path);
					}
					if(file_exists($medium_image_path)) 
					{
						unlink($medium_image_path);
					}
					if(file_exists($small_image_path)) 
					{
						unlink($small_image_path);
					}
	
				}
			}
		
			/* ======= DELETING TABLE_ASSIGNMENT_IMG_SUBMISSION RECORDS ======= */
			$query3 = "DELETE FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." WHERE AssignmentId = '".$aid."'";
			$proccess3 = $this->db->query($query3) or die("database query3 error");
	
			/* ======= DELETING TABLE_ASSIGN_OVERVIEW RECORDS ======= */
			$query4 = "DELETE FROM ".TABLE_ASSIGN_OVERVIEW." WHERE AssignmentId = '".$aid."'";
			$proccess4 = $this->db->query($query4) or die("mysql deleting assignment overview error");

			/* ======= DELETING TABLE_FAVOURITE_ASSIGNMENT RECORDS ======= */
			$query5 = "DELETE FROM ".TABLE_FAVOURITE_ASSIGNMENT." WHERE AssignmentId = '".$aid."'";
			$proccess5 = $this->db->query($query5) or die("mysql deleting favourite assignment error");


			/* ========= DELETING IMAGES FROM TABLE_REFERENCE_IMAGE ============ */
			$query6 = "SELECT image1, image2, image3 ".
				  "FROM ".TABLE_REFERENCE_IMAGE." ".
				  "WHERE AssignmentId = '".$aid."'";	
			$proccess6 = $this->db->query($query6) or die("database query6 error");
			$num_rows6 = $this->db->num_rows($proccess6);
			$result6 = $this->db->fetch($proccess6);
			if($num_rows6 > 0)
			{
				$image_path1 = "images/reference_img/".$result6['image1'];
				$image_path2 = "images/reference_img/".$result6['image2'];
				$image_path3 = "images/reference_img/".$result6['image3'];
				if(file_exists($image_path1)) 
				{
					unlink($image_path1);
				}
				if(file_exists($image_path2)) 
				{
					unlink($image_path2);
				}
				if(file_exists($image_path3)) 
				{
					unlink($image_path3);
				}
			}

			/* ========= DELETING RECORDS FROM TABLE_ASSIGNMENT ============ */
			$query7 = "DELETE FROM ".TABLE_ASSIGNMENT." WHERE AssignmentId = '".$aid."'";
			$proccess7 = $this->db->query($query7) or die("mysql deleting assignment error");

		}


		function getAllBrands()
		{
			$select = "SELECT UserId, profileName, emailAddress FROM ".TABLE_USER_MASTER." WHERE userType=1 AND isDisabled =1";
			$result = $this->db->fetch_array($select);
			return $result;
		}
		
		function getImageryName()
		{
			$select = "SELECT * FROM ".TABLE_IMAGERY_FOR."";
			$result = $this->db->fetch_array($select);
			return $result;	  
		}
		function getCategories()
		{
			$select = "SELECT * FROM ".TABLE_ASSIGNMENT_CATEGORY." WHERE status = 1";
			$result = $this->db->fetch_array($select);
			return $result;	  
		}

		function getRefNumber($user_id)
		{
			$query = "SELECT MAX(assignmentRefId) as ref_number ".
				 "FROM ".TABLE_ASSIGNMENT." ".
				 "WHERE UserId = '".$user_id."'";
			$result = $this->db->fetch_array($query);
			return $result[0]['ref_number'];
		}	

		
		function getAssignmentBasicDetails($assignment_id)
		{
			$select_qry  = "SELECT sa.AssignmentId, sa.UserId, sa.lookingFor, sa.lookingForText, sa.hopeToSelect, sa.ourBudget, sa.submissionDeadline, sa.imageryFor, sa.imageryForText, sa.rewards, sa.shareCondition1, sa.shareCondition2, sa.shareCondition3, sa.modelRelease, sa.imageryFormat, sa.referenceImageText, sa.createdAt, sa.isLaunched, sa.launchedDate, sa.orderBy, sa.AssignmentCategoryId, su.profileName, su.name, su.profileImg, su.tagLine, sr.image1, sr.image2, sr.image3 ".
			"FROM ".TABLE_ASSIGNMENT." AS sa ".
			"INNER JOIN ".TABLE_USER_MASTER." AS su ".
			"ON sa.UserId=su.UserId ".
			"LEFT JOIN ".TABLE_REFERENCE_IMAGE." AS sr ".
			"ON sa.AssignmentId=sr.AssignmentId ".
			"WHERE sa.AssignmentId = '".$assignment_id."'";
			
			$process_qry = $this->db->query($select_qry);
			$result      = $this->db->fetch($process_qry);
			return $result;	
		}


		function getAssignmentAllDetails($assignment_id)
		{
			// TABLE_REFERENCE_IMAGE
			$select_qry  = "SELECT sa.AssignmentId, sa.UserId, sa.lookingFor, sa.lookingForText, sa.hopeToSelect, sa.ourBudget, sa.submissionDeadline, sa.imageryFor, sa.imageryForText, sa.rewards, sa.shareCondition1, sa.shareCondition2, sa.shareCondition3, sa.modelRelease, sa.imageryFormat, sa.referenceImageText, sa.createdAt, sa.isLaunched, sa.launchedDate, sa.orderBy, su.profileName, su.profileImg, su.tagLine, sr.image1, sr.image2, sr.image3, sc.categoryName ".
			"FROM ".TABLE_ASSIGNMENT." AS sa ".
			"INNER JOIN ".TABLE_USER_MASTER." AS su ".
			"ON sa.UserId=su.UserId ".
			"LEFT JOIN ".TABLE_ASSIGNMENT_CATEGORY." AS sc ".
			"ON sa.AssignmentCategoryId=sc.AssignmentCategoryId ".
			"LEFT JOIN ".TABLE_REFERENCE_IMAGE." AS sr ".
			"ON sa.AssignmentId=sr.AssignmentId ".
			"WHERE sa.AssignmentId = '".$assignment_id."'";
			
			$process_qry = $this->db->query($select_qry);
			$result      = $this->db->fetch($process_qry);
			return $result;
		}


		function editAssignment($request,$assignment_id)
		{
			//echo "<pre>"; print_r($request['imagery_for']); die;
			$imagery_for = serialize($request['imagery_for']);
			$imagery_format = serialize($request['imagery_format']);
			$request =  $this->escapeString($request);
			
			/* ===========  checking a payment method ============ */
			if($request['our_budget']!='' && $request['rewards']!='') {
				$payment_method  = 3;
			} elseif($request['our_budget']!='' && $request['rewards']=='') {
				$payment_method  = 1;
			} elseif($request['rewards']!='' && $request['our_budget']=='') {
				$payment_method  = 2;
			}


			$update_qry = "UPDATE ".TABLE_ASSIGNMENT." SET 
						AssignmentCategoryId   =       '".$request['category_id']."',
				      		hopeToSelect           =       '".$request['hope_to_select']."',
						lookingForText         =       '".$request['looking_for_text']."',
						ourBudget              =       '".$request['our_budget']."',
						rewards                =       '".$request['rewards']."',
						shareCondition1        =       '".$request['share_condition_1']."',
						shareCondition2        =       '".$request['share_condition_2']."',
						shareCondition3        =       '".$request['share_condition_3']."',
						submissionDeadline     =       '".$request['submission_deadline']."',
						imageryFor             =       '".$imagery_for."',
						imageryForText         =       '".$request['imagery_for_text']."',
						modelRelease           =       '".$request['model_release']."',
						imageryFormat          =       '".$imagery_format."',
						referenceImageText     =       '".$request['reference_image_text']."',
						paymentMethod	       =       '".$payment_method."',
						orderBy                =       '".$request['order_by']."' 
						WHERE AssignmentId     =       '".$assignment_id."'"; 

			$process_update  = $this->db->query($update_qry);
		}

		function editReferenceImages($assignment_id,$key,$file_alias)
		{
			$select = "SELECT image".$key." ".
				  "FROM ".TABLE_REFERENCE_IMAGE." ".
				  "WHERE AssignmentId = '".$assignment_id."'";
			$process = $this->db->query($select);
			$result = $this->db->fetch($process);
			$image =  $result['image'.$key];	

			$full_image_path = "images/reference_img/".$image;
			if (file_exists($full_image_path)) 
			{	
				$del = unlink($full_image_path);
			}

			$update  = "UPDATE ".TABLE_REFERENCE_IMAGE." SET 
				    image".$key." = '".$file_alias."'
				    WHERE AssignmentId = '".$assignment_id."'";
			$update_process = $this->db->query($update) or die("aaaaa");
		}


		function getBuyerInfo($user_id)
		{
			$query = "SELECT name, profileName, tagLine, city, profileImg ".
				 "FROM ".TABLE_USER_MASTER." ".
				 "WHERE UserId = '".$user_id."'";
			$row = $this->db->fetch_array($query);
			return 	$row[0];
		}


		function getDeadlineDate($assignment_id)
		{
			$query = "SELECT submissionDeadline, isLaunched, launchedDate, createdAt ".
				 "FROM ".TABLE_ASSIGNMENT." ".
				 "WHERE AssignmentId = '".$assignment_id."'";
			$row = $this->db->fetch_array($query);
			return 	$row[0];	
		}

		#------ This function is to get imagery name array ------------
		/*
		* Params : array of imagery ids
		* Return : array of fetched imagery names
		*/
		function getImagery($imagery_for)
		{	
			$imagery_for_array = array();
			foreach($imagery_for as $key => $id)
			{
				$select = "SELECT imageryForName FROM ".TABLE_IMAGERY_FOR." ".
					  "WHERE ImageryForid = '".$id."'";
				$query = $this->db->query($select);
				$result = $this->db->fetch($query);	
				array_push($imagery_for_array,$result['imageryForName']);	
			}
			return $imagery_for_array;
		}


		#------ This function is to get imagery format name array ------------
		/*
		* Params : array of imagery ids
		* Return : array of fetched imagery names
		*/
		function getImageryFormat($imagery_format)
		{	
			$imagery_format_array = array();
			foreach($imagery_format as $key => $id)
			{
				if($id==1) 
				{ 
					$imagery_format_name = "Horizontal"; 
				} 
				elseif($id==2) 
				{	
					$imagery_format_name = "Vertical"; 
				}			
				array_push($imagery_format_array,$imagery_format_name);	
			}
			return $imagery_format_array;
		}

		function getTotalCountAssignment()
		{
			$select = "SELECT COUNT(AssignmentId) as total_count FROM ".TABLE_ASSIGNMENT." WHERE isEnabled=1";
			$query = $this->db->query($select);
			$result = $this->db->fetch($query);
			return $result['total_count'];
		}


	}
	
	
?>
