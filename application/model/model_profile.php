<?php
	class model_profile extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}
	
		function profileDetail($user_id)
		{
			$query = "SELECT UserId, name, emailAddress, password, userType, city, address, telephone, profileName, profileDescription, website, founded, tagLine, profileImg, createdAt ".
				 "FROM ".TABLE_USER_MASTER." ".
				 "WHERE UserId = '".$user_id."'";
			$result  = $this->db->fetch_array($query);
			return $result[0];
		}

		function updateProfile($user_id,$request)
		{
			$profile_name          = mysql_escape_string($request['profile_name']);
			$website               = mysql_escape_string($request['website']);
			$tag_line              = mysql_escape_string($request['tag_line']);
			$profile_description   = mysql_escape_string($request['profile_description']);
			$founded               = $request['founded'];
			$name                  = mysql_escape_string($request['full_name']);
			$address               = mysql_escape_string($request['address']);	
			$city                  = mysql_escape_string($request['city']);
			$telephone             = $request['telephone'];	
			

			$update = "UPDATE ".TABLE_USER_MASTER." SET 
				   profileName           =  '".$profile_name."',
				   website               =  '".$website."',
				   tagLine               =  '".$tag_line."',
				   profileDescription    =  '".$profile_description."',
				   founded 	         =  '".$founded."',
				   name                  =  '".$name."',	
				   address               =  '".$address."',
				   city                  =  '".$city."',
				   telephone             =  '".$telephone."'
				   WHERE UserId = '".$user_id."'";	

			$proccess = $this->db->query($update) or die("mysql database error");
			if($proccess) {
				return true;
			} else {
				return false;
			}
		}

		function updateProfileLogo($user_id, $filename)
		{
			$select = "SELECT profileImg FROM ".TABLE_USER_MASTER." ".
				  "WHERE UserId = '".$user_id."'";
			$process = $this->db->query($select);
			$result = $this->db->fetch($process);
			$image =  $result['profileImg'];	

			$full_image_path = "images/profile_img/".$image;
			if (file_exists($full_image_path)) 
			{
				if($image!='default_profile_logo.png') {	
					$del = unlink($full_image_path);
				}
			}

			$update = "UPDATE ".TABLE_USER_MASTER." SET 
				   profileImg	= '".$filename."' 
				   WHERE UserId = '".$user_id."'";
			$proccess = $this->db->query($update) or die("Mysql Server Database Error: function - updateProfileLogo");	
		}

		function checkOldPassword($user_id, $request)
		{
			$old_password = md5($request['old_password']);
			$query = "SELECT UserId FROM ".TABLE_USER_MASTER." ".
				 "WHERE UserId = '".$user_id."' ".
				 "AND password = '".$old_password."'";
			$proccess = $this->db->query($query);
			$num_rows = $this->db->num_rows($proccess);
			if($num_rows==1) {
				return true;
			} else {
				return false;
			}
		}

		function updatePassword($user_id, $request)
		{
			$new_password = md5($request['new_password']);
			$query = "UPDATE ".TABLE_USER_MASTER." SET 
				  password = '".$new_password."'
				  WHERE UserId = '".$user_id."'";
			$proccess = $this->db->query($query);
			if($proccess) {
				return true;	
			} else {
				return false;
			}	
		}


		function getBuyerInfo($user_id)
		{
			$query = "SELECT name, profileName, tagLine, city, profileImg ".
				 "FROM ".TABLE_USER_MASTER." ".
				 "WHERE UserId = '".$user_id."'";
			$row = $this->db->fetch_array($query);
			return 	$row[0];
		}	
	}
?>