<?php
//===================================================================================
/* This class is for the purpose to manage demo requests */
//===================================================================================

	class model_admin_demorequest extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		function viewdemorequest()
		{
			$query = "SELECT * FROM ".TABLE_DEMO_REQUEST."";

			$query .= " ORDER BY DemoRequestId desc";

			// ======= pagination starts here =======
			$current_page = 1;
			$param =  $this->getrequest();
			if($param['page']!='') 
			{
				$current_page = $param['page'];			
			} 
			$limit = 10;
			$this->include_file('new_paginator.php','application/lib');
			$page = new pagination;
			$result = $page->paginate($query,$limit,$current_page);

			return $result;
		}

	
		function demorequestdetails($rid)
		{
			$query = "SELECT * FROM ".TABLE_DEMO_REQUEST." WHERE DemoRequestId = '".$rid."'";
			$result = $this->db->fetch_array($query);
			return $result[0];
			//echo "<pre>"; print_r($result); die;
		}

		

		function deletedemorequest($rid)
		{
			$query = "DELETE FROM ".TABLE_DEMO_REQUEST." WHERE DemoRequestId = '".$rid."'";
			$result = $this->db->query($query);

		}

	}
?>
