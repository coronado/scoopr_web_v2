<?php
//===================================================================================
/* This class is for the purpose to show buyers overview functionality */
//===================================================================================

	class model_overview extends Application
	{
	
		function __construct()
		{ 
			
			$this->loadConfig('database_connection');
		}

		#------ This function is to get  number of number of pending assignments ---------
		/*
		* Params : USER ID
		* Return : 
		*/ 
		function countPendingAssignments($user_id)
		{
			 $select = "SELECT AssignmentId ".
				  "FROM ".TABLE_ASSIGNMENT." ".
				  "WHERE DATEDIFF(NOW(), launchedDate) < submissionDeadline ".
				  "AND UserId = '".$user_id."' ".	
				  "OR (isLaunched = 2 AND UserId = '".$user_id."')"; 

			//  ======== memcache implementation ===============
			$this->key_mo_pending =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mo_pending);
		    
		    	if($this->details)
		    	{	
		      		$numrows = $this->details;  
		    	}
		    	else
		    	{	
				$result = $this->db->query($select);
				$numrows = $this->db->num_rows($result);

		      		$this->memcache->set($this->key_mo_pending, $numrows, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}
			
			//$result = $this->db->query($select);
			//$numrows = $this->db->num_rows($result);
			return $numrows; 
		}

		function countTotalAssignments($user_id)
		{
			$select = "SELECT AssignmentId ".
				  "FROM ".TABLE_ASSIGNMENT." ".
				  "WHERE UserId = '".$user_id."'";

			//  ======== memcache implementation ===============
			$this->key_mo_total =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mo_total);
		    
		    	if($this->details)
		    	{	
		      		$numrows = $this->details;  
		    	}
		    	else
		    	{	
				$result = $this->db->query($select);
				$numrows = $this->db->num_rows($result);

		      		$this->memcache->set($this->key_mo_total, $numrows, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}
			
				  
			//$result = $this->db->query($select);
			//$numrows = $this->db->num_rows($result);
			return $numrows; 
		}

		function countLicensedContent($user_id)
		{
			$select = "SELECT AssignmentImgSubmissionId ".
				  "FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." ".
				  "WHERE buyerId = '".$user_id."' ".
				  "AND paymentStatus = 'success'";

			//  ======== memcache implementation ===============
			$this->key_mo_licensed =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mo_licensed);
		    
		    	if($this->details)
		    	{	
		      		$numrows = $this->details;  
		    	}
		    	else
		    	{	
				$result = $this->db->query($select);
				$numrows = $this->db->num_rows($result);

		      		$this->memcache->set($this->key_mo_licensed, $numrows, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}
			
			//$result = $this->db->query($select);
			//$numrows = $this->db->num_rows($result);
			return $numrows; 
		}

		function getGraphData($user_id)
		{
			$current_date = date('Y-m-d');
			$begin_date = date('Y-m-d',(strtotime($current_date." -1 month")));
		
			$select = "SELECT * FROM ".TABLE_ASSIGNMENT_OVERVIEW." ".
				  "WHERE buyerId = '".$user_id."' ".
				  "AND date BETWEEN '$begin_date' AND '$current_date'"; 

			//  ======== memcache implementation ===============
			$this->key_mo_getgraph =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mo_getgraph);
		    
		    	if($this->details)
		    	{	
		      		$records = $this->details;  
		    	}
		    	else
		    	{	
		      		$records      = $this->db->fetch_array($select);
		      		$this->memcache->set($this->key_mo_getgraph, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}

			//$records = $this->db->fetch_array($select);
			return $records;
		}

		function getPaidContent($user_id,$page_no)
		{
			 // load the pagination class
		  	 $this->include_file('pagination_class.php','application/lib');
			// create the object of pagination
		   	$page= new pagination;	
		   	// call the setMax method of pagination class with "no of records" value and requested page no as a parameter
			$page->setMax(10,$page_no);
         		// call the setData method of pagination class with "table name" as a parameter	
	 

			 $qrystr = "SELECT sais.AssignmentImgSubmissionId, sais.AssignmentId, sais.buyerId, sais.imageName, sais.mediumSizeImage, sais.ModelId, sais.creatorId, sais.isApproved, sais.paymentStatus, sais.paymentAmount, sais.paymentMadeTo, sais.paymentDate, sum.profileName, sum.emailAddress, sum.name, sum.city, sum.address, sum.telephone, sais.paymentMethod, sais.rewards, sais.originalAmount ".
			"FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." AS sais ".
			"INNER JOIN ".TABLE_USER_MASTER." AS sum ".
			"ON sais.creatorId = sum.UserId ".
			"WHERE sais.buyerId = '".$user_id."' ".
			"AND sais.paymentStatus = 'success' ".
			"ORDER BY sais.paymentDate desc"; 


			//  ======== memcache implementation ===============
			$this->key_mo_getPaidContent =  md5($qrystr.$page_no);
		    	$this->details = $this->memcache->get($this->key_mo_getPaidContent);
		    
		    	if($this->details)
		    	{	//echo "aaaa";
		      		$contents = $this->details;  
		    	}
		    	else
		    	{	
		      		//$records      = $this->db->fetch_array($select);

				$page->setData1($qrystr);
				// call the display method of pagination class with for getting the content of the database table
				$display = $page->display();         
				// call the displayLinks method of pagination class for getting the links by passing "number of links value and current page number from the url as a parameter
				$links = $page->displayLinks(5,$page_no);
				$contents = array();
		   		array_push($contents,$display);  
		   		array_push($contents,$links);

		      		$this->memcache->set($this->key_mo_getPaidContent, $contents, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}
			
			

			

	      		$total_record_count = $contents[0]['total_records'];
	      		$total_content = $contents[0]['total_content']; //total records in paginated array
	      		$count = 1;

	      		#-- create the array that contains the paginated record (paginated record)------
	      		for($counter = 1; $counter <= $total_content; $counter++ )
	      		{
	         		$contentKey = "content$count";
	         		foreach($contents[0]['header'] as $key => $value)
	         		{
	            			$temp_array[$value] = $contents[0][$contentKey][$key]; 
	         		}
	         		$paginated_array[]= $temp_array;
	         		$count++;
	      		}
	     		#-----------------------(/paginated array)----------------------
	
			$return['paginated_array']    = $paginated_array;
          		$return['pagination_links']   = $contents[1];   
          		$return['total_count']        = $total_record_count;
          		$return['num_paginated_rec']  = $total_content;

			return $return;
		}

		function getLicensedMediaContent($user_id,$page_no)
		{
						 // load the pagination class
		  	 $this->include_file('pagination_class.php','application/lib');
			// create the object of pagination
		   	$page= new pagination;	
		   	// call the setMax method of pagination class with "no of records" value and requested page no as a parameter
			$page->setMax(12,$page_no);
         		// call the setData method of pagination class with "table name" as a parameter	
	 

			 $qrystr = "SELECT sais.AssignmentImgSubmissionId, sais.AssignmentId, sais.buyerId, sais.imageName, sais.mediumSizeImage, sais.creatorId as creator_id, sum.profileName, sum.name ".
			"FROM ".TABLE_ASSIGNMENT_IMG_SUBMISSION." AS sais ".
			"INNER JOIN ".TABLE_USER_MASTER." AS sum ".
			"ON sais.creatorId = sum.UserId ".
			"WHERE sais.buyerId = '".$user_id."' ".
			"AND sais.paymentStatus = 'success' ".
			"ORDER BY sais.paymentDate desc"; 


			//  ======== memcache implementation ===============
			$this->key_mo_getLicensedMediaContent =  md5($qrystr.'licensed'.$page_no);
		    	$this->details = $this->memcache->get($this->key_mo_getLicensedMediaContent);
		    
		    	if($this->details)
		    	{	
		      		$contents = $this->details;  
		    	}
		    	else
		    	{	
		      		$page->setData1($qrystr);
         			// call the display method of pagination class with for getting the content of the database table
         			$display = $page->display();         
         			// call the displayLinks method of pagination class for getting the links by passing "number of links value and current page number from the url as a parameter
         			$links = $page->displayLinks(5,$page_no);

				$contents = array();
		   		array_push($contents,$display);  
		   		array_push($contents,$links);

		      		$this->memcache->set($this->key_mo_getLicensedMediaContent, $contents, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}

	      		$total_record_count = $contents[0]['total_records'];
	      		$total_content = $contents[0]['total_content']; //total records in paginated array
	      		$count = 1;

	      		#-- create the array that contains the paginated record (paginated record)------
	      		for($counter = 1; $counter <= $total_content; $counter++ )
	      		{
	         		$contentKey = "content$count";
	         		foreach($contents[0]['header'] as $key => $value)
	         		{
	            			$temp_array[$value] = $contents[0][$contentKey][$key]; 
	         		}
	         		$paginated_array[]= $temp_array;
	         		$count++;
	      		}
	     		#-----------------------(/paginated array)----------------------
	
			$return['paginated_array']    = $paginated_array;
          		$return['pagination_links']   = $contents[1];   
          		$return['total_count']        = $total_record_count;
          		$return['num_paginated_rec']  = $total_content;

			return $return;
		}

		function getBuyerInfo($user_id)
		{
			$select = "SELECT name, profileName, tagLine, city, profileImg ".
				 "FROM ".TABLE_USER_MASTER." ".
				 "WHERE UserId = '".$user_id."'";

			//  ======== memcache implementation ===============
			$this->key_mo_getbuyerinfo =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mo_getbuyerinfo);
		    
		    	if($this->details)
		    	{	
		      		$records = $this->details;  
		    	}
		    	else
		    	{	
		      		$records      = $this->db->fetch_array($select);
		      		$this->memcache->set($this->key_mo_getbuyerinfo, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}

			//$records = $this->db->fetch_array($select);
			return 	$records[0];
		}

		function getCreatorDetails($creator_id)
		{
			$select = "SELECT name, profileName, city, address, telephone, emailAddress, profileImg ".
				 "FROM ".TABLE_USER_MASTER." ".
				 "WHERE UserId = '".$creator_id."'";

			//  ======== memcache implementation ===============
			$this->key_mo_getcreatorinfo =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mo_getcreatorinfo);
		    
		    	if($this->details)
		    	{	
		      		$records = $this->details;  
		    	}
		    	else
		    	{	
		      		$records      = $this->db->fetch_array($select);
		      		$this->memcache->set($this->key_mo_getcreatorinfo, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}

			//$records = $this->db->fetch_array($select);
			return 	$records[0];			
		}

	}
?>
