<?php
	class model_create_assignment extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		#------ This function is to create assignment
		/*
		* Params : posted data from the create assignment page
		* Return : 
		*/
		function create_assignment($request)
		{
			// TABLE_REFERENCE_IMAGE
			//$submission_date = $request['year'].'-'.$request['month'].'-'.$request['day'].' '.$request['submission_time'];
			//$submission_date = date("Y-m-d H:i:s", strtotime($submission_date));
			//echo $submission_date; die;

			$diff = abs(strtotime($request['submission_deadline']) - strtotime(date('Y-m-d')));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

			
			$ref = $this->getRefNumber($this->getsession('user_id'));
			$ref_number = $ref+1;

			$imagery_for = serialize($request['imagery_for']);
			$imagery_format = serialize($request['imagery_format']);
			$request =  $this->escapeString($request);

			/* ===========  checking a payment method ============ */
			if($request['our_budget']!='' && $request['rewards']!='') {
				$payment_method  = 3;
			} elseif($request['our_budget']!='' && $request['rewards']=='') {
				$payment_method  = 1;
			} elseif($request['rewards']!='' && $request['our_budget']=='') {
				$payment_method  = 2;
			}

			
			$insert_qry = "INSERT INTO ".TABLE_ASSIGNMENT." SET 
				      		UserId               =       '".$_SESSION['user_id']."',
						assignmentRefId      =       '".$ref_number."',
						AssignmentCategoryId =       '".$request['category_id']."',
						hopeToSelect         =       '".$request['hope_to_select']."',
						lookingForText       =       '".strip_tags($request['looking_for_text'])."',
						ourBudget            =       '".$request['our_budget']."',
						rewards              =       '".strip_tags($request['rewards'])."',
						shareCondition1      =       '".$request['share_condition_1']."',
						shareCondition2      =       '".$request['share_condition_2']."',
						shareCondition3      =       '".$request['share_condition_3']."',
						submissionDeadline   =       '".$days."',
						imageryFor           =       '".$imagery_for."',
						imageryForText       =       '".strip_tags($request['imagery_for_text'])."',
						modelRelease         =       '".$request['model_release']."',
						imageryFormat        =       '".$imagery_format."',
						referenceImageText   =       '".strip_tags($request['reference_image_text'])."',
						paymentMethod	     =       '".$payment_method."',
						createdAt            =       NOW()"; 
			$process_insert  = $this->db->query($insert_qry);
			$assignment_id = (int)$this->db->insert_id();
			return $assignment_id;		      
		}

		function insertReferenceImages($assignment_id,$success_file_array)
		{
			$query = "INSERT INTO ".TABLE_REFERENCE_IMAGE." SET 
				  AssignmentId    =    '".$assignment_id."',	
				  image1    =    '".@$success_file_array[0]."',	
				  image2    =    '".@$success_file_array[1]."',	
				  image3    =    '".@$success_file_array[2]."'";

			$process = $this->db->query($query);
		}

		function getImageryName()
		{
			$select = "SELECT * FROM ".TABLE_IMAGERY_FOR."";
			
			//  ======== memcache implementation ===============
			$this->key_mca_getImageryName =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mca_getImageryName);
		    
		    	if($this->details)
		    	{	
		      		$records = $this->details;  
		    	}
		    	else
		    	{	
		      		$records      = $this->db->fetch_array($select);
		      		$this->memcache->set($this->key_mca_getImageryName, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}

			return $records;	  
		}

		function getCategories()
		{
			$select = "SELECT * FROM ".TABLE_ASSIGNMENT_CATEGORY." WHERE status = 1";

			//  ======== memcache implementation ===============
			$this->key_mca_getCategories =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mca_getCategories);
		    
		    	if($this->details)
		    	{	
		      		$records = $this->details;  
		    	}
		    	else
		    	{	
		      		$records      = $this->db->fetch_array($select);
		      		$this->memcache->set($this->key_mca_getCategories, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_DEFAULT);
		    	}

			return $records;	  
		}


		function getRefNumber($user_id)
		{
			$query = "SELECT MAX(assignmentRefId) as ref_number ".
				 "FROM ".TABLE_ASSIGNMENT." ".
				 "WHERE UserId = '".$user_id."'";
			$result = $this->db->fetch_array($query);
			return $result[0]['ref_number'];
		}	
		
		function getAssignmentDetails($assignment_id)
		{
			$select  = "SELECT sa.AssignmentId, sa.UserId, sa.lookingFor, sa.lookingForText, sa.hopeToSelect, sa.ourBudget, sa.submissionDeadline, sa.imageryFor, sa.imageryForText, sa.modelRelease, sa.imageryFormat, sa.referenceImageText, sa.rewards, sa.shareCondition1, sa.shareCondition2, sa.shareCondition3, sa.createdAt, sa.AssignmentCategoryId, sr.image1, sr.image2, sr.image3 ".
			"FROM ".TABLE_ASSIGNMENT." AS sa ".
			"INNER JOIN ".TABLE_REFERENCE_IMAGE." AS sr ".
			"ON sa.AssignmentId=sr.AssignmentId ".
			"WHERE sa.AssignmentId = '".$assignment_id."'";

			//  ======== memcache implementation ===============
			$this->key_mca_getAssignmentDetails =  md5($select);
		    	$this->details = $this->memcache->get($this->key_mca_getAssignmentDetails);
		    
		    	if($this->details)
		    	{	
		      		$records = $this->details;  
		    	}
		    	else
		    	{
				$process_qey = $this->db->query($select);	
		      		$records      = $this->db->fetch($process_qey);
		      		$this->memcache->set($this->key_mca_getAssignmentDetails, $records, MEMCACHE_COMPRESSED, MEMCACHE_TIMING_ONE_MINUTE);
		    	}

			return $records;
		}

		function edit_assignment($request,$assignment_id)
		{
			//$submission_date = $request['year'].'-'.$request['month'].'-'.$request['day'].' '.$request['submission_time'];
			//$submission_date = date("Y-m-d H:i:s", strtotime($submission_date));
			$diff = abs(strtotime($request['submission_deadline']) - strtotime(date('Y-m-d')));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			
			$imagery_for = serialize($request['imagery_for']);
			$imagery_format = serialize($request['imagery_format']);
			$request =  $this->escapeString($request);

			/* ===========  checking a payment method ============ */
			if($request['our_budget']!='' && $request['rewards']!='') {
				$payment_method  = 3;
			} elseif($request['our_budget']!='' && $request['rewards']=='') {
				$payment_method  = 1;
			} elseif($request['rewards']!='' && $request['our_budget']=='') {
				$payment_method  = 2;
			}

			$update_qry = "UPDATE ".TABLE_ASSIGNMENT." SET 
						AssignmentCategoryId    =       '".$request['category_id']."',
				      		hopeToSelect            =       '".$request['hope_to_select']."',
						lookingForText          =       '".strip_tags($request['looking_for_text'])."',
						ourBudget               =       '".$request['our_budget']."',
						rewards                 = 	'".strip_tags($request['rewards'])."',
						shareCondition1         =       '".$request['share_condition_1']."',
						shareCondition2         =       '".$request['share_condition_2']."',
						shareCondition3         =       '".$request['share_condition_3']."',
						submissionDeadline      =       '".$days."',
						imageryFor              =       '".$imagery_for."',
						imageryForText          =       '".strip_tags($request['imagery_for_text'])."',
						modelRelease            =       '".$request['model_release']."',
						imageryFormat           =       '".$imagery_format."',
						referenceImageText      =       '".strip_tags($request['reference_image_text'])."',
						paymentMethod	        =       '".$payment_method."' 
						WHERE AssignmentId      =       '".$assignment_id."'"; 

			$process_update  = $this->db->query($update_qry);
		}

		function editReferenceImages($assignment_id,$key,$file_alias)
		{
			$select = "SELECT image".$key." ".
				  "FROM ".TABLE_REFERENCE_IMAGE." ".
				  "WHERE AssignmentId = '".$assignment_id."'";
			$process = $this->db->query($select);
			$result = $this->db->fetch($process);
			$image =  $result['image'.$key];	

			$full_image_path = "images/reference_img/".$image;
			if (file_exists($full_image_path)) 
			{	
				$del = unlink($full_image_path);
			}

			$update  = "UPDATE ".TABLE_REFERENCE_IMAGE." SET 
				    image".$key." = '".$file_alias."'
				    WHERE AssignmentId = '".$assignment_id."'";
			$update_process = $this->db->query($update) or die("aaaaa");
		}	

	}
?>
