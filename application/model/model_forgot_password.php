<?php
//===================================================================================
/* This class is for the purpose to check the login and return the user basic info */
//===================================================================================

	class model_forgot_password extends Application
	{
	
		function __construct()
		{ 
			$this->loadConfig('database_connection');
		}

		function checkEmail($request)
		{
			$email_address = $request['email_address'];
			$query = "SELECT UserId FROM ".TABLE_USER_MASTER." WHERE emailAddress = '".$email_address."' AND userType=1 AND isDisabled=1";
			$result = $this->db->query($query);
			$numrows = $this->db->num_rows($result);
			
			if($numrows>0)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		
		function forgotPwdEmail($request)
		{
			
			$name    = "";
			$email   = $request['email_address'];
			$message = "";

			$to      = $email;
			$cc      = '';
			$bcc     = '';
			$from    = FROM_EMAIL; 
			$subject = 'Reset your password';
			$logosource = PATH."images/logo.png";

			$reset_key = $this->getResetKey($email);

			$message = '<tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">Hi There! Bummer You lost your password.</td>
                                                      </tr>
					<tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;">No worries, let us help you create a new one by clicking on the link below.</td>
                                                      </tr>
                                                      <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#4c4c4c; font-weight:normal;"><a href='.PATH.'index/index/reset_key/'.$reset_key.'>Click here</a></td>
                                                      </tr>';
			
			$this->email_touser($to, $from, $subject, $cc, $bcc, $message);
		}

		function email_touser($to, $from, $subject, $cc, $bcc, $message)
		{
			$this->include_file('email_class.php','application/lib');
			
			$email_obj = new email($to, $from, $subject, $cc, $bcc, $message);
		  	$send_email = $email_obj->send_email();
		}

		function getResetKey($email)
		{
			$qry = "SELECT resetKey FROM ".TABLE_USER_MASTER." WHERE emailAddress = '".$email."' AND userType = 1";
			$result = $this->db->fetch_array($qry);
			if($result[0]['resetKey']=='')
			{
				$cdate = date('Y-m-d h:i:s');
				$reset_key = sha1($cdate);
				$update = "UPDATE ".TABLE_USER_MASTER." SET resetKey = '".$reset_key."' WHERE emailAddress = '".$email."' AND userType = 1";
				$process = $this->db->query($update);
			}
			else
			{
				$reset_key = $result[0]['resetKey'];
			}
			return $reset_key;
		}	
	}
?>
