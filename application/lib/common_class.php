<?php
class CommonClass
{
	public static function calculateDateDiff($date1,$date2)
	{
		$diff = abs(strtotime($date2) - strtotime($date1));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ (60));
		$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60 ));
		
		$deadline = "";
		if($years==1) 
		{
			$deadline .= $years." year ";
		}
		elseif($years>1) 
		{
			$deadline .= $years." years ";
		}
		if($months==1) 
		{
			$deadline .= $months." month ";
		}
		elseif($months>1) 
		{
			$deadline .= $months." months ";
		}
		if($days==1) 
		{
			$deadline .= $days." day ";
		} 
		elseif($days>1) 
		{
			$deadline .= $days." days ";
		}
		if($hours==1) 
		{
			$deadline .= $hours." hour ";
		} 
		elseif($hours>1) 
		{
			$deadline .= $hours." hours ";
		}
		if($minutes==1) 
		{
			$deadline .= $minutes." minute ";
		} 
		elseif($minutes>1) 
		{
			$deadline .= $minutes." minutes ";
		}
		if($seconds==1) 
		{
			$deadline.= $seconds." second";
		}
		elseif($seconds>1) 
		{
			$deadline.= $seconds." seconds";
		}
		return $deadline;
	}
	
	public static function printSelectedMenu($menu_order)
	{
		for($x=1;$x<=5;$x++){
			echo "<aside>";
			if($menu_order==$x){echo "<img src='images/app_accnt_images/arrow_icon.png' width='14' height='10' alt=''>";}else{echo "&nbsp;";}
			echo "</aside>";
		}
	}

	public static function getAssignmentViews($assignmentId)
	{
		$select = "SELECT SUM(view) AS views, SUM(submission) AS submissions FROM AssignmentOverview ".
				  "WHERE AssignmentId = '".$assignmentId."'";
		$proccess = mysql_query($select);
		$result = mysql_fetch_array($proccess);
		return $result;
	}

	public static function getAssignmentSubmissions($assignmentId)
	{
		$select = "SELECT COUNT(AssignmentImgSubmissionId) AS submissions FROM AssignmentImgSubmission WHERE AssignmentId = '".$assignmentId."'";
		$proccess = mysql_query($select);
		$result = mysql_fetch_array($proccess);
		return $result['submissions'];
	}	
}