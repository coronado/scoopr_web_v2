<?php
/*
 * ===============================================================
 * Class to send push notifications to the devices
 * ===============================================================
 */

class PushNotification
{
	public function __construct() 
	{
		$this->gcm_api_key  = "AIzaSyDHdsYErAsg3-tJ7nMt5ThMsI1-vjBSFcU";
		$this->gcm_api_url  = "https://android.googleapis.com/gcm/send";
	}

	/** ----- This function is to send notification to ios device  (1) -------------------------------
   	* @PARAMS: message and device tokens on which notification is to be sent 
   	* @RETURN: void
   	* **/
	
   	private function push_to_ios($message = '', $receivers=array())
   	{
      		#--------------- initializations ------------------------------
		$message           = substr($message, 0, 255);
		$sound          = 'default';
		$development    = false;
		$badge          = '1';
      		$payload['aps'] = array('alert'=>$message,'badge'=>intval($badge),'sound'=>$sound);
      		$payload        = json_encode($payload);
      		$apns_url       = NULL;
      		$apns_cert      = NULL;
      		$apns_port      = 2195;
      		$passphrase =   'welcome';
		$apns_cert = __DIR__.'/ck.pem';
		
      		// developement or live server certificate
      		if($development)
      		{
          		$apns_url = 'gateway.sandbox.push.apple.com';
      		}
      		else
      		{
          		$apns_url = 'gateway.push.apple.com';
     		}
      		$stream_context = stream_context_create();
      		stream_context_set_option($stream_context, 'ssl', 'local_cert', $apns_cert);
      		stream_context_set_option($stream_context, 'ssl', 'passphrase', $passphrase);

      		$apns = stream_socket_client('ssl://' . $apns_url . ':' . $apns_port, $error, $error_string, 2, STREAM_CLIENT_CONNECT, $stream_context);
        	$device_tokens  = $receivers;
      	
		foreach($device_tokens as $device_token)
      		{
          		$apns_message = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device_token)) . chr(0) . chr(strlen($payload)) . $payload;
          		fwrite($apns, $apns_message);
      		}
		@socket_close($apns);
      		fclose($apns);
		
   	}
   
   	/** ----- This function is to send notification to android device  (2) -------------------------------
   	* @PARAMS: message and registeration ids of devices
   	* @RETURN: void
   	* **/

   	private function push_to_android($message = '', $receivers=array())
   	{
            	$apiKey          = $this->gcm_api_key;
      		$url             = $this->gcm_api_url;
      		$random_collapse = rand(11, 100);
      		$fields          =	array('registration_ids'=>$receivers,					'data'=>array("message"=>$message));
      		$headers         = 	array( 
                                 		'Authorization: key=' . $apiKey,
                                 		'Content-Type: application/json'
                              		);

      		#------- send notificatin using curl(curl)-------------------------
      		$curl_channel = curl_init();
      		curl_setopt( $curl_channel, CURLOPT_URL, $url );
      		curl_setopt( $curl_channel, CURLOPT_POST, true );
      		curl_setopt( $curl_channel, CURLOPT_HTTPHEADER, $headers);
      		curl_setopt( $curl_channel, CURLOPT_RETURNTRANSFER, true );
      		curl_setopt( $curl_channel, CURLOPT_POSTFIELDS, json_encode( $fields ) );
      		var_dump(curl_exec($curl_channel));
      		curl_close($curl_channel);
      		#-------------------------------(/curl)---
   	}

   
  	/** ----- This function is to send push notification to device (4) -------------------------------
   	* @PARAMS: message, device_token, device_type
   	* @RETURN: none
   	* **/
  
   	public function send_notification($message,$receivers=array(),$device_type='')
   	{      
      		$is_device_ios      = ($device_type == 1)?true:false;
      		$is_device_android  = ($device_type == 2)?true:false;
      		if($is_device_ios)
      		{
         		$this->push_to_ios($message,$receivers);
      		}
      		if( $is_device_android )
      		{
	         	$this->push_to_android($message,$receivers);
      		}
   	}
}