<?php
class email 
{
	   public $to;
	   public $from;
	   public $subject;
	   public $cc;
	   public $bcc;
	   public $message;
	   
		function __construct($to, $from, $subject, $cc=null, $bcc=null, $message)
		{
			$this->to           = $to;
			$this->from         = $from;
			$this->subject      = $subject;
			$this->cc           = $cc;
			$this->bcc          = $bcc;
			$this->message      = $message;
			$this->email_footer = $this->efooter();
			$this->email_header = $this->eheader();
			
			if(DB_REQUEST=='staging') 
			{
				$this->path    = "http://scoopr.a1technology.asia/";	
			}
			elseif(DB_REQUEST=='live')
			{
				$this->path    = "http://www.scooprmedia.com/";
			}
			else
			{
				$this->path    = "http://scoopr.a1.com/";
			}	
		}
		
		public function efooter()
		{
		   	/*$footer = '<tr>
  <td style="background:#f56f6c;text-align:center;color:#fff;font-size:12px;padding:10px;font-family:Arial, Helvetica, sans-serif;">&copy; Copyright 2013 Scoopr Media Inc. </td></tr></table>';
        		return $footer;*/


			$footer = '<tr>	
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#4c4c4c; font-weight:normal;">Thanks again!</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#4c4c4c; font-weight:bold;">The Scoopr Team</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#727272; font-weight:bold;">Scoopr is here to help you <span style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#5ab7ce; font-weight:bold;">easily gain the content you need.</span> For questions, please email us at the info@sccoprmedia.com. if you do not wish to receive the emails of this type sent by scoopr, please <a href="'.$this->path.'" style="font-size:12px; color:#ff0000; font-weight:bold;">click here</a>. Scoopr | New York </td>
                                                      </tr>
                                                        <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                        <tr>
                                                        <td valign="top" align="left">
                                                        	<table width="10%" border="0" cellspacing="0" cellpadding="0" align="left">
                                                              <tr>
                                                                <td><a href="https://www.facebook.com/Scooprmedia" title="Facebook"><img src="'.$this->path.'images/email/email_facebook_icon.png" alt="" border="0" /></a></td>
                                                                <td><a href="https://twitter.com/TeamScoopr" title="Twitter"><img src="'.$this->path.'images/email/email_twitter_icon.png" alt="" border="0"  /></a></td>
                                                              </tr>
                                                            </table>

                                                        </td>
                                                      </tr>
                                                    </table>
                                                </td>
                                              </tr>
                                            </table>
										 </td>
                                  </tr>
                                </table>
                        </td>
                      </tr>
                    </table>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
     </table>';

		return $footer;
		}

		public function eheader()
		{
			//$path    = "http://scoopr.a1technology.asia/";
		   	/*$header  = '<table style="600px;border:1px solid #5ab7ce;" cellpadding="0" cellspacing="0">
<tr>
  <td><img src="'.$this->path.'images/email/header.png" width="600" height="115" alt="scooper"></td></tr>
<tr>';
        		return $header;*/


			$header = '<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td>&nbsp;</td>
          </tr>

          <tr>
            <td>
            	<table width="786" border="0" cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;" align="center">
                      <tr>
                        <td style="padding:11px;">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td style="background-color:#5ab7ce; padding:10px;">
                                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tr>
                                                <td style="background-color:#FFFFFF; padding:22px;">
                                                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <tr>
                                                        <td valign="top">
                                                        	<a href="'.$this->path.'" title="Home"><img src="'.$this->path.'images/email/email_logo_scoopr.png" alt="" border="0" /></a>
                                                        </td>
                                                      </tr>
						      <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                      <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>
                                                       <tr>
                                                        <td valign="top">&nbsp;</td>
                                                      </tr>';

			return $header;
		}

		
		
		public function send_email($to='')
		{
			//change this to your email.
		    	if($to=='') { 
		     	$to      = $this->to;
		    	}
          
          		$from    = $this->from;
          		$subject = $this->subject;
          		//$path    = "scoopr.a1technology.asia";
        
          		//begin of HTML message
          		$message = $this->message;
         		//end of message

			// To send the HTML mail we need to set the Content-type header.
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers  .= "From: $from\r\n";
			$headers  .= "Reply-To: $from\r\n";
			//options to send to cc+bcc
			if($this->cc!="")
			{
				$headers .= "Cc: ".$this->cc."\r\n";
			}
			
			if($this->bcc!="")
			{
				$headers .= "Bcc: ".$this->bcc."\r\n";
			}
       
       
			
			$HTMLmessage = $this->eheader().$message.$this->efooter();
       			
       			$mail = mail($to, $subject, $HTMLmessage, $headers);
       
			if($mail)
			{
				return "success";
			}
			else
			{
				return "fail";
			}
       
		}

}

?>
