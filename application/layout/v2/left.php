<?php
$param = $this->getrequest();
$section = $param['section'];
$action = $this->getsession('actionName');
$controller = $this->getsession('controllerName');
$actionName = $controller.$action;
/* my account actions
assignments, submission, preview, update, licensedmedia
*/
$selectMenu = "style=color:#db6766";
?>

<div class="side-section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 menu_sides">
				<span class="head_accor">Navigation</span>
				<div class="accordionButton" id="dashboardMenu">
                                       
					<span onclick="javascript:window.location.href='overview'"><i class="fa fa-home"></i>Dashboard</span>
				</div>
				<div class="accordionButton">
                                       <span><i class="fa fa-envelope-o"></i>Messages</span>

					<span class="notification">3</span>
				</div>
				<div class="accordionButton" id="createAssignmentMenu">
                                       <span onclick="javascript:window.location.href='assignment/design'"><i class="fa fa-edit"></i>Create Assignment</span>
				</div>
				

				<div class="accordionButton" id="myAccountMenu">
					<span><i class="fa fa-briefcase"></i>My Account</span><span class="plusMinus">+</span>
				</div>

				<div class="accordionContent" id="myAccountSubMenu">
					<a href="/content/assignments" <?php if($actionName=='contentassignments') { echo $selectMenu; } ?>>Assignments</a>
					<a href="/content/submission" <?php if($actionName=='contentsubmission') { echo $selectMenu; } ?>>Assignment Galleries</a>
					<a href="/overview/licensedmedia" <?php if($actionName=='overviewlicensedmedia') { echo $selectMenu; } ?>>Licensed Media</a>
					<a href="javascript:alert('coming soon');">Model Releases</a>
					<a href="/profile/update" <?php if($actionName=='profileupdate') { echo $selectMenu; } ?>>Profile</a>
					<a href="javascript:alert('coming soon');">Storage</a>
				</div>


				<div class="accordionButton" id="dataAnalyticsMenu">
                                         <span><i><img src="public/img/data.png"></i>Data Analytics</span><span class="plusMinus">+</span>
				</div>
				<div class="accordionContent" id="dataAnalyticsSubMenu">
					<a href="javascript:alert('coming soon');">Image Tracking</a>
					<a href="javascript:alert('coming soon');">Visualization</a>
					<a href="javascript:alert('coming soon');">Geolocation</a>
				</div>


				<div class="accordionButton" id="paymentsMenu">
                                         <span><i><img src="public/img/dollar.png"></i>Payments</span><span class="plusMinus">+</span>
				</div>
				<div class="accordionContent" id="paymentsSubMenu">
					<a href="javascript:alert('coming soon');">Payments</a>
					<a href="javascript:alert('coming soon');">Payment Info</a>
					<a href="javascript:alert('coming soon');">Rewards</a>
					<a href="javascript:alert('coming soon');">Reward Manager</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sharing-ratios">
				<span class="head_accor">Information Summary</span>
				<ul>
					<li class="graph1"><span>Daily Shares</span>30,201</li>
					<li class="graph2"><span>Average Users</span>332,801</li>
					<li class="graph3"><span>Disk/Storage Usage</span>82.2%</li>
					<li class="graph4"><span>Image Views</span>922,440</li>
				</ul>
			</div>
		</div><!-- END .side-section -->

<script type="text/javascript">
	$(document).ready(function() {	
	    $('.accordionButton').click(function() {
	        $('.accordionButton').removeClass('on');
	        $('.accordionContent').slideUp('normal');
	        $('.plusMinus').text('+');
	        if($(this).next().is(':hidden') == true) {
	            $(this).addClass('on');
	            $(this).next().slideDown('normal');
	            $(this).children('.plusMinus').text('-');
	         } 
	     });
	    $('.accordionButton').mouseover(function() {
	        $(this).addClass('over');
	    }).mouseout(function() {
	        $(this).removeClass('over');
	    });

	    var section = '<?php echo $section; ?>';
	    var actionName =  '<?php echo $actionName; ?>';
	;
	    if(actionName=='contentassignments' || actionName=='contentsubmission' || actionName=='contentpreview' || actionName=='profileupdate'  || actionName=='overviewlicensedmedia' || actionName=='profileview') 
	    { 	
		$('#myAccountMenu .plusMinus').text('-');	
		$('#myAccountMenu').addClass('on');	
		$('#myAccountSubMenu').show();
		$('#dataAnalyticsSubMenu').hide();
		$('#paymentsSubMenu').hide();
	    }
	    else if(actionName=='assignmentdesign' || actionName=='assignmentpreview' || actionName=='assignmentlaunch')	
	    {
		$('#createAssignmentMenu').addClass('on');	
		$('#myAccountSubMenu').hide();
		$('#dataAnalyticsSubMenu').hide();
		$('#paymentsSubMenu').hide();
	    }		
	    else 
	    { 
		$('.accordionContent').hide(); 
		$('#dashboardMenu').addClass('on');	
	    }
	});
</script>