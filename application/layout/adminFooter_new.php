<footer class="site-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="footer-logo">
					<div class="brand">
						<a href="#">
							<h1 class="scoopr-logo">S<span class="letters-co">co</span>op<span class="letter-r">R</span></h1>
							<h3 class="scoopr-motto">ASSIGNMENTS FOR BRAND SPECIFIC CONTENT</h3>
						</a>
					</div>
				</div>
				<p>Copyright &copy; 2014. <a href="#">Scoopr Media Inc</a>. All Rights Reserved.</p>
			</div>
		</div>
	</div>
</footer>