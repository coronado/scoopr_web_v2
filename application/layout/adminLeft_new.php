<?php
$displayNone = "style='display:none';";
$displayBlock = "style='display:block';";


$param = $this->getrequest();
$section = $param['section'];
$action = $this->getsession('actionName');
$controller = $this->getsession('controllerName');
$actionName = $controller.$action;

session_start(); 
if($_SESSION['profile_name']!='') {
	$profile_name = $_SESSION['profile_name'];
} else {
	$profile_name = $_SESSION['name'];
}

if($_SESSION['name']!='') {
	$name = $_SESSION['name'];
} else {
	$name = $_SESSION['profile_name'];
}

if($this->getsession('user_id')!='') {
	$homeLink = "/overview";
} else {
	$homeLink = "/";
}

?>

<base href="<?php echo PATH; ?>">
<section class="full-site-container">
		<div class="side-section">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 menu_sides">
				<span class="head_accor" style="margin:0px;">Navigation</span>
				<div class="accordionButton" id="dashboardMenu">
                                      
					<span><i class="fa fa-home"></i>Dashboard</span>
				</div>
				<!--<div class="accordionButton" id="messageMenu">
					<span><i class="fa fa-envelope-o"></i>Messages</span>
					<span class="notification">3</span>
				</div>-->
				<div class="accordionButton" id="myaccountMenu">
					<span><i class="fa fa-briefcase"></i>Accounts</span><span class="plusMinus">+</span>
				</div>
				<div class="accordionContent" id="myaccountSubMenu">
					
					<a href="/admin/home"  >View Account</a>
					<a href="/adminaccount/createuser" >Add Account</a>
                                	</div>
				
				<div class="accordionButton" id="assignmentsMenu">
					<span><i class="fa fa-edit"></i>Assignments</span><span class="plusMinus">+</span>
				</div>
				<div class="accordionContent" id="assignmentsSubMenu">
					<a href="/adminassignment/designassignment" >Create Assignment</a>
					<a href="admin/home/type/assignment" >View Assignments</a>
					<a href="/adminsubmission/index" >Assignment Galleries</a>
				        <a href="/adminsubmission/index" >Licensed Media</a>
					<!--<a href="/adminsubmission/modelreleases" >Model Releases</a>-->
					<a href="javascript:alert('coming soon');">Model Releases</a>
				</div>
				
				
				<!--<div class="accordionButton" id="submissionsManu">
					<span><i><img src="public/admin/img/dollar.png"></i>Submissions</span><span class="plusMinus">+</span>
				</div>
				<div class="accordionContent" id="subManu">
					<a href="/adminsubmission/index" >View</a>
				</div>
				
				<div class="accordionButton" id="MediaManu">
					<span><i><img src="public/admin/img/dollar.png"></i>Licenced Media</span><span class="plusMinus">+</span>
				</div>
				<div class="accordionContent" id="SubMediaManu"">
					<a href="/adminsubmission/licensedmedia" >View</a>
				</div>
				
				<div class="accordionButton" id="ModelManu">
					<span><i><img src="public/admin/img/dollar.png"></i>Model Releases</span><span class="plusMinus">+</span>
				</div>
				<div class="accordionContent" id="SubModelManu">
					<a href="/adminsubmission/modelreleases" >View</a>
				</div>
				
				<div class="accordionButton" id="ContentManu">
					<span><i><img src="public/admin/img/dollar.png"></i>Content Management</span><span class="plusMinus">+</span>
				</div>
				<div class="accordionContent" id="SubContentManu">
					<a href="/admincontentmanagement/update/id/1/type/FAQ" >FAQ</a>
					<a href="/admincontentmanagement/update/id/2/type/PRIVACY POLICY" >Privacy Policy</a>
					<a href="/admincontentmanagement/update/id/3/type/TERMS OF SERVICES" >Terms of services</a>
		
				</div>-->
				
				<div class="accordionButton" id="dataManu">
					<span><i><img src="public/admin/img/data.png"></i>Data Analytics</span><span class="plusMinus">+</span>
				</div>
				<div class="accordionContent" id="SubDataManu">
					<a href="javascript:alert('coming soon');">link</a>
					
				</div>
				<div class="accordionButton" id="PaymentsManu">
					<span><i><img src="public/admin/img/dollar.png"></i>Payments</span><span class="plusMinus">+</span>
				</div>
				<div class="accordionContent" id="SubPaymentsManu">
					<a href="javascript:alert('coming soon');">link</a>
					
				</div>
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sharing-ratios">
				<span class="head_accor">Information Summary</span>
				<ul>
					<li class="graph1"><span>Daily Shares</span>30,201</li>
					<li class="graph2"><span>Average Users</span>332,801</li>
					<li class="graph3"><span>Disk/Storage Usage</span>82.2%</li>
					<li class="graph4"><span>Image Views</span>922,440</li>
				</ul>
			</div>
		</div><!-- END .side-section -->

<script type="text/javascript">
		
	    $(document).ready(function() {	
	    $('.accordionButton').click(function() {
	        $('.accordionButton').removeClass('on');
	        $('.accordionContent').slideUp('normal');
	        $('.plusMinus').text('+');
	        if($(this).next().is(':hidden') == true) {
	            $(this).addClass('on');
	            $(this).next().slideDown('normal');
	            $(this).children('.plusMinus').text('-');
	         } 
	     });
	    $('.accordionButton').mouseover(function() {
	        $(this).addClass('over');
	    }).mouseout(function() {
	        $(this).removeClass('over');
	    });

	    var section = '<?php echo $section; ?>';
	    var actionName =  '<?php echo $actionName; ?>';
        if(actionName=='adminaccountcreateuser' || actionName=='adminaccountedituser'|| actionName=='adminhome') 
	    { 	
		$('#myaccountMenu .plusMinus').text('-');	
		$('#myaccountMenu').addClass('on');	
		$('#myaccountSubMenu').show();
		$('#assignmentsSubMenu').hide();
		$('#subManu').hide();
		$('#SubMediaManu').hide();
		$('#SubModelManu').hide();
		$('#SubContentManu').hide();
		$('#SubDataManu').hide();
		$('#SubPaymentsManu').hide();
	    }
	    else if(actionName=='adminassignmentdesignassignment' || actionName=='adminassignmentindex' || actionName=='adminsubmissionindex')	
	    {
		
		$('#assignmentsMenu').addClass('on');	
		$('#myaccountSubMenu').hide();
		$('#assignmentsSubMenu').show();
		$('#subManu').hide();
		$('#SubMediaManu').hide();
		$('#SubModelManu').hide();
		$('#SubContentManu').hide();
		$('#SubDataManu').hide();
		$('#SubPaymentsManu').hide();
	    }
	     else if(actionName=='adminsubmissionindex')	
	    {
		
		$('#assignmentsMenu').addClass('on');	
		$('#myaccountSubMenu').hide();
		$('#assignmentsSubMenu').hide();
		$('#subManu').show();
		$('#SubMediaManu').hide();
		$('#SubModelManu').hide();
		$('#SubContentManu').hide();
		$('#SubDataManu').hide();
		$('#SubPaymentsManu').hide();
	    }		
	    
	    
	       else if(actionName=='admincontentmanagementupdate' || actionName=='admincontentmanagementupdate'|| actionName=='admincontentmanagementupdate')	
	    {
		
		$('#assignmentsMenu').addClass('on');	
		$('#myaccountSubMenu').hide();
		$('#assignmentsSubMenu').hide();
		$('#subManu').show();
		$('#SubMediaManu').hide();
		$('#SubModelManu').hide();
		$('#SubContentManu').hide();
		$('#SubDataManu').hide();
		$('#SubPaymentsManu').hide();
	    }   

	    else  {
		$('.accordionContent').hide(); 
		$('#dashboardMenu').addClass('on');
	    }
	   
	});

</script>
<script>
		
</script>