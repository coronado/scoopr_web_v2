<?php
//==================================================================================================
/* This class is for the purpose to show demo requests */
//==================================================================================================

	class Demorequest extends Application
	{
		function __construct()
		{
		    $this->startsession();

		    if($this->getsession('admin_user_id')=="")
		    {	
			$this->redirect('admin','index');
		    }
		    $this->loadModel('model_admin_demorequest');	
			
		}



		function viewdemorequest()
		{
			$details = $this->model_admin_demorequest->viewdemorequest();
			
			$data['paginator_arr'] = $details;

			$errMsg = "";
			$param = $this->getrequest();
			
			$errMsg = $param['msg'];
			$data['errMsg'] = $errMsg;
			$data['openPanel'] = "accounts";

			$this->loadView('admin/viewdemorequest', $data);			
		}

		function demorequestdetails()
		{
			$parameter = $this->getrequest();
			$rid = $parameter['rid'];
			$details = $this->model_admin_demorequest->demorequestdetails($rid);
			
			
			$data['details'] = $details;
			$data['openPanel'] = "accounts";
			$this->loadView('admin/demorequestdetails', $data);
		}

		function deletedemorequest()
		{
			$parameter = $this->getrequest();
			$rid = $parameter['rid'];

			$delMsg = $this->model_admin_demorequest->deletedemorequest($rid);

			$errMsg = "Request has been removed sucessfully.";
			$data['msg'] = $errMsg;	

			$this->redirect('demorequest','viewdemorequest',$data);
		}
		
    }