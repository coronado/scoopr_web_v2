<?php

//===============================================================
/* USER CONTROLLER - calling the home page of the website */
//===============================================================

	class User extends Application
	{
		function __construct()
		{
			$this->loadModel('model_user');
		}

		# ---------------  index action (1) -------------------
		function index()
		{
			$this->loadView('user/index');
		}
		#-------------------------(/1)--------------------------


		#------ function to change password (2) ---------
		/*
		* paramas : requested parameters new and confirm passwords
		* Return : if success, changes the password
		*/ 
		function change_pass()
		{
			$params  = $this->getrequest();
			if(!empty($_POST['Submit_ChangePass']))
			{
				if($_POST['new_password']!=$_POST['confirm_password'])
				{
					$params['message'] = "Confirm password did not match!!";
				}
				else if(empty($_POST['user']))
				{
					$params['message'] = "Invalid user";
				}
				else
				{					
					$success = $this->model_user->update_password($_POST['user'],$_POST['new_password']);
					if($success)
					{
						$params['success'] = "Password changed successfully";
					}
				}
				$params['token'] = $_POST['token'];
				$params['user'] = $_POST['user'];
			}
			else
			{
				if(empty($params['token']) || empty($params['user']))
				{
					$params['message'] = "Invalid request";
				}
				else
				{
					$userid = $this->model_user->check_forgot_user($params['token'],$params['user']);
					if($userid > 0 )
					{
					}
					else
					{
						$params['message'] = "Invalid request";
					}
				}
			}
			$this->loadView('user/change_pass',$params);	
		}
		#-----------------------------------(/2)----------------------------------


		#------ function to reset password (3) ---------
		/*
		* paramas : requested parameters
		* Return : if success, reset the password
		*/ 
		function resetpwd()
		{
			$params  = $this->getrequest();
			$reset_key = $params['reset_key'];
			$errMsg = '';
			if(!empty($_POST['submit']))
			{	
				$isResetKeyExists = $this->model_user->checkResetKey($reset_key);
				if($isResetKeyExists)
				{
					$this->model_user->reset_password($_POST);
					$errMsg = "Your Password has been reset successfully";
				}
				else
				{
					$errMsg = "Invalid Request";
				}
			}
			
			$data['reset_key'] = $reset_key;	
			$data['errMsg'] = $errMsg;
			$this->loadView('user/resetpassword',$data);	
		}
		#-----------------------(/3)-----------------------------	
	}