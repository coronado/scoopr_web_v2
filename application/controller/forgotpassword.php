<?php
//==================================================================================================
/* This class is for forgot password functionality and to send the new password */
//==================================================================================================

	class Forgotpassword extends Application
	{
		function __construct()
		{
		        $this->startsession();
			$this->loadModel('model_forgot_password');
		}

		#------ this is the action where we posting password data (1) ---------
		/*
		* Params : posted data from the page, email address
		* Return : if sucsess , it sends an email to user to reset password
		*/ 
		function index()
		{
			$errFlag = 0;
			$errorMsg = '';
			if(!empty($_POST['submit']))
			{	
				$isEmailExists = $this->model_forgot_password->checkEmail($_POST);	
				if($isEmailExists)
				{
					$this->model_forgot_password->forgotPwdEmail($_POST);	
					$errorMsg = "We have sent you an email to reset a password.";
					$errFlag =1;
				}
				else
				{
					$errorMsg = "Email address does not exists.";
				}
			}
			
			$data['errorMsg'] = $errorMsg;

			$this->loadView('user/forgotpassword',$data);
			/*if($errFlag==1) {
				$this->redirect('forgotpassword', 'success');
			} else {
				
			}*/
			
		}

		function success()
		{
			$this->loadView('user/forgotPwdMsg');
		}
		#------------------------------------(/1)------------------------------------
        }