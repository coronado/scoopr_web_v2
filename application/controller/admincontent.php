<?php
//==================================================================================================
/* This class is for the purpose to show admin section*/
//==================================================================================================

	class Admincontent extends Application
	{
		function __construct()
		{
			$this->startsession();
			if($this->getsession('admin_user_id')=="")
			{	
				$this->redirect('admin','index');
			}
			$this->loadModel('model_admin_content');
		}

		function manage()
		{
			$params = $this->getRequest();
			$content_id = $params['id'];
			
			if(isset($_POST['save']))		
			{	
				$result = $this->model_admin_content->updateContent($content_id);
			}

			$details = $this->model_admin_content->getContent($content_id);

			$this->loadView('admin/content');
		}
       }