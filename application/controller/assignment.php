<?php
//===================================================================================
/* This controller is for the purpose to create, edit, view and launch assignment */
//===================================================================================
	class Assignment extends Application
	{
		function __construct()
		{
			$this->startsession();
			if($_SESSION['user_id']=="")
	   		{	$this->setsession("invalid","yes");
	      			$this->redirect('index','index');
	   		}

			
		}

		#------------------------ This action is to create assignment (1)----------------------
		/*
		* Desc : loading model, checking form submission, checking validation, inserting into database
		*/
		function design()
		{
			$this->loadModel('model_create_assignment');
			
			//$mode = $this->getsession('mode');   // edit mode or create mode
			$parameter = $this->getrequest();
			$mode = $parameter['mode'];
			$data['mode'] = $mode;

			if($mode=='edit' && $this->getsession('assignment_id')=='')
			{
				$this->redirect('invalidaction','index');
			} 

			if($mode=='edit' && $this->getsession('assignment_id')!='') 
			{
				$data['details'] = $this->model_create_assignment->getAssignmentDetails($this->getsession('assignment_id'));
				// to do task
			}

			/* ------ setting the assignment pages by default fields ------- */
			$data['current_year'] = date('Y'); 
			
			$imagery_name = $this->model_create_assignment->getImageryName();
			$data['imagery_name'] = $imagery_name;

			$categories = $this->model_create_assignment->getCategories();
			$data['categories'] = $categories;
			/* -------- finish here ------------- */


			if(!empty($_POST['next']) || !empty($_POST['save_now']))
			{	
				$redirectController = "assignment";
				$redirectMethod     = "preview";

				if(!empty($_POST['save_now'])) 
				{
					$redirectController = "assignment";
					$redirectMethod     = "preview";
				}
									
				$errors  = $this->validateAssignment($_POST);
				//$errors = array();  
				if(empty($errors))
				{	
					if($mode!='edit')
					{	
						$assignment_id = $this->model_create_assignment->create_assignment($_POST);
						if($assignment_id > 0)
						{
							$this->uploadReferenceImages($_FILES,$assignment_id);
							$this->setsession('assignment_id',$assignment_id);

							$this->redirect($redirectController,$redirectMethod);
						}
					}
					else
					{ 	
						$this->model_create_assignment->edit_assignment($_POST,$this->getsession('assignment_id'));
						//echo "<pre>"; print_r($_FILES); die;
						$this->updateReferenceImages($_FILES,$this->getsession('assignment_id'));
						$this->redirect($redirectController,$redirectMethod);
					}
				}
				else
				{
					$data['errors'] = $errors;
				}
				
			}

			$this->loadView('assignment/design',$data);
		}
		#------------------------------------(/1)------------------------------------


		#------ function to upload assignment refrence images (2) ---------
		/*
		* Params : image files, assignment id
		* Return : if sucsess , insert images to the database
		*/
		function uploadReferenceImages($files,$assignment_id)
		{
			$upload_dir_path=ROOT_PATH."images/reference_img/"; 
			if(!file_exists($upload_dir_path))
			{ 
				mkdir($upload_dir_path);
				chmod($upload_dir_path,0777);
			}
			
			$success_file_array =  array();
			foreach($files['upload_image']['tmp_name'] as $key => $tmp_name)
			{
				$file_name = $files['upload_image']['name'][$key];
				$file_size = $files['upload_image']['size'][$key];
				$file_tmp_name = $files['upload_image']['tmp_name'][$key];
				$file_type     = $files['upload_image']['type'][$key];
				$file_error    = $files['upload_image']['error'][$key];
			
				$pngBase64 = base64_encode(file_get_contents($file_tmp_name));
				$ext = strtolower(end(explode('.',$file_name)));
				$file_alias = sha1(substr($pngBase64, 0, 10).rand(11111, 99999)).'_'.$assignment_id.'.'.$ext;

				if($file_size>0)
				{
					if($file_error==0)
					{
						if(!file_exists($upload_dir_path.$file_alias))
						{	
							$success=move_uploaded_file($file_tmp_name,$upload_dir_path.$file_alias);
							$this->uploadImages($upload_dir_path,$file_alias);
							if($success)
							{ 
								array_push($success_file_array,$file_alias);
							}
						}
					}
				}
			}
			
			$this->model_create_assignment->insertReferenceImages($assignment_id,$success_file_array);
		}
		#------------------------------------(/2)------------------------------------


		#------ function to create small size image based on aspect ratio (3) ---------
		/*
		* Params : directory path, file name
		* Return : create the thumbnails using php imagik
		*/
		function uploadImages($upload_dir_path,$filename)
		{	//echo $filename; 
			list($origWidth, $origHeight) = @getimagesize($upload_dir_path.$filename);
			$origRatio = $origWidth/$origHeight; 
			$resultWidth = 320;
			

			if($origWidth>$resultWidth)
			{					
				$resultHeight = $resultWidth / $origRatio;
				$imageUrl="http://".SERVER_PATH."/images/".$filename;
				$thumb = new Imagick($upload_dir_path.$filename);
				$thumb->resizeImage($resultWidth,$resultHeight,Imagick::FILTER_LANCZOS,1);
				$thumb->writeImage($upload_dir_path.$filename);
				$thumb->destroy(); 
			}
			else 
			{
				$thumb = new Imagick($upload_dir_path.$filename);						$thumb->writeImage($upload_dir_path.$filename);
				$thumb->destroy(); 					  
			}
		}
		#------------------------------------(/3)------------------------------------


		#------ function to update images at the time of edit assignment (4) ---------
		/*
		* Params : image files, assignment id
		* Return : upload the reference image and update it to database
		*/
		function updateReferenceImages($files,$assignment_id)
		{
			$upload_dir_path=ROOT_PATH."images/reference_img/"; 
			if(!file_exists($upload_dir_path))
			{ 
				mkdir($upload_dir_path);
				chmod($upload_dir_path,0777);
			}
			
			$success_file_array =  array();
			foreach($files['upload_image']['tmp_name'] as $key => $tmp_name)
			{
				$file_name     = $files['upload_image']['name'][$key];
				$file_size     = $files['upload_image']['size'][$key];
				$file_tmp_name = $files['upload_image']['tmp_name'][$key];
				$file_type     = $files['upload_image']['type'][$key];
				$file_error    = $files['upload_image']['error'][$key];
			
				$pngBase64 = base64_encode(file_get_contents($file_tmp_name));
				$ext = strtolower(end(explode('.',$file_name)));
				$file_alias = sha1(substr($pngBase64, 0, 10).rand(11111, 99999)).'_'.$assignment_id.'.'.$ext;

				if($file_size>0)
				{
					if($file_error==0)
					{	
						if(!file_exists($upload_dir_path.$file_alias))
						{
							$update = $this->model_create_assignment->editReferenceImages($assignment_id,$key+1,$file_alias);	
							$success=move_uploaded_file($file_tmp_name,$upload_dir_path.$file_alias);
							$this->uploadImages($upload_dir_path,$file_alias);  
						}
					}
				}
			}
		}
		#------------------------------------(/4)------------------------------------


		#--------------- This action is to validate assignment form (5)----------------------
		/*
		*Desc : validating inpute fields
		*/
		function validateAssignment($request)
		{
			$errors = array();

			if(empty($request['looking_for_text'])) {
				$errors['looking_for_text']  =  "Please Enter Description";
			} 

			if(empty($request['hope_to_select'])) {
				$errors['hope_to_select'] = "Please choose an option";
			}
			
			if(empty($request['submission_deadline'])) {
				$errors['submission_deadline']   =  "Please select a deadline";
			} 
			
			/*if(empty($request['year'])) {
				$errors['year']   =  "Please select a year";
			} 

			if(empty($request['month'])) {
				$errors['month']   =  "Please select a month";
			} 			

			if(empty($request['day'])) {
				$errors['day']   =  "Please select a day";
			} 			

			if(empty($request['submission_time'])) {
				$errors['submission_time']   =  "Please select a time";
			}*/

			if(empty($request['imagery_for'])) {
				$errors['imagery_for']   =  "Please choose at least an option";
			}

			

			if(empty($request['model_release'])) {
				$errors['model_release'] = "Please choose an option";
			}

			if(empty($request['imagery_format'])) {
				$errors['imagery_format'] = "Please choose an option";
			}

			if(empty($request['reference_image_text'])) {
				$errors['reference_image_text']   =  "Please provide a description";
			} 

			return $errors;
		}
		#------------------------------------(/5)------------------------------------


		#---------- This action is to review assignment (6)----------------------
		function preview()
		{
			$this->loadModel('model_review_assignment');
			
			$assignment_id = $this->getsession('assignment_id');
			if($assignment_id=='')
			{
				$this->redirect('assignment','design');
			}

			$result = $this->model_review_assignment->preview_assignment($assignment_id);

			$imagery_for = unserialize($result['imageryFor']);
			$imagery_for_array = $this->model_review_assignment->getImagery($imagery_for);

			$imagery_format = unserialize($result['imageryFormat']);
			$imagery_format_array = $this->model_review_assignment->getImageryFormat($imagery_format);

			$data['result'] = $result;
			$data['imagery_for_array'] = $imagery_for_array;
			$data['imagery_format_array'] = $imagery_format_array;
			#---------- submission date & deadline ----------------------
			$submission_deadline = $result['submissionDeadline']; 
			if($result['is_launched']==2) 
			{
				$submission_date = "Not Launched yet";
				$submission_deadline = $result['submission_deadline']. " days after launch date";
			} 
			else 
			{
				$submission_date = date('F jS Y',(strtotime($result['launchedDate']." +$submission_deadline day")));
				$current_date = date('Y-m-d'); 
				$days_left = $this->calculateDays($current_date,$submission_date);
				$submission_deadline = $days_left. " days left";
			}
			$data['submission_date'] = $submission_date;
			$data['submission_deadline'] = $submission_deadline;
			#----------------- (END) ----------------------

			/* --------- date time difference ------------  */
			//$submission_deadline = $result['submission_deadline'];
			
			//$current_date = date('Y-m-d H:i:s'); 
			//$deadline = $this->calculateDateDiff($current_date,$submission_deadline);
			//$data['deadline'] = $deadline;
			/* -----------end here --------------- */
			$this->loadView('assignment/preview',$data);
		}
		#------------------------------------(/6)------------------------------------


		#------ function to get days between two dates (7) ---------
		/*
		* Params : date1, date2
		* Return : number of days
		*/
		protected function calculateDays($date1, $date2)
		{
			$diff = abs(strtotime($date2) - strtotime($date1));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			return $days;
		}
		#--------------------------------(/7)---------------------------------

		
		#------ Get year,month,day, hour,minute and seconds between two dates(8) ---------
		/*
		* Params : date1, date2
		* Return : year, month, day, hour, minute, seconds
		*/
		function calculateDateDiff($date1, $date2)
		{
			$diff = abs(strtotime($date2) - strtotime($date1));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

			$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
			$minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ (60));
			$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60 ));
			
			$deadline = "";
			if($years==1) {
				$deadline .= $years." year ";
			} elseif($years>1) {
				$deadline .= $years." years ";
			}
			if($months==1) {
				$deadline .= $months." month ";
			} elseif($months>1) {
				$deadline .= $months." months ";
			}

			if($days==1) {
				$deadline .= $days." day ";
			} elseif($days>1) {
				$deadline .= $days." days ";
			}
	
			if($hours==1) {
				$deadline .= $hours." hour ";
			} elseif($hours>1) {
				$deadline .= $hours." hours ";
			}

			if($minutes==1) {
				$deadline .= $minutes." minute ";
			} elseif($minutes>1) {
				$deadline .= $minutes." minutes ";
			}

			if($seconds==1) {
				$deadline.= $seconds." second";
			} elseif($seconds>1) {
				$deadline.= $seconds." seconds";
			}

			return $deadline;
		}
		#------------------------------------(/8)------------------------------------


		#------ This action is just to show launch assignment page with details (9)---------
		/*
		* Params : assignment id session value
		* Return : assignment details
		*/
		function launch()
		{
			/*$this->loadModel('model_review_assignment');

			$assignment_id = $this->getsession('assignment_id'); 
			if($assignment_id=='')
			{
				$this->redirect('assignment','design');
			}

			$result = $this->model_review_assignment->preview_assignment($assignment_id);

			$imagery_for = unserialize($result['imageryFor']);
			$imagery_for_array = $this->model_review_assignment->getImagery($imagery_for);

			$imagery_format = unserialize($result['imageryFormat']);
			$imagery_format_array = $this->model_review_assignment->getImageryFormat($imagery_format);

			$data['result'] = $result;
			$data['imagery_for_array'] = $imagery_for_array;
			$data['imagery_format_array'] = $imagery_format_array;*/

			/* --------- date time difference ------------  */
			/*$submission_deadline = $result['submission_date'];
			$current_date = date('Y-m-d H:i:s'); 
			$deadline = $this->calculateDateDiff($current_date,$submission_deadline);
			$data['deadline'] = $deadline;*/
			/* -----------end here --------------- */
			
			$this->loadView('assignment/launch');
		}
		#------------------------------------(/9)------------------------------------


		#------ This action is just to redirect to edit mode (10)---------
		function edit()
		{
			$params =  $this->getrequest();
			$this->setsession('assignment_id',$params['id']);
			//$this->setsession('mode','edit');
			$pass_para = array('mode'=>'edit');
			$this->redirect('assignment','design',$pass_para);
		}
		#--------------------------------(/10)---------------------------------


		#------ This action is to launch assignment (11)-----------
		function launchit()
		{
			$this->loadModel('model_review_assignment');
			$assignment_id = $this->getsession('assignment_id'); 
			$userid = $this->getsession('user_id'); 
			$profile_name = $this->getsession('profile_name'); 

 			$success = $this->model_review_assignment->launch_assignment($assignment_id);
			if($success)	
			{
				/* ------------------ push notifaction ---------------------------*/
				$this->include_file('push_notification.php','application/lib');
				$PushNotification =  new PushNotification();
				$msg = $profile_name." has a new Scoopr Assignment!";
				
				//$device_token = array('3af252ac2dc60c3d2f5c5fdbaba1fae78db855395d99d402415a9cd2ee9a2020');
				$device_token = $this->model_review_assignment->getAllDeviceToken();
				
				$_tokenArr=$device_token;
				//echo "<pre>"; print_r($_tokenArr); 
				// For Iphone
				$PushNotification->send_notification($msg,$_tokenArr,1);
				

				$this->unsetsession(array('assignment_id'));
				$this->redirect('assignment','launch');
			}
		}
		#------------------------------------(/11)----------------------------------
		
		
	}
?>
