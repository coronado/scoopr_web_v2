<?php

//===================================================================================
/* This controller is for the purpose to show profile details and settings */
//===================================================================================

	class profile extends Application
	{
		function __construct()
		{
			$this->startsession();
			if($this->getsession('user_id')=="")
			{	
				$this->setsession("invalid","yes");
				$this->redirect('index','index');
			}
			$this->user_id = $this->getsession('user_id');
			$this->loadModel('model_profile');
		}

		#------ This action is to show the buyer profile details (1) ---------
		/*
		* Return : buyer profile details
		*/ 
		function view()
		{
			$buyer_info = $this->model_profile->getBuyerInfo($this->user_id);
			$data['buyer_info'] = $buyer_info;

			$detail = $this->model_profile->profileDetail($this->user_id);
			$data['detail'] = $detail;

			$this->loadView('profile/view',$data);
		}
		#-------------------------(/1)---------------------------------


		#------ This action is to update the buyer profile details (2)---------
		/*
		* Return : update the cubmitted details
		*/ 
		function update()
		{
			global $errors;
			$errors = array();
			
			if(isset($_POST['save_profile']))
			{
					$updateInfo = $this->model_profile->updateProfile($this->user_id,$_POST);
					if($updateInfo)
					{	
						$this->setsession("name",$_POST['full_name']);
						$this->setsession("profile_name",$_POST['profile_name']);
						
						if($_FILES["company_logo"]["name"]!='')
						{
							$this->updateLogo($_FILES);
						}
						
						if($_POST['old_password']!='' && $_POST['new_password']!='')
						{
							$isOldPwdExists = $this->model_profile->checkOldPassword($this->user_id,$_POST);
							if($isOldPwdExists)
							{
								$result = $this->model_profile->updatePassword($this->user_id,$_POST);
								//$this->redirect('profile','view');				
							}
							else 
							{	
								$errors['old_password'] = "Your old password doesn't exists. Please enter correct information.";
							}
						}
						
						$errors['success'] = "Your profile has been updated successfully.";
					}
			}


			$buyer_info = $this->model_profile->getBuyerInfo($this->user_id);
			$data['buyer_info'] = $buyer_info;
		
			$detail = $this->model_profile->profileDetail($this->user_id);
			$data['detail'] = $detail;

			$data['errors'] = $errors;
			$this->loadView('profile/update',$data);
		}
		#-------------------------(/2)---------------------------------


		#------ This action is to upload the image (3) ---------
		/*
		* Params : uploading directory path, image name
		* Return : update the submitted image
		*/ 
		function uploadImages($upload_dir_path,$filename)
		{	
			list($origWidth, $origHeight) = @getimagesize($upload_dir_path.$filename);
			$resultWidth = 90;
			$resultHeight = 90;

			// creating thumb images based on aspect ratio
			if($origWidth>$resultWidth)
			{					
				$thumb = new Imagick($upload_dir_path.$filename);
				$thumb->resizeImage($resultWidth,$resultHeight,Imagick::FILTER_LANCZOS,1);
				$thumb->writeImage($upload_dir_path.$filename);
				$thumb->destroy(); 
			}
			else 
			{
				$thumb = new Imagick($upload_dir_path.$filename);						$thumb->writeImage($upload_dir_path.$filename);
				$thumb->destroy(); 					  
			}
		}
		#-------------------------(/3)---------------------------------


		#------ This action is to update the image (4) ---------
		/*
		* Params : file array
		* Return : update the image to the database
		*/ 
		function updateLogo($files)
		{
			
			$upload_dir_path=ROOT_PATH."images/profile_img/"; 
			if(!file_exists($upload_dir_path))
			{ 
				mkdir($upload_dir_path);
				chmod($upload_dir_path,0777);
			}
			$pngBase64 = base64_encode(file_get_contents($files['company_logo']['tmp_name']));
			$ext = strtolower(end(explode('.',$files["company_logo"]["name"])));
			$filename = sha1(substr($pngBase64, 0, 10).rand(11111, 99999)).'_'.$this->user_id.'.'.$ext;

			if($files['company_logo']['size'] > 0)
			{
				if($files['company_logo']['error'] == 0)
				{ 
					if(!file_exists($upload_dir_path.$filename))
					{ 
						$this->model_profile->updateProfileLogo($this->user_id,$filename);
						$success=move_uploaded_file($_FILES['company_logo']['tmp_name'],$upload_dir_path.$filename);
						$this->uploadImages($upload_dir_path,$filename);
						$this->setsession("profile_img",$filename);
					}
				}
			}
		}
		#-------------------------(/4)---------------------------------


		#------ This action is to change the password (5) ---------
		/*
		* Return : if successfull, it changes the password
		*/ 
		function settings()
		{
			$errMsg = '';
			if(!empty($_POST['change_password']))
			{ 
				$isOldPwdExists = $this->model_profile->checkOldPassword($this->user_id,$_POST);
				if($isOldPwdExists)
				{	
					$result = $this->model_profile->updatePassword($this->user_id,$_POST);
					if($result)
					{
						$errMsg = "Your password has been changed successfully.";
					}
					else
					{
						$errMsg = "PHP Mysql Database error.";	
					}
				}
				else
				{	
					$errMsg = "Your old password doesn't exists. Please enter correct information.";		
				}	
			}
			$data['errMsg'] = $errMsg;
			$this->loadView('profile/settings',$data);
		}
		#-------------------------(/5)---------------------------------

		
	}
?>
