<?php
//==================================================================================================
/* This class is for the purpose to show assignment list and content list in the dashboard section*/
//==================================================================================================

	class Content extends Application
	{
		function __construct()
		{
			$this->startsession();
			if($this->getsession('user_id')=="")
			{	
				$this->setsession("invalid","yes");
				$this->redirect('index','index');
			}
			$this->loadModel('model_content');
		}

		#------ This function is to show list of content for specific assignment ---------
		/*
		* Params : assignment id, favourite flag (optional)
		* Return : 
		*/ 
		function submission()
		{
			/* -------------- LIST OF ALL ASSIGNMENT IDS ------------ */	
			$assignment_ids = $this->model_content->getAssignmentIds($this->getsession('user_id'));

			$data['assignment_ids'] = $assignment_ids;
			reset($assignment_ids); 
			$first_assignment_id = key($assignment_ids);

			/* -------------- GETTING ASSIGNMENT ID ------------ */		
			$params  = $this->getrequest();
			if($params['assignment_id']=='' && !empty($assignment_ids)) {
				$assignment_id = $first_assignment_id;
			} elseif($params['assignment_id']!='') {
				$assignment_id = $params['assignment_id'];
			} else {
				$assignment_id = "";
			}
			$data['assignment_id'] = $assignment_id;
			
			/* -------------- SETTING PAGE NO ------------ */		
			$current_page_no = 1;
			if($params['page_no']!='') {
				$current_page_no = $params['page_no'];
			}
			$data['current_page_no'] = $current_page_no;

			$fav = ((int)$params['fav']==1?1:0);
			$data['fav'] = $fav;
			
			/* --------------  CONTENT LISTINGS ------------ */	
			$listings = $this->model_content->getContent($assignment_id,$this->getsession('user_id'),$fav,$current_page_no);
			$data['listings'] = $listings;

			/* --------------  ASSIGNMENT DETAILS ------------ */	
			$result = $this->model_content->getAssignmentDetails($assignment_id);
			$data['result'] = $result;

			/* --------------  SUBMISSION DEADLINE & DATE ------------ */	
			$deadline = $this->submissionDeadlineAndDate($assignment_id);
			$data['deadline'] = $deadline;


			/* --------------------  BUYER INFO --------------- */
			$buyer_info = $this->model_content->getBuyerInfo($this->getsession('user_id'));
			$data['buyer_info'] = $buyer_info;

			$this->loadView('content/submission',$data);
		}

		#-------------- preview page before making payment (buyer dashboard) ------------
		/*
		* Params : assignment id, favourite flag (optional)
		* Return : list of content
		*/ 
		function preview()
		{
			/* -------------- LIST OF ALL ASSIGNMENT IDS ------------ */
			$assignment_ids = $this->model_content->getAssignmentIds($this->getsession('user_id'));
			$data['assignment_ids'] = $assignment_ids;
			reset($assignment_ids); 
			$first_assignment_id = key($assignment_ids);

			/* -------------- GETTING PARAMETERS ------------ */
			$params  = $this->getrequest();
			$assignment_id = $params['assignment_id'];

			$content_id    = $params['content_id'];	
			
			$page_no       = $params['page_no'];
			$fav = ((int)$params['fav']==1?1:0);		
			$data['assignment_id'] = $assignment_id;
			$data['fav'] = $fav;

			/* -------------- GETTING LIST OF CONTENT ------------ */
			$listings = $this->model_content->getContentPreview($assignment_id,$this->getsession('user_id'),$fav,$page_no);
			$data['listings'] = $listings;

			/* --------------  SUBMISSION DEADLINE & DATE ------------ */	
			$deadline = $this->submissionDeadlineAndDate($assignment_id);
			$data['deadline'] = $deadline;


			/* --------------------  BUYER INFO --------------- */
			$buyer_info = $this->model_content->getBuyerInfo($this->getsession('user_id'));
			$data['buyer_info'] = $buyer_info;

				
			$this->loadView('content/preview',$data);
		}

		#-------------- assignment detail page in buyer dashboard ------------
		/*
		* Params : assignment id
		* Return : assignment details
		*/ 
		function assignmentDetail()
		{
			$params  = $this->getrequest();
			$this->setsession('assignment_id',$params['aid']);
			$this->redirect('assignment','preview');

			/* -------------- LIST OF ALL ASSIGNMENT IDS ------------ */	
			/*$assignment_ids = $this->model_content->getAssignmentIds($this->getsession('user_id'));
			$data['assignment_ids'] = $assignment_ids;
			reset($assignment_ids); 
			$first_assignment_id = key($assignment_ids);*/

			/* -------------- GETTING ASSIGNMENT ID ------------ */				
			/*if($params['assignment_id']=='' && !empty($assignment_ids)) {
				$assignment_id = $first_assignment_id;
			} elseif($params['assignment_id']!='') {
				$assignment_id = $params['assignment_id'];
			} else {
				$assignment_id = "";
			}
			$data['assignment_id'] = $assignment_id;*/

			/* --------------  ASSIGNMENT DETAILS ------------ */	
			/*$result = $this->model_content->getAssignmentDetails($assignment_id);
			$data['result'] = $result;*/

			/* --------------  SUBMISSION DEADLINE & DATE ------------ */	
			/*$deadline = $this->submissionDeadlineAndDate($assignment_id);
			$data['deadline'] = $deadline;*/
			
			/* --------------------  BUYER INFO --------------- */
			/*$buyer_info = $this->model_content->getBuyerInfo($this->getsession('user_id'));
			$data['buyer_info'] = $buyer_info;

			$imagery_format = unserialize($result['imagery_format']);
			$imagery_format_array = $this->model_content->getImageryFormat($imagery_format);
			$data['imagery_format_array'] = $imagery_format_array;*/

			//echo "<pre>"; print_r($assignment_details); die;
			//$this->loadView('content/assignmentDetail',$data);
		}

		#-------------- to calculate submission deadline and date------------
		/*
		* Params : assignment id
		* Return : submission dealine and date
		*/ 
		protected function submissionDeadlineAndDate($assignment_id)
		{
			$result = $this->model_content->getDeadlineDate($assignment_id);
			/* --------------  SUBMISSION DEADLINE & DATE ------------ */
			$submission_deadline = $result['submissionDeadline'];
			if($result['is_launched']==2) 
			{
				$submission_date = "Not Launched yet";
				$submission_deadline = $result['submissionDeadline']. " days after launch date";
			} 
			else 
			{
				$submission_date = date('F jS Y',(strtotime($result['launchedDate']." +$submission_deadline day")));
				$current_date = date('Y-m-d'); 
				$days_left = $this->calculateDays($current_date,$submission_date);
				$submission_deadline = $days_left. " days left";
			}

			$arr['submission_date'] = $submission_date;
			$arr['submission_deadline'] = $submission_deadline;
			$arr['created_at'] = $result['createdAt'];
			return $arr;
		}

		#-------------- to calculate days between current date and launched date------------
		/*
		* Params : current date, assignment launched date
		* Return : days (deadline)
		*/ 
		protected function calculateDays($date1, $date2)
		{
			$diff = abs(strtotime($date2) - strtotime($date1));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			return $days;
		}

		#-------------- add to favourite functionality ------------
		/*
		* Params : assignment id, user id
		* Return : true or false
		*/ 
		function addToFav()
		{
			$params  = $this->getrequest();
			
			$content_id = $params['content_id'];

			$num_rows = $this->model_content->addToFavourite($content_id, $this->getsession('user_id'));
  			$userid = $this->getsession('user_id');
			/*Push notificatio for Add to Favorites (25/12)*/
				 //$device_token = $this->model_content->getUserDeviceToken($content_id);
				 
			$device_token = $this->model_content->getUserDeviceToken($content_id);
			$_tokenArr=$device_token;
				 
			$this->include_file('push_notification.php','application/lib');
			$PushNotification =  new PushNotification();
				
			$msg = "Your image has been added in to favorites";
				
			$PushNotification->send_notification($msg,$_tokenArr,1);
			echo $num_rows;
		}


		#-------------- remove from favourite functionality ------------
		/*
		* Params : assignment id, user id
		* Return : true or false
		*/ 
		function removeFromFav()
		{
			$params  = $this->getrequest();
			$content_id = $params['content_id'];

			$num_rows = $this->model_content->removeFromFavourite($content_id, $this->getsession('user_id'));
			echo $num_rows; 	
		}
		
		#-------------- page from where buyer can make payment ------------
		/*
		* Params : 
		* Return : 
		*/ 
		function paypal()
		{
			$assignment_ids = $this->model_content->getAssignmentIds($this->getsession('user_id'));
			$data['assignment_ids'] = $assignment_ids;

			$this->loadView('content/paypal',$data);
		}


		function assignments()
		{
			$details = $this->model_content->getAssignmentListing($this->getsession('user_id'));
			//print_r($details); die;
			//echo "<pre>"; print_r($assignments); die;
			//$data['listings'] = $listings;
			$data['paginator_arr'] = $details;
			$this->loadView('content/assignments',$data);
		}

		
	
}
