<?php
//==================================================================================================
/* This class is to show assignemnt module in admin section */
//==================================================================================================

	class Adminassignment extends Application
	{
		function __construct()
		{
			$this->startsession();
			if($this->getsession('admin_user_id')=="")
			{	
				$this->redirect('admin','index');
			}

			$this->loadModel('model_admin_assignment');
	       	}

		#------ this is the action where we show assignment listings ---------
		/*
		* Params : null
		* Return : paginated array of records
		*/ 
		function index()
		{
			$current_page = 1;
			$param =  $this->getrequest();
			if($param['page']!='') 
			{
				$current_page = $param['page'];			
			} 
			$uid = $param['uid'];
			
			if(!empty($_POST['search']))
			{
				$assignment_id = $_POST['assignment_id'];
				$email_address = $_POST['email_address'];
			}

			$search_arr = array('current_page'=>$current_page, 'uid'=>$uid, 'is_enabled'=>'1','aid'=>$assignment_id,'email_address'=>$email_address);

			$data['search_arr'] = $search_arr;
			$details = $this->model_admin_assignment->viewAssignmentListing($search_arr);
			$data['paginator_arr'] = $details;
			$data['openPanel'] = "assignments";
			$this->loadView('admin/assignmentlisting', $data);
		}

		function disableit()
		{
			$param =  $this->getrequest();
			$aid = $param['aid'];

			$disableIt = $this->model_admin_assignment->disableassignment($aid);
			$this->redirect('adminassignment','disabledlist');
		}

		function enableit()
		{
			$param =  $this->getrequest();
			$aid = $param['aid'];

			$enableIt = $this->model_admin_assignment->enableassignment($aid);
			$this->redirect('adminassignment','index');
		}

		function deleteit()
		{
			$param =  $this->getrequest();
			$aid = $param['aid'];

			$enableIt = $this->model_admin_assignment->deleteassignment($aid);
			
			$errMsg = "Assignment number ".$aid." has been deleted successfully.";
			$data['msg'] = $errMsg;	

			$this->redirect('adminassignment','disabledlist',$data);
		}
		
		function disabledlist()
		{
			$current_page = 1;
			$param =  $this->getrequest();
			if($param['page']!='') 
			{
				$current_page = $param['page'];			
			} 
			$uid = $param['uid'];

			if(!empty($_POST['search']))
			{
				$assignment_id = $_POST['assignment_id'];
				$email_address = $_POST['email_address'];
			}


			$search_arr = array('current_page'=>$current_page, 'uid'=>$uid, 'is_enabled'=>'0', 'aid'=>$assignment_id, 'email_address'=>$email_address);
			$data['search_arr'] = $search_arr;

			$details = $this->model_admin_assignment->viewAssignmentListing($search_arr);
			$data['paginator_arr'] = $details;
			$data['openPanel'] = "assignments";
			$data['errMsg'] = $param['msg'];
			$this->loadView('admin/disabledassignment', $data);
		}
				
		function designassignment()
		{ 
			$parameter = $this->getrequest();
			$mode = $parameter['mode'];
			$data['mode'] = $mode;
			$aid = $parameter['aid'];
			$uid = $parameter['uid'];
			$data['uid'] = $uid;
			
			$data['brands'] = $this->model_admin_assignment->getAllBrands();
			if($mode=='edit' && $aid!='') 
			{
				$data['details'] = $this->model_admin_assignment->getAssignmentBasicDetails($aid);
			}
			
			$imagery_name = $this->model_admin_assignment->getImageryName();
			$data['imagery_name'] = $imagery_name;

			$categories = $this->model_admin_assignment->getCategories();
			$data['categories'] = $categories;
			
			
			if(!empty($_POST['save_now']))
			{				
				if($mode!='edit')
				{	
					$assignment_id = $this->model_admin_assignment->createAssignment($_POST);
					if($assignment_id > 0)
					{
						$this->uploadReferenceImages($_FILES,$assignment_id);
						$this->redirect('adminassignment','index');
					}
				}
				else
				{ 
					$this->model_admin_assignment->editAssignment($_POST,$aid);
					$this->updateReferenceImages($_FILES,$aid);
					$this->redirect('adminassignment','assignmentDetail/aid/'.$aid);
				}	
			}

			$data['total_count'] = $this->model_admin_assignment->getTotalCountAssignment();
			$data['openPanel'] = "assignments";
			$this->loadView('admin/designassignment',$data);
		}



		function uploadReferenceImages($files,$assignment_id)
		{
			$upload_dir_path=ROOT_PATH."images/reference_img/"; 
			if(!file_exists($upload_dir_path))
			{ 
				mkdir($upload_dir_path);
				chmod($upload_dir_path,0777);
			}
			
			$success_file_array =  array();
			foreach($files['upload_image']['tmp_name'] as $key => $tmp_name)
			{
				$file_name     = $files['upload_image']['name'][$key];
				$file_size     = $files['upload_image']['size'][$key];
				$file_tmp_name = $files['upload_image']['tmp_name'][$key];
				$file_type     = $files['upload_image']['type'][$key];
				$file_error    = $files['upload_image']['error'][$key];
			
				$pngBase64 = base64_encode(file_get_contents($file_tmp_name));
				$ext = strtolower(end(explode('.',$file_name)));
				$file_alias = sha1(substr($pngBase64, 0, 10).rand(11111, 99999)).'_'.$assignment_id.'.'.$ext;

				if($file_size>0)
				{
					if($file_error==0)
					{
						if(!file_exists($upload_dir_path.$file_alias))
						{	
							$success=move_uploaded_file($file_tmp_name,$upload_dir_path.$file_alias); $this->uploadImages($upload_dir_path,$file_alias); 
							if($success)
							{ 
								array_push($success_file_array,$file_alias);
							}
						}
					}
				}
			}
			
			$this->model_admin_assignment->insertReferenceImages($assignment_id,$success_file_array);
		}

		function uploadImages($upload_dir_path,$filename)
		{	//echo $filename; 
			list($origWidth, $origHeight) = @getimagesize($upload_dir_path.$filename);
			$origRatio = $origWidth/$origHeight; 
			$resultWidth = 320;
			

			if($origWidth>$resultWidth)
			{					
				$resultHeight = $resultWidth / $origRatio;

				$imageUrl="http://".SERVER_PATH."/images/".$filename;
				$thumb = new Imagick($upload_dir_path.$filename);
				$thumb->resizeImage($resultWidth,$resultHeight,Imagick::FILTER_LANCZOS,1);
				$thumb->writeImage($upload_dir_path.$filename);
				$thumb->destroy(); 
			}
			else 
			{
				$thumb = new Imagick($upload_dir_path.$filename);						$thumb->writeImage($upload_dir_path.$filename);
				$thumb->destroy(); 					  
			}
		}


		function updateReferenceImages($files,$assignment_id)
		{
			$upload_dir_path=ROOT_PATH."images/reference_img/"; 
			if(!file_exists($upload_dir_path))
			{ 
				mkdir($upload_dir_path);
				chmod($upload_dir_path,0777);
			}
			
			$success_file_array =  array();
			foreach($files['upload_image']['tmp_name'] as $key => $tmp_name)
			{
				$file_name     = $files['upload_image']['name'][$key];
				$file_size     = $files['upload_image']['size'][$key];
				$file_tmp_name = $files['upload_image']['tmp_name'][$key];
				$file_type     = $files['upload_image']['type'][$key];
				$file_error    = $files['upload_image']['error'][$key];
			
				$pngBase64 = base64_encode(file_get_contents($file_tmp_name));
				$ext = strtolower(end(explode('.',$file_name)));
				$file_alias = sha1(substr($pngBase64, 0, 10).rand(11111, 99999)).'_'.$assignment_id.'.'.$ext;

				if($file_size>0)
				{
					if($file_error==0)
					{	
						if(!file_exists($upload_dir_path.$file_alias))
						{
							$update = $this->model_admin_assignment->editReferenceImages($assignment_id,$key+1,$file_alias);	
							$success=move_uploaded_file($file_tmp_name,$upload_dir_path.$file_alias);
							$this->uploadImages($upload_dir_path,$file_alias);   
						}
					}
				}
			}
		}



		#-------------- assignment detail page in admin section ------------
		/*
		* Params : assignment id
		* Return : assignment details
		*/ 
		function assignmentDetail()
		{
			$params  = $this->getrequest();
			$assignment_id = $params['aid'];
			$buyer_id      = $params['uid'];
			$data['assignment_id'] = $assignment_id;

			/* --------------  ASSIGNMENT DETAILS ------------ */	
			$result = $this->model_admin_assignment->getAssignmentAllDetails($assignment_id);
			$data['result'] = $result;

			/* --------------  SUBMISSION DEADLINE & DATE ------------ */	
			$deadline = $this->submissionDeadlineAndDate($assignment_id);
			$data['deadline'] = $deadline;
			
			/* --------------------  BUYER INFO --------------- */
			$buyer_info = $this->model_admin_assignment->getBuyerInfo($buyer_id);
			$data['buyer_info'] = $buyer_info;

			/* ------------------- IMAGERY FOR INFO ------------- */
			$imagery_for = unserialize($result['imageryFor']);
			$imagery_for_array = $this->model_admin_assignment->getImagery($imagery_for);
			$data['imagery_for_array'] = $imagery_for_array;

			$imagery_format = unserialize($result['imagery_format']);
			$imagery_format_array = $this->model_admin_assignment->getImageryFormat($imagery_format);
			$data['imagery_format_array'] = $imagery_format_array;

			//echo "<pre>"; print_r($data); die;
			$data['openPanel'] = "assignments";
			$this->loadView('admin/assignmentDetail',$data);
		}

		#-------------- to calculate submission deadline and date------------
		/*
		* Params : assignment id
		* Return : submission dealine and date
		*/ 
		protected function submissionDeadlineAndDate($assignment_id)
		{
			$result = $this->model_admin_assignment->getDeadlineDate($assignment_id);
			/* --------------  SUBMISSION DEADLINE & DATE ------------ */
			$submission_deadline = $result['submission_deadline']; 
			if($result['is_launched']==2) 
			{
				$submission_date = "Not Launched yet";
				$submission_deadline = $result['submission_deadline']. " days after launch date";
			} 
			else 
			{
				$submission_date = date('F jS Y',(strtotime($result['launched_date']." +$submission_deadline day")));
				$current_date = date('Y-m-d'); 
				$days_left = $this->calculateDays($current_date,$submission_date);
				$submission_deadline = $days_left. " days left";
			}

			$arr['submission_date'] = $submission_date;
			$arr['submission_deadline'] = $submission_deadline;
			$arr['created_at'] = $result['created_at'];
			return $arr;
		}

		#-------------- to calculate days between current date and launched date------------
		/*
		* Params : current date, assignment launched date
		* Return : days (deadline)
		*/ 
		protected function calculateDays($date1, $date2)
		{
			$diff = abs(strtotime($date2) - strtotime($date1));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			return $days;
		}



}
