<?php

//===================================================================================
/* This controller is for the purpose to show static pages content */
//===================================================================================

	class info extends Application
	{
		function __construct()
		{
			$this->startsession();
			$this->loadModel('model_info');
		}

		#------ action to show about us content (1) ---------
		function aboutus()
		{
			$this->loadView('info/aboutus');
		}
		#-------------------------(/1)------------------------


		#------ action to show ourapp content (2) ---------
		function ourapp()
		{
			$this->loadView('info/ourapp');
		}
		#-------------------------(/2)-----------------------


		#------ action to show terms of service content (3) ---------
		function termsofservices()
		{
			$id =3;
			$content = $this->model_info->getContent($id);
			$data['content'] = $content;
			$this->loadView('info/termsofservices',$data);
		}
		#-------------------------(/3)-----------------------


		#------ action to show faq content (4) ---------
		function faq()
		{
			$id =1;
			$content = $this->model_info->getContent($id);
			$data['content'] = $content;
			$this->loadView('info/faq',$data);
		}
		#-------------------------(/4)-----------------------

		
		#------ action to show privacy policy content (5) ---------
		function privacypolicy()
		{
			$id =2;
			$content = $this->model_info->getContent($id);
			$data['content'] = $content;
			$this->loadView('info/privacypolicy',$data);
		}
		#-------------------------(/5)-----------------------


		#------ action to send contact us message (6) ---------
		function contactus()
		{
			$errMsg = '';
			if(!empty($_POST))
			{	
				$message = $this->model_info->contactus($_POST);
				$errMsg = "Thank you for contacting with us. We will get back to you asap.";
			}
			$data['errMsg'] = $errMsg;
			//echo "aaaaa"; die;
			echo $errMsg; 
			//$this->loadView('info/contactus',$data);
		}
		#-------------------------(/6)-----------------------	

	}
?>
