<?php

//===============================================================
/* INDEX CONTROLLER - calling the home page of the website */
//===============================================================

	class Index extends Application
	{
		// construct method - session start
		function __construct()
		{
			$this->startsession();
		}
		
		#------- default index method loading template (1) ------
		function index()
		{
			$param = $this->getrequest();
			$data['param'] = $param;			

			$this->loadView('index/index',$data);
		}
		#----------------------(/1)--------------------------
	}
?>
