<?php

//===================================================================================
/* This controller class is for the purpose to register user functionality */
//===================================================================================

	class Signup extends Application
	{
		function __construct()
		{
			$this->loadModel('model_signup');
		}

		#------ by default action index used to show signup page (1)---------
		/*
		* desc : validate signup, add user to database, upload company logo
		* Return : if success redirect to the thanks page
		*/ 
		function index()
		{
			global $errors;
			$errors = array();
			if(!empty($_POST['Submit_SignUp']))
			{ 
				$errors = $this->validateSignUp($_POST);
				if(empty($errors))
				{ 
					$user_id = $this->model_signup->add_user($_POST);
					//echo "userid : ".$user_id;die;
					if($_FILES["company_logo"]["name"]!='' && $user_id > 0)
					{
						$upload_dir_path=ROOT_PATH."images/profile_img/"; 
						if(!file_exists($upload_dir_path))
						{ 
							mkdir($upload_dir_path);
							chmod($upload_dir_path,0777);
						}
						$pngBase64 = base64_encode(file_get_contents($_FILES['company_logo']['tmp_name']));
						$ext = strtolower(end(explode('.',$_FILES["company_logo"]["name"])));
						$filename = sha1(substr($pngBase64, 0, 10).rand(11111, 99999)).'_'.$user_id.'.'.$ext;
						
						if($_FILES['company_logo']['size'] > 0)
						{
							if($_FILES['company_logo']['error'] == 0)
							{ 
							if(!file_exists($upload_dir_path.$filename))
							{ 
							$success=move_uploaded_file($_FILES['company_logo']['tmp_name'],$upload_dir_path.$filename);
							if($success)
							{
								$this->model_signup->update_profile_pic($user_id,$filename);
							}
							}
							}
						}
					}
					$this->setsession("added_user",$user_id);
					
					echo "<script>parent.jQuery.fancybox.close();window.parent.location.href = '/signup/thanks';</script>";
				}
				
			}
			$this->loadView('user/signup');
		}
		#----------------------(/1)---------------------------------------

		
		/* ========== just to load thanks page template (2)========= */
		function thanks()
		{
			$this->loadView('user/thanks'); 
		}
		#----------------------(/2)--------------------------

		
		/* ========== just to load thanks page after fb signup (3) ========= */
		function fbthanks()
		{
			$this->loadView('user/fbthanks'); 
		}
		#----------------------(/3)--------------------------


		#------ server side validation to check bad inputs(4) ---------
		/*
		* paramas : posted field values in array format
		* Return : if success return blank else error message
		*/ 
		function validateSignUp($request)
		{ 
			$errors = '';
			# Email address invalid
			if(empty($request['email_address'])){	
				$errors[] = "Please enter email address";
			}
			elseif(!filter_var($request['email_address'],FILTER_VALIDATE_EMAIL)){
				$errors[] = "Email address invalid";
			}
			elseif($this->model_signup->check_email($request['email_address'])){
				$errors[] = "Email already exist";
			}
			if(empty($request['password'])){	
				$errors[] = "Please provide a password";
			}
			elseif(strlen($request['password']) < 6){
				$errors[] = "Your password must be at least 6 characters long";
			}
			if(empty($request['profile_name'])){	
				$errors[] = "Please enter company name";
			}
			
			return $errors;
		}
		#----------------------(/4)------------------------------------


		#------ this function id for email verification functionality (5)--------
		/*
		* Params : 
		* Return : 
		*/
		function email_verify()
		{
			$parameter  = $this->getrequest();
	   		$tokenid    = $parameter['tokenid'];
	   		$verification = $this->model_signup->emailverification($tokenid);
			$this->loadView('user/verify',$verification);
		}
		#----------------------(/5)----------------------------------		
	}
?>