<?php
//==================================================================================================
/* This class is for the purpose to show admin section*/
//==================================================================================================

	class Admin extends Application
	{
		function __construct()
		{
		    $this->startsession();
			
			
		}

		function checkSession()
		{
			if($this->getsession('admin_user_id')=="")
			{	
				$this->redirect('admin','index');
			}
		}

		function index()
		{
			if($this->getsession('admin_user_id')!="")
			{	
				$this->redirect('admin','home');
			}
			$this->loadView('admin/login');
		}


		#------ this is the action where we posting login form data ---------
		/*
		* Params : posted data from the login page
		* Return : if sucess redirected to home else redirected to login page with error msg
		*/ 
		function check_login()
		{ 
			$this->loadModel('model_admin_login');
			$response = $this->model_admin_login->check_login($_POST);
			
			if($response=='')
			{ 
				$userdetails = $this->model_admin_login->get_user_details($_POST['email_address']);

				$this->setsession("admin_user_id",$userdetails['admin_user_id']);
				$this->setsession("admin_email_address",$userdetails['admin_email_address']);
				$this->setsession("admin_name",$userdetails['admin_name']);
				$this->setsession("admin_profile_name",$userdetails['admin_profile_name']);
				$this->setsession("admin_profile_img",$userdetails['admin_profile_img']);
				
				$this->redirect('admin','home');
			}
			else
			{ 
				$data['errorMsg'] = $response;
		         	$this->loadView('admin/login',$data);
			}	
		}


		#------ this action is used to log out the user ---------
		/*
		* Params : nothing
		* Return : it redirects the user to login page with a sucessfully logout msg
		*/ 
		function logout()
		{
			$this->removesession();
			$this->redirect('admin','index');
		}



		function home()
		{
			$this->checkSession();
			$this->loadModel('model_admin_users');

			if(!empty($_POST['search']))
			{
				$email_address = $_POST['email_address'];
			}
			$search_arr = array('is_disabled'=>'1','email_address'=>$email_address,'user_type'=>1);
			
			$details = $this->model_admin_users->viewUserListing($search_arr);
			
			$data['paginator_arr'] = $details;

			$errMsg = "";
			$param = $this->getrequest();
			
			$errMsg = $param['msg'];
			$data['errMsg'] = $errMsg;
			$data['openPanel'] = "accounts";
			$this->loadView('admin/home', $data);
		}
		
		function disabledlist()
		{
			$this->checkSession();
			$this->loadModel('model_admin_users');
			
			if(!empty($_POST['search']))
			{
				$email_address = $_POST['email_address'];
			}
			$search_arr = array('is_disabled'=>'0','email_address'=>$email_address, 'user_type'=>1);
			$details = $this->model_admin_users->viewUserListing($search_arr);
			
			$data['paginator_arr'] = $details;

			$errMsg = "";
			$param = $this->getrequest();
			
			$errMsg = $param['msg'];
			$data['errMsg'] = $errMsg;
			$data['openPanel'] = "accounts";
			$this->loadView('admin/disabledbrand', $data);
		}
		
		
		function changeadminpassword()
		{
			$this->checkSession();
			$this->loadModel('model_admin_users');
			
			$admin_user_id = $this->getsession('admin_user_id');
			$errMsg = '';
			if(!empty($_POST['change_password']))
			{ 
				$isOldPwdExists = $this->model_admin_users->checkOldPassword($admin_user_id,$_POST);
				if($isOldPwdExists)
				{	
					$result = $this->model_admin_users->updatePassword($admin_user_id,$_POST);
					if($result)
					{
						$errMsg = "Your password has been changed successfully.";
					}
					else
					{
						$errMsg = "PHP Mysql Database error.";	
					}
				}
				else
				{	
					$errMsg = "Your old password doesn't exists. Please enter correct information.";		
				}	
			}
			$data['errMsg'] = $errMsg;
			$data['openPanel'] = "settings";
			$this->loadView('admin/changeadminpassword', $data);
		}
		
		function viewcreators()
		{
			$this->checkSession();
			$this->loadModel('model_admin_users');

			if(!empty($_POST['search']))
			{
				$email_address = $_POST['email_address'];
			}
			$search_arr = array('is_disabled'=>'1','email_address'=>$email_address,'user_type'=>2);
			
			$details = $this->model_admin_users->viewUserListing($search_arr);
			
			$data['paginator_arr'] = $details;

			$errMsg = "";
			$param = $this->getrequest();
			
			$errMsg = $param['msg'];
			$data['errMsg'] = $errMsg;
			$data['openPanel'] = "creators";
			$this->loadView('admin/creatorlisting', $data);
		}

		function disabledcreatorlist()
		{
			$this->checkSession();
			$this->loadModel('model_admin_users');

			if(!empty($_POST['search']))
			{
				$email_address = $_POST['email_address'];
			}
			$search_arr = array('is_disabled'=>'0','email_address'=>$email_address,'user_type'=>2);
			
			$details = $this->model_admin_users->viewUserListing($search_arr);
			
			$data['paginator_arr'] = $details;

			$errMsg = "";
			$param = $this->getrequest();
			
			$errMsg = $param['msg'];
			$data['errMsg'] = $errMsg;
			$data['openPanel'] = "creators";
			$this->loadView('admin/disabledcreatorlist', $data);
		}

		function flushmemcache()
		{
			$this->checkSession();
			$this->loadModel('model_admin_settings');

			$details = $this->model_admin_settings->flushCache();

			$data['openPanel'] = "settings";
			$this->loadView('admin/memcache', $data);
		}
		
    }