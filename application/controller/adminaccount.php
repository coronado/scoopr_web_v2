<?php
//======================================================
/* This class is for the purpose to create admin section*/
//======================================================

class Adminaccount extends Application 
{
	function __construct() 
	{
		$this->startsession();
		
		$this->loadModel('model_admin_account');
	}

	#------ this function is to check session weather its valid or not ---------
	/*
	* Params : null
	* Return : if session invalid it redirects to the admin login page
	*/ 
	function checkSession() 
	{
		if ($this->getsession('admin_user_id') == "") 
		{
			$this->redirect('admin', 'index');
		}
	}


	#------ this action is to create web app users ---------
	/*
	* Params : null
	* Return : error or success message
	*/ 
	function createuser() 
	{
		global $errors;
		$errors = array();
		$errMsg = '';
		if(!empty($_POST['save_profile']))
		{ 
			$errors = $this->validateUser($_POST);
			if(empty($errors))
			{ 
				$userid = $this->model_admin_account->create($_POST);
				
				#echo "$userid : ".$userid;die("here");
				if($_FILES["profile_img"]["name"]!='' )
				{
					$upload_dir_path=ROOT_PATH."images/profile_img/"; 
					if(!file_exists($upload_dir_path))
					{ 
						mkdir($upload_dir_path);
						chmod($upload_dir_path,0777);
					}
					$pngBase64 = base64_encode(file_get_contents($_FILES['profile_img']['tmp_name']));
					$ext = strtolower(end(explode('.',$_FILES["profile_img"]["name"])));
					$filename = sha1(substr($pngBase64, 0, 10).rand(11111, 99999)).'_'.$userid.'.'.$ext;
						
					if($_FILES['profile_img']['size'] > 0)
					{
						if($_FILES['profile_img']['error'] == 0)
						{ 
							if(!file_exists($upload_dir_path.$filename))
							{ 
								$success=move_uploaded_file($_FILES['profile_img']['tmp_name'],$upload_dir_path.$filename);
								$this->uploadImages($upload_dir_path,$filename);
								if($success)
								{
									$this->model_admin_account->update_admin_profile_pic($userid,$filename);
								}
							}
						}
					}
				}
				$this->setsession("added_user",$userid);
				
				$errMsg = "New account has been added successfully.";
				$this->redirect('admin','home/msg/'.$errMsg);
			}
			
				
		}
		$data['errMsg'] = $errMsg;
		$data['openPanel'] = "accounts";
		$this->loadView('admin/createusers',$data);
	}
		


	function edituser() 
	{
		global $errors;
		$errors = array();
		$this->checkSession();
		$param = $this->getrequest();
		$id= $param['id'];
		$page = $param['page'];
	
		if(!empty($_POST['save_profile']))
		{
			$updateInfo = $this->model_admin_account->updateUsers($id,$_POST);
			if($_FILES["profile_img"]["name"]!='')
			{
				$this->updateLogo($_FILES,$id);
			}

			$errMsg = "Account has been updated successfully.";
			//$this->redirect('admin','home/page/'.$page.'/msg/'.$errMsg);	
			$this->redirect('adminaccount','brandDetails/uid/'.$id);	
		}

		$detail = $this->model_admin_account->getUserData($id);
		$data['detail'] = $detail;

		$data['openPanel'] = "accounts";	
		$this->loadView('admin/editusers',$data);
	}


	function uploadImages($upload_dir_path,$filename)
	{	
		list($origWidth, $origHeight) = @getimagesize($upload_dir_path.$filename);
		$resultWidth = 90;
		$resultHeight = 90;

		if($origWidth>$resultWidth && $origHeight>$resultHeight)
		{					
			$thumb = new Imagick($upload_dir_path.$filename);
			$thumb->resizeImage($resultWidth,$resultHeight,Imagick::FILTER_LANCZOS,1);
			$thumb->writeImage($upload_dir_path.$filename);
			$thumb->destroy(); 
		}
		else 
		{
			$thumb = new Imagick($upload_dir_path.$filename);						$thumb->writeImage($upload_dir_path.$filename);
			$thumb->destroy(); 					  
		}
	}

	function updateLogo($files,$user_id)
	{
		$upload_dir_path=ROOT_PATH."images/profile_img/"; 
		if(!file_exists($upload_dir_path))
		{ 
			mkdir($upload_dir_path);
			chmod($upload_dir_path,0777);
		}
		$pngBase64 = base64_encode(file_get_contents($files['profile_img']['tmp_name']));
		$ext = strtolower(end(explode('.',$files["profile_img"]["name"])));
		$filename = sha1(substr($pngBase64, 0, 10).rand(11111, 99999)).'_'.$this->user_id.'.'.$ext;

		if($files['profile_img']['size'] > 0)
		{
			if($files['profile_img']['error'] == 0)
			{ 
				if(!file_exists($upload_dir_path.$filename))
				{ 
					$this->model_admin_account->updateProfileLogo($user_id,$filename);
					$success=move_uploaded_file($_FILES['profile_img']['tmp_name'],$upload_dir_path.$filename);
					$this->uploadImages($upload_dir_path,$filename);
				}
			}
		}
	}
		
		
	function validateUser($request)
	{ 
		$errors = '';
		# Email address invalid
		if(empty($request['email_address']))
		{	
			$errors[] = "Please enter email address";
		}
		elseif(!filter_var($request['email_address'],FILTER_VALIDATE_EMAIL))
		{
			$errors[] = "Email address invalid";
		}
		elseif($this->model_admin_account->checkEmailExistence($request['email_address']))
		{
			$errors[] = "Email address already exist. Please enter different email address to create a new account.";
		}
		
			
		return $errors;
	}


	function disableit()
	{ 
		$errMsg = '';
		$param = $this->getrequest('id');
		$user_id = $param['id'];
		$page  = $param['page'];

		$delete = $this->model_admin_account->disableUser($user_id);
		
		$this->redirect('admin', 'disabledlist');
	}

	function disablecreator()
	{
		$errMsg = '';
		$param = $this->getrequest('id');
		$creator_id = $param['id'];
		$page  = $param['page'];

		$delete = $this->model_admin_account->disableCreator($creator_id);
		
		$this->redirect('admin', 'disabledcreatorlist');
	}

	function enableit()
	{ 
		$errMsg = '';
		$param = $this->getrequest('id');
		$user_id = $param['id'];
		$page  = $param['page'];

		$delete = $this->model_admin_account->enableUser($user_id);
		
		$this->redirect('admin', 'home');
	}

	function deleteit()
	{ 
		$errMsg = '';
		$param = $this->getrequest('id');
		$user_id = $param['id'];
		$page  = $param['page'];

		$delete = $this->model_admin_account->deleteUser($user_id);

		$errMsg = "User has been deleted successfully.";
		$data['msg'] = $errMsg;	

		$this->redirect('admin', 'disabledlist',$data);
	}


	function brandDetails()
	{
		//$buyer_info = $this->model_profile->getBuyerInfo($uid);
		//$data['buyer_info'] = $buyer_info;
		$param = $this->getrequest('uid');
		$uid = $param['uid'];

		$detail = $this->model_admin_account->profileDetail($uid);
		$data['detail'] = $detail;
		$data['openPanel'] = "accounts";
		$this->loadView('admin/branddetail',$data);
	}	

}
 
