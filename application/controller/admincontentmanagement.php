<?php
//==================================================================================================
/* This class is for the purpose to show admin content management section*/
//==================================================================================================

	class Admincontentmanagement extends Application
	{
		function __construct()
		{
		   	$this->startsession();

			if($this->getsession('admin_user_id')=="")
			{	
				$this->redirect('admin','index');
			}

			$this->loadModel('model_admin_content_management');
			
			
		}


		function update()
		{
			$param =  $this->getrequest();
			$id    = $param['id'];
			$type  = $param['type'];

			$errMsg = "";
			if(isset($_POST['update_content']))
			{
				if($_POST['content']!='')
				{
					$updateIt = $this->model_admin_content_management->updateContent($id,$type,$_POST);
					if($updateIt) 
					{
						$errMsg = "Content has been updated successfully.";
					}
				}
				else
				{
					$errMsg = "Please enter content.";
				}
			}

			
			$content = $this->model_admin_content_management->getContent($id,$type);

			$data['errMsg'] = $errMsg;
			$data['content'] = $content;
			$data['type'] = $type;
			$data['openPanel'] = "contentmanagement";
			$this->loadView('admin/contentmanagement', $data);
		}

		
    }