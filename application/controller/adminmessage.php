<?php
//==================================================================================================
/* This class is for the purpose to handle brand messages*/
//==================================================================================================

	class Adminmessage extends Application
	{
		function __construct()
		{
		    $this->startsession();

		    if($this->getsession('admin_user_id')=="")
		    {	
			$this->redirect('admin','index');
		    }
		    $this->loadModel('model_admin_message');	
			
		}

		function addbrandmessage()
		{
			$parameter = $this->getrequest();
			$bid = $parameter['bid'];
			$data['bid'] = $bid;	
			
	
			$data['brands'] = $this->model_admin_message->getAllBrands();

			
			$errMsg = '';
			if(!empty($_POST['add_message']))
			{ 
				$result = $this->model_admin_message->addbrandmessage($_POST);
				if($result) {
					$errMsg = "New message has been added sucessfully.";
				} else {
					$errMsg = "Database error. Please try again letter.";
				}
			}
			

			$data['errMsg'] = $errMsg;
			$data['openPanel'] = "accounts";
			$this->loadView('admin/addbrandmessage', $data);
		}

		function editbrandmessage()
		{
			$parameter = $this->getrequest();
			$mid = $parameter['mid'];
			$mode = $parameter['mode'];	
			$data['mode'] = $mode;	
	
			$data['brands'] = $this->model_admin_message->getAllBrands();
			
			
			$errMsg = "";
			if(!empty($_POST['edit_message']))
			{
				$result = $this->model_admin_message->editbrandmessage($mid, $_POST);
				if($result) {
					$errMsg = "Message has been updated sucessfully.";		
				} else {
					$errMsg = "Database error. Please try again later.";		
				}
			}

			$data['details'] = $this->model_admin_message->getMessageDetails($mid);

			$data['errMsg'] = $errMsg;
			$data['openPanel'] = "accounts";
			$this->loadView('admin/editbrandmessage', $data);
		}

		function viewbrandmessage()
		{
			$details = $this->model_admin_message->viewbrandmessage();
			
			$data['paginator_arr'] = $details;

			$errMsg = "";
			$param = $this->getrequest();
			
			$errMsg = $param['msg'];
			$data['errMsg'] = $errMsg;
			$data['openPanel'] = "accounts";

			$this->loadView('admin/viewbrandmessage', $data);			
		}

		function brandmessagedetails()
		{
			$parameter = $this->getrequest();
			$mid = $parameter['mid'];
			$brandMsgDetails = $this->model_admin_message->brandmessagedetails($mid);
			
			$repliedMsgDetails = $this->model_admin_message->repliedmessagedetails($mid);

			$data['brandMsgDetails'] = $brandMsgDetails;
			$data['repliedMsgDetails'] = $repliedMsgDetails;
			$data['openPanel'] = "accounts";
			$this->loadView('admin/brandmessagedetails', $data);
		}

		function deletemessage()
		{
			$parameter = $this->getrequest();
			$mid = $parameter['mid'];

			$delMsg = $this->model_admin_message->deletemessage($mid);

			$errMsg = "Message has been deleted sucessfully.";
			$data['msg'] = $errMsg;	

			$this->redirect('admin','viewbrandmessage',$data);
		}
		
    }