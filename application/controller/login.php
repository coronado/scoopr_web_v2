<?php
//===================================================================================
/* This controller class is for the purpose to check the login functionality */
//===================================================================================

	class Login extends Application
	{
		function __construct()
		{
			//unset($_SESSION['user_id']);
			$this->startsession();
			$this->loadModel('model_login');
		}


		#------ by default action index used to show login page(1) ---------
		/*
		* Params : get message from logout in $params
		* Return : logout message only
		*/ 
		function index()
		{
			if(@$_SESSION['user_id']!='')
			{
				// $this->redirect('index','index');
			}
			$params  = $this->getrequest();
			$this->loadView('user/login',$params);
		}
		#----------------------(/1)--------------------------



		#------ this is the action where we posting login form data(2) ---------
		/*
		* Params : posted data from the login page
		* Return : if sucess redirected to home else redirected to login page with error msg
		*/ 
		function check_login()
		{ 
			$response = $this->model_login->check_login($_POST);

			if($response=='')
			{ 
			        $this->remember($_POST['remember_me'],$_POST['email_address'],$_POST['password'], $_POST['user_type']);
			
				$userdetails = $this->model_login->get_user_details($_POST['email_address']);
				
				if($userdetails['user_type']==1)
				{
					$this->setsession("user_id",$userdetails['user_id']);
					$this->setsession("email_address",$userdetails['email_address']);
					$this->setsession("name",$userdetails['name']);
					$this->setsession("profile_name",$userdetails['profile_name']);
					$this->setsession("profile_img",$userdetails['profile_img']);
					
					echo "<script>parent.jQuery.fancybox.close();window.parent.location.href = '/overview';</script>";
				}
				else
				{
					$this->setsession("admin_user_id",$userdetails['user_id']);
					$this->setsession("admin_email_address",$userdetails['email_address']);
					$this->setsession("admin_name",$userdetails['name']);
					$this->setsession("admin_profile_name",$userdetails['profile_name']);
					$this->setsession("admin_profile_img",$userdetails['profile_img']);
				
					echo "<script>parent.jQuery.fancybox.close();window.parent.location.href = '/admin/home';</script>";
				}
			}
			else
			{ 
				$data['errorMsg'] = $response;
		         	$this->loadView('user/login',$data);
			}	
		}
		#------------------------------(/2)------------------------------------


		#------ this action is used for remember me functionality to set the cookie(3) ---------
		/*
		* Params : email address, password , user type
		* Return : if checked it sets the cookie otherwise it destorys the cookie
		*/ 
		function remember($remember_me, $email_address, $password, $user_type)
		{
		   if($remember_me ==1)
		   {  
		      setcookie("remember_me",$remember_me,time()+60*60*24*30 , "/");
		      setcookie("email_address",$email_address,time()+60*60*24*30 , "/");
		      setcookie("password",$password,time()+60*60*24*30 , "/");		
		      setcookie("user_type",$user_type,time()+60*60*24*30 , "/");			
		   }
		   else
		   {
		      setcookie("remember_me","",time()-3600, "/");
		      setcookie("email_address","",time()-3600, "/");
		      setcookie("password","",time()-3600, "/");
		      setcookie("user_type","",time()-3600, "/");	
		   }
		}
		#----------------------(/3)---------------------------------------


		#------ this action is used to log out the user (4)---------
		/*
		* Params : nothing
		* Return : it redirects the user to login page with a sucessfully logout msg
		*/ 
		function logout()
		{
			$this->removesession();
			//$paraArr = array('errorMsg'=>"Successfully Logged Out");
		   	$this->redirect('index','index');
		}
		#----------------------(/4)-----------------------------------


		#------ this action is used to login with facebook (5) ---------
		/*
		* Params : 
		* Return : 
		*/ 
		function facebook_login()
		{       
			$this->loadFbLib('sdk/','facebook');
			$config = array();
			$config['appId'] = '1391246324438148';
			$config['secret'] = '5cbcc2eea84ea671e4cf113ce58cbdf6';
			$site_url = "http://scoopr.a1.com/login";

			$facebook = new Facebook($config);
			$user = $facebook->getUser();

			//$user_profile = array();
			if ($user) { // check if current user is authenticated
    			try {
        		// Proceed knowing you have a logged in user who's authenticated.
        		$user_profile = $facebook->api('/me');  //get current user's profile information using open graph

            		    }
         			catch(Exception $e){}
			    }

			$response = $this->model_login->check_fb_login($user_profile);
			$userdetails = $this->model_login->get_user_details($user_profile['email']);

			$this->setsession("user_id",$userdetails['user_id']);
			$this->setsession("email_address",$userdetails['email_address']);
			$this->setsession("name",$userdetails['name']);
			$this->setsession("profile_img",$userdetails['profile_img']);

			if($response==1) {
			echo "<script>parent.jQuery.fancybox.close();window.parent.location.href = '/overview';</script>";
			} else {
			echo "<script>parent.jQuery.fancybox.close();window.parent.location.href = '/signup/fbthanks';</script>";			
			}	
			/*if($user){
				// Get logout URL
				$logoutUrl = $facebook->getLogoutUrl();
			}else{
				// Get login URL
				$loginUrl = $facebook->getLoginUrl(array(
					'scope'		=> 'read_stream, publish_stream, user_birthday, user_location, user_work_history, user_hometown, user_photos',
					'redirect_uri'	=> $site_url,
					));
			}

			print_r($logoutUrl);
			echo "<br><b>login url:</b>";
			print_r($loginUrl); die;*/
		}
		#----------------------(/5)---------------------------------------

	}
?>
