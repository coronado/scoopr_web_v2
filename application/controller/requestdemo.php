<?php

//===================================================================================
/* This controller class is for the purpose to check the send request demo functionality */
//===================================================================================
#ini_set('display_errors',1);

	class Requestdemo extends Application
	{
		function __construct()
		{
		        $this->startsession();
			$this->loadModel('model_request_demo');
		}
		
		function index()
		{
			$errFlag =0;
			$errMsg = "";
			if(!empty($_POST['submit']))
			{	
				$isEmailExists = $this->model_request_demo->checkEmail($_POST);
				if($isEmailExists)
				{	
					$errMsg =  "Email address already exists.";
				}
				else
				{	
					$this->model_request_demo->requestdemo($_POST);
					$errMsg =  "Your request has been sent.";
					$errFlag = 1;
				}
			}
			
			$data['errMsg'] = $errMsg; 
			
			//echo "<script>parent.jQuery.fancybox.close();window.parent.location.href = '/index/index/show/requestademo';</script>";
			if($errFlag==1) {
				$this->redirect('requestdemo', 'success');
			} else {
			 $this->loadView('request-demo/index',$data);
			}
		}

		function success()
		{
			$this->loadView('request-demo/success',$data);
		}

	}
?>
