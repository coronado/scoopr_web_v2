<?php
//==================================================================================================
/* This class is for the purpose to show overview module in the buyer dashboard section */
//==================================================================================================

	class Overview extends Application
	{
		function __construct()
		{
			$this->startsession();
			if($this->getsession('user_id')=="")
	   		{	
	      			$this->redirect('index','index');
	   		}

			$this->user_id = $this->getsession('user_id');
			$this->loadModel('model_overview');
		}

		#------ default action of overview module- showing graph and no of assignments (1)---------
		/*
		* Params : 
		* Return : graphical data
		*/ 
		function index()
		{
			$data['pending_assignments'] = $this->model_overview->countPendingAssignments($this->user_id);
			$data['total_assignments']   = $this->model_overview->countTotalAssignments($this->user_id);
			$data['licensed_content']    = $this->model_overview->countLicensedContent($this->user_id);
			
			$result = $this->model_overview->getGraphData($this->user_id);
			//$result = $this->model_overview->getGraphData('77');
			if(count($result)>0)
			{
				for($i=0; $i<count($result); $i++)
				{
					//$views .= $result[$i]['view'].',';
					//$submissions .= $result[$i]['submission'].',';
					//$date .= "'".date('M-d',strtotime($result[$i]['date']))."',";
					list($year,$month,$day) = explode('-',$result[$i]['date']);
					//echo $year; die;
					$views .= "[Date.UTC(".$year.",".($month-1).",".$day."), ".$result[$i]['view']."],";
					$submissions .= "[Date.UTC(".$year.",".($month-1).",".$day."), ".$result[$i]['submission']."],";
					//[Date.UTC(2010, 0, 1), 29.9],
				}
			}
			//echo $views; die;
			$data['num_rows'] = count($result);
			$data['views']	= rtrim($views,',');
			$data['submissions'] = rtrim($submissions,',');
			//$data['date'] = rtrim($date,',');

			$this->loadView('overview/index',$data);
		}
		#------------------------------------(/1)----------------------------------

		#------ payments page of overview module- showing list of paid contents (2)---------
		/*
		* Params : 
		* Return : array of paid contents
		*/ 
		function payments()
		{
			$params = $this->getrequest();
			$page_no    = $params['page_no'];

			/* -------------- SETTING PAGE NO ------------ */		
			$current_page_no = 1;
			if($params['page_no']!='') {
				$current_page_no = $params['page_no'];
			}
			$data['current_page_no'] = $current_page_no;

			$listings = $this->model_overview->getPaidContent($this->user_id,$page_no);
			$data['listings'] = $listings;

			$this->loadView('overview/payments',$data);
		}
		#------------------------------------(/2)----------------------------------


		#------ this action is to show creator details (3)---------
		/*
		* Params : creator id
		* Return : creator details
		*/ 
		function creatorDetails()
		{
			$params = $this->getrequest();
			$creator_id = $params['creator_id'];
			$details = $this->model_overview->getCreatorDetails($creator_id);
			$data['details'] = $details;
			
			$this->loadView('overview/creatordetails',$data);
		}
		#------------------------------(/3)------------------------------


		#------ licensed media module - showing list of paid contents (4)---------
		/*
		* Params : 
		* Return : array of paid contents
		*/ 
		function licensedmedia()
		{
			$params = $this->getrequest();
			$page_no    = $params['page_no'];

			/* -------------- SETTING PAGE NO ------------ */		
			$current_page_no = 1;
			if($params['page_no']!='') {
				$current_page_no = $params['page_no'];
			}
			$data['current_page_no'] = $current_page_no;
		
			$buyer_info = $this->model_overview->getBuyerInfo($this->user_id);
			$data['buyer_info'] = $buyer_info;
			
			$listings = $this->model_overview->getLicensedMediaContent($this->user_id,$page_no);
			$data['listings'] = $listings;

			$this->loadView('overview/licensedmedia',$data);
		}
		#------------------------------(/4)------------------------------	
		
	}
?>
