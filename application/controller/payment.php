<?php
//==================================================================================================
/* This controller is for the purpose to save paypal transaction details and show error if failure*/
//==================================================================================================
	class payment extends Application
	{
		function __construct()
		{
			$this->startsession();
			if($this->getsession('user_id')=="")
			{	
				$this->setsession("invalid","yes");
				$this->redirect('index','index');
			}
			$this->user_id = $this->getsession('user_id');
			$this->loadModel('model_payment');
		}

		#------ If payment success payment gateway redirect to this action (1)---------
		/*
		* Params : posted values from payment gateway
		* Return : payment details
		* Desc : This action will show payment details and send an email to buyer and admin
		*/ 
		function success()
		{
			if(!empty($_POST)) 
			{ 
				$result = $this->model_payment->savePaymentDetails($_REQUEST, $this->getsession('content_id'));

				$saveNotification  = $this->model_payment->saveNotification($this->getsession('content_id'));

				$userid = $this->getsession('user_id');
				/* ------------------ push notifaction ---------------------------*/
				$this->include_file('push_notification.php','application/lib');
				$PushNotification =  new PushNotification();
				$msg = "Your image has been selected";
				
				//$device_token = array('3af252ac2dc60c3d2f5c5fdbaba1fae78db855395d99d402415a9cd2ee9a2020');
				$device_token = $this->model_payment->getUserDeviceToken($this->getsession('content_id'));
				$_tokenArr=$device_token;
				// For Iphone
				$PushNotification->send_notification($msg,$_tokenArr,1);
			}
			$buyer_info = $this->model_payment->getBuyerInfo($this->user_id);
			$data['buyer_info'] = $buyer_info;
			//echo "<pre>"; print_r($buyer_info);
			$content_info = $this->model_payment->getContentInfo($this->getsession('content_id'));
			//echo "<pre>"; print_r($content_info); die;
			$data['content_info'] = $content_info;
			

			$mail_to_buyer = $this->model_payment->mailToBuyer();
			$mail_to_creator = $this->model_payment->mailToCreator($this->getsession('content_id'));
			$mail_to_admin = $this->model_payment->mailToAdmin($content_info,$buyer_info);

			$this->loadView('payment/success',$data);
		}
		#------------------------------------(/1)----------------------------------


		#------ If payment fails payment gateway redirect to this action (2)---------
		/*
		* Params : session user id
		* Return : shows error message
		*/ 
		function fail()
		{
			$buyer_info = $this->model_payment->getBuyerInfo($this->user_id);
			$data['buyer_info'] = $buyer_info;
			$this->loadView('payment/fail',$data);
		}
		#------------------------------------(/2)----------------------------------

		
		#------ If buyer chhose to payment using rewards only , this action called (3)---------
		/*
		* Params : image submission id
		* Return : content info, reward info
		* Desc : shows content info, reward info and send email to buyer, admin and creator
		*/ 
		function rewards()
		{
			$result = $this->model_payment->saveRewardStatus($this->getsession('content_id'));


			/* ------------------ saving notifaction ---------------------------*/
			$saveNotification  = $this->model_payment->saveNotification($this->getsession('content_id'));
			$userid = $this->getsession('user_id');

			/* ------------------ push notifaction ---------------------------*/
			$this->include_file('push_notification.php','application/lib');
			$PushNotification =  new PushNotification();
			$msg = "Your image has been selected";
			
			$device_token = $this->model_payment->getUserDeviceToken($this->getsession('content_id'));
			$_tokenArr=$device_token;
			$PushNotification->send_notification($msg,$_tokenArr,1);
			

			/* ------------------ buyer & content info -------------------*/
			$buyer_info = $this->model_payment->getBuyerInfo($this->user_id);
			$data['buyer_info'] = $buyer_info;
			
			$content_info = $this->model_payment->getContentInfo($this->getsession('content_id'));
			$data['content_info'] = $content_info;

			$mail_to_buyer = $this->model_payment->mailToBuyer();
			$mail_to_creator = $this->model_payment->mailToCreator($this->getsession('content_id'));
			$mail_to_admin = $this->model_payment->mailToAdmin($content_info,$buyer_info);

			$this->loadView('payment/rewards',$data);
		}
		#------------------------------------(/3)----------------------------------
		
	}
?>
