<?php if (!defined('BASE_PATH')) exit('Not allowed.');
	
//===================================================================================
/* This class is for basic functions to load controller, model , view etc */
/* These are the basic function which we are using in alomst every pages of the website*/
//===================================================================================

	class Application
	{
		var $uri;
		var $model;
		
		function __construct($uri)
		{
			$this->uri = $uri;
			
		}

		#------ this is the function to load controller (1)---------
		/*
		* Params : class name
		* Return : if sucess redirected to controller else error page
		*/
		function loadController($class)
		{
			
			$this->setsession('controllerName',$this->uri['controller']);
			$this->setsession('actionName',$this->uri['method']);
			
			$file = "application/controller/".$this->uri['controller'].".php";
			
			if(!file_exists($file)) 
			{
				$file ="application/controller/error.php";$class="error";
			}
			
			require_once($file);
         
			$class = str_replace("-","_",$class);
			$controller = new $class(); 

	     		if($this->uri['method'] == "")
         		{
           			$controller->index($this->uri['controller']);	
			}         
			else if(method_exists($controller, $this->uri['method']))
			{			 	
			 	$controller->{$this->uri['method']}($this->uri['var']);
			} 
			else 
			{
			  	$this->loadView('error/index');
			}
			
		}
		#---------------------------(/1)-----------------------------------


		#------ this is the function to load templates (2) ---------
		/*
		* Params : template name and paramater array
		* Return : if sucess load view of the page
		*/
		function loadView($view,$vars="")
		{
			if(is_array($vars) && count($vars) > 0)
				extract($vars, EXTR_PREFIX_SAME, "wddx");
			require_once('view/'.$view.'.php');
		}
		#-------------------------(/2)---------------------------------

		
		#------ this is the function to model classes (3)---------
		/*
		* Params : model name
		* Return : if sucess load model class
		*/
		function loadModel($model)
		{
			require_once('model/'.$model.'.php');
			$this->$model = new $model;
		}
		#-------------------------(/3)---------------------------------


		#------ this is the function to load facebook library (4) ---------
		/*
		* Params : directory name, library name
		* Return : include the library file
		*/
		function loadFbLib($directory, $libName)
		{
			require_once('lib/'.$directory.$libName.'.php');
		}
		#-------------------------(/4)---------------------------------


		#------ this is the function to load config file (5) ---------
		/*
		* Params : config file name
		* Return : it connects to the datavase
		*/
		function loadConfig($config)
		{
			require_once('config/'.$config.'.php');
			 // create the $db object 
         		$this->db = Database::obtain(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
         		// connect to the server 
         		$this->db->connect(); 
		}
		#-------------------------(/5)---------------------------------

		

		#------ this is the function to redirect the page (6)---------
		/*
		* Params : controller name, view and its parameters
		* Return : redirects to the given controlled
		*/
		function redirect($controller, $view, $paraArr)
		{
		   	if(!empty($paraArr)) 
		   	{
		      		foreach ($paraArr as $key => $value) 
				{
                   			$paraString.= $key."/".$value."/";
                		}
            
         		}       
		   	$path = PATH.$controller."/".$view."/".$paraString;	   
		   	return header("location:$path");
		}
		#-------------------------(/6)---------------------------------

		
		#------ to get parameters values from the url (7) ---------
		/*
		* Params : null
		* Return : returns the values in array format
		*/
		public static function getrequest()
		{
			$request_url = $_SERVER['REQUEST_URI'];		  
		   	$url         =  str_replace(LOCAL_PATH,"",$request_url);		   
		   	$parameters   = explode("/",$url);
		   
		   	if($_SERVER['HTTP_HOST']!='localhost')
		   	{
		   	 	$total = count($parameters)-1;
			 	unset($parameters[0]);		   
			 	unset($parameters[1]);
			 	unset($parameters[2]);	
		   	}	   
		   	$slipt = array_chunk($parameters, 2);
	       		$response = array();
	       		foreach($slipt as $val)
	       		{
	          		if( isset($val[1]) )
	            		$response[$val[0]] = $val[1];
	       		}         
          		return $response;
		}
		#-------------------------(/7)---------------------------------

		
		#------ function to start the session (8)---------
		/*
		* Params : null
		* Return : its just starts the session
		*/
		function startsession()
		{
		   	return session_start();
		}
		#-------------------------(/8)---------------------------------

			
		#------ function to start the session (9)---------
		/*
		* Params : session_name
		* Return : set session value
		*/
		function setsession($name, $value)
		{
		   	$_SESSION[$name] = $value;
			
		}
		#-------------------------(/9)---------------------------------

		
		#------ function to start the session (10) ---------
		/*
		* Params : session_name
		* Return : session_value
		*/
		function getsession($name)
		{ 
		  	 return $_SESSION[$name];
		}
		#-------------------------(/10)---------------------------------

		
		#------ function to start the session (11) ---------
		/*
		* Params : session_name
		* Return : 1 for true and 0 for false
		*/
		function checksession($name)
		{
		   	if($_SESSION[$name] == "")
		   	{
		     		return 0;
		   	}
		   	else
		   	{
		     		return 1;
		   	}
		}
		#-------------------------(/11)------------------------------


		#------ function to remove the session (12)---------
		/*
		* Params : null
		* Return : destroy all the sessions which exists in website
		*/
		function removesession()
		{
		   	return session_destroy();
		}
		#-------------------------(/12)---------------------------------

		
		#------ function to include files (13)---------
		/*
		* Params : file name, its path
		* Return : include the given file name
		*/
		function include_file($file,$path)
		{
			$new_path = ROOT_PATH.$path."/".$file;				
			if(file_exists($new_path))
			{
			   require_once($new_path);
			}
			else
			{ 
			   require_once('view/error/index.php');
			}
		}
		#-------------------------(/13)---------------------------------
		

		#------ function to trace ip address (14)---------
		/*
		* Params : net geo url
		* Return : return the html
		*/
     		function traceip($NetGeoURL)
     		{
        
         		if($NetGeoFP = fopen($NetGeoURL,r))
         		{ 
         	 		ob_start();
	         		fpassthru($NetGeoFP);
	         		$NetGeoHTML = ob_get_contents();
	         		ob_end_clean();
	         		fclose($NetGeoFP);
         		}
         		return $NetGeoHTML; 
     		}
		#-------------------------(/14)---------------------------------
     
     
     		#------ function to convert array to json format (15)---------
		/*
		* Params : array value
		* Return : json value
		*/
		function array2json($arr) 
		{ 
    			if(function_exists('json_encode')) return json_encode($arr); //Lastest versions of PHP already has this functionality.
    			$parts = array(); 
    			$is_list = false; 

    			//Find out if the given array is a numerical array 
   			 $keys = array_keys($arr); 
    			 $max_length = count($arr)-1; 
    			if(($keys[0] == 0) and ($keys[$max_length] == $max_length)) 
			{//See if the first key is 0 and last key is length - 1
        			$is_list = true; 
        			for($i=0; $i<count($keys); $i++) 
				{ //See if each key correspondes to its position
            				if($i != $keys[$i]) 
					{ //A key fails at position check. 
                				$is_list = false; //It is an associative array. 
                				break; 
            				} 
        			} 
    			} 

    			foreach($arr as $key=>$value) 
			{ 
        			if(is_array($value)) 
				{ //Custom handling for arrays 
            				if($is_list) $parts[] = array2json($value); /* :RECURSION: */ 
            				else $parts[] = '"' . $key . '":' . array2json($value); /* :RECURSION: */
        			} 
				else 
				{ 
            				$str = ''; 
            				if(!$is_list) $str = '"' . $key . '":'; 

            				//Custom handling for multiple data types 
            				if(is_numeric($value)) $str .= $value; //Numbers 
            				elseif($value === false) $str .= 'false'; //The booleans 
            				elseif($value === true) $str .= 'true'; 
            				else $str .= '"' . addslashes($value) . '"'; //All other things 
            				// :TODO: Is there any more datatype we should be in the lookout for? (Object?)

            				$parts[] = $str; 
        			} 
    			} 
    			$json = implode(',',$parts); 
     
    			if($is_list) return '[' . $json . ']';//Return numerical JSON 
    			return '{' . $json . '}';//Return associative JSON 
    		}
		#-------------------------(/15)---------------------------------
    
    
    		#------ destroy the session variables based on the array passed to it (16)---------
		/*
		* Params : array value
		* Return : unset the session variables
		*/
		function unsetSession($unset_array)
    		{ //echo "<pre>"; print_r($unset_array); die;
        		if(is_array($unset_array))
       			{
		        	foreach ($unset_array as $session_variable)
          			{
             				unset($_SESSION[$session_variable]);
          			}
       			}
       			else
       			{
          			unset($_SESSION[$unset_array]); 
       			}   
    		}
		#-------------------------(/16)---------------------------------
    
    
	
		#------ function to escape string (17) ---------
		/*
		* Params : string variables
		* Return : returns the string by escaping characters
		*/
   		function escapeString($string)
   		{
      			$resultArr =  array();
      			if(is_array($string)) 
      			{
         			foreach($string as $key=>$str)
         			{
           				$resultArr[$key] = mysql_escape_string($str);
         			}
         		return $resultArr;
      			}
      			else
      			{
         			return mysql_escape_string($string);
      			}
   		}
		#-------------------------(/17)---------------------------------

}
?>
